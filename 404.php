<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

/*
$ch = curl_init();
$url = "http://assos.adv-online.ru/robots.txt";
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_HEADER, 1);
$result = curl_exec($ch);  
curl_close($ch);
echo $result;
*/


$APPLICATION->SetTitle("страница не найдена :("); ?>

<div class="no_page">Страница которую вы ищете, не найдена или не существует</div>

<div class="error_box">
	<div class="error">404</div>
	
	<div class="give_try">попробуйте начать <br>с <a href="/">главной</a>, или...</div>
				
	<div class="slider_error ">
		<div class="slide">
			<div class="thumb">
				<a href="/">
					<img src="/local/templates/main/tpl/images/static/img_error1.jpg" alt="">
				</a>
			</div>

			<div class="name">
				<a href="/">подобрать защиту <br>и экипировку</a>
			</div>
		</div>

		<div class="slide">
			<div class="thumb">
				<a href="/">
					<img src="/local/templates/main/tpl/images/static/img_error2.jpg" alt="">
				</a>
			</div>

			<div class="name">
				<a href="/">выбрать подходящий велосипед</a>
			</div>
		</div>

		<div class="slide">
			<div class="thumb">
				<a href="/">
					<img src="/local/templates/main/tpl/images/static/img_error3.jpg" alt="">
				</a>
			</div>

			<div class="name">
				<a href="/">обратиться <br>в сервис-центр</a>
			</div>
		</div>

		<div class="slide">
			<div class="thumb">
				<a href="/">
					<img src="/local/templates/main/tpl/images/static/img_error3.jpg" alt="">
				</a>
			</div>

			<div class="name">
				<a href="/">обратиться в сервис-центр</a>
			</div>
		</div>
	</div>

	<div class="info">* все мемы взяты в интернете. мы не претендуем на авторство и не затираем копирайты</div>
</div>


<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>