<? set_time_limit(0); 
header('Content-Type: text/html; charset=utf-8');
$deleteFiles = true;

#define("LANG", "ru"); 
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// Формируем кэш имен файлов на основе таблицы b_file.
$arFilesCache = array();
$result = $DB->Query('SELECT FILE_NAME, SUBDIR FROM b_file WHERE MODULE_ID = "iblock"');
while ($row = $result->Fetch()) {
    $arFilesCache[ $row['FILE_NAME'] ] = $row['SUBDIR'];
}

$rootDirPath = $_SERVER['DOCUMENT_ROOT'] . "/upload/iblock";
$hRootDir = opendir($rootDirPath);
$count = 0;
$allFileSize = 0;
while (false !== ($subDirName = readdir($hRootDir))) {
    if ($subDirName == '.' || $subDirName == '..') 
        continue;
    $filesCount = 0;
    $subDirPath = "$rootDirPath/$subDirName";
    $hSubDir = opendir($subDirPath);    
    while (false !== ($fileName = readdir($hSubDir))) {
        if ($fileName == '.' || $fileName == '..') 
            continue;
        if (array_key_exists($fileName, $arFilesCache)) {
            $filesCount++;
            continue;
        }
        $fullPath = "$subDirPath/$fileName";
        if ($deleteFiles) {
                $fSize = filesize($fullPath);
                $allFileSize += $fSize;
            if (unlink($fullPath)) {
                echo "Removed: " . $fullPath . ", size: " . $fSize . PHP_EOL;
            }
        }
        else {
            $filesCount++;
            echo $fullPath . PHP_EOL;
        }
    }
    closedir($hSubDir);
    if ($deleteFiles && !$filesCount) {
        rmdir($subDirPath);
    }
}
closedir($hRootDir);

echo "deleted $allFileSize";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");