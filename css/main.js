$(window).on('load', function () {

    setTimeout(function () {
        $('.preloader').addClass('is-loaded');

    }, 500);
	
	$(".item__images").bxSlider({
        auto: false,
        mode: 'vertical',
        minSlides: 4,
        maxSlides: 5,
		pager: false,
		responsive: false,
		infiniteLoop: false,
        //ticker: true,
        //speed: 6000
        //autoControls: true,
        //controls: false,
        slideWidth: 110,
		controls: true,
		adaptiveHeight: false,
		nextText: 'Next',
		
    });
	
	 $('.item__images_div a').click(function(){

		$('.item__images_div a').removeClass('is-active');
		$(this).addClass('is-active');
        var slide = $(this).data('count');
		var carousel = $('.item__slider');
		carousel.trigger('to.owl.carousel', slide);
        return false;
    });
	
	if( $(".color_slider li").length > 5 ) {
	
		$(".color_slider").bxSlider({
			auto: false,
			mode: 'horizontal',
			minSlides: 1,
			maxSlides: 5,
			pager: false,
			infiniteLoop: false,
			hideControlOnEnd: true,
			hideControlOnStart: true,
			moveSlides : 1,
			slideWidth: 110,
			controls: true,

		});
	}
	
	
});


$(function() {
    var load_more = false;

    $(window).scroll(function() {
        if($("#next_page").length && $("#next_page").val() && !load_more ) {
            var url = $("#next_page").val();
			var offset_button = $("#next_page").offset();
            if($(this).scrollTop() >= offset_button.top - $(window).height()) {
                load_more = true;
				$('#page_preloader').show();
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {IS_AJAX: 'Y'},
                    success: function(data) {
                        $("#next_page").after(data);
                        $("#next_page").remove();
						$('#page_preloader').remove();
                        load_more = false;
                    }
                });
            }
        }
    });
});

$(document).ready(function () {
    //я тут функции немного по галимому назвал, надеюсь разберешься плиз,я больше так не буду
    //перед началом игры "пойми меня" дам подсказку: каждый иф можно разбить на отдельные функции
    //main functional

    cityScroll();
    citySelect();
    search();
    menuDD();
    menuClose();
    // Move to /local/templates/main/components/bitrix/iblock.element.add.form/add_review/script.js
    //addComment();
    headerLogin();
    showPass();
    catalogScroll();

    //mobile
    mobileMenu();
    mobileCatalog();
    mobileCatalogDD();
    headerDD();
    nestedDD();

    //personal kabina
    personalTabs();
    removeOrder();

    //help funcs
    headerRespond();
    widthChecks();
    styles();

    //sliders
    sliders();
    owlDots();

    //elems
    ask();
    // Move to User profile, Register and Make order
    // regTimer();
    counter();
    basket();
    select();
    priceSlider();
    card();
    item();
    filter();
    orderNewValidate();
    orderOldValidate();
    copyToBuffer();

    //validation
    validate();
    masks();

    //popups
    popupCard();
    popupToggle();
    itemSizes();
    itemSizesTable();
    dropZone();


    if ($('#contacts__map').length > 0) {
        mapStart();
    }
	
	
	
	
});
function catalogScroll() {
    if ($(window).width() > 1280) {

        $('.header__menu-dd').mCustomScrollbar({});
    }
}

function copyToBuffer() {

    if ($('.item__share-links').length > 0) {

        $('.item__share-links').on('click',

            '.item__copy',

            function () {
                $('.item__share-url').focus();
                $('.item__share-url').select();
                document.execCommand('copy');
                return false;
            });
    }
}

function dropZone() {

    if ($('#dZUpload').length > 0) {

        $('#dZUpload').dropzone({
            paramName: "form_file",
            url: "/local/tools/ajax/file_upload.php",

            addRemoveLinks: true, //add remove link

            dictRemoveFile: 'Удалить файл', //remove link text

            dictCancelUpload: 'Отменить загрузку',

            init: function () {
                this.on("addedfile", function () {

                    if (this.files[1] != null) {

                        this.removeFile(this.files[0]);
                    }
                });
            },
            success: function (file, result) {
                result = $.parseJSON(result);

                if (result instanceof Object) {
                    if (result['STATUS'] == 'ERROR') {
                        return;
                    }

                    var form = $(this.element).parents('form'),
                        resume = form.find('#resume');

                    if (resume.length == 0) {
                        resume = $('<input>').attr({
                            id: 'resume',
                            name: 'resume',
                            type: 'hidden'
                        });

                        resume.appendTo(form);
                    }

                    resume.val(result['FILE_ID']);
                }
            }
        });
    }
}

function itemSizesTable() {

    if ($('.item__sizes-table').length > 0) {
        var block = $('.item__sizes-table');

        $('.item__sizes-table').on('click',

            '.item__sizes-table-closer',

            function () {

                $(this).parents('.item__sizes-table').removeClass('is-visible');

                return false;
            });


        $(document).on('click', function (e) {

            if (!block.is(e.target)
                && block.has(e.target).length === 0) {

                block.removeClass('is-visible');
            }
        });
    }
}

function itemSizes() {

    if ($('.item__about-sizes').length > 0) {
        var block = $('.item__get-sizes');

        $('.item__about-sizes').on('click',

            '.item__get-sizes-closer',

            function () {

                $(this).parents('.item__get-sizes').removeClass('is-visible');

                return false;
            });

        $(document).on('click', function (e) {

            if (!block.is(e.target)
                && block.has(e.target).length === 0) {

                block.removeClass('is-visible');
            }
        });
    }
}

function popupToggle() {

    if ($('.popup').length > 0) {
        var block = $('.popup__inner');

        //close
        $('.popup').on('click',

            '.popup__closer',

            function () {
                $(this).parents('.popup').removeClass('is-visible');
                BX.closeWait();

                return false;
            });

        $(document).on('mouseup', function (e) {

            if (!block.is(e.target)
                && block.has(e.target).length === 0) {

                block.parent('.popup').removeClass('is-visible');

            }
        });
    }

    $('body').on('click',

        '[data-modal]',

        function () {
            var target = $(this).attr('data-modal');
            $(target).addClass('is-visible');

            var vacancyId = $(this).data('id');

            if (Number(vacancyId) > 0) {
                var vacancyField = $(target).find('#vacancy');

                if (vacancyField.length == 0) {
                    vacancyField = $('<input>').attr({
                        'name': 'vacancy',
                        'type': 'hidden',
                        'id': 'vacancy'
                    }).appendTo($(target).find('form'));
                }

                vacancyField.val(vacancyId);
            }

            return false;
        });
}

function popupCard() {

    if ($('.popup#card').length > 0) {

        //card colors
        $('.popup__card-switchs').on('click',

            '.popup__card-switcher',

            function () {
                $(this).siblings('.popup__card-switcher').children('.popup__card-switcher-value').prop('checked', false)
                $(this).siblings('.popup__card-switcher').removeClass('is-active');
                $(this).addClass('is-active');
                $(this).children('.popup__card-switcher-value').prop('checked', true);

                return false;
            });

        //card sizes
        $('.popup__card-sizes, .item__get-sizes').on('click',

            '.popup__card-size-link',

            function () {
                var value = $(this).text();
                var result = $('.item__about-sizes-input');

                $(this).parents('.popup__card-size').siblings('.popup__card-size').find('.popup__card-size-value').prop('checked', false);
                $(this).parents('.popup__card-size').siblings('.popup__card-size').removeClass('is-active');
                $(this).parents('.popup__card-size').addClass('is-active');
                $(this).children('.popup__card-size-value').prop('checked', true);

                result.val(value);
                $('.item__get-sizes').removeClass('is-visible');
                return false
            });
    }
}

function showPass() {

    if ($('.showpw').length > 0) {

        $('body').on('click',

            '.showpw',

            function () {

                if ($(this).siblings('input').attr('type') === 'password') {

                    $(this).siblings('input').attr('type', 'text');

                } else {

                    $(this).siblings('input').attr('type', 'password');
                }


                return false;
            });
    }
}

function headerLogin() {

    if ($('.header__auth-login').length > 0) {
        var block = $('.header__auth-login');

        $('.header__top').on('click',

            '.header__auth-sign',

            function () {
                $(this).toggleClass('is-active');
                $('.header__auth-login').toggleClass('is-active');

                return false;
            });

        $(document).on('click', function (e) {

            if (!block.is(e.target)
                && block.has(e.target).length === 0) {

                block.removeClass('is-active');
                $('.header__auth-sign').removeClass('is-active');
            }
        });
    }
}

function item() {
    // ну если очень надо будет то можно на отдельные функции разбить каждый if
    //item compare

    if ($('.item__about-compare').length > 0) {

        $('.item').on('click',

            '.item__about-compare',

            function () {
                $(this).toggleClass('is-active');

                return false;
            });
    }
    //item about

    if ($('.item__description .item__about-link').length > 0) {

        $('.item__description').on('click',

            '.item__about-link',

            function () {
                $(this).siblings('.item__description-body').toggleClass('is-full');

                return false;
            });

    }
    //item table

    if ($('.item__table').length > 0) {

        $('.item__table').on('click',

            'tfoot a',

            function () {
                $(this).parents('table').children('tbody').toggleClass('is-full');


                return false;
            });
    }


    if ($('.item__share-copy').length > 0) {

        var block = $('.item__share-copy');

        $('.item__share-links').on('click',

            '.item__share-link--share',

            function () {
                $('.item__share-copy').toggleClass('is-visible');

                return false;
            });

        $(document).on('mouseup', function (e) {

            if (!block.is(e.target)
                && block.has(e.target).length === 0) {

                block.removeClass('is-visible');
            }
        });
    }

	/*
    if ($('.item__about-images').length > 0) {

        $('.item__about-images').on('click',

            '.item__about-image',

            function () {
                $(this).siblings('.item__about-image').children('input').prop('checked', false)
                $(this).siblings('.item__about-image').removeClass('is-active');

                $(this).addClass('is-active');
                $(this).children('input').prop('checked', true)
                return false;
            });
    }
	*/

    if ($('.item__current-city').length > 0) {

        $('.item__delivery-header-text').on('click',

            '.item__current-city',

            function () {
                $('.header__city').removeClass('selected');
                $('.header__city').addClass('onload active');

            });
    }
}

function masks() {
    // if ( $('.mask-card').length > 0 ) {

    // 	$('.mask-card').mask('9999 - 9999 - 9999 - 9999')
    // }

    if ($('.mask-phone').length > 0) {

        $('.mask-phone').mask('+7 (999) - 999 - 99 - 99')
    }

    if ($('.mask-bday').length > 0) {

        $('.mask-bday').mask('99.99.9999')
    }
}

function orderNewAddress() {

    if ($('.order').length > 0) {

        $('.order').validate().destroy();

        $('.order').validate({
            rules: {

                surname: {
                    required: true
                },

                firstname: {
                    required: true
                },

                email: {
                    required: true
                },

                phone: {
                    required: true
                },

                city: {
                    required: true
                },

                street: {
                    required: true
                },

                home: {
                    required: true
                }
            },

            messages: {
                city: '',
                street: '',
                home: '',
                email: '',
                surname: '',
                firstname: '',
                phone: ''
            }
        });
    }
}

function orderOldValidate() {
    if ($('.order').length > 0) {

        $('.order').on('click',

            '.order__radio--old',

            function () {
                orderOldAddress();
            });
    }
}

function orderNewValidate() {
    if ($('.order').length > 0) {

        $('.order').on('click',

            '.order__radio--new label',

            function () {
                orderNewAddress();
            });
    }
}

function orderOldAddress() {
    if ($('.order').length > 0) {

        $('.order').validate().destroy();

        $('.order').validate({

            rules: {

                surname: {
                    required: true
                },

                firstname: {
                    required: true
                },

                email: {
                    required: true
                },

                phone: {
                    required: true
                }
            },

            messages: {
                email: '',
                surname: '',
                firstname: '',
                phone: ''
            }
        });
    }
}

function validate() {

    //order
    // Move to /local/templates/main/components/bitrix/sale.order.ajax/order/script.js
    /*if ($('.order').length > 0) {

        $('.order').validate({

            rules: {

                surname: {
                    required: true
                },

                firstname: {
                    required: true
                },

                email: {
                    required: true
                },

                phone: {
                    required: true
                }
            },

            messages: {
                email: '',
                surname: '',
                firstname: '',
                phone: ''
            }
        });
    }*/

    //vacancy
    if ($('.popup--vac').length > 0) {

        $('.popup--vac .popup__inner').validate({

            rules: {

                surname: {
                    required: true
                },

                name: {
                    required: true
                },

                email: {
                    required: true
                },

                phone: {
                    required: true
                },

                comment: {
                    required: true
                }
            },

            messages: {

                surname: '',
                name: '',
                email: '',
                phone: '',
                comment: ''
            }
        });
    }

    //data
    if ($('.data__form--password').length > 0) {

        $('.data__form--password').validate({

            rules: {

                /*'NEW_PASSWORD': {
                    required: true
                },*/

                'NEW_PASSWORD_CONFIRM': {
                    equalTo: '#password'
                }
            },

            messages: {
                // 'NEW_PASSWORD': '',
                'NEW_PASSWORD_CONFIRM': ''
            }
        });
    }

    if ($('.data__form--address').length > 0) {

        $('.data__form--address').validate({

            rules: {
                'UF_CITY': {
                    required: true
                },

                'UF_STREET': {
                    required: true
                },

                'UF_HOUSE': {
                    required: true
                }
            },

            messages: {
                'UF_CITY': '',
                'UF_STREET': '',
                'UF_HOUSE': ''

            }
        });
    }

    if ($('.data__form--main').length > 0) {

        $('.data__form--main').validate({

            rules: {

                'LAST_NAME': {
                    required: true
                },

                'NAME': {
                    required: true
                },

                'EMAIL': {
                    required: true
                },

                'PERSONAL_PHONE': {
                    required: true
                }
            },

            messages: {
                'LAST_NAME': '',
                'NAME': '',
                'EMAIL': '',
                'PERSONAL_PHONE': ''
            }
        });
    }

    //add comment
    // Move to template /local/templates/main/components/bitrix/iblock.element.add.form/add_review/script.js
    /*if ($('.add-comment').length > 0) {

        $('.add-comment').validate({

            rules: {

                itemName: {
                    required: true
                },

                userName: {
                    required: true
                },

                email: {
                    required: true
                },

                commentHeader: {
                    required: true
                },

                comment: {
                    required: true
                }
            },

            messages: {
                itemName: '',
                userName: '',
                email: '',
                commentHeader: '',
                comment: ''
            }
        });
    }*/

    //header login
    if ($('.header__auth-login').length > 0) {

        $('.header__auth-login').validate({

            rules: {

                email: {
                    required: true
                },

                password: {
                    required: true
                }
            },

            messages: {
                email: '',
                password: ''
            }
        });
    }

    //reg
    if ($('form.reg').length > 0) {

        $('form.reg').validate({

            rules: {

                'REGISTER[LAST_NAME]': {
                    required: true
                },

                'REGISTER[NAME]': {
                    required: true
                },

                'REGISTER[PERSONAL_PHONE]': {
                    required: true
                },

                'REGISTER[EMAIL]': {
                    required: true
                },

                'REGISTER[PASSWORD]': {
                    required: true
                },

                'REGISTER[CONFIRM_PASSWORD]': {
                    equalTo: '#PASSWORD'
                }

            },

            messages: {
                'REGISTER[LAST_NAME]': '',
                'REGISTER[NAME]': '',
                'REGISTER[PERSONAL_PHONE]': '',
                'REGISTER[EMAIL]': '',
                'REGISTER[PASSWORD]': '',
                'REGISTER[CONFIRM_PASSWORD]': ''
            }
        });
    }
}

function addComment() {

    if ($('.i-prew--transp').length > 0) {

        $('.add-comment__area-items ').on('click',

            '.i-prew',

            function () {

                var name;
                var nameArea = $('.add-comment__input--item-name input'),
                  hiddenField = $('.add-comment__input--item-name input:hidden');

                $('.i-prew').children('input[type=radio]').attr('checked', false);
                $('.i-prew').removeClass('is-active');

                $(this).children('input[type=radio]').attr('checked', true);
                $(this).addClass('is-active');

                name = $(this).find('.i-prew__name').text();
                nameArea.val(name);
                hiddenField.val($(this).data('id'));

                return false;
            });
    }
}

function styles() {
    if ($('.styles table tfoot a').length > 0) {

        $('.styles').on('click',

            'tfoot a',

            function () {
                $(this).parents('table').children('tbody').toggleClass('is-full');

                return false;
            });
    }
}

function menuClose() {
    $('body').on('click',

        '.header__menu-dd-closer',

        function () {

            $(this).parents('.header__menu-item').removeClass('is-active');


            return false;
        });
}

function select() {

    //comments => Move to /local/templates/main/components/bitrix/news.list/reviews_list/script.js
    /*if ($('.select--filter').length > 0) {

        //first
        $('.c-filter__area .select--add-first').select2({
            dropdownCssClass: 'select--filtered',
            dropdownParent: $('.c-filter__area--first')
        });

        //second
        $('.c-filter__area .select--add-second').select2({
            dropdownCssClass: 'select--filtered',
            dropdownParent: $('.c-filter__area--second')
        });
    }*/

    /* Move to => /local/templates/main/components/bitrix/catalog/catalog/bitrix/catalog.smart.filter/.default/script.js
    if ($('.select--checks').length > 0) {

        //catalog

        $('.select--filter-first').select2({
            dropdownCssClass: 'select--filtered',
            dropdownParent: $('.filter__select--first')
        });

        $('.select--checks').select2({
            closeOnSelect: false,
            dropdownCssClass: 'select--filtered',
            dropdownParent: $('.filter__select--second')
        });
    }*/

    //add comment => Move to /local/templates/main/components/bitrix/iblock.element.add.form/add_review/script.js
    /*if ($('.select--comment').length > 0) {

        $('.select--comment').select2({
            dropdownCssClass: 'select--comment',
            dropdownParent: $('.add-comment__select'),
            minimumResultsForSearch: Infinity // Скрывет поле поиска
        });
    }*/

    //basket
    if ($('.select--basket').length > 0) {

        $('.select--basket').select2({
            dropdownParent: $('.basket__select'),
            select: function(e) {
                console.log('e', e);
            }
        });
    }

    //compare => Move to /local/templates/main/components/bitrix/catalog/catalog/bitrix/catalog.compare.result/.default/script.js
    /*if ($('.compare__select').length > 0) {

        $('.select--compare').select2({

            dropdownCssClass: 'select--comment',
            dropdownParent: $('.compare__select')
        });
    }*/

    //order 
    if ($('.order__select').length > 0) {

        $('.order__select select').select2({
            dropdownParent: $('.order__select')
        });
    }
}

function card() {
    // Move to => /local/templates/main/components/bitrix/catalog/catalog/bitrix/catalog.section/.default/script.js
    /*if ($('.card').length > 0) {

        $('.card').on('click',

            '.card__compare',

            function () {
                $(this).toggleClass('is-active');

                return false;
            });
    }*/
}

function filter() {
    $('.catalog').on('click',

        '.catalog__filter-button',

        function () {
            $('.filter').toggleClass('is-visible')
            return false;
        });
}

function basket() {

    $('.basket').on('click',

        '.basket__item-remove',

        function () {

            $(this).parents('.basket__item').fadeOut(300, function () {

                $(this).remove();
            });
        });
}

//counter
function counter() {

    if ($('.counter').length > 0) {
        // Used standard JS in basket
        /*var result = $('.counter__result');
        var minus = $('.counter__minus');
        var plus = $('.counter__plus');

        minus.on('click', function () {

            if ($(this).siblings('.counter__result').val() > 1) {

                var value = $(this).siblings('.counter__result').val();
                $(this).siblings('.counter__result').val(+value - 1);
            }
        });

        plus.on('click', function () {

            var value = $(this).siblings('.counter__result').val();
            $(this).siblings('.counter__result').val(+value + 1);
        });*/
    }
}

function regTimer(startValue, endValue) {

    if ($('#reg__timer').length > 0) {

        startValue = startValue || 0;
        endValue = endValue || 48;

        $('#reg__timer').slider({
            step: 1,
            range: true,
            min: 0,
            max: 48,
            values: [startValue, endValue],

            slide: function (event, ui) {
                var startValue = $("#reg__timer").slider("values", 0);
                var endValue = $("#reg__timer").slider("values", 1);

                if (startValue % 2 == 0) {
                    $("#startTime").val(startValue / 2 + ':00');

                } else {

                    $("#startTime").val(Math.floor(startValue / 2) + ':30');

                }

                if (endValue % 2 == 0) {
                    $("#endTime").val(endValue / 2 + ':00');

                } else {

                    $("#endTime").val(Math.floor(endValue / 2) + ':30');
                }

                // $("#startTime").val(startValue);
                // $("#endTime").val(endValue);
            },

            stop: function (event, ui) {
                var startValue = $("#reg__timer").slider("values", 0);
                var endValue = $("#reg__timer").slider("values", 1);

                if (startValue % 2 == 0) {
                    $("#startTime").val(startValue / 2 + ':00');

                } else {

                    $("#startTime").val(Math.floor(startValue / 2) + ':30');

                }

                if (endValue % 2 == 0) {
                    $("#endTime").val(endValue / 2 + ':00');

                } else {

                    $("#endTime").val(Math.floor(endValue / 2) + ':30');
                }

                // $("#startTime").val(startValue);
                // $("#endTime").val(endValue);
            },

            create: function (event, ui) {
                var startValue = $("#reg__timer").slider("values", 0);
                var endValue = $("#reg__timer").slider("values", 1);

                if (startValue % 2 == 0) {
                    $("#startTime").val(startValue / 2 + ':00');

                } else {

                    $("#startTime").val(Math.floor(startValue / 2) + ':30');

                }

                if (endValue % 2 == 0) {
                    $("#endTime").val(endValue / 2 + ':00');

                } else {

                    $("#endTime").val(Math.floor(endValue / 2) + ':30');
                }

                // $("#startTime").val(startValue);
                // $("#endTime").val(endValue);
            }
        });
    }
}

function priceSlider() {

    if ($('#price-slider').length > 0) {

        /* Move to
        $('#price-slider').slider({
            step: 1,
            range: true,
            min: 0,
            max: 1000,
            values: [0, 1000],

            // change: function(event, ui) {
            // 	$("#startPrice").val($("#price-slider").slider("values",0));
            // 	$("#endPrice").val($("#price-slider").slider("values",1));
            // },

            create: function (event, ui) {
                $("#startPrice").val($("#price-slider").slider("values", 0));
                $("#endPrice").val($("#price-slider").slider("values", 1));
            },

            slide: function (event, ui) {
                $("#startPrice").val($("#price-slider").slider("values", 0));
                $("#endPrice").val($("#price-slider").slider("values", 1));
            },

            stop: function (event, ui) {
                $("#startPrice").val($("#price-slider").slider("values", 0));
                $("#endPrice").val($("#price-slider").slider("values", 1));
            }
        });*/
    }
}

function removeOrder() {
    $('body').on('click',

        '.data__order-remove',

        function () {
            $(this).parents('.data__order').fadeOut(300, function () {
                $(this).remove();
            });

            return false;
        });
}

function personalTabs() {
    $('body').on('click',

        '.data__nav-button',

        function () {
            var index = $(this).index();
            console.log(index);

            $('.data__nav-button').removeClass('is-active');
            $(this).addClass('is-active');

            $('.data__tab').removeClass('is-active');
            $('.data__tab').eq(index).addClass('is-active');
            return false;
        });


}

function ask() {
    var current = $('.ask__questions.is-active').index() + 1;
    var total = $('.ask__questions').length;
    var visible = current - 1;

    $('.ask__count-total').text('/' + total);
    $('.ask__header .ask__count-current ').text(current);

    if (current < total) {
        $('.ask__nav .ask__count-current').text(current + 1);

    } else {

        $('.ask__nav .ask__count-current').text(current);
    }
    /*$('body').on('click',

        '.ask__more',

        function () {

            if (visible < total - 1) {

                $('.ask__questions').eq(visible).removeClass('is-active');
                $('.ask__questions').eq(visible + 1).addClass('is-active');


                visible++;

                $('.ask__header .ask__count-current ').text(visible + 1);

                if (visible < total - 1) {

                    $('.ask__nav .ask__count-current').text(visible + 2);

                } else {

                    $('.ask__nav .ask__count-current').text(visible + 1);
                }

            } else {

                return false;
            }

            return false;
        });*/
    $('body').on('click',

        '.ask__more',

        function () {
            var formData = $("#choose_bike_form").serialize();
            $.ajax({
                url: '/include/ajax/bike_choose.php',
                type: "POST",
                data: formData,
                dataType: json,
                success: function(data) {
                    alert('ok');
                }
            });
        });
}

function sliders() {
    //sliders fix:
    $.fn.andSelf = function () {
        return this.addBack.apply(this, arguments);
    }

    //weekly
    if ($('.weekly').length > 0) {

        $('.weekly').owlCarousel({
            items: 1,
            margin: 50,

            responsive: {
                0: {
                    nav: false
                },

                768: {
                    nav: true
                }
            }


        });
    }

    //index
    if ($('.slider--index').length > 0) {

        $('.slider--index').owlCarousel({
            items: 1,
            loop: true,
            dotsClass: 'slider__nums',
            dotClass: 'slider__num',

            responsive: {
                0: {
                    dots: false
                },

                768: {
                    dots: true
                }
            }
        });
    }

    if ($('.cat-slider__slider').length > 0) {

        $('.cat-slider__slider').owlCarousel({
            margin: 30,
            nav: true,
            loop: true,

            responsive: {

                0: {
                    items: 1
                },

                768: {
                    items: 2
                },

                1024: {
                    margin: 10,
                    items: 3
                },

                1600: {
                    items: 4
                }
            }


        });
    }

    if ($('.not-found__slider').length > 0) {

        $('.not-found__slider').owlCarousel({
            loop: true,
            nav: true,

            responsive: {
                0: {
                    items: 1,
                    margin: 100,
                    nav: false
                },

                480: {
                    nav: true,
                    items: 1,
                    margin: 100
                },

                1280: {
                    items: 3
                }
            }


        });
    }

    if ($('.styles .slider').length > 0) {

        $('.styles__main .slider').owlCarousel({
            items: 1,
            loop: true,
            nav: true
        });
    }


    if ($('.styles__slider').length > 0) {

        $('.styles__slider').owlCarousel({
            nav: true,
            loop: true,
            margin: 10,

            responsive: {
                0: {
                    items: 1,
                    nav: false
                },

                768: {
                    items: 2
                },

                1280: {
                    items: 3
                }
            }
        });
    }

    /* Move to /local/templates/main/components/bitrix/catalog/catalog/bitrix/catalog.compare.result/.default/script.js
    if ($('.compare__items').length > 0) {

        $('.compare__items').owlCarousel({
            loop: true,
            nav: true,

            responsive: {

                0: {
                    items: 1
                },

                768: {
                    items: 2

                },

                1024: {
                    items: 3
                },

                1280: {
                    items: 4
                },

                1500: {
                    items: 5
                }
            }
        });
    }*/

    if ($('.item-slider').length > 0) {

        $('.item-slider .slider').owlCarousel({
            nav: true,
            //loop: true,
			URLhashListener:true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },

                768: {
                    items: 2,
                    nav: true
                },

                1280: {
                    items: 4
                }
            }
        });
    }

    if ($('.item__slider').length > 0) {

			var carousel = $('.item__slider');
			
			carousel.owlCarousel({
				items: 1,
				onDragged: callback
			});

			

			

			function callback(event) {
			
				setTimeout(function () {
					var slide = carousel.find('.owl-item.active').find('.item__slider-item').data('count');
					//alert(event.current);
					$('.item__images_div a').removeClass('is-active');
					$('.item__images').find('[data-count="' + slide + '"]').addClass('is-active');

				}, 100);

			}
		
			/*
			var slider = $('.item__images');
			slider.owlCarousel({
                items: 2,
                loop: false,
                nav: true,
                autoplay: false,
				autoWidth: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: false,
				animateOut : "slideOutUp",
				animateIn : "slideInUp",
                //onChanged: attachmentChangeGallery
            });
			*/
			carousel.trigger('to.owl.carousel', 1);
			//carousel.trigger("owl.goTo", 2);
    }
}

function owlDots() {

    var elem = $('.slider__num')
    var total = $('.slider__num').length;
    var num = 1;

    for (var i = 0; i < total; i++) {

        elem.eq(i).text('0' + num);

        num++;
    }

    num = 1;
}

function nestedDD() {
    $('body').on('click',

        '.header__menu-dd-head',

        function () {

            if ($(window).width() < 768) {

                $(this).siblings('.header__menu-dd-list').slideToggle();

                $(this).toggleClass('is-active');


                $(window).on('resize', function () {
                    $(this).removeClass('is-active');
                });

                console.log($(window).width());
            }

        });
}

function mobileCatalogDD() {

    $('body').on('click',

        '.header__menu-item--dd.mobile .header__menu-link--dd',

        function () {

            $(this).siblings('.header__menu-dd').slideToggle(250);

            if ($(this).siblings().length > 0) {

                $(this).toggleClass('is-active');
            }
        });
}

function headerRespond() {
    var width = $(window).width();

    if (width > 1279) {
        $('.header__menu-dd').css({
            'display': 'block'
        });

        $('.header__menu-item--dd').removeClass('mobile');
        $('.header__menu-item--dd').addClass('desktop');

    } else {
        $('.header__menu-dd').css({
            'display': 'none'
        });

        $('.header__menu-item--dd').addClass('mobile');
        $('.header__menu-item--dd').removeClass('desktop');
    }
}

function widthChecks() {

    $(window).on('resize', function () {
        headerRespond();
    });
}

function citySelect() {

    var block = $('.header__city');

    $('body').on('click',

        '.header__city-accept',

        function () {

            block.removeClass('onload');
            block.addClass('selected');
            block.removeClass('active');

            return false;
        });

    $('body').on('click',

        '.header__city',

        function () {

            block.addClass('active');
            block.removeClass('selected');
            block.addClass('onload');
        });

    $('body').on('click',

        '.header__city-choose',

        function () {

            block.removeClass('onload');
            block.addClass('select');

            return false;
        });

    $(document).on('mouseup', function (e) {

        if (!block.is(e.target)
            && block.has(e.target).length === 0) {

            block.removeClass('active onload select');
        }
    });
}

function cityScroll() {
    $('.header__city-wrapper').mCustomScrollbar({
        theme: 'custom'
    });
}

function search() {
    var block = $('.header__search');

    $('body').on('click',

        '.header__search-icon.is-hidden',

        function () {

            $('.header__search-area').addClass('is-active');
            $('.header__search-icon').removeClass('is-hidden');
            $('.header__search-icon').addClass('is-active');
            $('.header__search').addClass('is-active');


            return false;
        });

    $(document).on('mouseup', function (e) {

        if (!block.is(e.target)
            && block.has(e.target).length === 0) {

            $('.header__search-area').removeClass('is-active');
            $('.header__search-icon').addClass('is-hidden');
            $('.header__search-icon').removeClass('is-active');
            $('.header__search').removeClass('is-active');
        }
    });
}

function menuDD() {
    var block = $('.header__menu-item--dd');


    $('body').on('click',

        '.header__menu-link--dd',

        function () {

            if ($(this).parents('.header__menu-item--dd.desktop').hasClass('is-active')) {
                $(this).parents('.header__menu-item--dd.desktop').removeClass('is-active');

            } else {

                $('.header__menu-item--dd.desktop').removeClass('is-active');
                $(this).parents('.header__menu-item--dd.desktop').addClass('is-active');
            }

        });


    $(document).on('mouseup', function (e) {

        if (!block.is(e.target)
            && block.has(e.target).length === 0) {

            $('.header__menu-item--dd').removeClass('is-active');
        }
    });
}

function mobileMenu() {

    $('body').on('click',

        '.header__menu-opener',

        function () {

            if (!$(this).hasClass('is-active')) {
                $(this).addClass('is-active');

                $('.header__nav').addClass('is-active');

            } else {
                $(this).removeClass('is-active');
                $('.header__nav').removeClass('is-active');
            }
        });
}

function mobileCatalog() {
    $('body').on('click',

        '.header__catalog-button',

        function () {

            if (!$(this).hasClass('is-active')) {
                $(this).addClass('is-active');

                $('.header__menu').fadeIn();


            } else {
                $(this).removeClass('is-active');
                $('.header__menu').fadeOut();
            }

            return false;
        });


    $('body').on('click',

        '.header__menu-closer',

        function () {

            $('.header__menu').fadeOut();
            $('.header__catalog-button').removeClass('is-active');

            return false;
        });
		
		
		 $('body').on('click',

			'.popup__card-submit',

			function () {


				return false;
        });
}

function headerDD() {
    if ($(window).width() < 1280) {

        $('body').on('click',

            '.header__nav-link',

            function () {

                $(this).siblings('.header__dd').slideToggle();

                return false;
            });
    }
}