<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
if ($_REQUEST["type"] == 'checkinput') {
    $sectionID = post_data_processing($_REQUEST["val"]);
    $intCount = CIBlockSection::GetCount(array('IBLOCK_ID' => $iblockID, 'SECTION_ID' => $sectionID));
    if ($intCount < 1) {
        $res = CIBlockSection::GetByID($sectionID);
        if ($ar_res = $res->GetNext()) {
            $url = $ar_res['DESCRIPTION'];
            $parentSectionID = $ar_res["IBLOCK_SECTION_ID"];
            $res2 = CIBlockSection::GetByID($parentSectionID);
            if ($ar_res2 = $res2->GetNext()) {
                $parentSectionID2 = $ar_res2["IBLOCK_SECTION_ID"];
                if ($parentSectionID2) {
                    echo json_encode(array('last' => 'y', 'url' => $url));
                } else {
                    echo json_encode(array('last' => 'y', 'url' => $url,'first' => 'y'));
                    //echo json_encode(array('last' => 'n', 'first' => 'y'));
                }
            }
        }

    } else {
        $res = CIBlockSection::GetByID($sectionID);
        if ($ar_res = $res->GetNext()) {
            //if($arSection = $rsSections->GetNext()) {
            $parentSectionID = $ar_res["IBLOCK_SECTION_ID"];
            $res2 = CIBlockSection::GetByID($parentSectionID);
            if ($ar_res2 = $res2->GetNext()) {
                $parentSectionID2 = $ar_res2["IBLOCK_SECTION_ID"];
                if ($parentSectionID2) {
                    echo json_encode(array('last' => 'n'));
                } else {
                    echo json_encode(array('last' => 'n', 'first' => 'y'));
                }
            }
        }
    }
    exit();
} elseif ($_REQUEST["type"] == 'prev') {
    $secID = post_data_processing($_REQUEST["section_id"]);
    $iblockID = post_data_processing($_REQUEST["iblock_id"]);
    /*$arFilter = array(
        "IBLOCK_ID" => $iblockID,
        "GLOBAL_ACTIVE" => "Y",
        "IBLOCK_ACTIVE" => "Y",
        "SECTION_ID" => $secID
    );
    $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');

    $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
        "ID",
        "IBLOCK_SECTION_ID",
        "DEPTH_LEVEL",
        "NAME",
        "SECTION_PAGE_URL",
        "PICTURE",
        "DESCRIPTION"
    ));*/
    $res = CIBlockSection::GetByID($secID);
    if ($ar_res = $res->GetNext()) {
        //if($arSection = $rsSections->GetNext()) {
        $parentSectionID = $ar_res["IBLOCK_SECTION_ID"];
        $sectionID = $parentSectionID;
        $iblockID = post_data_processing($_REQUEST["iblock_id"]);
        $res = CIBlockSection::GetByID($sectionID);
        if ($ar_res = $res->GetNext())
            $question = $ar_res['DESCRIPTION'];
        /* if ($_REQUEST["type"] == 'last') {
             echo $question;
             exit();
         }*/
//echo $sectionID;

        $arFilter = array(
            "IBLOCK_ID" => $iblockID,
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ACTIVE" => "Y",
            "SECTION_ID" => $sectionID
        );
        $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');

        $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
            "ID",
            "IBLOCK_SECTION_ID",
            "DEPTH_LEVEL",
            "NAME",
            "SECTION_PAGE_URL",
            "PICTURE",
            "DESCRIPTION"
        ));
        $arResult['SECTIONS'] = array();
        while ($arSection = $rsSections->GetNext()) {
            $arResult["SECTIONS"][] = $arSection;
        }
        $is_last_question = true;
        foreach ($arResult["SECTIONS"] as $SECTION) {
            $intCount = CIBlockSection::GetCount(array('IBLOCK_ID' => $iblockID, 'SECTION_ID' => $SECTION["ID"]));
            if ($intCount > 0) {
                $is_last_question = false;
                break;
            }
        }

        $res2 = CIBlockSection::GetByID($parentSectionID);
        if ($ar_res2 = $res2->GetNext()) {
            $parentSectionID2 = $ar_res2["IBLOCK_SECTION_ID"];
        }        
//dump($arResult["SECTIONS"]);
        ?>
        <div class="ask__header <?= 'ttt' . $parentSectionID ?>">
            <!-- <div class="ask__count"><span class="ask__count-current"> </span><span class="ask__count-total"> </span></div>-->
            <div class="ask__name"><?= $question ?></div>
        </div>
        <div class="ask__qs">
            <div class="ask__questions is-active">
                <? foreach ($arResult["SECTIONS"] as $key => $item) { ?>
                    <div class="ask__question">
                        <input type="radio" id="question_<?= $item["ID"] ?>" name="q1" value="<?= $item["ID"] ?>">
                        <label for="question_<?= $item["ID"] ?>"><?= $item["NAME"] ?></label>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="ask__nav">
            <div class="buttons_wrap">
                <? if (!$is_last_question) {
                ?>
                <a href="javascript:void(0)" class="prev_question"<? if (!$parentSectionID2) {?>style="visibility: hidden;"<?}?>>

                    <div class="ask__count">
                        <!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                        <div class="prev_question-text">Я передумал</div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="ask__more">
                    <div class="ask__count">
                        <div class="ask__more-text">Еще <br> вопрос</div>
                    </div>
                </a>
            </div>
            <? } else {
            ?>
        </div>
        <a href="javascript:void(0)" class="prev_question" style="visibility: hidden;">

            <div class="ask__count">
                <!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                <div class="prev_question-text">Я передумал</div>
            </div>
        </a>
        <button type="submit" class="ask__submit"> Показать <br> результат</button>
    <? } ?>
        </div>
        <script>
            $('body').on('click',
                '.ask__more',
                function () {
                    var $form = $("#choose_bike_form");
                    var formData = $form.serialize();
                    formData += '&type=ajax';
                    console.log(formData);
                    $.ajax({
                        url: '/include/ajax/bike_choose.php',
                        type: "POST",
                        data: formData,
                        success: function (data) {
                            console.log('ok');
                        }
                    });
                });
        </script>
    <?
    }
} else {
    $sectionID = post_data_processing($_REQUEST["q1"]);
    $iblockID = post_data_processing($_REQUEST["iblock_id"]);
    $res = CIBlockSection::GetByID($sectionID);
    if ($ar_res = $res->GetNext())
        $question = $ar_res['DESCRIPTION'];
    if ($_REQUEST["type"] == 'last') {
        echo $question;
        exit();
    }
//echo $sectionID;

    $arFilter = array(
        "IBLOCK_ID" => $iblockID,
        "GLOBAL_ACTIVE" => "Y",
        "IBLOCK_ACTIVE" => "Y",
        "SECTION_ID" => $sectionID
    );
    $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');

    $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
        "ID",
        "IBLOCK_SECTION_ID",
        "DEPTH_LEVEL",
        "NAME",
        "SECTION_PAGE_URL",
        "PICTURE",
        "DESCRIPTION"
    ));
    $arResult['SECTIONS'] = array();
    while ($arSection = $rsSections->GetNext()) {
        $parentSectionID = $arSection["IBLOCK_SECTION_ID"];
        $arResult["SECTIONS"][] = $arSection;
    }
    $is_last_question = true;
    foreach ($arResult["SECTIONS"] as $SECTION) {
        $intCount = CIBlockSection::GetCount(array('IBLOCK_ID' => $iblockID, 'SECTION_ID' => $SECTION["ID"]));
        if ($intCount > 0) {
            $is_last_question = false;
            break;
        }
    }
    $res2 = CIBlockSection::GetByID($parentSectionID);
    if ($ar_res2 = $res2->GetNext()) {
        $parentSectionID2 = $ar_res2["IBLOCK_SECTION_ID"];
    }
//dump($arResult["SECTIONS"]);
    ?>
    <div class="ask__header">
        <!-- <div class="ask__count"><span class="ask__count-current"> </span><span class="ask__count-total"> </span></div>-->
        <div class="ask__name"><?= $question ?></div>
    </div>
    <div class="ask__qs">
        <div class="ask__questions is-active">
            <? foreach ($arResult["SECTIONS"] as $key => $item) { ?>
                <div class="ask__question">
                    <input type="radio" id="question_<?= $item["ID"] ?>" name="q1" value="<?= $item["ID"] ?>">
                    <label for="question_<?= $item["ID"] ?>"><?= $item["NAME"] ?></label>
                </div>
            <? } ?>
        </div>
    </div>
    <div class="ask__nav">
        <div class="buttons_wrap">
            <? if (!$is_last_question) {
            ?>
            <a href="javascript:void(0)" class="prev_question" <? if (!$parentSectionID2) {?>style="visibility: hidden;"<?}?>>

                <div class="ask__count">
                    <!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                    <div class="prev_question-text">Я передумал</div>
                </div>
            </a>
            <a href="javascript:void(0)" class="ask__more">
                <div class="ask__count">
                    <div class="ask__more-text">Еще <br> вопрос</div>
                </div>
            </a>
        </div>
        <? } else {
        ?>
        <a href="javascript:void(0)" class="prev_question">

            <div class="ask__count">
                <!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                <div class="prev_question-text">Я передумал</div>
            </div>
        </a>
    </div>
    <button type="submit" class="ask__submit"> Показать <br> результат</button>
<? } ?>
    </div>
    <script>
        $('body').on('click',
            '.ask__more',
            function () {
                var $form = $("#choose_bike_form");
                var formData = $form.serialize();
                formData += '&type=ajax';
                console.log(formData);
                $.ajax({
                    url: '/include/ajax/bike_choose.php',
                    type: "POST",
                    data: formData,
                    success: function (data) {
                        console.log('ok');
                    }
                });
            });
    </script>
<? } ?>