<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>

 <div class="weekly simple">
	
	<a href="/test/">
		<img src="/local/templates/main/tpl/images/content/choose_week.jpg" alt="">
		<div class="weekly__about">
			<div class="weekly__category">Протестируй лучшее</div>
			<div class="weekly__name">Тест-драйвы велосипедов Specialized</div>
		</div>
	</a>
 </div>
<?
/*

if (!empty($arResult['ITEMS'])) {
    if ($arParams["DISPLAY_TOP_PAGER"]) {
        ?><? echo $arResult["NAV_STRING"]; ?><?
    }

    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => Loc::getMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

    if ($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y') { ?>
        <div class="bx-section-desc">
            <p class="bx-section-desc-post"><?= $arResult["DESCRIPTION"] ?></p>
        </div>
    <? } ?>
    <div
        class="weekly slider slider--navs">
	
	
	
	
	
        <?
        foreach ($arResult['ITEMS'] as $key => $arItem) {
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            $strMainID = $this->GetEditAreaId($arItem['ID']);

            $arItemIDs = array(
                'ID' => $strMainID,
                'PRICE' => $strMainID . '_price',
            );

            $minPrice = false;
            if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
                $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
            }

            ?>
        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="weekly__slide" id="<? echo $strMainID; ?>">
            <div class="weekly__image"><img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt=""></div>
            <div class="weekly__about">
                <div class="weekly__category"><?= Loc::getMessage("CT_BCS_CHOOSE_WEEK"); ?></div>
                <div class="weekly__name"><?= $arItem['NAME'] ?></div>
             </div>  
				
            </a><?
        }
        ?>
    </div>
    <?
    if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
        ?><? echo $arResult["NAV_STRING"]; ?><?
    }
} 
*/
?>
