<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

if (!empty($arResult['ITEMS'])) {
  $arEmptyPreview = false;
  $strEmptyPreview = $this->GetFolder().'/images/no_photo.png';

  if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview)) {
    $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);

    if (!empty($arSizes)) {
      $arEmptyPreview = array(
        'SRC' => $strEmptyPreview,
        'WIDTH' => intval($arSizes[0]),
        'HEIGHT' => intval($arSizes[1])
      );
    }

    unset($arSizes);
  }

  unset($strEmptyPreview);
}

foreach ($arResult["ITEMS"] as &$arItem) {
  $srcPic = false;

  if (is_array($arItem['PREVIEW_PICTURE'])) {
    $srcPic = $arItem['PREVIEW_PICTURE'];
  } elseif (is_array($arItem['DETAIL_PICTURE'])) {
    $srcPic = $arItem['DETAIL_PICTURE'];
  } elseif (is_array($arEmptyPreview)) {
    $srcPic = $arEmptyPreview;
  }
  


  	foreach ($arItem['OFFERS'] as &$arOffer) {
  
		if ( is_array($arOffer["PREVIEW_PICTURE"]) ){
		
			$srcPic = $arOffer["PREVIEW_PICTURE"];
			continue;
		}
		
	}
  
  

  if ($srcPic['ID']) {
    $resPic = CFile::ResizeImageGet($srcPic['ID'], array(
      'height' => $arParams['IMAGE_HEIGHT'],
      'width' => $arParams['IMAGE_WIDTH']
    ), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);

    if ($resPic) {
      $arItem['PREVIEW_PICTURE']['SRC'] = $resPic['src'];
      $arItem['PREVIEW_PICTURE']['WIDTH'] = $resPic['width'];
      $arItem['PREVIEW_PICTURE']['HEIGHT'] = $resPic['height'];
    }
  }
}