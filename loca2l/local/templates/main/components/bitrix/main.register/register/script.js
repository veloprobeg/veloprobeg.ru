BX.ready(function() {
    var form = BX('reg_form'),
        obBtn = BX('reg_button'),
        obProcData = BX('UF_PROC_PERS_DATA'),
	obErrors = BX('reg_errors');
	
	var errors = $('#reg_errors').html();
	if ( errors ){
		showError(BX('popup-errors'), errors);
	}

    BX.bind(obBtn, 'click', BX.proxy(function (e) {
        if (obProcData.checked) {
        }
        else {
	    showError(BX('popup-errors'), BX.message('ERROR_REGISTER_PERSON'));
            e.preventDefault();
        }
	
	
	
	
	
    }, this));
    
    
        function showError(node, msg, border) {
            if (BX.type.isArray(msg)) {
                msg = msg.join('<br />');
            }
		
            var errorContainer = BX('popup-errors').querySelector('.alert.alert-danger'), animate;
            if (errorContainer && msg.length) {
	    
		
                BX.cleanNode(errorContainer);
                errorContainer.appendChild(BX.create('DIV', {html: msg}));

                errorContainer.style.opacity = 0;
                errorContainer.style.display = '';
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
                    step: function (state) {
                        errorContainer.style.opacity = state.opacity / 100;
                    },
                    complete: function () {
                        errorContainer.removeAttribute('style');
                    }
                }).animate();

                if (!!border) {
                    BX.addClass(BX('popup-errors'), 'bx-step-error');
                }
                BX.addClass(BX('popup-errors'), 'is-visible');
            }
        }
    
    
    
})