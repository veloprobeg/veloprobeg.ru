<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/gp.php");
$name = "gp";
$title = GetMessage("BOOKMARK_HANDLER_GP");
$icon_url_template = "<a href=\"https://plus.google.com/share?url=#PAGE_URL#\" onclick=\"javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\" class=\"item__share-link item__share-link--gogel\"></a>\n";
$sort = 500;
?>