<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>
<?
use \Bitrix\Main\Localization\Loc;

if (strlen($arResult["PAGE_URL"]) > 0) {
  ?>
  <div class="item__share">
    <h2 class="item__share-head"><?= Loc::getMessage("SHARE_URL"); ?></h2>
    <div class="item__share-links">
      <? foreach ($arResult["BOOKMARKS"] as $name => $arBookmark): ?>
        <?= $arBookmark["ICON"] ?>
      <? endforeach ?>
      <a href="#" class="item__share-link item__share-link--share"></a>
      <div class="item__share-copy">
        <input type="text" value="http://<?= SITE_SERVER_NAME.$arParams['PAGE_URL'] ?>"
               class="item__share-url default-input" style="text-transform: none;">
        <a href="#" class="button button--orange item__copy"><?= Loc::getMessage("SHARE_COPY_LINK"); ?></a>
      </div>
    </div>
  </div>
  <?
} else {
  ?><?= Loc::getMessage("SHARE_ERROR_EMPTY_SERVER") ?><?
}
?>