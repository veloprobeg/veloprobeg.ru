<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<? if (!empty($arResult)): ?>
    <ul class="header__nav-list">
    <?
    $previousLevel = 0;
    $countRootElements = 0;
foreach ($arResult as $k => $arItem) {
    // Ограничение в меню на 4 пункта
    if ($arItem['DEPTH_LEVEL'] == 1 && $countRootElements > 3) {
        break;
    }
    ?>
    <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
        <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
    <? endif ?>

    <?
    if ($arItem["IS_PARENT"]) {
    ?>
    <li class="header__nav-item">
    <a href="<?= $arItem["LINK"] ?>" class="header__nav-link"><?= $arItem["TEXT"] ?></a>
    <ul class="header__dd">
    <?
} else {
    ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
        <li class="header__nav-item">
            <a href="<?= $arItem['LINK'] ?>" class="header__nav-link"
               title="<?= $arItem['TEXT'] ?>"><?= $arItem["TEXT"] ?></a>
        </li>
    <? else: ?>
        <li class="header__dd-item">
            <a href="<?= $arItem['LINK'] ?>" class="header__dd-link"
               title="<?= $arItem['TEXT'] ?>"><?= $arItem["TEXT"] ?></a>
        </li>
    <? endif ?>
    <?
}
    $previousLevel = $arItem["DEPTH_LEVEL"];
    if ($arItem['DEPTH_LEVEL'] == 1) {
        $countRootElements++;
    }
}
    ?>

    <? if ($previousLevel > 1)://close last item tags?>
        <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <? endif ?>
    </ul>
<? endif ?>
