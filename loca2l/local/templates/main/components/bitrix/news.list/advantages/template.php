<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
?>
<div class="benefits">
    <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="benefits__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div style="background-image: url(<?= $arItem['PROPERTIES']['IMAGE_SVG']['SRC'] ?>)"
                 class="benefits__image"></div>
            <div class="benefits__content">
                <div
                    class="benefits__name<?= intval($arItem['NAME']) > 0 ? ' benefits__name--numbers' : '' ?>"><?= $arItem['NAME'] ?></div>
                <div class="benefits__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
            </div>
        </div>
    <? endforeach ?>
</div>