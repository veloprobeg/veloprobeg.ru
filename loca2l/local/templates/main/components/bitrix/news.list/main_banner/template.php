<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
?>
<? if (!empty($arResult['ITEMS'])): ?>
    <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE'];?>" style="background-image: url(<?=$arItem['PREVIEW_PICTURE']['SRC'];?>)" class="exq" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="exq__header"><?=$arItem['PROPERTIES']['HEAD']['VALUE']['TEXT'];?></div>
            <div class="exq__name"><?=$arItem['PROPERTIES']['BODY']['VALUE']['TEXT'];?></div>
            <div class="exq__from"><?=$arItem['PROPERTIES']['BTN']['VALUE']['TEXT'];?></div>
        </a>
    <? endforeach ?>
<? endif ?>