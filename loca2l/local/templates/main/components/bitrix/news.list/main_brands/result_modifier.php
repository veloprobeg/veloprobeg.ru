<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;

//получаем всех производителей
$IBLOCK_ID = 79;
$arBrend = Array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>"CML2_MANUFACTURER"));
while($enum_fields = $property_enums->GetNext())
{	
	$arBrend[$enum_fields["VALUE"]] = $enum_fields["XML_ID"];

}

foreach ($arResult['ITEMS'] as $key => $arItem){
	$nameBrend = $arItem["NAME"];
	if ( $arBrend[$nameBrend] ){
		$arResult['ITEMS'][$key]["URL"] = '/catalog/veloprobeg_ru/filter/cml2_manufacturer-is-' . $arBrend[$nameBrend] . '/apply/';
	}
}


?>

