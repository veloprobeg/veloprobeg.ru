(function (window) {
  if (!!window.CJSReviewsList) {
    return;
  }

  window.CJSReviewsList = function (params) {
    this.obOrder = null;
    this.obReviewOf = null;

    this.url = params.URL || null;
    this.commentId = params.COMMENT_ID || null;
    this.ajaxId = params.AJAX_ID || null;

    if (!!params.ORDER_ID) {
      this.obOrder = BX(params.ORDER_ID);
    }

    if (!!params.REVIEW_OF_ID) {
      this.obReviewOf = BX(params.REVIEW_OF_ID);
    }

    this.params = params;
    this.init();
  };

  window.CJSReviewsList.prototype = {
    init: function () {
      if ($('.select--filter').length > 0) {

        //first
        $('.c-filter__area .select--add-first').select2({
          dropdownCssClass: 'select--filtered',
          dropdownParent: $('.c-filter__area--first'),
        }).on('select2:select', BX.proxy(this.change, this));

        //second
        $('.c-filter__area .select--add-second').select2({
          dropdownCssClass: 'select--filtered',
          dropdownParent: $('.c-filter__area--second')
        }).on('select2:select', BX.proxy(this.change, this));
      }
    },

    buildUrl: function() {
      var params = {
        'bxajaxid': this.ajaxId,
        'ORDER_BY': this.obOrder.value,
        'REVIEW_OF': this.obReviewOf.value,
        'AJAX': 'Y'
      };

      this.ajaxUrl = this.url + '?';

      var isFirst = true;

      for (var u in params) {
        if (params.hasOwnProperty(u)) {
          if (!isFirst) {
            this.ajaxUrl += '&';
          }

          this.ajaxUrl += u + '=' + params[u];
          isFirst = false;
        }
      }
    },

    change: function(e) {
      if (!this.url) {
        return;
      }

      this.buildUrl();

      BX.ajax.insertToNode(this.ajaxUrl, this.commentId);
    },
  }
})(window);
