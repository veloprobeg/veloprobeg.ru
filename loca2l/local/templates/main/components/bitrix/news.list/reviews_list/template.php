<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

$isAjax = $_REQUEST['AJAX'] == 'Y';
?>
<? if (!$isAjax): ?>
  <div class="c-filter">
    <form class="c-filter__filters">
      <div class="c-filter__area c-filter__area--first">
        <div class="c-filter__name"><?= Loc::getMessage("REVIEWS_ORDER_BY"); ?></div>
        <select class="select select--filter select--add-comm select--add-first" name="ORDER_BY" id="ORDER_BY">
          <? foreach ($arResult['SORT'] as $sort): ?>
            <option value="<?= $sort['CODE'] ?>"><?= Loc::getMessage($sort['NAME']); ?></option>
          <? endforeach ?>
        </select>
      </div>
      <div class="c-filter__area c-filter__area--second">
        <div class="c-filter__name"><?= Loc::getMessage("REVIEWS_SHOW_REVIEWS"); ?></div>
        <select class="select select--filter select--add-comm select--add-second" name="PROPERTY_REVIEW_OF"
                id="REVIEW_OF">
          <? foreach ($arResult['REVIEW_OF'] as $reviewOf): ?>
            <option value="<?= $reviewOf['ID'] ?>"><?= $reviewOf['VALUE'] ?></option>
          <? endforeach ?>
        </select>
      </div>
    </form>
    <a href="/about/reviews/add-a-review/"
       class="button button--white-transp button--hover comment-add"><?= Loc::getMessage("REVIEWS_ADD_REVIEW"); ?></a>
  </div>
  <div class="comments" id="comments_<?= $arParams['AJAX_ID'] ?>">
    <? endif ?>
    <? foreach ($arResult['COLUMNS'] as $arColumn): ?>
      <div class="comments__area">
        <? foreach ($arColumn as $arItem): ?>
          <?
          $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
          $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
          ?>
          <div class="comment" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="comment__head">
              <div class="comment__date"><?= $arItem['DATE'] ?></div>
              <div class="comment__author"><?= $arItem['DISPLAY_PROPERTIES']['NAME']['VALUE'] ?>:</div>
            </div>
            <? if ($arItem['PRODUCT']): ?>
              <div class="comment__target">
                <a href="<?= $arItem['PRODUCT']['DETAIL_PAGE_URL'] ?>" class="i-prew">
                  <div class="i-prew__image"><img src="<?= $arItem['PRODUCT']['PICTURE']['SRC'] ?>" class="i-prew__img"
                                                  alt=""></div>
                  <div class="i-prew__about">
                    <div class="i-prew__name"><?= $arItem['PRODUCT']['NAME'] ?></div>
                    <? if ($arItem['PRODUCT']['PRICE']): ?>
                      <div class="i-prew__price"><?= $arItem['PRODUCT']['PRICE_FORMATED']; ?></div>
                    <? endif ?>
                  </div>
                </a>
              </div>
            <? endif ?>
            <div class="comment__content">
              <div class="comment__name"><?= $arItem['NAME'] ?></div>
              <div class="comment__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
            </div>
            <? if (!empty($arItem['DETAIL_TEXT'])): ?>
              <div class="comment__content">
                <div class="comment__name"><?= Loc::getMessage("REVIEWS_ANSWER"); ?></div>
                <div class="comment__text"><?= $arItem['DETAIL_TEXT'] ?></div>
              </div>
            <? endif ?>
          </div>
        <? endforeach ?>
      </div>
    <? endforeach ?>
    <? if (!$isAjax): ?>
  </div>
  <?
  $jsParams = array(
    'ORDER_ID' => 'ORDER_BY',
    'REVIEW_OF_ID' => 'REVIEW_OF',
    'COMMENT_ID' => 'comments_'.$arParams['AJAX_ID'],
    'AJAX_ID' => $arParams['AJAX_ID'],
    'URL' => $APPLICATION->GetCurDir()
  );
  ?>
  <script>
    var oCJSReviewsList = null;

    BX.ready(function () {
      oCJSReviewsList = new CJSReviewsList(<?= CUtil::PhpToJSObject($jsParams)?>);
    });
  </script>
<? endif ?>
