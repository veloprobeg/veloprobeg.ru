<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="no_page">Страница которую вы ищете, не найдена или не существует</div>


<div class="error_box">
	<div class="error">404</div>
	
	<div class="give_try">попробуйте начать <br>с <a href="/">главной</a>, или...</div>
				
	<div class="slider_error ">
	
		<?foreach($arResult["ITEMS"] as $arItem):?>
	
			<div class="slide">
				<div class="thumb">
					<a href="<?=$arItem["PROPERTIES"]["link"]["VALUE"]?>">
						<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
					</a>
				</div>

				<div class="name">
					<a href="<?=$arItem["PROPERTIES"]["link"]["VALUE"]?>"><?=$arItem["~NAME"]?></a>
				</div>
			</div>

		<?endforeach;?>
		
	</div>

	<div class="info">* все мемы взяты в интернете. мы не претендуем на авторство и не затираем копирайты</div>
</div>



