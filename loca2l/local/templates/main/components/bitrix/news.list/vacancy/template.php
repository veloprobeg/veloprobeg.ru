<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

?>
<? if (!empty($arResult['ITEMS'])): ?>
    <div class="vacancies">
        <div class="i-vac">
            <?= $arResult['DESCRIPTION'] ?>
        </div>
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="vacancy" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="vacancy__inner">
                    <div class="vacancy__header">
                        <div class="vacancy__name"><?= $arItem['NAME'] ?></div>
                        <a href="#" data-modal="#vac" class="button button--orange"
                           data-id="<?= $arItem['ID'] ?>"><?= Loc::getMessage("CT_BNL_APPLY_JOB"); ?></a>
                    </div>
                    <div class="vacancy__body">
                        <div class="vacancy__lists">
                            <? foreach ($arItem['PROPERTIES'] as $prop): ?>
                                <div class="vacancy__about">
                                    <div class="vacancy__about-name"><?= $prop['NAME'] ?></div>
                                    <ul class="vacancy__list">
                                        <? foreach ($prop['VALUE'] as $val): ?>
                                            <li class="vacancy__list-item"><?= $val ?></li>
                                        <? endforeach ?>
                                    </ul>
                                </div>
                            <? endforeach ?>
                        </div>
                        <div class="vacancy__desc"><?= $arItem['PREVIEW_TEXT'] ?></div>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>
<? endif ?>