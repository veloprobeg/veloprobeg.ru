<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

$arResult['TYPES'] = array(
  0 => 'big',
  1 => 'white',
  3 => 'white',
  5 => 'big',
  10 => 'big'
);

foreach ($arResult['ITEMS'] as &$arItem) {
  // Format date
  $date = $arItem['DATE_ACTIVE_FROM'];
  if (empty($date)) {
    $date = $arItem['TIMESTAMP_X'];
  }

  $arItem['DATE'] = date(\Bitrix\Main\Type\Date::getFormat(), strtotime($date));

  // Resize images
  $srcPic = false;

  if (is_array($arItem['PREVIEW_PICTURE'])) {
    $srcPic = $arItem['PREVIEW_PICTURE'];
  } elseif (is_array($arItem['DETAIL_PICTURE'])) {
    $srcPic = $arItem['DETAIL_PICTURE'];
  }

  if ($srcPic) {
    $resPic = CFile::ResizeImageGet($srcPic, array(
      'width' => 740,
      'height' => 475
    ), BX_RESIZE_IMAGE_EXACT, true);

    if ($resPic) {
      $arItem['PREVIEW_PICTURE']['SRC'] = $resPic['src'];
      $arItem['PREVIEW_PICTURE']['WIDTH'] = $resPic['width'];
      $arItem['PREVIEW_PICTURE']['HEIGHT'] = $resPic['height'];
    }
  }
}