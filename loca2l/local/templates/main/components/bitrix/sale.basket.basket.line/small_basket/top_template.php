<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}

?>
  <a href="/personal/cart/" class="header__basket-link">
    <span class="header__basket-value"><?= $arResult['TOTAL_PRICE']; ?></span>
  </a>
