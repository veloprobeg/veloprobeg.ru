<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

global $USER;



foreach ($arResult['GRID']['ROWS'] as &$item) {
  
	$item['sum'] = number_format(floatval(preg_replace('/\s+/', '', $item['SUM'])), $arParams['COUNT_DECIMALS'], '.', ' ');
	$item['fullPrice'] = number_format(floatval(preg_replace('/\s+/', '', $item['FULL_PRICE'])), $arParams['COUNT_DECIMALS'], '.', ' ');
	$item['price'] = number_format(floatval(preg_replace('/\s+/', '', $item['PRICE'])), $arParams['COUNT_DECIMALS'], '.', ' ');

	if ( !$item['PREVIEW_PICTURE_SRC'] ){
	
		$arFilter = Array(
			"ID" => $item['PRODUCT_ID']
		);
		$res = CIBlockElement::GetList(Array(), $arFilter, Array("PROPERTY_CML2_LINK"));
		if($ar_fields = $res->GetNext()){
			$elementID = $ar_fields["PROPERTY_CML2_LINK_VALUE"];
		}
		
		$arFilter = Array(
			"PROPERTY_CML2_LINK" => $elementID,
			"!PREVIEW_PICTURE" => false,
			"PROPERTY_TSVET_BAZOVYY_VALUE" => $item['PROPS_ALL']['TSVET_BAZOVYY']['VALUE']
		);
		
	
		$res = CIBlockElement::GetList(Array(), $arFilter, Array("PREVIEW_PICTURE"));
		if($ar_fields = $res->GetNext()){
			$item['PREVIEW_PICTURE_SRC'] = CFile::GetPath($ar_fields["PREVIEW_PICTURE"]);
		}

	}

}

if (\Bitrix\Main\Loader::includeModule('sale')) {
  // Delivery list
  $arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();

  // Get user account
  if (\Bitrix\Main\Loader::includeModule('currency')) {
    $arResult['BASE_CURRENCY'] = \CCurrency::GetList($by = '', $order = '', array(
      'BASE' => 'Y'
    ))->Fetch();

    $arResult['USER_ACCOUNT'] = \CSaleUserAccount::GetByUserID($USER->GetID(), $arResult['BASE_CURRENCY']['CURRENCY']);

    if ($arResult['USER_ACCOUNT']) {
      $arResult['USER_ACCOUNT']['CURRENT_BUDGET'] = number_format($arResult['USER_ACCOUNT']['CURRENT_BUDGET'], 0, '.', ' ');
    }
  }
}

if (isset($_SESSION['BASKET_BONUSES'][$USER->GetID()]['USED_BONUSES'])) {
  $arResult['USED_BONUSES'] = floatval($_SESSION['BASKET_BONUSES'][$USER->GetID()]['USED_BONUSES']);
  $arResult['usedBonuses'] = CCurrencyLang::CurrencyFormat($arResult['USED_BONUSES'], 'RUB');
}

$arResult['DELIVERY_ID'] = intval($_SESSION['BASKET_DELIVERY'][$USER->GetID()]['DELIVERY_ID']);

if (intval($arResult['DELIVERY_ID']) > 0 && isset($arResult['DELIVERY'][$arResult['DELIVERY_ID']])) {
  $arResult['allSum'] += $arResult['DELIVERY'][$arResult['DELIVERY_ID']]['CONFIG']['MAIN']['PRICE'];
}

// Format Total price
$arResult['allPriceWithoutDiscount'] = CCurrencyLang::CurrencyFormat(($arResult['DISCOUNT_PRICE_ALL'] + $arResult['allSum']), 'RUB');

// Format price
$arResult['allSum'] -= $arResult['USED_BONUSES'];
if ($arResult['allSum'] >= 0) {
  $arResult['allPrice'] = number_format($arResult['allSum'], $arParams['COUNT_DECIMALS'], '.', ' ');
}

// Format discount price
if (is_array($arResult['COUPON_LIST'])) {
  foreach ($arResult['COUPON_LIST'] as $k => $coupon) {
    if ($coupon['JS_STATUS'] == 'BAD') {
      unset($arResult['COUPON_LIST'][$k]);
    }
  }
}

$restrictions = \Bitrix\Sale\Delivery\Restrictions\Manager::getList(array(
  'filter' => array(
    'SERVICE_TYPE' => \Bitrix\Sale\Delivery\Restrictions\Manager::SERVICE_TYPE_SHIPMENT,
  )
))->fetchAll();

foreach ($arResult['DELIVERY'] as $deliveryId => $delivery) {
  foreach ($restrictions as $restriction) {
    if ($deliveryId == $restriction['SERVICE_ID']) {
      $restriction['PARAMS'] = is_array($restriction['PARAMS']) ? $restriction['PARAMS'] : array();

      if ($restriction['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Restrictions\ByPublicMode') {
        $checked = $restriction['CLASS_NAME']::check('', $restriction['PARAMS'], $deliveryId);
        if (!$checked) {
          unset($arResult['DELIVERY'][$deliveryId]);
        }
      }
    }
  }
}

foreach ($arResult['GRID']['HEADERS'] as $key => $header) {
    if ($header['id'] == 'SUM') {
        $sumIndex = $key;
    }
    elseif ($header['id'] == 'QUANTITY') {
        $quantityIndex = $key;
    }

    if (!empty($sumIndex) && !empty($quantityIndex)) {
        array_swap($arResult['GRID']['HEADERS'], $sumIndex, $quantityIndex);
    }
}
