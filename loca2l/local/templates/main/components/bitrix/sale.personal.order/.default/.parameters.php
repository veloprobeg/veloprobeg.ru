<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

if (CModule::IncludeModule('sale')) {
  $dbStat = CSaleStatus::GetList(array('sort' => 'asc'), array('LID' => LANGUAGE_ID), false, false, array(
    'ID',
    'NAME'
  ));
  $statList = array();
  while ($item = $dbStat->Fetch()) {
    $statList[$item['ID']] = $item['NAME'];
  }

  $statList['PSEUDO_CANCELLED'] = 1;

  $availColors = array(
    'green' => GetMessage("SPO_STATUS_COLOR_GREEN"),
    'yellow' => GetMessage("SPO_STATUS_COLOR_YELLOW"),
    'red' => GetMessage("SPO_STATUS_COLOR_RED"),
    'gray' => GetMessage("SPO_STATUS_COLOR_GRAY"),
  );

  $colorDefaults = array(
    'N' => 'green',
    // new
    'P' => 'yellow',
    // payed
    'F' => 'gray',
    // finished
    'PSEUDO_CANCELLED' => 'red'
    // cancelled
  );

  foreach ($statList as $id => $name) {
    $arTemplateParameters["STATUS_COLOR_".$id] = array(
      "NAME" => $id == 'PSEUDO_CANCELLED' ? GetMessage("SPO_PSEUDO_CANCELLED_COLOR") : GetMessage("SPO_STATUS_COLOR").' "'.$name.'"',
      "TYPE" => "LIST",
      "MULTIPLE" => "N",
      "VALUES" => $availColors,
      "DEFAULT" => empty($colorDefaults[$id]) ? 'gray' : $colorDefaults[$id],
    );
  }
}

// Вставка шаблона пути
if (CModule::IncludeModule('iblock')) {
  $arTemplateParameters['TRACK_NUMBER_URL'] = array(
    "NAME" => GetMessage('TRACK_NUMBER_URL'),
    "TYPE" => "CUSTOM",
    "DEFAULT" => "https://track24.ru/?code=#TRACK_NUMBER#",
    "JS_FILE" => BX_ROOT."/js/iblock/path_templates.js",
    "JS_EVENT" => "IBlockComponentProperties",
    "JS_DATA" => str_replace("\n", "", CUtil::PhpToJSObject(array(
      "mnu_TRACK_NUMBER_URL",
      5000,
      array(
        array(
          "TEXT" => GetMessage("IB_COMPLIB_POPUP_TRACK_NUMBER"),
          "TITLE" => "#TRACK_NUMBER# - ".GetMessage("IB_COMPLIB_POPUP_TRACK_NUMBER"),
          "ONCLICK" => "window.IBlockComponentPropertiesObj.Action('#TRACK_NUMBER#', 'mnu_TRACK_NUMBER_URL', '')",
        ),
      )
    ))),
  );
}
?>