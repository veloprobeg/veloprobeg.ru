(function (window) {
    if (!!window.CJSOrderAjax) {
        return;
    }

    window.CJSUserProfiles = function (params) {
        this.obForm = null;
        this.obButton = null;
        this.ajaxUrl = params.AJAX_URL || null;
        this.obProfileChange = null;
        this.obProfileWrapper = null;
        this.mainErrorsNode = null;
        this.hasErrorSection = null;

        this.defaultDeliveryTimeFrom = params.DEFAULT_DELIVERY_TIME_FROM || '';
        this.defaultDeliveryTimeTo = params.DEFAULT_DELIVERY_TIME_TO || '';

        this.profileName = params.PROFILE_NAME || null;
        this.citySelectorId = params.JS_CONTROL_GLOBAL_ID || null;

        this.userProfiles = params.USER_PROFILES || [];
        this.refreshUserProfileFields = params.REFRESH_USER_PROFILE_FIELDS || [];

        this.result = {};

        if (!!params.FORM_ID) {
            this.obForm = BX(params.FORM_ID);
        }

        if (!!params.BUTTON_ID) {
            this.obButton = BX(params.BUTTON_ID);
        }

        if (!!params.PROFILE_CHANGE_ID) {
            this.obProfileChange = BX(params.PROFILE_CHANGE_ID);
        }

        if (!!params.PROFILE_WRAPPER_ID) {
            this.obProfileWrapper = BX(params.PROFILE_WRAPPER_ID);
        }

        if (!!params.RESULT) {
            this.result = params.RESULT;
        }

        if (!!params.PAY_SYSTEM_WRAPPER_ID) {
            this.obPaySystemWrapper = BX(params.PAY_SYSTEM_WRAPPER_ID);
        }

        if (!!params.DELiVERY_WRAPPER_ID) {
            this.obDeliveryWrapper = BX(params.DELiVERY_WRAPPER_ID);
        }

        if (!!params.MAIN_ERRORS_BLOCK) {
            this.mainErrorsNode = BX(params.MAIN_ERRORS_BLOCK);
        }

        if (!!params.PUBLIC_OFFER) {
            this.publicOffer = BX(params.PUBLIC_OFFER);
        }


        this.params = params;

        this.init();
    };

    window.CJSUserProfiles.prototype = {
        init: function () {
            if (!!this.obButton) {
                BX.bind(this.obButton, 'click', BX.proxy(function (e) {
                    this.sendRequest('saveProfileAjax');
                    e.preventDefault();
                }, this));
            }

            if (!!this.obDelivery) {
                $(this.obDelivery).on('select2:select', BX.proxy(this.changeDelivery, this));
            }

            var profiles = BX.findChildren(this.obForm, {
                attr: {
                    name: this.profileName,
                    type: 'radio'
                }
            }, true);

            if (!!profiles) {
                for (var i in profiles) {
                    if (profiles.hasOwnProperty(i)) {
                        if (profiles[i].CODE != 'LOCATION') {
                            BX.bind(profiles[i], 'change', BX.proxy(this.changeProfile, this));
                        }
                    }
                }
            }

            this.sendRequest();
        },

        send: function () {
            BX.ajax.submit(this.obForm, BX.proxy(this.sendHandler, this));
        },

        showError: function (node, msg, border) {
            if (BX.type.isArray(msg)) {
                msg = msg.join('<br />');
            }

            var errorContainer = node.querySelector('.alert.alert-danger'), animate;
            if (errorContainer && msg.length) {
                BX.cleanNode(errorContainer);
                errorContainer.appendChild(BX.create('DIV', {html: msg}));

                errorContainer.style.opacity = 0;
                errorContainer.style.display = '';
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
                    step: function (state) {
                        errorContainer.style.opacity = state.opacity / 100;
                    },
                    complete: function () {
                        errorContainer.removeAttribute('style');
                    }
                }).animate();

                if (!!border) {
                    BX.addClass(node, 'bx-step-error');
                }
            }
        },

        showErrors: function (errors, scroll) {
            var errorNodes = this.obForm.querySelectorAll('div.alert.alert-danger'),
                section, k, blockErrors;

            for (k = 0; k < errorNodes.length; k++) {
                section = BX.findParent(errorNodes[k], {className: 'bx-soa-section'});
                BX.removeClass(section, 'bx-step-error');
                errorNodes[k].style.display = 'none';
                BX.cleanNode(errorNodes[k]);
            }

            if (!errors || BX.util.object_keys(errors).length < 1) {
                return;
            }

            for (k in errors) {
                if (!errors.hasOwnProperty(k)) {
                    continue;
                }

                blockErrors = errors[k];
                switch (k.toUpperCase()) {
                    case 'MAIN':
                        this.showError(this.mainErrorsNode, blockErrors);
                        //this.animateScrollTo(this.mainErrorsNode, 800, 20);
                        scroll = false;
                        break;
                }
            }

            //!!scroll && this.scrollToError();
        },

        /**
         * Showing loader image with overlay.
         */
        startLoader: function () {
            if (this.BXFormPosting === true) {
                return false;
            }

            this.BXFormPosting = true;

            if (!this.loadingScreen) {
                this.loadingScreen = new BX.PopupWindow("loading_screen", null, {
                    overlay: {
                        backgroundColor: 'white',
                        opacity: '80'
                    },
                    events: {
                        onAfterPopupShow: BX.proxy(function () {
                            BX.cleanNode(this.loadingScreen.popupContainer);
                            BX.removeClass(this.loadingScreen.popupContainer, 'popup-window');
                            this.loadingScreen.popupContainer.appendChild(
                                BX.create('IMG', {props: {src: this.templateFolder + "/images/loader.gif"}})
                            );
                            this.loadingScreen.popupContainer.removeAttribute('style');
                            this.loadingScreen.popupContainer.style.display = 'block';
                        }, this)
                    }
                });
                BX.addClass(this.loadingScreen.popupContainer, 'bx-step-opacity');
            }

            return setTimeout(BX.proxy(function () {
                this.loadingScreen.show()
            }, this), 100);
        },

        /**
         * Hiding loader image with overlay.
         */
        endLoader: function (loaderTimer) {
            this.BXFormPosting = false;
            if (this.loadingScreen && this.loadingScreen.isShown()) {
                this.loadingScreen.close();
            }

            clearTimeout(loaderTimer);
        },

        changeProfile: function (e) {
            var i = e.target.value,
                deliveryTimeFrom = this.defaultDeliveryTimeFrom,
                deliveryTimeTo = this.defaultDeliveryTimeTo,
                userProfile = false,
                userProfileValues = {},
                location = '',
                profiles = this.userProfiles;

            if (this.userProfiles.hasOwnProperty(i)) {
                userProfile = this.userProfiles[i];
            }

            if (userProfile && userProfile.hasOwnProperty('VALUES')) {
                userProfileValues = userProfile['VALUES'];
            }

            if (!!profiles) {
                for (var i in profiles) {
                    if (profiles[i].ID != userProfile.ID) {
                        profiles[i].CHECKED = 'N';
                    }
                    else {
                        profiles[i].CHECKED = 'Y';
                    }
                }
            }

            for (var j in this.refreshUserProfileFields) {
                if (this.refreshUserProfileFields.hasOwnProperty(j)) {
                    var field,
                        code = this.refreshUserProfileFields[j],
                        newValue = '';

                    switch (code) {
                        case 'DELIVERY_TIME_FROM':
                            field = BX('startTime');
                            break;
                        case 'DELIVERY_TIME_TO':
                            field = BX('endTime');
                            break;
                        default:
                            field = BX(code);
                    }

                    if (code == 'LOCATION') {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (userProfileValues[code].hasOwnProperty('CITY') && userProfileValues[code].CITY.hasOwnProperty('CODE')) {
                                location = userProfileValues[code].CITY.CODE;
                            }

                            if (userProfileValues[code].hasOwnProperty('PROP_ID')) {
                                this.locationId = userProfileValues[code].PROP_ID;
                            }
                        }
                    }

                    if (!!field) {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (code == 'DELIVERY_TIME_FROM') {
                                deliveryTimeFrom = userProfileValues[code].VALUE;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                deliveryTimeTo = userProfileValues[code].VALUE;
                            }

                            newValue = userProfileValues[code].VALUE;
                        } else {
                            if (code == 'DELIVERY_TIME_FROM') {
                                newValue = this.defaultDeliveryTimeFrom;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                newValue = this.defaultDeliveryTimeTo;
                            }
                        }

                        field.value = newValue;
                    }
                }
            }

            window.BX.locationSelectors[this.citySelectorId].setValue(location);

            this.refreshDeliveryTimer(this.getDeliveryTimeForSlider(deliveryTimeFrom), this.getDeliveryTimeForSlider(deliveryTimeTo));

            this.sendRequest();
        },

        saveProfile: function () {
            var userProfile = false,
                userProfileValues = {},
                location = '';

            if (this.userProfiles.hasOwnProperty(i)) {
                userProfile = this.userProfiles[i];
            }

            if (userProfile && userProfile.hasOwnProperty('VALUES')) {
                userProfileValues = userProfile['VALUES'];
            }

            for (var j in this.refreshUserProfileFields) {
                if (this.refreshUserProfileFields.hasOwnProperty(j)) {
                    var field,
                        code = this.refreshUserProfileFields[j],
                        newValue = '';

                    switch (code) {
                        case 'DELIVERY_TIME_FROM':
                            field = BX('startTime');
                            break;
                        case 'DELIVERY_TIME_TO':
                            field = BX('endTime');
                            break;
                        default:
                            field = BX(code);
                    }

                    if (code == 'LOCATION') {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (userProfileValues[code].hasOwnProperty('CITY') && userProfileValues[code].CITY.hasOwnProperty('CODE')) {
                                location = userProfileValues[code].CITY.CODE;
                            }

                            if (userProfileValues[code].hasOwnProperty('PROP_ID')) {
                                this.locationId = userProfileValues[code].PROP_ID;
                            }
                        }
                    }

                    if (!!field) {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (code == 'DELIVERY_TIME_FROM') {
                                deliveryTimeFrom = userProfileValues[code].VALUE;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                deliveryTimeTo = userProfileValues[code].VALUE;
                            }

                            newValue = userProfileValues[code].VALUE;
                        } else {
                            if (code == 'DELIVERY_TIME_FROM') {
                                newValue = this.defaultDeliveryTimeFrom;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                newValue = this.defaultDeliveryTimeTo;
                            }
                        }

                        field.value = newValue;
                    }
                }
            }

            window.BX.locationSelectors[this.citySelectorId].setValue(location);

            this.refreshDeliveryTimer(this.getDeliveryTimeForSlider(deliveryTimeFrom), this.getDeliveryTimeForSlider(deliveryTimeTo));

            this.sendRequest();
        },

        getDeliveryTimeForSlider: function (time) {
            if (isNaN(Number(time))) {
                var timeSourceJS = time.toString().split(':'),
                    addTimeJS = 0;

                if (timeSourceJS[1] == 30) {
                    addTimeJS++;
                }

                return timeSourceJS[0] * 2 + addTimeJS;
            } else {
                return time;
            }
        },

        getParams: function() {
            var params = [],
                profiles = this.userProfiles,
                obSession = BX('sessid');

            for (var j in this.refreshUserProfileFields) {
                if (this.refreshUserProfileFields.hasOwnProperty(j)) {
                    var code = this.refreshUserProfileFields[j],
                        newCode,
                        field = BX(code);

                    if (code == 'DELIVERY_TIME_FROM') {
                        newCode = 'startTime';
                    }
                    else if (code == 'DELIVERY_TIME_TO') {
                        newCode = 'endTime';
                    }

                    if (newCode) {
                        field = BX(newCode);
                        params[code] = field.value;
                    }
                    else {
                        field = BX(code);
                        params[code] = field.value;
                    }
                }
            }

            if (!!profiles) {
                for (var i in profiles) {
                    if (profiles[i].CHECKED === 'Y') {
                        params['profileID'] = profiles[i].ID;
                    }
                }
            }

            if (!!obSession) {
                params['sessid'] = obSession.value;
            }

            return params;
        },

        /**
         * Send ajax request with order data and executes callback by action
         */
        sendRequest: function (action, actionData) {
            var loaderTimer, form;
            if (!(loaderTimer = this.startLoader())) {
                return;
            }

            this.firstLoad = false;
            action = BX.type.isString(action) ? action : 'refreshProfilesAjax';

            if (action == 'saveProfileAjax') {
                form = BX('bx-soa-profiles-form');
                if (form) {
                    form.querySelector('input[type=hidden][name=sessid]').value = BX.bitrix_sessid();
                }

                BX.ajax({
                    timeout: 60,
                    method: 'POST',
                    dataType: 'json',
                    url: this.ajaxUrl,
                    data: this.getParams(),
                    onsuccess: BX.proxy(function (result) {
                        if (result.success) {
                            location.reload();
                        }
                        this.endLoader(loaderTimer);
                    }, this),
                    onfailure: BX.proxy(function () {
                        this.endLoader(loaderTimer);
                    }, this)
                });

                this.endLoader(loaderTimer);
            } else {
                this.endLoader(loaderTimer);
            }
        },

        getDeliveryTimeForSlider: function (time) {
            if (isNaN(Number(time))) {
                var timeSourceJS = time.toString().split(':'),
                    addTimeJS = 0;

                if (timeSourceJS[1] == 30) {
                    addTimeJS++;
                }

                return timeSourceJS[0] * 2 + addTimeJS;
            } else {
                return time;
            }
        },

        refreshDeliveryTimer: function (startTime, endTime) {
            $('#reg__timer').slider("values", [
                startTime,
                endTime
            ]);
        },

        refreshProfiles: function (result) {
            if (result.error) {
                this.showError(this.mainErrorsNode, result.error);
            } else if (result.order.SHOW_AUTH) {
                // TODO Show auth form
            } else {
                this.result = result.order;
            }
        },

        getData: function (action, actionData) {
            var data = {
                sessid: BX.bitrix_sessid(),
                via_ajax: 'Y',
                SITE_ID: this.siteId,
                action: action,
                signedParamsString: this.signedParamsString
            };

            return data;
        },

        getAllFormData: function () {
            var prepared = BX.ajax.prepareForm(this.obForm),
                i;

            for (i in prepared.data) {
                if (prepared.data.hasOwnProperty(i) && i == '') {
                    delete prepared.data[i];
                }
            }

            return !!prepared && prepared.data ? prepared.data : {};
        }
    }
})(window);