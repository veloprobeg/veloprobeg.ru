<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
use \Bitrix\Main\Localization\Loc;
?>

<? if (!empty($arResult)): ?>
  <div class="styles__looked" id="styles__looked">
    <div class="styles__looked-head"><?= Loc::getMessage("VIEW_HEADER"); ?></div>
    <div class="styles__looked-content">
      <? foreach ($arResult as $arItem): ?>
        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="styles__looked-item">
          <? if ($arParams["VIEWED_IMAGE"] == "Y" && is_array($arItem["PICTURE"])): ?>
            <div style="background-image: url(<?= $arItem['PICTURE']['src'] ?>)" class="styles__looked-image"></div>
          <? endif ?>
          <? if ($arParams["VIEWED_NAME"] == "Y"): ?>
            <div class="styles__looked-name"><?= $arItem['NAME'] ?></div>
          <? endif ?>
          <? if ($arParams["VIEWED_PRICE"] == "Y" && $arItem["CAN_BUY"] == "Y"): ?>
            <div class="styles__looked-price"><?= $arItem['PRICE_FORMATED'] ?></div>
          <? endif ?>
        </a>
      <? endforeach; ?>
    </div>
  </div>
<? endif; ?>
<script>
  // Hack copy block to #styles__promos
  // Через SetViewTarget не прокатывает нормально
  BX.ready(function() {
    var container = BX('styles__promos'),
      looked = BX('styles__looked');

    container.appendChild(looked);
  });
</script>
