<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<form class="header__search" action="<?= $arResult["FORM_ACTION"] ?>">
    <button type="submit" class="header__search-icon is-hidden"><img
            src="<?= $templateFolder ?>/images/search-hover.svg" alt=""></button>
    <div class="header__search-area">
        <? if ($arParams["USE_SUGGEST"] === "Y"): ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:search.suggest.input",
                "search",
                array(
                    "NAME" => "q",
                    "VALUE" => "",
                    "INPUT_SIZE" => 15,
                    "DROPDOWN_SIZE" => 10,
                ),
                $component, array("HIDE_ICONS" => "Y")
            ); ?>
        <? else: ?>
            <input class="header__search-input" type="text" name="q" value="" size="15"
                   maxlength="50"/>
        <? endif; ?>
    </div>
</form>