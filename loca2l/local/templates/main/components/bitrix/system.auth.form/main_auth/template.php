<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
use \Bitrix\Main\Localization\Loc;

$isError = $arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'];
?>

<? if ($arResult["FORM_TYPE"] == "login"): ?>
  <div class="header__auth">
    <a href="javascript:void(0)"
       class="header__auth-link header__auth-sign<?= $isError ? ' is-active' : '' ?>"><?= Loc::getMessage("AUTH_LOGIN_BUTTON"); ?></a>
    <a href="<?= $arParams['REGISTER_URL'] ?>" class="header__auth-link"><?= Loc::getMessage("AUTH_REGISTER"); ?></a>
    <form class="header__auth-login<?= $isError ? ' is-active' : '' ?>" action="<?= $arResult["AUTH_URL"] ?>" method="post" target="_top">
      <? if ($arResult["BACKURL"] <> ''): ?>
        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
      <? endif ?>
      <?
      if ($isError) {
        ShowMessage($arResult['ERROR_MESSAGE']);
      }
      ?>
      <? foreach ($arResult["POST"] as $key => $value): ?>
        <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
      <? endforeach ?>
      <input type="hidden" name="AUTH_FORM" value="Y"/>
      <input type="hidden" name="TYPE" value="AUTH"/>
      <? if ($arResult["AUTH_SERVICES"]): ?>
        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
          array(
            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            "AUTH_URL" => $arResult["AUTH_URL"],
            "POST" => $arResult["POST"],
            "POPUP" => "Y",
            "SUFFIX" => "form",
          ),
          $component,
          array("HIDE_ICONS" => "Y")
        );
        ?>
      <? endif ?>
      <div class="header__auth-area">
        <div class="header__auth-input-area">
          <div class="header__auth-input-name"><?= Loc::getMessage("AUTH_LOGIN") ?></div>
          <input type="email" name="USER_LOGIN" placeholder="example@example.ru"
                 class="default-input header__auth-input">
        </div>
        <div class="header__auth-input-area">
          <a href="javascript:void(0)" class="header__auth-showpw showpw"></a>
          <div class="header__auth-input-name"><?= Loc::getMessage("AUTH_PASSWORD") ?></div>
          <input type="password" name="USER_PASSWORD" class="default-input header__auth-input">
        </div>
      </div>
      <button type="submit"
              class="button button--orange header__auth-button"><?= Loc::getMessage("auth_form_comp_auth"); ?></button>
    </form>
  </div>
  <?
elseif ($arResult["FORM_TYPE"] == "otp"):
  ?>
  <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
        action="<?= $arResult["AUTH_URL"] ?>">
    <? if ($arResult["BACKURL"] <> ''): ?>
      <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
    <? endif ?>
    <input type="hidden" name="AUTH_FORM" value="Y"/>
    <input type="hidden" name="TYPE" value="OTP"/>
    <table width="95%">
      <tr>
        <td colspan="2">
          <? echo Loc::getMessage("auth_form_comp_otp") ?><br/>
          <input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off"/>
        </td>
      </tr>
      <? if ($arResult["CAPTCHA_CODE"]): ?>
        <tr>
          <td colspan="2">
            <? echo Loc::getMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
            <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>
            <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>"
                 width="180"
                 height="40" alt="CAPTCHA"/><br/><br/>
            <input type="text" name="captcha_word" maxlength="50" value=""/></td>
        </tr>
      <? endif ?>
      <? if ($arResult["REMEMBER_OTP"] == "Y"): ?>
        <tr>
          <td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER"
                                  value="Y"/></td>
          <td width="100%"><label for="OTP_REMEMBER_frm"
                                  title="<? echo Loc::getMessage("auth_form_comp_otp_remember_title") ?>"><? echo Loc::getMessage("auth_form_comp_otp_remember") ?></label>
          </td>
        </tr>
      <? endif ?>
      <tr>
        <td colspan="2"><input type="submit" name="Login"
                               value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?>"/></td>
      </tr>
      <tr>
        <td colspan="2">
          <noindex>
            <a href="<?= $arResult["AUTH_LOGIN_URL"] ?>"
               rel="nofollow"><? echo Loc::getMessage("auth_form_comp_auth") ?></a>
          </noindex>
          w
          <br/></td>
      </tr>
    </table>
  </form>

  <?
else:
  ?>
  <form action="<?= $arResult["AUTH_URL"] ?>">
    <div class="header__auth header__auth--logged">
      <a href="<?= $arParams['PROFILE_URL'] ?>"
         class="header__auth-name"><?= $arResult['USER_NAME'] ?></a>
      <a href="javascript:void(0)" class="header__logout"
         onclick="$(this).parents('form').find('[type=submit]').click()"><?= Loc::getMessage("AUTH_LOGOUT_BUTTON") ?></a>
    </div>
    <? foreach ($arResult["GET"] as $key => $value): ?>
      <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
    <? endforeach ?>
    <input type="hidden" name="logout" value="yes"/>
    <input type="submit" name="logout_butt" style="display:none"
           value="<?= Loc::getMessage("AUTH_LOGOUT_BUTTON") ?>"/>
  </form>
<? endif ?>
