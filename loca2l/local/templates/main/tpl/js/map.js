function mapStart() {

    mapInitialize = function () {
        var myMapPlace = document.getElementById("contacts__map"),
            myLatlng = new google.maps.LatLng(55.843652426528, 37.56996989250206),
            myOptions = {
                scrollwheel: true,
                disableDefaultUI: true,
                zoom: 11,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            map = new google.maps.Map(myMapPlace, myOptions);

        // var styles = [
        //     {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
        //     {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
        //     {"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#ff0000"}]},
        //     {"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"lightness":"100"}]},
        //     {"featureType":"landscape.man_made","elementType":"labels","stylers":[{"visibility":"off"}]},
        //     {"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"lightness":"100"}]},
        //     {"featureType":"landscape.natural","elementType":"labels","stylers":[{"visibility":"off"}]},
        //     {"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},
        //     {"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"lightness":"100"}]},
        //     {"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"visibility":"off"},{"lightness":"23"}]},
        //     {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
        //     {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
        //     {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
        //     {"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd900"}]},
        //     {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
        //     {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
        //     {"featureType":"water","elementType":"all","stylers":[{"color":"#ffd900"},{"visibility":"on"}]},
        //     {"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#cccccc"}]},
        //     {"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}];
        // map.setOptions({styles: styles});

        // mapMarker SEE /local/templates/main/components/bitrix/news.list/contacts/template.php
        var companyLogo = new google.maps.MarkerImage(mapMarker,
            new google.maps.Size(23, 31),
            new google.maps.Point(0, 0),
            new google.maps.Point(9, 22)
        );

        // Массив данных офисов
        // shops SEE /local/templates/main/components/bitrix/news.list/contacts/template.php
        var coords = [],
            markers = [],
            infoboxes = [];

        for (var i = 0; i < shops.length; i++) {
            coords[i] = new google.maps.LatLng(eval(shops[i].LAT), eval(shops[i].LNG));
            markers[i] = new google.maps.Marker({
                position: coords[i],
                map: map,
                icon: companyLogo,
                title: shops[i].NAME,
                id: shops[i].ID,
                i: i,
                optimized: false,
                zIndex: 10
            });
        }
        ;

        var winMob = document.documentElement.clientWidth < 950 ? 1 : 0,
            infoboxX = winMob === 0 ? -325 : -500,
            infoboxY = winMob === 0 ? -285 : -260;

        for (var i = 0; i < shops.length; i++) {
            infoboxes[i] = new InfoBox({
                content: document.getElementById("point_" + shops[i].ID),
                // content: $(".point_"+shops[i].ID).html(),
                pixelOffset: new google.maps.Size(-147, 10),
                zIndex: null,
                closeBoxURL: "",
                isHidden: false,
                visible: true
            });
        }

        if ($(window).width() < 480) {
            for (var i = 0; i < shops.length; i++) {
                infoboxes[i] = new InfoBox({
                    content: document.getElementById("point_" + shops[i].ID),
                    // content: $(".point_"+shops[i].ID).html(),
                    pixelOffset: new google.maps.Size(-100, 10),
                    zIndex: null,
                    closeBoxURL: "",
                    isHidden: false,
                    visible: true
                });
            }
        }

        function closeInfobox() {
            for (var i = 0; i < infoboxes.length; i++) {
                infoboxes[i].close();
            }
            ;
        }

        for (var i = 0; i < markers.length; i++) {
            markers[i].addListener('click', function () {
                map.setZoom(15);
                map.panTo(this.getPosition());
                closeInfobox();
                infoboxes[this.i].open(map, this);
                // console.log('open'+this.i);
            });
        }

        $(".infobox-btn-close").click(function () {
            closeInfobox();

            return false;
        });

        // Кастомные кнопки зума
        var controlWrapper = document.createElement('div'),
            zoomInButton = document.createElement('div'),
            zoomOutButton = document.createElement('div');

        controlWrapper.appendChild(zoomInButton);
        controlWrapper.appendChild(zoomOutButton);

        controlWrapper.index = 1;

        zoomInButton.className = "map-zoom-more";
        zoomOutButton.className = "map-zoom-less";

        // Setup the click event listener - zoomIn
        google.maps.event.addDomListener(zoomInButton, 'click', function () {
            map.setZoom(map.getZoom() + 1);
        });
        // Setup the click event listener - zoomOut
        google.maps.event.addDomListener(zoomOutButton, 'click', function () {
            map.setZoom(map.getZoom() - 1);
        });
        // Применение на карту
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(controlWrapper);

        changeCity = function changeCity(lat, lng, zoom) {
            closeInfobox();
            $('.js-mobmenu-toggle').click();
            map.setCenter({lat: lat, lng: lng});
            // map.panTo(new GLatLng(lat,lng));
            map.setZoom(zoom);
            return false;
        }

    };

    if ($("#contacts__map").length > 0) {
        mapInitialize();
    }
    ;
};

// $(function(){
//     /*--- Подключим maps api -----------------------------------------------*/
//     if ($("#map-wrap").length > 0){
//         $.getScript(
//             // еще ключ определен в GOOGLE_API_KEY
//             function(){
//                 // console.log("Кaрта загружена");
//                 $.getScript(
//                     "assets/js/lib/infobox.js",

//                     function(){
//                         // console.log("Infobox загружен");
//                         mapStart();
//                     }
//                 );
//             }
//         );
//     };
// }); 