<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
global $USER_FIELD_MANAGER;

if (!\Bitrix\Main\Loader::includeModule("iblock")) {
    return;
}

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array(
    "TYPE" => $arCurrentValues["IBLOCK_TYPE"],
    "ACTIVE" => "Y"
));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}

$arProperty_UF = array();
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_" . $arCurrentValues["IBLOCK_ID"] . "_SECTION", 0, LANGUAGE_ID);
foreach ($arUserFields as $FIELD_NAME => $arUserField) {
    $arUserField['LIST_COLUMN_LABEL'] = (string)$arUserField['LIST_COLUMN_LABEL'];
    $arProperty_UF[$FIELD_NAME] = $arUserField['LIST_COLUMN_LABEL'] ? '[' . $FIELD_NAME . ']' . $arUserField['LIST_COLUMN_LABEL'] : $FIELD_NAME;
}

$arEventType = Array();
$dbType = CEventType::GetList(array(
    'LID' => 'ru'
));
while ($arType = $dbType->GetNext()) {
    $arEventType[$arType["EVENT_NAME"]] = "[" . $arType['ID'] . "] " . $arType["EVENT_NAME"];
}

$arEvent = Array();

if ($arCurrentValues['EVENT_TYPE_ID'] > 0) {
    $arFilter = array(
        'TYPE_ID' => $arCurrentValues['EVENT_TYPE_ID']
    );

    $dbType = CEventMessage::GetList($by = "ID", $order = "DESC", $arFilter);

    while ($arType = $dbType->GetNext()) {
        $arEvent[$arType["ID"]] = "[" . $arType["ID"] . "] " . $arType["SUBJECT"];
    }
}

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CP_BCSL_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CP_BCSL_IBLOCK_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "SAVE_IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CP_BCSL_SAVE_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "SAVE_IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CP_BCSL_SAVE_IBLOCK_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "EVENT_TYPE_ID" => Array(
            "NAME" => GetMessage("CP_BCSL_EMAIL_TYPES"),
            "TYPE" => "LIST",
            "VALUES" => $arEventType,
            "DEFAULT" => "",
            "COLS" => 25,
            "PARENT" => "BASE",
            "REFRESH" => "Y",
        ),
        "EVENT_MESSAGE_ID" => Array(
            "NAME" => GetMessage("CP_BCSL_EMAIL_TEMPLATES"),
            "TYPE" => "LIST",
            "VALUES" => $arEvent,
            "DEFAULT" => "",
            "MULTIPLE" => "Y",
            "COLS" => 25,
            "PARENT" => "BASE",
        ),
        ///////////////////////////////////////////

        'SUCCESS_MESSAGE' => array(
            "NAME" => GetMessage("CP_BCSL_SUCCESS_MESSAGE"),
            "TYPE" => "STRING",
            "DEFAULT" => GetMessage("CP_BCSL_SUCCESS_MESSAGE_DEFAULT")
        )
    ),
);
?>