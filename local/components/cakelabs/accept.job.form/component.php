<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader,
    Bitrix\Main,
    Bitrix\Iblock;

/*************************************************************************
 * Processing of received parameters
 *************************************************************************/
if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 36000000;
}

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SAVE_IBLOCK_TYPE"] = trim($arParams["SAVE_IBLOCK_TYPE"]);
$arParams["SAVE_IBLOCK_ID"] = intval($arParams["SAVE_IBLOCK_ID"]);

$arParams['EVENT_MESSAGE_ID'] = is_array($arParams['EVENT_MESSAGE_ID']) ? $arParams['EVENT_MESSAGE_ID'] : array();
$arParams['EVENT_TYPE_ID'] = !empty($arParams['EVENT_TYPE_ID']) ? $arParams['EVENT_TYPE_ID'] : 'ADDED_ACCEPT_JOB';

/*************************************************************************
 * save
 *************************************************************************/

if (Loader::includeModule('iblock')) {
    if ($_REQUEST['IS_SEND'] == 'Y') {
        $arResult['IS_SEND'] = true;
        $errors = array();
        $data = array();

        if ($arParams['IBLOCK_ID'] <= 0) {
            $errors[] = 'Не указан информационный блок для сохранения';
        }

        $name = isset($_REQUEST['name']) ? $DB->ForSql($_REQUEST['name']) : '';
        $lastName = isset($_REQUEST['surname']) ? $DB->ForSql($_REQUEST['surname']) : '';
        $email = isset($_REQUEST['email']) ? $DB->ForSql($_REQUEST['email']) : '';
        $phone = isset($_REQUEST['phone']) ? $DB->ForSql($_REQUEST['phone']) : '';
        $resume = isset($_REQUEST['resume']) ? intval($_REQUEST['resume']) : 0;
        $comment = isset($_REQUEST['comment']) ? $DB->ForSql($_REQUEST['comment']) : '';
        $vacancy = isset($_REQUEST['vacancy']) ? intval($_REQUEST['vacancy']) : 0;

        $arResult['VACANCY'] = $vacancy;

        if (!check_bitrix_sessid()) {
            $errors[] = 'Ваша сессия устарела';
        }

        if (empty($name)) {
            $errors['name'] = 'Не заполнено поле "Имя"';
        }

        if (empty($lastName)) {
            $errors['surname'] = 'Не заполнено поле "Фамилия"';
        }

        if (empty($email) && preg_match('/\@/', $email)) {
            $errors['email'] = 'Не заполнено или не верно заполнено поле "Email"';
        }

        if (empty($phone)) {
            $errors['phone'] = 'Не заполнено поле "Телефон"';
        }

        /*if (intval($resume) <= 0) {
            $errors['resume'] = 'Не заполнено поле "Резюме"';
        }*/

        if (empty($comment)) {
            $errors['comment'] = 'Не заполнено поле "О себе"';
        }

        if ($vacancy <= 0) {
            $errors['vacancy'] = 'Не передано поле "Вакансия"';
        }

        // Check vacancy
        $vacancyArr = CIBlockElement::GetList(array(), array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ID' => $vacancy
        ), false, false, array(
            'ID',
            'NAME'
        ))->Fetch();

        if (!$vacancyArr) {
            $errors[] = 'Не найдена вакансия';
        }

        $data = array(
            'IBLOCK_ID' => $arParams['SAVE_IBLOCK_ID'],
            'NAME' => $name . ' ' . $lastName . ', ' . $vacancyArr['NAME'] . ', ' . date('d.m.Y H:i'),
            'DATE_ACTIVE_FROM' => date(\Bitrix\Main\Type\DateTime::getFormat()),
            'PREVIEW_TEXT' => $comment,
            'PROPERTY_VALUES' => array(
                'NAME' => $name,
                'LAST_NAME' => $lastName,
                'EMAIL' => $email,
                'PHONE' => $phone,
                'RESUME' => $resume,
                'VACANCY_ID' => $vacancy,
            )
        );

        $arResult['DATA'] = array(
            'name' => $name,
            'surname' => $lastName,
            'email' => $email,
            'phone' => $phone,
            'resume' => $resume,
            'comment' => $comment,
            'vacancy' => $vacancy
        );

        if (empty($errors)) {
            $iBlockElement = new \CIBlockElement();
            $id = $iBlockElement->Add($data);

            if ($id <= 0) {
                $errors[] = 'Не удалось сохранить ваше резюме. Ошибка: ' . $iBlockElement->LAST_ERROR;
            }

            if (empty($errors)) {
                $arResult['ID'] = $id;
                $arResult['IS_COMPLETE'] = true;
                $arResult['DATA'] = array();
                // Send email
                foreach ($arParams['EVENT_MESSAGE_ID'] as $messageId) {
                    $sendId = CEvent::Send($arParams['EVENT_TYPE_ID'], array('s1'), array(
                        'IBLOCK_ID' => $arParams['SAVE_IBLOCK_ID'],
                        'IBLOCK_TYPE' => $arParams['SAVE_IBLOCK_TYPE'],
                        'ID' => $id,
                        'NAME' => $name,
                        'LAST_NAME' => $lastName,
                        'EMAIL' => $email,
                        'PHONE' => $phone,
                        'ABOUT_SELF' => $comment,
                    ), 'Y', $messageId, array($resume));
                }
            }
        }

        $arResult['ERRORS'] = $errors;
    }
}

$this->includeComponentTemplate();
