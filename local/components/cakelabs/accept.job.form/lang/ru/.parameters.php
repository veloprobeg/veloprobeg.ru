<?
$MESS['CP_BCSL_SUCCESS_MESSAGE'] = 'Сообщение об успешной отправке формы';
$MESS['CP_BCSL_SUCCESS_MESSAGE_DEFAULT'] = 'Спасибо что откликнулись на нашу вакансию, мы с вами свяжемся';
$MESS['CP_BCSL_IBLOCK_TYPE'] = 'Тип инфоблока вакансий';
$MESS['CP_BCSL_IBLOCK_ID'] = 'ID иноблока вакансий';
$MESS['CP_BCSL_SAVE_IBLOCK_TYPE'] = 'Тип инфоблока откликов на вакансии';
$MESS['CP_BCSL_SAVE_IBLOCK_ID'] = 'ID инфоблока откликов на вакансии';
$MESS['CP_BCSL_EMAIL_TEMPLATES'] = 'Почтовые шаблоны для отправки письма';
$MESS['CP_BCSL_EMAIL_TYPES'] = 'Тип почтового события';