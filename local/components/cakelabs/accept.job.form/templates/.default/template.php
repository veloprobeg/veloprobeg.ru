<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

?>
<form class="popup__inner" action="<?= $APPLICATION->GetCurPageParam() ?>" method="post"
      enctype="multipart/form-data">
    <input type="hidden" name="IS_SEND" value="Y">
    <?= bitrix_sessid_post() ?>
    <? if ($arResult['IS_SEND']): ?>
        <input type="hidden" name="vacancy" value="<?= $arResult['VACANCY'] ?>">
    <? endif ?>
    <a href="#" class="popup__closer"></a>
    <div class="popup__name"><?= Loc::getMessage("ACCEPT_JOB"); ?></div>
    <? if (!empty($arResult['ERRORS'])): ?>
        <div class="errors">
            <ul>
                <? foreach ($arResult['ERRORS'] as $error): ?>
                    <li><?= $error ?></li>
                <? endforeach ?>
            </ul>
        </div>
    <? endif ?>
    <div class="popup__row popup__row--space-betw">
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("LAST_NAME"); ?></div>
            <input type="text" name="surname" class="default-input" value="<?= $arResult['DATA']['surname'] ?>">
        </div>
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("NAME"); ?></div>
            <input type="text" name="name" class="default-input" value="<?= $arResult['DATA']['name'] ?>">
        </div>
    </div>
    <div class="popup__row popup__row--space-betw">
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("EMAIL"); ?></div>
            <input type="text" name="email" class="default-input" value="<?= $arResult['DATA']['email'] ?>">
        </div>
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("PHONE"); ?></div>
            <input type="text" name="phone" class="default-input mask-phone" value="<?= $arResult['DATA']['phone'] ?>">
        </div>
    </div>
    <div class="popup__row">
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("RESUME_FILE"); ?></div>
            <div id="dZUpload"><span class="dz-icon"></span></div>
        </div>
    </div>
    <div class="popup__row">
        <div class="popup__input-area">
            <div class="popup__input-name"><?= Loc::getMessage("ABOUT_SELF"); ?></div>
            <textarea name="comment"><?= $arResult['DATA']['comment'] ?></textarea>
        </div>
    </div>
    <div class="popup__row popup__row--center">
        <button type="submit" class="button button--orange"><?= Loc::getMessage("BUTTON_SEND"); ?></button>
    </div>
</form>
<div class="popup popup--thx<? if ($arResult['IS_COMPLETE']): ?> is-visible<? endif ?>" id="success-accept-job-form">
    <div class="popup__inner">
        <a href="#" class="popup__closer"></a>
        <p><?= $arParams['SUCCESS_MESSAGE'] ?></p>
    </div>
</div>
<script>
    <? if($arResult['IS_SEND']): ?>
    validate();
    masks();
    dropZone();
    <? endif ?>
    <? if($arResult['IS_COMPLETE']): ?>
    BX.closeWait();
    window.setTimeout(function () {
        $('#success-accept-job-form').removeClass('is-visible');
    }, 5000);
    <? endif ?>
</script>
