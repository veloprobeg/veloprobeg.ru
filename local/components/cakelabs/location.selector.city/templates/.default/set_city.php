<?

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
define("NO_AGENT_CHECK", true);

use Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');
$cityCode = isset($_REQUEST['cityCode']) ? $_REQUEST['cityCode'] : '';
$result = array(
  'STATUS' => 'SUCCESS',
  'MESSAGE' => '',
);

global $APPLICATION, $DB;

try {
  if (!Loader::includeModule('sale')) {
    throw new \Exception('Не поключен модуль ИМ');
  }
  
  if (empty($cityCode)) {
    throw new \Exception('Пустой код города');
  }

  $result['CITY'] = \Bitrix\Sale\Location\LocationTable::getList(array(
    'select' => array(
      'ID' => 'ID',
      'CODE' => 'CODE',
      'CITY_NAME' => 'NAME.NAME',
    ),
    'filter' => array(
      'TYPE.CODE' => 'CITY',
      'NAME.LANGUAGE_ID' => 'ru',
      'CODE' => $DB->ForSql($cityCode)
    )
  ))->fetch();

  if (!$result['CITY']) {
    throw new \Exception('Не удалось найти город');
  }
  

  $arVal = CSaleLocation::GetByID( $arResult['CITY']['ID'] );
  $APPLICATION->set_cookie('CURRENT_CITY', $result['CITY']['CODE']);
  $APPLICATION->set_cookie('CURRENT_REGION', $arVal['REGION_ID']);
  $result['QUESTION'] = 'Ваш город '.$result['CITY']['CITY_NAME'].'?';
} catch (\Exception $ex) {
  $result['STATUS'] = 'ERROR';
  $result['MESSAGE'] = $ex->getMessage();
}

echo CUtil::PhpToJSObject($result);