<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
} ?>


<?
use Bitrix\Main\Localization\Loc;
$arVal = CSaleLocation::GetByID( $arResult['CURRENT_CITY']['ID'] );
$APPLICATION->set_cookie('CURRENT_REGION', $arVal['REGION_ID']);
global $REGION_ID;
global $CITY_ID;
global $CITY_CODE;
global $CITY_NAME;
$CITY_NAME = $arResult['CURRENT_CITY']['CITY_NAME'];
$CITY_ID = $arResult['CURRENT_CITY']['ID'];
$CITY_CODE = $arVal['CODE'];
$REGION_ID = $arVal['REGION_ID'];
Loc::loadMessages(__FILE__);
?>
<!--Что бы отображать по дефолту после загрузки страницы плашку с подтверждением города добавь класс 'onload' на элемент 'header__city'-->
<div class="header__city header__nav-item<?= $arResult['IS_NEED_SHOW'] ? ' onload' : '' ?>"
     id="select-city-wrapper"><?= Loc::getMessage("SALE_SLS_YOUR_CITY"); ?>
  <div class="header__city-current" id="current-city-name"><?= $arResult['CURRENT_CITY']['CITY_NAME'] ?></div>
  <div class="header__city-approve">
    <div class="header__city-question" id="is-current-city-question"><?= Loc::getMessage("SALE_SLS_IS_YOUR_CITY", array(
        '#CITY#' => $arResult['CURRENT_CITY']['CITY_NAME']
      )); ?>
    </div>
    <div class="header__city-control">
      <a href="javascript:void(0)" class="header__city-accept" id="city-accept" data-id="<?= $arResult['CURRENT_CITY']['ID'] ?>"
         data-code="<?= $arResult['CURRENT_CITY']['CODE'] ?>"><?= Loc::getMessage("SALE_SLS_YES"); ?></a>
      <a href="javascript:void(0)" class="header__city-choose"><?= Loc::getMessage("SALE_SLS_NO"); ?></a>
    </div>
  </div>
  <form class="header__city-selection">
    <div class="header__city-nav">
      <div class="header__city-question"><?= Loc::getMessage("SALE_SLS_SELECT_CITY"); ?></div>
      <input type="text" placeholder="Поиск по городам..." class="header__city-input" id="search-city" autocomplete="off">
    </div>
    <div class="header__city-wrapper" id="selector-city-list-wrapper">
      <? foreach ($arResult['SORTED_CITIES'] as $letter => $cities): ?>
        <? $isFirstCity = true ?>
        <? if (is_numeric($letter)): ?>
          <div class="header__city-names-wrapper">
            <a href="javascript:void(0)" class="header__city-names" data-id="<?= $cities['ID'] ?>"
               data-code="<?= $cities['CODE'] ?>"><?= $cities['CITY_NAME'] ?></a>
          </div>
        <? else: ?>
          <ul class="header__city-list">
            <? for ($i = 0; count($cities) > $i; $i++): ?>
              <li class="header__city-item">
                <? if ($isFirstCity): ?>
                  <span class="header__city-section"><?= $letter ?></span>
                  <? $isFirstCity = false ?>
                <? endif ?>
                <a href="javascript:void(0)" class="header__city-link" data-id="<?= $cities[$i]['ID'] ?>"
                   data-code="<?= $cities[$i]['CODE'] ?>"><?= $cities[$i]['CITY_NAME'] ?></a>
              </li>
              <? if ($i == count($cities) - 1): ?>
                <li class="header__city-item"></li>
              <? endif ?>
            <? endfor ?>
          </ul>
        <? endif ?>
      <? endforeach ?>
    </div>
  </form>
</div>

<?
$jsParams = array(
  'WRAPPER_ID' => 'select-city-wrapper',
  'WRAPPER_LIST_ID' => 'selector-city-list-wrapper',
  'AJAX_URL' => $templateFolder.'/set_city.php',
  'CURRENT_CITY_ID' => 'current-city-name',
  'QUESTION_ID' => 'is-current-city-question',
  'BUTTON_ACCEPT_ID' => 'city-accept',
  'INPUT_SEARCH_ID' => 'search-city'
);
?>
<script>
  var choiceCity = new CJSChoiceCity(<?= CUtil::PhpToJSObject($jsParams) ?>);
</script>