<?
$MESS["SALE_SLS_NOTHING_FOUND"] = "К сожалению, ничего не найдено";
$MESS["SALE_SLS_ERROR_OCCURED"] = "К сожалению, произошла внутренняя ошибка";
$MESS["SALE_SLS_CLEAR_SELECTION"] = "Отменить выбор";
$MESS["SALE_SLS_EDIT"] = "Редактировать";
$MESS["SALE_SLS_INPUT_SOME"] = "Введите название";

$MESS["SALE_SLS_YOUR_CITY"] = "Ваш город";
$MESS["SALE_SLS_IS_YOUR_CITY"] = "Ваш город #CITY#?";
$MESS["SALE_SLS_YES"] = "Да";
$MESS["SALE_SLS_NO"] = "Нет, выбрать город";
$MESS["SALE_SLS_SELECT_CITY"] = "Выберите город:";
$MESS["SALE_SLS_"] = "";
?>