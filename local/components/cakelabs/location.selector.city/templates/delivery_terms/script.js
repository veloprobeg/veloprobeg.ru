(function (window) {
  if (!!window.CJSChoiceCity) {
    return;
  }

  window.CJSChoiceCity = function (params) {
    this.obWrapper = null;
    this.obListWrapper = null;
    this.obCurrentCity = null;
    this.obQuestion = null;
    this.obAcceptButton = null;
    this.obSearchInput = null;

    this.ajaxUrl = params.AJAX_URL || null;

    this.data = {};

    if (!!params.WRAPPER_ID) {
      this.obWrapper = BX(params.WRAPPER_ID);
    }

    if (!!params.WRAPPER_LIST_ID) {
      this.obListWrapper = BX(params.WRAPPER_LIST_ID);
    }

    if (!!params.CURRENT_CITY_ID) {
      this.obCurrentCity = BX(params.CURRENT_CITY_ID);
    }

    if (!!params.QUESTION_ID) {
      this.obQuestion = BX(params.QUESTION_ID);
    }

    if (!!params.BUTTON_ACCEPT_ID) {
      this.obAcceptButton = BX(params.BUTTON_ACCEPT_ID);
    }

    if (!!params.INPUT_SEARCH_ID) {
      this.obSearchInput = BX(params.INPUT_SEARCH_ID);
    }

    this.params = params;

    this.init();
  };

  window.CJSChoiceCity.prototype = {
    init: function () {
      if (!!this.obListWrapper) {
        BX.bindDelegate(this.obListWrapper, 'click', {
          property: {
            tagName: 'A'
          }
        }, BX.proxy(this.selectCity, this));
      }

      if (!!this.obAcceptButton) {
        BX.bind(this.obAcceptButton, 'click', BX.proxy(this.selectCity, this));
      }

      if (!!this.obSearchInput) {
        BX.bind(this.obSearchInput, 'keyup', BX.proxy(this.searchCity, this));
      }
    },

    searchCity: function() {
      var searchString = this.obSearchInput.value,
        cities = BX.findChildren(this.obListWrapper, {
          tagName: 'A'
        }, true);

      if (!!cities) {
        for (var i in cities) {
          if (cities.hasOwnProperty(i)) {
            var parent = cities[i].parentNode,
              reg = new RegExp(searchString, 'i');

            if (!!parent) {
              var linkText = cities[i].innerHTML;
              if (reg.test(linkText)) {
                if (parent.tagName == 'LI') {
                  BX.removeClass(parent, 'hidden');
                } else {
                  BX.removeClass(cities[i], 'hidden');
                }
              } else {
                if (parent.tagName == 'LI') {
                  BX.addClass(parent, 'hidden');
                } else {
                  BX.addClass(cities[i], 'hidden');
                }
              }
            }
          }
        }
      }
    },

    buildData: function (cityCode) {
      this.data = {
        sessid: BX.bitrix_sessid(),
        cityCode: cityCode
      }
    },

    selectCity: function (e) {
      var obCity = e.target,
        cityCode = '';

      if (!!obCity) {
        cityCode = BX.data(obCity, 'code');
      }

      if (!this.ajaxUrl) {
        console.error('URL IS EMPTY');
        return;
      }

      this.buildData(cityCode);

      BX.showWait(this.obListWrapper);

      BX.ajax.loadJSON(this.ajaxUrl, this.data, BX.delegate(this.selectCityHandler, this), function () {
        BX.closeWait();
      });
    },

    selectCityHandler: function (result) {
      BX.closeWait();

      if (result.STATUS == 'ERROR') {
        console.error(result.MESSAGE);
        return;
      }

      this.setCurrentCity(result);

      if (!!this.obWrapper) {
        BX.removeClass(this.obWrapper, 'onload');
        BX.removeClass(this.obWrapper, 'select');
      }

      BX.data(this.obAcceptButton, 'id', result.CITY.ID);
      BX.data(this.obAcceptButton, 'code', result.CITY.CODE);

      this.obSearchInput.value = '';
      this.searchCity();
    },
    
    showError: function (node, msg, border) {
            if (BX.type.isArray(msg)) {
                msg = msg.join('<br />');
            }
            var errorContainer = node.querySelector('.alert.alert-danger'), animate;
            if (errorContainer && msg.length) {
                BX.cleanNode(errorContainer);
                errorContainer.appendChild(BX.create('DIV', {html: msg}));

                errorContainer.style.opacity = 0;
                errorContainer.style.display = '';
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
                    step: function (state) {
                        errorContainer.style.opacity = state.opacity / 100;
                    },
                    complete: function () {
                        errorContainer.removeAttribute('style');
                    }
                }).animate();

                if (!!border) {
                    BX.addClass(node, 'bx-step-error');
                }

                BX.addClass(BX('popup-errors'), 'is-visible');
            }
        },

    setCurrentCity: function (result) {
      if (!!this.obCurrentCity) {
        this.obCurrentCity.innerHTML = result.CITY.CITY_NAME;
      }

      if (!!this.obQuestion) {
        this.obQuestion.innerHTML = result.QUESTION;
      }
	  
	  if ( $('#ORDER_PROP_6').length ){
		  this.showError(BX('popup-errors'), 'Вы поменяли город, поэтому поменялись способы доставки автоматически');
		  var timerId = setInterval(function() {
			if ( !$('#popup-errors').hasClass('is-visible') ){
				window.location.href = '';
			}
		  }, 1000);
	  }
	  else {
		window.location.href = '';
	  }
    }
  }
})(window);