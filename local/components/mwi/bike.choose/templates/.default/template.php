<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//printData($arResult,'','block');
$arFirstStep = array_values($arResult["SECTIONS"]["CHILD"]);
$sectionID = $arFirstStep[0]["ID"];
//dump($arFirstStep);
?>
<!--700 × 550-->

<form style="background-image: url('/layout/dist (6)/dist/assets/images/content/ask__image.jpg')" class="ask" id="choose_bike_form">
    <input type="hidden" value="<?=$arParams["IBLOCK_ID"]?>" name="iblock_id">
    <input type="hidden" value="<?=$sectionID?>" name="section_id" id="section_id">
    <div class="ask__screen">
        <div class="ask__header">
            <!-- <div class="ask__count"><span class="ask__count-current"> </span><span class="ask__count-total"> </span></div>-->
            <div class="ask__name"><?=$arFirstStep[0]["NAME"]?></div>
        </div>
        <div class="ask__qs">
            <div class="ask__questions is-active">
                <? foreach ($arFirstStep[0]["CHILD"] as $key=>$item) {?>
                <div class="ask__question">
                    <input type="radio" id="question_<?=$key?>" name="q1" value="<?=$key?>">
                    <label for="question_<?=$key?>"><?=$item["NAME"]?></label>
                </div>
                <?}?>
            </div>
        </div>
        <div class="ask__nav">
            <div class="buttons_wrap">
                <a href="javascript:void(0)" class="prev_question" style="visibility: hidden;">

                    <div class="ask__count"><!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                        <div class="prev_question-text">Я передумал</div>
                    </div>
                </a>
            <a href="javascript:void(0)" class="ask__more">
                <div class="ask__count"><!--<span class="ask__count-current"></span><span class="ask__count-total"></span>-->
                    <div class="ask__more-text">Еще <br> вопрос</div>
                </div>
            </a>

            </div>
            <button type="submit" style="display: none;" class="ask__submit"> Показать <br> результат</button>
        </div>
    </div>
</form>