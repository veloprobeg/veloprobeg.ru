<?

class Agents {
  public static function updateProductMinimalPrice() {
    $strDefaultCurrency = 'RUB';

    if (\Bitrix\Main\Loader::IncludeModule('currency')) {
      $strDefaultCurrency = \CCurrency::GetBaseCurrency();
    }

    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      $res = \CIBlockElement::GetList(array(), array(
        'IBLOCK_ID' => IBLOCK_CATALOG_ID,
        'ACTIVE' => 'Y'
      ));

      while ($product = $res->Fetch()) {
        $ELEMENT_ID = false;
        $IBLOCK_ID = false;
        $OFFERS_IBLOCK_ID = false;
        $OFFERS_PROPERTY_ID = false;

        //Check if iblock has offers
        $arOffers = \CIBlockPriceTools::GetOffersIBlock($product["IBLOCK_ID"]);

        if (is_array($arOffers)) {
          $ELEMENT_ID = $product["ID"];
          $IBLOCK_ID = $product["IBLOCK_ID"];
          $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
          $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
        }

        if ($ELEMENT_ID) {
          static $arPropCache = array();

          if (!array_key_exists($IBLOCK_ID, $arPropCache)) {
            //Check for MINIMAL_PRICE property
            $rsProperty = \CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();

            if ($arProperty) {
              $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            } else {
              $arPropCache[$IBLOCK_ID] = false;
            }
          }

          if ($arPropCache[$IBLOCK_ID]) {
            //Compose elements filter
            $arProductID = array();
            if ($OFFERS_IBLOCK_ID) {
              $rsOffers = \CIBlockElement::GetList(
                array(),
                array(
                  "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                  "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                ),
                false,
                false,
                array("ID")
              );

              while ($arOffer = $rsOffers->Fetch()) {
                $arProductID[] = $arOffer["ID"];
              }

              if (!is_array($arProductID)) {
                $arProductID = array($ELEMENT_ID);
              }
            } else {
              $arProductID = array($ELEMENT_ID);
            }

            $minPrice = false;
            $maxPrice = false;

            //Get prices
            $rsPrices = \CPrice::GetList(
              array(),
              array(
                "PRODUCT_ID" => $arProductID,
              )
            );

            while ($arPrice = $rsPrices->Fetch()) {
              if (\Bitrix\Main\Loader::includeModule('currency') && $strDefaultCurrency != $arPrice['CURRENCY']) {
                $arPrice["PRICE"] = \CCurrencyRates::ConvertCurrency($arPrice["PRICE"], $arPrice["CURRENCY"], $strDefaultCurrency);
              }

              $PRICE = $arPrice["PRICE"];

              if ($minPrice === false || $minPrice > $PRICE) {
                $minPrice = $PRICE;
              }

              if ($maxPrice === false || $maxPrice < $PRICE) {
                $maxPrice = $PRICE;
              }
            }

            //Save found minimal price into property
            if ($minPrice !== false) {
              \CIBlockElement::SetPropertyValuesEx(
                $ELEMENT_ID,
                $IBLOCK_ID,
                array(
                  "MINIMUM_PRICE" => $minPrice,
                  "MAXIMUM_PRICE" => $maxPrice,
                )
              );
            }
          }
        }
      }
    }

    return '\Agents\updateProductMinimalPrice();';
  }

  public static function checkSubscribe() {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      $obUser = new \CUser();

      $resUsers = $obUser->GetList($by = '', $order = '', array(
        'UF_SUBSCRIBE' => 1
      ), array(
        'SELECT' => array("UF_*"),
        'FIELDS' => array(
          'ID',
          'NAME',
          'EMAIL',
          'LAST_NAME',
          'SECOND_NAME'
        )
      ));

      $users = array();
      while ($user = $resUsers->Fetch()) {
        $users[$user['ID']] = $user;
      }

      $news = array();

      $newsRes = \CIBlockElement::GetList(array(), array(
        'IBLOCK_ID' => IBLOCK_NEWS_ID,
        'ACTIVE' => 'Y',
        array(
          'LOGIC' => 'OR',
          array(
            '<=DATE_ACTIVE_FROM' => date(\Bitrix\Main\Type\DateTime::getFormat(), time()),
            '>DATE_ACTIVE_FROM' => date(\Bitrix\Main\Type\DateTime::getFormat(), time() - 86400)
          ),
          array(
            'DATE_ACTIVE_FROM' => false,
            '<=DATE_CREATE' => date(\Bitrix\Main\Type\DateTime::getFormat(), time()),
            '>DATE_CREATE' => date(\Bitrix\Main\Type\DateTime::getFormat(), time() - 86400)
          )
        ),
      ), false, false, array(
        'ID',
        'IBLOCK_ID',
        'NAME',
        'CODE',
        'PREVIEW_TEXT',
        'PREVIEW_PICTURE',
        'DATE_ACTIVE_FROM'
      ));

      while($new = $newsRes->Fetch()) {
        $news[$new['ID']] = $new;
      }

      $actions = array();

      $actionsRes = \CIBlockElement::GetList(array(), array(
        'IBLOCK_ID' => IBLOCK_ACTIONS_ID,
        'ACTIVE' => 'Y',
        array(
          'LOGIC' => 'OR',
          array(
            '<=DATE_ACTIVE_FROM' => date(\Bitrix\Main\Type\DateTime::getFormat(), time()),
            '>DATE_ACTIVE_FROM' => date(\Bitrix\Main\Type\DateTime::getFormat(), time() - 86400)
          ),
          array(
            'DATE_ACTIVE_FROM' => false,
            '<=DATE_CREATE' => date(\Bitrix\Main\Type\DateTime::getFormat(), time()),
            '>DATE_CREATE' => date(\Bitrix\Main\Type\DateTime::getFormat(), time() - 86400)
          )
        ),
      ), false, false, array(
        'ID',
        'IBLOCK_ID',
        'NAME',
        'CODE',
        'PREVIEW_TEXT',
        'PREVIEW_PICTURE',
        'DATE_ACTIVE_FROM'
      ));

      while($action = $actionsRes->Fetch()) {
        $actions[$action['ID']] = $action;
      }

      \Debug::dtc($news, 'news');
      \Debug::dtc($users, 'users');
      \Debug::dtc($actions, 'actions');

      if (!empty($users) && (!empty($news) || !empty($actions))) {
        $content = '';

        if (!empty($news)) {
          $content .= '<div><h3>Последние новости</h3>';

          foreach ($news as $new) {
            $content .= '<div>'.$new['DATE'].'</div>';
            $content .= '<div><a href="http://'.SITE_SERVER_NAME.'/news/'.$new['CODE'].'/">'.$new['NAME'].'</a></div>';

            if ($new['PREVIEW_PICTURE'] > 0) {
              $content .= '<div><img width="150" height="120" src="http://'.SITE_SERVER_NAME.'/'.CFile::GetPath($new['PREVIEW_PICTURE']).'" alt=""></div>';
            }

            $content .= '<div>'.$new['PREVIEW_TEXT'].'</div>';
          }

          $content .= '</div>';
        }

        if (!empty($actions)) {
          $content .= '<div><h3>Последние Акции и скидки</h3>';

          foreach ($actions as $action) {
            $content .= '<div>'.$action['DATE'].'</div>';
            $content .= '<div><a href="http://'.SITE_SERVER_NAME.'/news/'.$action['CODE'].'/">'.$action['NAME'].'</a></div>';

            if ($action['PREVIEW_PICTURE'] > 0) {
              $content .= '<div><img width="150" height="120" src="http://'.SITE_SERVER_NAME.'/'.CFile::GetPath($action['PREVIEW_PICTURE']).'" alt=""></div>';
            }

            $content .= '<div>'.$action['PREVIEW_TEXT'].'</div>';
          }

          $content .= '</div>';
        }

        if (!empty($content)) {
          foreach ($users as $user) {
            if (preg_match('/@/', $user['EMAIL'])) {
              \CEvent::Send('SEND_SUBSCRIBE_NEWS', array('s1'), array(
                'CONTENT' => $content,
                'USER_MAIL' => $user['EMAIL'],
                'NAME' => $user['NAME']
              ));
            }
          }
        }
      }
    }

    return '\Agents::checkSubscribe();';
  }
}
