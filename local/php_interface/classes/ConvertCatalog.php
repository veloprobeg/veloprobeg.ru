<?php

use Bitrix\Catalog;

class ConvertCatalog {
  const IBLOCK_TYPE = 'catalog_view';
  static $emptySections = array();
  static $emptySectionsTree = array();
  static $noEmptySections = array();
  static $deleteSections = array();
  static $deleteProductSections = array();
  static $sections = array();
  static $elements = array();
  static $maxDepth = 0;

  static $page = 1;
  static $countStep = 1;
  static $sectionNumber = 0;

  static $elementsFileName = 'elements.json';
  static $sectionsFileName = 'sections.json';
  static $sectionsMaxDepth = 'max_depth.json';
  static $treeFileName = 'tree.json';

  static $iBlockType = null;
  static $iBlocks = array();
  static $catalogs = array();
  static $props = array();
  static $errors = array();
  static $result = array(
    'COUNT_ELEMENTS_ALL' => 0,
    'COUNT_SKIP_PRODUCTS_BY_OFFERS' => 0,
    'ALL_COUNT_OFFERS' => 0,
    'ALL_COUNT_OFFERS_SKIP' => 0,
    'COUNT_PRICES' => 0,
    'COUNT_RESTS' => 0,
    'COUNT_DELETE_RESTS' => 0,
    'COUNT_OFFERS' => 0,
    'COUNT_PROC' => 0
  );

  static $debug = true;

  private static $iBlockId;

  public static function convert($iBlockId, $step = 'structure', $page = 1, $countStep = 1) {
    if ($iBlockId > 0) {
      self::$iBlockId = $iBlockId;
    } else {
      self::getIBlockId();
    }

    self::$page = $page;
    self::$countStep = $countStep;
    \Debug::dtc($step, '$step');
    switch ($step) {
      case 'structure':
        if (self::$debug) {
          \Debug::start(0);
        }

        self::checkStructure();

        if (self::$debug) {
          self::$result['times']['checkStructure'] = $time = \Debug::stop(0);
        }
        break;
      case 'products':
        if (self::$debug) {
          \Debug::start(0);
        }

        self::isExistsSubSections(false);

        if (self::$debug) {
          self::$result['times']['isExistsSubSections'] = $time = \Debug::stop(0);
          \Debug::start(1);
        }

        self::getIBlock('catalog');
        self::getIBlock('offers');
        self::getProps('catalog');
        self::getProps('offers');

        if (self::$debug) {
          self::$result['times']['getIBlocks'] = $time = \Debug::stop(1);
          \Debug::start(2);
        }

        self::moveSections(false);

        if (self::$debug) {
          self::$result['times']['moveSections'] = $time = \Debug::stop(2);
        }
        break;
      case 'offers':
        break;
    }

    return self::$result;
  }

  //region CHECK IBLOCKS STRUCTURE
  private static function getIBlockId($iBlockType = '1c_catalog') {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      $IBlock = \IBlock::GetList(array(), array(
        'TYPE' => $iBlockType
      ))->Fetch();

      self::$iBlockId = $IBlock['ID'];
    }
  }

  private static function checkStructure() {

    self::$result['errors'] = array();

    self::checkIBlockType();
    self::checkIBlock();
    self::checkIBlock(false);
    self::checkProps();
    self::checkCatalog(false);
    self::checkCatalog();
  }

  private static function checkIBlockType($isSecond = false) {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      self::$iBlockType = \CIBlockType::GetList(array(), array(
        '=ID' => self::IBLOCK_TYPE
      ))->Fetch();

      if (!$isSecond) {
        if (!self::$iBlockType) {
          $iBlockType = new \CIBlockType();

          $isAdded = $iBlockType->Add(array(
            'ID' => self::IBLOCK_TYPE,
            'SORT' => 1000,
            'SECTIONS' => 'Y',
            'LANG' => array(
              'en' => array(
                'NAME' => 'private catalog',
                'SECTION_NAME' => 'Sections',
                'ELEMENT_NAME' => 'Products'
              ),
              'ru' => array(
                'NAME' => 'Публичный каталог',
                'SECTION_NAME' => 'Разделы',
                'ELEMENT_NAME' => 'Товары'
              )
            )
          ));

          if ($isAdded) {
            self::checkIBlockType(true);
          } else {
            self::$result['errors'][] = 'Не удалось добавить тип ИБ. Ошибка: '.$iBlockType->LAST_ERROR;
          }
        }
      }

      if (!self::$iBlockType) {
        self::$result['errors'][] = 'Не удалось получить тип ИБ.';
      }
    }
  }

  private static function checkIBlock($isProduct = true, $isSecond = false) {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      if (is_array(self::$iBlockType) && !empty(self::$iBlockType['ID'])) {
        $code = $isProduct ? 'catalog' : 'offers';
        $name = $isProduct ? 'Публичный каталог' : 'Торговые предложения публичного каталога';
        $sort = $isProduct ? 100 : 200;
        $description = $isProduct ? 'ИБ товаров публичного каталога' : 'ИБ торговых предложений публичного каталога';
        $listPageUrl = $isProduct ? '#SITE_DIR#/catalog/' : '';
        $sectionPageUrl = $isProduct ? '#SITE_DIR#/catalog/#SECTION_CODE#/' : '';
        $detailPageUrl = $isProduct ? '#SITE_DIR#/catalog/#SECTION_CODE#/#ELEMENT_CODE#/' : '';
        $elementName = $isProduct ? 'овар' : 'орговое предложение';
        $elementName2 = $isProduct ? 'овары' : 'орговые предложения';

        self::getIBlock($code);

        if (!$isSecond) {
          if (!self::$iBlocks[$code]) {
            $iBlock = new \CIBlock();

            $isAdded = $iBlock->Add(array(
              'IBLOCK_TYPE_ID' => self::IBLOCK_TYPE,
              'SORT' => $sort,
              'CODE' => $code,
              'NAME' => $name,
              'ACTIVE' => 'Y',
              'SITE_ID' => 's1',
              'LIST_PAGE_URL' => $listPageUrl,
              'SECTION_PAGE_URL' => $sectionPageUrl,
              "DETAIL_PAGE_URL" => $detailPageUrl,
              "DESCRIPTION" => $description,
              "DESCRIPTION_TYPE" => 'text',
              'WORKFLOW' => 'N',
              "GROUP_ID" => array(
                "2" => "R",
                // Reed - ALL
                "8" => "W"
                // Write CONTENT MANAGER
              )
            ));

            \CIBlock::SetMessages(
              $isAdded,
              array(
                'ELEMENT_NAME' => 'Т'.$elementName,
                'ELEMENTS_NAME' => 'Т'.$elementName2,
                'ELEMENT_ADD' => 'Добавить т'.$elementName,
                'ELEMENT_EDIT' => 'Изменить т'.$elementName,
                'ELEMENT_DELETE' => 'Удалить т'.$elementName,
              )
            );

            if ($isAdded) {
              self::checkIBlock($isProduct, true);
            } else {
              self::$result['errors'][] = 'Не удалось добавить ИБ. Ошибка: '.$iBlock->LAST_ERROR;
            }
          }
        }

        if (!self::$iBlocks[$code]) {
          self::$result['errors'][] = 'Не удалось получить ИБ.';
        }
      } else {
        self::$result['errors'][] = 'Переданный объект не является типом ИБ';
      }
    }
  }

  private static function checkProps() {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      foreach (self::$iBlocks as $iBlock) {
        if (is_array($iBlock) && !empty($iBlock['CODE'])) {
          $props = array();

          if ($iBlock['CODE'] == 'catalog') {
            $props = self::getProductProperties();
          } elseif ($iBlock['CODE'] == 'offers') {
            $props = self::getOfferProperties();
          }

          $iBlockId = $iBlock['ID'];
          $sort = 100;

          if ($iBlockId <= 0) {
            self::$result['errors'][] = 'Не задан ИД ИБ при добавлении свойств';
            return;
          }

          foreach ($props as $propCode => $prop) {
            $propRes = \CIBlockProperty::GetList(array(), array(
              'CODE' => $propCode,
              'IBLOCK_ID' => $iBlockId
            ))->Fetch();

            if (!$propRes) {
              $iBlockProperty = new \CIBlockProperty();

              $propFields = array_merge($prop, array(
                'CODE' => $propCode,
                'IBLOCK_ID' => $iBlockId,
                'SORT' => $sort
              ));

              $propId = $iBlockProperty->Add($propFields);

              $sort += 100;

              if ($propId <= 0) {
                self::$result['errors'][] = 'Не удалось добавить свойство с кодом "'.$propCode.'" в инфоблок "'.$iBlock['NAME'].'".';
              }
            }
          }
        } else {
          self::$result['errors'][] = 'Переданный объект не является экземпляром класса \CIBlock';
        }
      }
    }
  }

  private static function checkCatalog($isProduct = true) {
    if (\Bitrix\Main\Loader::includeModule('iblock') && \Bitrix\Main\Loader::includeModule('catalog')) {
      $obCatalog = new \CCatalog();
      $code = $isProduct ? 'catalog' : 'offers';

      $iBlock = \CIBlock::GetList(array(), array(
        'CODE' => $code,
        'IBLOCK_TYPE_ID' => self::IBLOCK_TYPE
      ))->Fetch();

      if (!$iBlock) {
        self::$result['errors'][] = 'Не удалось получить ИБ для проверки Каталога';
        return;
      }

      $catalog = $obCatalog->GetList(array(), array(
        'IBLOCK_ID' => $iBlock['ID']
      ))->Fetch();

      if (!$catalog) {
        if ($isProduct) {
          $obCatalog->Add(array(
            'IBLOCK_ID' => $iBlock['ID']
          ));
        } else {
          $iBlockProduct = \CIBlock::GetList(array(), array(
            'CODE' => 'catalog',
            'IBLOCK_TYPE_ID' => self::IBLOCK_TYPE
          ))->Fetch();

          $linkProp = \CIBlockProperty::GetList(array(), array(
            'CODE' => 'CML2_LINK',
            'IBLOCK_ID' => $iBlock['ID']
          ))->Fetch();

          if (!$linkProp) {
            self::$result['errors'][] = 'Не удалось получить свойство для привязки ТП к товарам';
            return;
          }

          if (!$iBlockProduct) {
            self::$result['errors'][] = 'Не удалось получить ИБ ТП для привязки ТП к товарам';
            return;
          }

          $catalogId = $obCatalog->Add(array(
            'IBLOCK_ID' => $iBlock['ID'],
            'PRODUCT_IBLOCK_ID' => $iBlockProduct['ID'],
            'SKU_PROPERTY_ID' => $linkProp['ID']
          ));

          if ($catalogId <= 0) {
            global $APPLICATION;
            $ex = $APPLICATION->GetException();

            self::$result['errors'][] = 'Не удалось добавить каталог к ИБ "'.$iBlock['NAME'].'". Ошибка: '.$ex->GetString();
          }
        }
      }
    }
  }
  //endregion

  //region SECTIONS
  private static function isExistsSubSections($force = false) {
    self::getIBlockSectionElements($force);
    self::getSections($force);

    foreach (self::$sections as $section) {
      if (!isset(self::$elements[$section['ID']])) {
        if (!preg_match('/^(У|у)далить(:)?/', $section['NAME'])) {
          self::$emptySections[$section['ID']] = $section;
        } else {
          self::$deleteProductSections[$section['ID']] = $section;
        }
      } else {
        if (!preg_match('/^(У|у)далить(:)?/', $section['NAME'])) {
          self::$noEmptySections[$section['ID']] = $section;
        } else {
          self::$deleteSections[$section['ID']] = $section;
        }
      }
    }
  }

  private static function makeIBlockSectionElements() {
    self::$elements = array();

    if (\Bitrix\Main\Loader::includeModule('iblock') && \Bitrix\Main\Loader::includeModule('catalog')) {
      // Get offer rests
      $restsRes = \CCatalogStoreProduct::GetList(array(), array(
        '>AMOUNT' => 0
      ));

      $rests = array();
      while ($rest = $restsRes->Fetch()) {
        $rests[$rest['PRODUCT_ID']][] = $rest;
      }

      // Get offer prices
      $priceRes = \CPrice::GetList(array(), array());

      $prices = array();
      while ($price = $priceRes->Fetch()) {
        $prices[$price['PRODUCT_ID']][] = $price;
      }

      $elementRes = \CIBlockElement::GetList(array(
        'ID' => 'ASC'
      ), array(
        'IBLOCK_ID' => self::$iBlockId
      ), false, false, array(
        'ID',
        'IBLOCK_ID',
        'IBLOCK_SECTION_ID',
        'NAME',
        'XML_ID'
      ));

      while ($element = $elementRes->Fetch()) {
        if (isset($prices[$element['ID']])) {
          $element['PRICES'] = $prices[$element['ID']];
        }

        if (isset($rests[$element['ID']])) {
          $element['RESTS'] = $rests[$element['ID']];
        }

        self::$elements[$element['IBLOCK_SECTION_ID']][$element['ID']] = $element;
        self::$result['COUNT_ELEMENTS_ALL']++;
      }

      foreach (self::$elements as $productId => $element) {
        if (isset(self::$elements[$productId][$element]['RESTS'])) {
          self::$result['ELEMENT_YES_RESTS']++;
        } else {
          self::$result['ELEMENT_NO_RESTS']++;
        }

        if (empty(self::$elements[$productId])) {
          unset(self::$elements[$productId]);
          self::$result['COUNT_SKIP_PRODUCTS_BY_OFFERS']++;
        }
      }
    }

    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$elementsFileName, json_encode(self::$elements));
  }

  private static function makeSections() {
    self::$sections = array();

    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      $sectionRes = \CIBlockSection::GetList(array(
        'ID' => 'ASC'
      ), array(
        'IBLOCK_ID' => self::$iBlockId
      ), false, array(
        'ID',
        'IBLOCK_SECTION_ID',
        'NAME',
        'XML_ID',
        'SORT',
        'CODE',
        'DEPTH_LEVEL',
      ));

      while ($section = $sectionRes->Fetch()) {
        self::$sections[] = $section;

        if (self::$maxDepth < $section['DEPTH_LEVEL']) {
          self::$maxDepth = $section['DEPTH_LEVEL'];
        }
      }
    }

    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsFileName, json_encode(self::$sections));
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsMaxDepth, json_encode(self::$maxDepth));
  }

  private static function makeSectionsTree() {
    self::$emptySectionsTree = array();

    foreach (self::$elements as $sectionId => $offers) {
      if (isset(self::$noEmptySections[$sectionId])) {
        foreach ($offers as $offer) {
          if (isset($offer['RESTS'])) {
            self::$noEmptySections[$sectionId]['OFFERS'][$offer['ID']] = $offer;
            self::$result['ALL_COUNT_OFFERS']++;
          } else {
            self::$result['ALL_COUNT_OFFERS_SKIP']++;
          }
        }
      }
    }

    // Перебираем разделы-товары для добавления к разделам товаров и ТП
    foreach (self::$noEmptySections as $noEmptySection) {
      // Если существует раздел с идентификатором, то добавляем к нему товары
      if (isset(self::$emptySections[$noEmptySection['IBLOCK_SECTION_ID']])) {
        if (!empty($noEmptySection['OFFERS'])) {
          self::$emptySections[$noEmptySection['IBLOCK_SECTION_ID']]['PRODUCTS'][$noEmptySection['ID']] = $noEmptySection;
        }

        if (isset(self::$emptySections[$noEmptySection['IBLOCK_SECTION_ID']]['OFFERS'][$noEmptySection['ID']])) {
          self::$emptySections[$noEmptySection['IBLOCK_SECTION_ID']]['PRODUCTS'][$noEmptySection['ID']]['OFFERS'] =
            self::$emptySections[$noEmptySection['IBLOCK_SECTION_ID']]['OFFERS'][$noEmptySection['ID']];
        }
      }
    }

    for ($i = 0; $i < self::$maxDepth; $i++) {
      foreach (self::$emptySections as $emptySection) {
        self::$emptySections[$emptySection['IBLOCK_SECTION_ID']]['SUB_SECTIONS'][$emptySection['ID']] = $emptySection;
      }
    }

    self::$emptySectionsTree = self::$emptySections[""]['SUB_SECTIONS'];

    if (isset(self::$emptySectionsTree[""])) {
      unset(self::$emptySectionsTree[""]);
    }

    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$treeFileName, json_encode(self::$emptySectionsTree));
  }

  private static function moveSections($force = false) {
    self::getSectionsTree($force);

    $iBlockSection = new \CIBlockSection();

    self::moveSection(array_pop(self::$emptySectionsTree), 0, self::$iBlocks['catalog']['ID'], $iBlockSection);
  }

  private static function moveSection($section, $parentSectionId = 0, $iBlockId, $iBlockSection) {
    \Debug::lm('moveSection');
    global $APPLICATION;

    self::$sectionNumber++;
    self::$result['countSections']++;

    if (\Bitrix\Main\Loader::includeModule('iblock') && \Bitrix\Main\Loader::includeModule('catalog')) {
      \Debug::lm('INCLUDE_MODULES');
      if ($parentSectionId > 0) {
        $section['IBLOCK_SECTION_ID'] = $parentSectionId;
      }

      $section['IBLOCK_ID'] = self::$iBlocks['catalog']['ID'];
      $s = \CIBlockSection::GetList(array(), array(
        'XML_ID' => $section['XML_ID'],
        'IBLOCK_ID' => $iBlockId
      ), false, array(
        'ID'
      ))->Fetch();

      if ($s) {
        $sectionId = $s['ID'];
      } else {
        $sectionId = $iBlockSection->Add($section);
      }

      \Debug::lm(self::$sectionNumber, 'self::$sectionNumber');
      \Debug::lm(self::$page * self::$countStep, 'self::$page * self::$countStep');
      \Debug::lm((self::$page + 1) * self::$countStep, 'self::(self::$page + 1) * self::$countStep');

      if (self::$sectionNumber >= self::$page * self::$countStep && self::$sectionNumber < (self::$page + 1) * self::$countStep) {
        \Debug::lm('GOOD_PAGE');
        if (is_array($section['PRODUCTS'])) {
          $obPrice = new CPrice();

          foreach ($section['PRODUCTS'] as $product) {
            $p = \CIBlockElement::GetList(array(), array(
              'XML_ID' => $product['XML_ID'],
              'IBLOCK_ID' => self::$iBlocks['catalog']['ID']
            ), false, false, array(
              'ID'
            ))->Fetch();

            $iBlockElement = new \CIBlockElement();

            //region REFACTORED
            // Process product
            $product['IBLOCK_ID'] = self::$iBlocks['catalog']['ID'];
            $product['IBLOCK_SECTION_ID'] = $sectionId;

            $productProps = array();
            $availProductProps = self::getProductProperties();
            $availOfferProps = self::getOfferProperties();

            unset($product['ID']);

            if ($p) {
              $updated = $iBlockElement->Update($p['ID'], $product);

              if (!$updated) {
                self::$result['errors'][] = 'Не удалось обновить товар "'.$product['NAME'].'". Ошибка: '.$iBlockElement->LAST_ERROR;
              }

              $productId = $p['ID'];
            } else {
              $productId = $iBlockElement->Add($product);

              if ($productId <= 0) {
                self::$result['errors'][] = 'Не удалось добавить товар "'.$product['NAME'].'". Ошибка: '.$iBlockElement->LAST_ERROR;
              }
            }

            if (is_array($product['OFFERS'])) {
              foreach ($product['OFFERS'] as $offer) {
                // Check exists oldOffer
                $newOfferFilter = array(
                  'XML_ID' => $offer['XML_ID'],
                  'IBLOCK_ID' => self::$iBlocks['offers']['ID']
                );

                $newOffer = $iBlockElement->GetList(array(), $newOfferFilter)->Fetch();

                $oldOffer = $iBlockElement->GetList(array(), array(
                  'ID' => $offer['ID']
                ))->GetNextElement();

                // Get oldOffer oldRests
                $oldRests = $offer['RESTS'];

                // Get oldOffer oldPrices
                $oldPrices = $offer['PRICES'];

                $oldFields = $oldOffer->GetFields();
                $oldProps = $oldOffer->GetProperties();
                $offerProduct = \CCatalogProduct::GetByID($offer['ID']);

                if (empty($oldPrices)) {
                  $oldFields['ACTIVE'] = 'N';
                }

                // Delete not needed fields
                unset($oldFields['TIMESTAMP_X']);

                foreach ($oldFields as $f => $field) {
                  if (is_null($field) || preg_match('/^~/', $f)) {
                    $oldFields[mb_substr($f, 1)] = $field;
                    unset($oldFields[$f]);
                  }
                }

                // Add props
                //region Get all oldProps info from base and format values
                foreach ($oldProps as $prop) {
                  if (isset($availOfferProps[$prop['CODE']])) {
                    if ($prop['PROPERTY_TYPE'] == 'L') {
                      $propValueId = '';

                      if (isset(self::$props['offers'][$prop['CODE']]['VALUES'][$prop['VALUE']])) {
                        $propValueId = self::$props['offers'][$prop['CODE']]['VALUES'][$prop['VALUE']]['ID'];
                      } else {
                        if (!empty($prop['VALUE'])) {
                          $propValueId = \CIBlockPropertyEnum::Add(array(
                            'PROPERTY_ID' => self::$props['offers'][$prop['CODE']]['ID'],
                            'VALUE' => $prop['VALUE'],
                          ));

                          if (!$propValueId) {
                            self::$result['errors'][] = 'Не удалось добавить вариант значения "'.$prop['VALUE'].'" для свойства "'.$prop['NAME'].'"';
                          } else {
                            self::$props['offers'][$prop['CODE']]['VALUES'][$prop['VALUE']] = array(
                              'ID' => $propValueId,
                              'PROPERTY_ID' => self::$props['offers'][$prop['CODE']]['ID'],
                              'VALUE' => $prop['VALUE'],
                            );
                          }
                        }
                      }

                      if ($propValueId) {
                        $oldFields['PROPERTY_VALUES'][$prop['CODE']] = $propValueId;
                      }
                    } elseif ($prop['PROPERTY_TYPE'] == 'S') {
                      if ($prop['MULTIPLE'] == 'Y') {
                        foreach ($prop['VALUE'] as $k => $v) {
                          $oldFields['PROPERTY_VALUES'][$prop['CODE']][] = array(
                            'VALUE' => $v,
                            'DESCRIPTION' => $prop['DESCRIPTION'][$k]
                          );
                        }
                      } else {
                        $oldFields['PROPERTY_VALUES'][$prop['CODE']] = $prop['VALUE'];
                      }
                    } elseif ($prop['PROPERTY_TYPE'] == 'F') {
                      if ($prop['MULTIPLE'] == 'Y') {
                        $oldFields['PROPERTY_VALUES'][$prop['CODE']] = $prop['VALUE'];
                      } else {
                        $oldFields['PROPERTY_VALUES'][$prop['CODE']] = $prop['VALUE'];
                      }
                    }
                  }

                  if (isset($availProductProps[$prop['CODE']])) {
                    if ($prop['PROPERTY_TYPE'] == 'L') {
                      $propValueId = '';

                      if (isset(self::$props['catalog'][$prop['CODE']]['VALUES'][$prop['VALUE']])) {
                        $propValueId = self::$props['catalog'][$prop['CODE']]['VALUES'][$prop['VALUE']]['ID'];
                      } else {
                        if (!empty($prop['VALUE'])) {
                          $propValueId = \CIBlockPropertyEnum::Add(array(
                            'PROPERTY_ID' => self::$props['catalog'][$prop['CODE']]['ID'],
                            'VALUE' => $prop['VALUE'],
                          ));

                          if (!$propValueId) {
                            self::$result['errors'][] = 'Не удалось добавить вариант значения "'.$prop['VALUE'].'" для свойства "'.$prop['NAME'].'"';
                          } else {
                            self::$props['catalog'][$prop['CODE']]['VALUES'][$prop['VALUE']] = array(
                              'ID' => $propValueId,
                              'PROPERTY_ID' => self::$props['catalog'][$prop['CODE']]['ID'],
                              'VALUE' => $prop['VALUE'],
                            );
                          }
                        }
                      }

                      if ($propValueId) {
                        $productProps[$prop['CODE']] = $propValueId;
                      }
                    } elseif ($prop['PROPERTY_TYPE'] == 'S') {
                      if ($prop['MULTIPLE'] == 'Y') {
                        foreach ($prop['VALUE'] as $k => $v) {
                          $productProps[$prop['CODE']][] = array(
                            'VALUE' => $v,
                            'DESCRIPTION' => $prop['DESCRIPTION'][$k]
                          );
                        }
                      } else {
                        $productProps[$prop['CODE']] = $prop['VALUE'];
                      }
                    } elseif ($prop['PROPERTY_TYPE'] == 'F') {
                      if ($prop['MULTIPLE'] == 'Y') {
                        $productProps[$prop['CODE']] = $prop['VALUE'];
                      } else {
                        $productProps[$prop['CODE']] = $prop['VALUE'];
                      }
                    }
                  }
                }
                //endregion

                $oldFields['PROPERTY_VALUES']['CML2_LINK'] = $productId;

                unset($oldFields['ID']);// remove ID

                if ($newOffer) {
                  $offerId = $newOffer['ID'];
                  $upd = $iBlockElement->Update($offerId, $oldFields);

                  if (!$upd) {
                    self::$result['errors'][] = 'Не удалось обновит ТП "'.$oldFields['NAME'].'" ['.$offerId.']';
                  }
                } else {
                  $oldFields['IBLOCK_ID'] = self::$iBlocks['offers']['ID'];

                  $offerId = $iBlockElement->Add($oldFields);
                }

                if (!$offerId) {
                  self::$result['errors'][] = 'Не удалось добавить ТП "'.$oldFields['NAME'].'" ['.$offerId.']';
                } else {
                  // Add/update catalog
                  $arCatalogProduct = array();
                  if (isset($offerProduct['WEIGHT']) && '' != $offerProduct['WEIGHT']) {
                    $arCatalogProduct['WEIGHT'] = $offerProduct['WEIGHT'];
                  }

                  if (isset($offerProduct['WIDTH']) && '' != $offerProduct['WIDTH']) {
                    $arCatalogProduct['WIDTH'] = $offerProduct['WIDTH'];
                  }
                  if (isset($offerProduct['LENGTH']) && '' != $offerProduct['LENGTH']) {
                    $arCatalogProduct['LENGTH'] = $offerProduct['LENGTH'];
                  }

                  if (isset($offerProduct['HEIGHT']) && '' != $offerProduct['HEIGHT']) {
                    $arCatalogProduct['HEIGHT'] = $offerProduct['HEIGHT'];
                  }

                  if (isset($offerProduct['VAT_INCLUDED']) && !empty($offerProduct['VAT_INCLUDED'])) {
                    $arCatalogProduct['VAT_INCLUDED'] = $offerProduct['VAT_INCLUDED'];
                  }

                  if (isset($offerProduct['QUANTITY_TRACE']) && !empty($offerProduct['QUANTITY_TRACE'])) {
                    $arCatalogProduct['QUANTITY_TRACE'] = $offerProduct['QUANTITY_TRACE'];
                  }

                  if (isset($offerProduct['MEASURE']) && is_string($offerProduct['MEASURE']) && (int)$offerProduct['MEASURE'] > 0) {
                    $arCatalogProduct['MEASURE'] = $offerProduct['MEASURE'];
                  }

                  if (
                    isset($offerProduct['PURCHASING_PRICE']) && is_string($offerProduct['PURCHASING_PRICE']) && $offerProduct['PURCHASING_PRICE'] != ''
                    && isset($offerProduct['PURCHASING_CURRENCY']) && is_string($offerProduct['PURCHASING_CURRENCY']) && $offerProduct['PURCHASING_CURRENCY'] != ''
                  ) {
                    $arCatalogProduct['PURCHASING_PRICE'] = $offerProduct['PURCHASING_PRICE'];
                    $arCatalogProduct['PURCHASING_CURRENCY'] = $offerProduct['PURCHASING_CURRENCY'];
                  }

                  if (!Catalog\ProductTable::isExistProduct($offerId)) {
                    $arCatalogProduct['ID'] = $offerId;
                    \CCatalogProduct::Add($arCatalogProduct, false);
                  } else {
                    if (!empty($arCatalogProduct)) {
                      \CCatalogProduct::Update($offerId, $arCatalogProduct);
                    }
                  }
                }

                // Add/update rests and prices
                if ($offerId) {
                  // Get newOffer prices
                  $newPriceRes = $obPrice->GetListEx(array(), array(
                    'PRODUCT_ID' => $offerId,
                  ));

                  $newPrices = array();

                  while ($newPrice = $newPriceRes->Fetch()) {
                    $newPrices[$newPrice['CATALOG_GROUP_ID']] = $newPrice;
                  }

                  foreach ($oldPrices as $oldPrice) {
                    // Add/update prices
                    $oldPrice['PRODUCT_ID'] = $offerId;

                    // Remove ID and TIMESTAMP_X
                    unset($oldPrice['ID']);
                    unset($oldPrice['TIMESTAMP_X']);

                    if (isset($newPrices[$oldPrice['CATALOG_GROUP_ID']])) {
                      $priceId = $obPrice->Update($newPrices[$oldPrice['CATALOG_GROUP_ID']]['ID'], $oldPrice);
                    } else {
                      $priceId = $obPrice->Add($oldPrice);
                    }

                    $exp = $APPLICATION->GetException();

                    if ($priceId <= 0 || $exp) {
                      self::$result['errors'][] = 'Не удалось '.(isset($newPrices[$oldPrice['CATALOG_GROUP_ID']]) ? 'обновить' : 'добавить').' цену для товара "'.$oldFields['NAME'].'" ['.$offerId.']. Ошибка: '.$exp->GetString();
                    }

                    self::$result['COUNT_PRICES']++;
                  }

                  // Get newOffer current Rests
                  $newRests = array();
                  $newRestsRes = \CCatalogStoreProduct::GetList(array(), array(
                    'PRODUCT_ID' => $offerId,
                    '>AMOUNT' => 0
                  ));

                  while ($newRest = $newRestsRes->Fetch()) {
                    $newRests[$newRest['STORE_ID']] = $newRest;
                  }

                  foreach ($oldRests as $oldRest) {
                    if (!isset($newRests[$oldRest['STORE_ID']])) {
                      // Add
                      $storeProductId = \CCatalogStoreProduct::Add(array(
                        'PRODUCT_ID' => $offerId,
                        'AMOUNT' => $oldRest['AMOUNT'],
                        'STORE_ID' => $oldRest['STORE_ID']
                      ));
                    } else {
                      // Update amount
                      $storeProductId = \CCatalogStoreProduct::Update($newRests[$oldRest['STORE_ID']]['ID'], array(
                        'AMOUNT' => $oldRest['AMOUNT'],
                      ));
                    }

                    if (!$storeProductId) {
                      self::$result['errors'][] = 'Не удалось '.(isset($newRests[$oldRest['STORE_ID']]) ? 'обновить' : 'добавить').' остаток для предложения "'.$offer['NAME'].'" ['.$offerId.']';
                    }

                    self::$result['COUNT_RESTS']++;
                  }

                  // TODO Make rests array for delete
                  $deleteRests = array();
                  foreach ($deleteRests as $deleteRest) {
                    $deleted = \CCatalogStoreProduct::Delete($deleteRest['ID']);

                    if (!$deleted) {
                      self::$result['errors'][] = 'Не удалось удалить остаток со склада "'.$deleteRest['STORE_NAME'].'" для предложения "'.$offer['NAME'].'" ['.$offerId.']';
                    } else {
                      self::$result['COUNT_DELETE_RESTS']++;
                    }
                  }
                }

                self::$result['COUNT_OFFERS']++;
              }
            }

            // Add product props
            \CIBlockElement::SetPropertyValuesEX($productId, self::$iBlocks['catalog']['ID'], $productProps);
            //endregion

            self::$result['COUNT_PROC']++;
          }
        }
      }

      if ($sectionId > 0) {
        if (!empty($section['SUB_SECTIONS'])) {
          foreach ($section['SUB_SECTIONS'] as $subSection) {
            if (self::$sectionNumber < (self::$page + 1) * self::$countStep) {
              if (!isset($subSection['SUB_SECTIONS']) && !isset($subSection['PRODUCTS'])) {
                self::$result['END'] = 'Y';
              } else {
                self::moveSection($subSection, $sectionId, $iBlockId, $iBlockSection);
              }
            }
          }
        }
      } else {
        self::$result['errors'][] = 'Не удалось добавить раздел "'.$section['NAME'].'". Ошибка: '.$iBlockSection->LAST_ERROR;
      }
    }
  }

  //endregion

  //region PROPERTIES
  private
  static function getProductProperties() {
    return array(
      // Base props
      'MODELNYY_GOD' => array(
        "NAME" => "Модельный год",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'DATA_OBNOVLENIYA_OSTATKA' => array(
        "NAME" => "Дата обновления остатка",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
      ),
      'PROIZVODITEL' => array(
        "NAME" => "Производитель",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'VES_S_UPAKOVKOY' => array(
        "NAME" => "Вес с упаковкой",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'CML2_BASE_UNIT' => array(
        "NAME" => "Базовая единица",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
      ),
      'CML2_TAXES' => array(
        "NAME" => "Ставки налогов",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "MULTIPLE" => "Y",
        "WITH_DESCRIPTION" => "Y",
      ),
      'CML2_MANUFACTURER' => array(
        "NAME" => "Производитель",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'OBYEM_S_UPAKOVKOY' => array(
        "NAME" => "Объём с упаковкой",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'UROVEN' => array(
        "NAME" => "Уровень",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
    );
  }

  private
  static function getOfferProperties() {
    return array(
      // Link to products
      'CML2_LINK' => array(
        "NAME" => "Элемент каталога",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "E",
        "LIST_TYPE" => "L",
        "MULTIPLE" => "N",
        "LINK_IBLOCK_ID" => self::$iBlocks['catalog']['ID'],
        "USER_TYPE" => 'SKU',
        "USER_TYPE_SETTINGS" => array(
          'VIEW' => 'A',
          'SHOW_ADD' => 'N',
          'MAX_WIDTH' => 0,
          'MIN_HEIGHT' => 24,
          'MAX_HEIGHT' => 1000,
          'BAN_SYM' => ',;',
          'REP_SYM' => ' ',
          'OTHER_REP_SYM' => '',
          'IBLOCK_MESS' => 'N',
        )
      ),
      // Base props
      'CML2_BAR_CODE' => array(
        "NAME" => "ШтрихКод",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
      ),
      'CML2_ARTICLE' => array(
        "NAME" => "Артикул",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
      ),
      'CML2_ATTRIBUTES' => array(
        "NAME" => "Характеристики",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "MULTIPLE" => "Y",
        "WITH_DESCRIPTION" => "Y",
      ),
      'CML2_TRAITS' => array(
        "NAME" => "Реквизиты",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "MULTIPLE" => "Y",
        "WITH_DESCRIPTION" => "Y",
      ),
      'CML2_BASE_UNIT' => array(
        "NAME" => "Базовая единица",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
      ),
      'MORE_PHOTO' => array(
        "NAME" => "Картинки",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "F",
        "MULTIPLE" => "Y",
      ),
      'FILES' => array(
        "NAME" => "Файлы",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'EST_V_NALICHII' => array(
        "NAME" => "Есть в наличии",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      // Additional props
      'SIZE' => array(
        "NAME" => "Размер",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "SEARCHABLE" => 'Y',
      ),
      'GENDER' => array(
        "NAME" => "Пол",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "SEARCHABLE" => 'Y',
      ),
      'COLOR' => array(
        "NAME" => "Цвет",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "SEARCHABLE" => 'Y',
      ),
      'TYPE' => array(
        "NAME" => "Тип",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
      ),
      'SIZE_COMPATIBLE' => array(
        "NAME" => "Размер (совместимость)",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'SIZE_WHEEL' => array(
        "NAME" => "Размер (колес)",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'VOLUME' => array(
        "NAME" => "Объем",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S"
      ),
      'LONG' => array(
        "NAME" => "Длинная",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'AVERAGE' => array(
        "NAME" => "Средняя",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'SHORT_FOOT' => array(
        "NAME" => "Короткая лапка",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'LEFT' => array(
        "NAME" => "Левая",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'RIGHT' => array(
        "NAME" => "Правая",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'STARTS' => array(
        "NAME" => "Звезды",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'LENGTH' => array(
        "NAME" => "Длина",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S"
      ),
      'SIZE_STAR' => array(
        "NAME" => "Размер (заезды)",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'STANDARD_MOUNTING' => array(
        "NAME" => "Стандарт крепелния",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'FRONT' => array(
        "NAME" => "Передний",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'REAR' => array(
        "NAME" => "Задний",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'COMPATIBILITY' => array(
        "NAME" => "Совместимость",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "MULTIPLE" => "Y",
      ),
      'COMPOSITION' => array(
        "NAME" => "Состав",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "MULTIPLE" => "Y",
      ),
      'LEFT2' => array(
        "NAME" => "Левый",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'RIGHT2' => array(
        "NAME" => "Правый",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L",
        "LIST_TYPE" => 'C',
        "VALUES" => array(
          array(
            "VALUE" => "Да",
            "XML_ID" => "Y",
            "DEF" => "N",
            "SORT" => "100"
          )
        )
      ),
      'TECHNOLOGY' => array(
        "NAME" => "Техология",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'HOLES' => array(
        "NAME" => "Отверстия",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'STANDARD_DRUM' => array(
        "NAME" => "Стандарт барабана",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'MOUNT' => array(
        "NAME" => "Крепление",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'STANDARD' => array(
        "NAME" => "Стандарт",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "L"
      ),
      'NIPPLE' => array(
        "NAME" => "Нипель",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S"
      ),
      'STOCK' => array(
        "NAME" => "Шток",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S"
      ),
      'AXIS' => array(
        "NAME" => "Ось",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S"
      ),
      'ADDITIONAL_FEATURES' => array(
        "NAME" => "Доп. характеристики",
        "ACTIVE" => "Y",
        "PROPERTY_TYPE" => "S",
        "MULTIPLE" => "Y",
      ),
    );
  }

//endregion

  /**
   * @param string $code
   */
  private
  static function getIBlock($code = 'catalog') {
    if (\Bitrix\Main\Loader::includeModule('iblock')) {
      self::$iBlocks[$code] = \CIBlock::GetList(array(), array(
        'CODE' => $code,
        'IBLOCK_TYPE_ID' => self::IBLOCK_TYPE
      ))->Fetch();
    }

    if (\Bitrix\Main\Loader::includeModule('catalog')) {
      self::$catalogs[$code] = CCatalogSKU::GetInfoByIBlock(self::$iBlocks[$code]["ID"]);
    }
  }

  /**
   * @param $iBlockCode
   */
  private
  static function getProps($iBlockCode) {
    if (\Bitrix\Main\Loader::includeModule('iblock') && isset(self::$iBlocks[$iBlockCode]['ID'])) {
      $propRes = \CIBlockProperty::GetList(array(), array(
        'IBLOCK_ID' => self::$iBlocks[$iBlockCode]['ID']
      ));

      while ($prop = $propRes->Fetch()) {
        if ($prop['PROPERTY_TYPE'] == 'L') {
          $prop['VALUES'] = array();

          $propValuesRes = \CIBlockProperty::GetPropertyEnum(
            $prop['ID'],
            array("SORT" => "asc"),
            array()
          );

          while ($propValue = $propValuesRes->Fetch()) {
            $prop['VALUES'][$propValue['VALUE']] = $propValue;
          }
        }

        self::$props[$iBlockCode][$prop['CODE']] = $prop;
      }
    }
  }

  /**
   * @param bool $force
   */
  private
  static function getSectionsTree($force = false) {
    if ($force || !file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$treeFileName)) {
      self::makeSectionsTree();
    } else {
      self::$emptySectionsTree = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$treeFileName), true);

      if (!self::$emptySectionsTree) {
        self::makeSectionsTree();
      }
    }
  }

  /**
   * @param bool $force
   */
  private
  static function getSections($force = false) {
    if ($force || !file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsFileName) || !file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsMaxDepth)) {
      self::makeSections();
    } else {
      self::$sections = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsFileName), true);
      self::$maxDepth = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$sectionsMaxDepth), true);
      if (!self::$sections) {
        self::makeSections();
      }
    }

    self::$result['COUNT_SECTIONS'] = count(self::$sections);
  }

  /**
   * @param bool $force
   */
  private
  static function getIBlockSectionElements($force = false) {
    if ($force || !file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$elementsFileName)) {
      self::makeIBlockSectionElements();
    } else {
      self::$elements = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.self::$elementsFileName), true);

      if (!self::$elements) {
        self::makeIBlockSectionElements();
      }
    }

    self::$result['COUNT_ELEMENTS'] = count(self::$elements);
  }
}