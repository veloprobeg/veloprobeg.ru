<?

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

$eventManager = Main\EventManager::getInstance();
$eventManager->addEventHandler("", "AGPorogiNakopitelnykhSkidokOnBeforeUpdate", "AGPorogiNakopitelnykhSkidokBeforeUpdate");
$eventManager->addEventHandler("", "KartyLoyalnostiOnBeforeUpdate", "KartyLoyalnostiBeforeUpdate");

/**
 * @param Entity\Event $event
 *
 */
function AGPorogiNakopitelnykhSkidokBeforeUpdate(Entity\Event $event)
{
    Loader::includeModule("highloadblock");


    $entity = $event->getEntity();
    $arFields = $event->getParameter("fields");
    $productExtId = $arFields["UF_NOMENKLATURA"];
    $cardTypeXmlId = $arFields["UF_VIDKARTY"];

    //Ищем тип карты лояльности

    $hlblock = HL\HighloadBlockTable::getById(HL_IBLOCK_VIDY_KART__ID)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
        "filter" => array("UF_XML_ID" => $cardTypeXmlId)  // Задаем параметры фильтра выборки
    ));

    if ($arData = $rsData->Fetch()) {
        $cardName = $arData["UF_NAME"];
    }

    $a = (int)$arFields["UF_ZNACHENIEPOROGA"];
    $b = (int)$arFields["UF_KURS"];
    $k = ceil($a * 100 / $b);//Коэффициент в процентах для расчета баллов по каждому товару
    if (empty($arFields["UF_NIZHNYAYAGRANITSA"])) {// Если не задана нижняя граница
        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "EXTERNAL_ID" => $productExtId);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement())//Это элемент
        {
            $arFields = $ob->GetFields();
            $productId = $arFields["ID"];
            $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG_SKU_ID, 'PROPERTY_CML2_LINK' => $productId));
            while ($obOffer = $rsOffers->GetNextElement()) {
                $arSkuFields = $obOffer->GetFields();
                $offerID = $arSkuFields["ID"];
                if ("Бонусные" == $cardName) {
                    CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS" => $k));
                } elseif ("Супер" == $cardName) {
                    CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS_VIP" => $k));
                } else {
                    CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS_OLD" => $k));
                }
            }

        } else {  //Это категория
            $arProductId = array();
            /*$arSelect = Array("ID", "NAME");*/
            $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "EXTERNAL_ID" => $productExtId);
            $rsSect = CIBlockSection::GetList(Array(), $arFilter);
            if ($arSect = $rsSect->GetNext()) {
                $sectionId = $arSect["ID"];
                $arSelect = Array("ID", "NAME");
                $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "SECTION_ID" => $sectionId);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $productId = $arFields["ID"];
                    $arProductId[] = $productId;
                }
                foreach ($arProductId as $productId) {
                    $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG_SKU_ID, 'PROPERTY_CML2_LINK' => $productId));
                    while ($obOffer = $rsOffers->GetNextElement()) {
                        /*$productId = $obOffer["ID"];*/
                        $arSkuFields = $obOffer->GetFields();
                        $offerID = $arSkuFields["ID"];
                        if ("Бонусные" == $cardName) {
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS" => $k));
                        } elseif ("Супер" == $cardName) {
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS_VIP" => $k));
                        } else {
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS_OLD" => $k));
                        }
                    }
                }
            }
        }
    } else {//Если задана нижняя граница - значит это правило для всех элементов секции, подходящей под условия.
        if (strstr($arFields["UF_NAME"], 'Veloprobeg.ru')) { //Если есть Veloprobeg.ru в названии - значит это правило для всего каталога
            if ($arFields["UF_NIZHNYAYAGRANITSA"] == 0) {
                if ("Бонусные" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS";
                } elseif ("Супер" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_VIP";
                } else {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_OLD";
                }
            } elseif ($arFields["UF_NIZHNYAYAGRANITSA"] == 10000) {
                if ("Бонусные" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_FROM_10000";
                } elseif ("Супер" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_VIP_FROM_10000";
                } else {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_OLD_FROM_10000";
                }
            } elseif ($arFields["UF_NIZHNYAYAGRANITSA"] == 500000) {
                if ("Бонусные" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_FROM_500000";
                } elseif ("Супер" == $cardName) {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_VIP_FROM_500000";
                } else {
                    $propertyCode = "LOGICTIM_BONUS_BALLS_OLD_FROM_500000";
                }
            }
            $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG_SKU_ID));
            while ($obOffer = $rsOffers->GetNextElement()) {
                $arSkuFields = $obOffer->GetFields();
                $offerID = $arSkuFields["ID"];
                CIBlockElement::SetPropertyValuesEx($offerID, false, array($propertyCode => $k));
            }
        } else {
            $arProductId = array();
            $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "EXTERNAL_ID" => $productExtId, ">catalog_PRICE_11" => $arFields["UF_NIZHNYAYAGRANITSA"]);
            $rsSect = CIBlockSection::GetList(Array(), $arFilter);
            if ($arSect = $rsSect->GetNext()) {
                $sectionId = $arSect["ID"];
                $arSelect = Array("ID", "NAME");
                $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "SECTION_ID" => $sectionId);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $productId = $arFields["ID"];
                    $arProductId[] = $productId;
                }
                foreach ($arProductId as $productId) {
                    $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => IBLOCK_CATALOG_SKU_ID, 'PROPERTY_CML2_LINK' => $productId));
                    while ($obOffer = $rsOffers->GetNextElement()) {

                        $arSkuFields = $obOffer->GetFields();
                        $offerID = $arSkuFields["ID"];
                        if ($arFields["UF_NIZHNYAYAGRANITSA"] == 0) {
                            if ("Бонусные" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS";
                            } elseif ("Супер" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_VIP";
                            } else {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_OLD";
                            }
                        } elseif ($arFields["UF_NIZHNYAYAGRANITSA"] == 10000) {
                            if ("Бонусные" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_FROM_10000";
                            } elseif ("Супер" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_VIP_FROM_10000";
                            } else {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_OLD_FROM_10000";
                            }
                        } elseif ($arFields["UF_NIZHNYAYAGRANITSA"] == 500000) {
                            if ("Бонусные" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_FROM_500000";
                            } elseif ("Супер" == $cardName) {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_VIP_FROM_500000";
                            } else {
                                $propertyCode = "LOGICTIM_BONUS_BALLS_OLD_FROM_500000";
                            }
                        }
                        /*if ("Бонусные" == $cardName) {
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS" => $k));
                        } elseif ("Супер" == $cardName) {
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array("LOGICTIM_BONUS_BALLS_VIP" => $k));
                        } else {*/
                            CIBlockElement::SetPropertyValuesEx($offerID, false, array($propertyCode => $k));
                        /*}*/
                    }
                }
            }
        }
    }
}

function KartyLoyalnostiBeforeUpdate(Entity\Event $event)
{
    Loader::includeModule("logictim.balls");
    $entity = $event->getEntity();
    $arFields = $event->getParameter("fields");
    $userExtId = $arFields["UF_KONTRAGENT"]; // EXT_ID юзера
    $cardTypeXmlId = $arFields["UF_VLADELETS"]; // EXT_ID типа карты
    $summaProdazh = $arFields["UF_SUMMAPRODAZH"]; // Сумма продаж
    $groupProdazhiId = false;
    if ($summaProdazh < 10000) {
        $groupProdazhiId = GROUP_PRODAZHI_BEFORE_10000;
    } elseif ($summaProdazh < 500000) {
        $groupProdazhiId = GROUP_PRODAZHI_FROM_10000;
    } elseif ($summaProdazh > 500000) {
        $groupProdazhiId = GROUP_PRODAZHI_FROM_500000;
    }

    //Ищем тип карты лояльности по EXT_ID
    $hlblock = HL\HighloadBlockTable::getById(HL_IBLOCK_VIDY_KART__ID)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
        "filter" => array("UF_XML_ID" => $cardTypeXmlId)  // Задаем параметры фильтра выборки
    ));

    if ($arData = $rsData->Fetch()) {
        $cardName = $arData["UF_NAME"];
    }
    if ("Бонусные" == $cardName) {
        $groupId = GROUP_BONUS__ID;
    } elseif ("Супер" == $cardName) {
        $groupId = GROUP_VIP__ID;
    } elseif ("Старые" == $cardName) {
        $groupId = GROUP_OLD__ID;
    } else {
        $groupId = false;
    }

    $ballsNumber = $arFields["UF_OSTATOKBONUSOV"];
    $filter = Array("XML_ID" => $userExtId);
    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
    if ($arUser = $rsUsers->Fetch()) {
        //dump($arUser);
        $userID = $arUser["ID"];
    }
    $arGroups = CUser::GetUserGroup($userID);
    if (($key = array_search(GROUP_OLD__ID, $arGroups)) !== false) {
        unset($arGroups[$key]);
    }
    if (($key = array_search(GROUP_VIP__ID, $arGroups)) !== false) {
        unset($arGroups[$key]);
    }
    if (($key = array_search(GROUP_BONUS__ID, $arGroups)) !== false) {
        unset($arGroups[$key]);
    }
    if ($groupId) {
        $arGroups[] = $groupId;
    }
    if ($groupProdazhiId) {
        $arGroups[] = $groupProdazhiId;
    }
    CUser::SetUserGroup($userID, $arGroups);
    //die($arUser);
    $arFields = array(
        "SET_BONUS" => $ballsNumber,
        "USER_ID" => $userID,
        "OPERATION_TYPE" => 'USER_BALLANCE_CHANGE',
        "OPERATION_NAME" => 'Импорт из 1-С',
        "ORDER_ID" => '',
        "DETAIL_TEXT" => 'Описание',
        "MAIL_EVENT" => array(
            "EVENT_NAME" => "",
            "CUSTOM_FIELDS" => array(
                "TEST_1" => '',
                "TEST_2" => '',
            )
        )
    );
    logictimBonusApi::SetBonus($arFields);
    /*die($productId);*/
}