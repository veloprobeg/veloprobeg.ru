<?

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array(
  "IBlockEventHandlers",
  "onAfterIBlockElementAddHandler"
));

class IBlockEventHandlers {
  public static function onAfterIBlockElementAddHandler(&$arFields) {
    // Send mail massage about add review
    if ($arFields['IBLOCK_ID'] == IBLOCK_REVIEWS_ID) {
      $element = \CIBlockElement::GetList(array(), array(
        'ID' => $arFields['ID']
      ))->GetNextElement();

      $props = $element->GetProperties();

      $iBlock = \CIBlock::GetByID($arFields['IBLOCK_ID'])->Fetch();

      $fields = array(
        'ID' => $arFields['ID'],
        'IBLOCK_ID' => $arFields['IBLOCK_ID'],
        'IBLOCK_TYPE' => $iBlock['IBLOCK_TYPE_ID'],
        'TITLE' => $arFields['NAME'],
        'REVIEW' => $arFields['PREVIEW_TEXT'],
      );

      foreach ($props as $prop) {
        if (in_array($prop['CODE'], array(
          'REVIEW_OF',
          'NAME',
          'EMAIL',
          'PHONE',
        ))) {
          $fields[$prop['CODE']] = $prop['VALUE'];
        }
      }

      CEvent::Send('ADD_NEW_REVIEW', array('s1'), $fields);
    }
  }
}