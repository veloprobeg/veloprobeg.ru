<?

class IBlockFilters {
  /**
   * Filter for choice week in main page
   *
   * @return array
   */
  public static function getMainFilterChooseWeek() {
   
	/*
   $props = array(
      'CHOOSE_WEEK' => ''
    );

    $phpCache = new CPhpCache();

    if ($phpCache->InitCache('3600', 'viewChoiceWeekInMain')) {
      $vars = $phpCache->GetVars();
      $props = $vars['PROPS'];
    } elseif ($phpCache->StartDataCache()) {
      $chooseWeekRes = \CIBlockPropertyEnum::GetList(
        array(),
        array(
          'XML_ID' => 'Y',
          'CODE' => 'CHOOSE_WEEK'
        )
      )->Fetch();

      $chooseWeek = $chooseWeekRes['ID'];

      $props = array(
        'CHOOSE_WEEK' => $chooseWeek,
      );

      $phpCache->EndDataCache(array(
        'PROPS' => $props
      ));
    }
	*/
    return array(
      '!PROPERTY_CHOOSE_WEEK' => false
    );
  }

  /**
   * Filter for catalog sections on main page
   *
   * @return array
   */
  public static function getMainSectionsFilter() {
    return array(
      'UF_SHOW_ON_HOMEPAGE' => '1'
    );
  }

  /**
   * Filter for Special products on main page
   *
   * @return mixed
   */
  public static function getMainSpecialFilter() {
    $props = array(
      'SHOW_IN_MAIN' => ''
    );

    $phpCache = new CPhpCache();

    if ($phpCache->InitCache('3600', 'viewSpecInMain')) {
      $vars = $phpCache->GetVars();
      $props = $vars['PROPS'];
    } elseif ($phpCache->StartDataCache()) {
      $shoInMainRes = \CIBlockPropertyEnum::GetList(
        array(),
        array(
          'XML_ID' => 'Y',
          'CODE' => 'SHOW_IN_MAIN'
        )
      )->Fetch();

      $shoInMain = $shoInMainRes['ID'];

      $props = array(
        'SHOW_IN_MAIN' => $shoInMain
      );

      $countShowInMain = \CIBlockElement::GetList(array(), array(
        'IBLOCK_ID' => IBLOCK_CATALOG_ID,
        'PROPERTY_SHOW_IN_MAIN' => $shoInMain
      ), array());

      if ($countShowInMain == 0) {
        $props = array();
      }

      $phpCache->EndDataCache(array(
        'PROPS' => $props
      ));
    }

    /*$allDiscountProductsId = array(-1);
    $priceId = 0;

    if (\Bitrix\Main\Loader::includeModule("catalog")) {
      $dbProductDiscounts = \CCatalogDiscount::GetList(
        array("SORT" => "ASC"),
        array(
          "ACTIVE" => "Y",
        ),
        false,
        false
      );

      while ($arProductDiscounts = $dbProductDiscounts->Fetch()) {
        if (isset($arProductDiscounts['PRODUCT_ID']) > 0) {
          $allDiscountProductsId[] = $arProductDiscounts['PRODUCT_ID'];
        } elseif ($arProductDiscounts['SECTION_ID'] > 0) {
          $elementRes = CIBlockElement::GetList(array(), array(
            'IBLOCK_ID' => IBLOCK_CATALOG_ID,
            'SECTION_ID' => $arProductDiscounts['SECTION_ID']
          ), false, false, array(
            'ID',
            'NAME'
          ));

          while($element = $elementRes->Fetch()) {
            $allDiscountProductsId[] = $element['ID'];
          }
        } elseif ($arProductDiscounts['GROUP_ID']) {
          if (\CSite::InGroup($arProductDiscounts)) {
            $allDiscountProductsId = array();
          }
        } elseif ($arProductDiscounts['CATALOG_GROUP_ID']) {
          $priceId = $arProductDiscounts['CATALOG_GROUP_ID'];
        }
      }
    }

    $allDiscountProductsId = array_unique($allDiscountProductsId);

    $filter = array(
      'ID' => $allDiscountProductsId
    );

    if ($priceId) {
      $filter['>CATALOG_PRICE_'.$priceId] = 0;
    }
    if (!empty($props)) {*/
    $filter['PROPERTY_SHOW_IN_MAIN'] = $props['SHOW_IN_MAIN'];
    /*}*/

    return $filter;
  }

  /**
   * Filter for reviews list
   *
   * @return array
   */
  public static function getReviewsFilter() {
    $filter = array(
      'AJAX' => $_REQUEST['AJAX'] == 'Y' ? 'Y' : 'N'
    );

    $reviewOf = isset($_REQUEST['REVIEW_OF']) ? intval($_REQUEST['REVIEW_OF']) : 0;

    if ($reviewOf > 0) {
      $filter['PROPERTY_REVIEW_OF'] = $reviewOf;
    }

    $filter[SORT_VARIABLE] = self::getReviewsSort();

    return $filter;
  }

  public static function getSort($acceptOrder, $acceptBy) {
    global $APPLICATION;
    $order = $acceptOrder[0];
    $by = $acceptBy[0];

    if (isset($_REQUEST[SORT_VARIABLE])) {
      $orderArr = explode('-', $_REQUEST[SORT_VARIABLE]);

      if (in_array(mb_strtoupper($orderArr[1]), $acceptBy)) {
        $by = $orderArr[1];
      }

      if (in_array(mb_strtoupper($orderArr[0]), $acceptOrder)) {
        $order = $orderArr[0];
      }

      $APPLICATION->set_cookie(SORT_VARIABLE.'_order', $order);
      $APPLICATION->set_cookie(SORT_VARIABLE.'_by', $by);
    } else {
      $order = $APPLICATION->get_cookie(SORT_VARIABLE.'_order');
      $by = $APPLICATION->get_cookie(SORT_VARIABLE.'_by');
    }

    return array(
      $order,
      $by
    );
  }

  public static function getSortList($order, $by, $prefix) {
    // Sort list
    $sortList = array();

    foreach ($order as $itemOrder) {
      foreach ($by as $itemBy) {
        $sortList[] = array(
          'CODE' => $itemOrder.'-'.$itemBy,
          'NAME' => $prefix.$itemOrder.'_'.$itemBy,
        );
      }
    }

    return $sortList;
  }

  public static function getReviewsSort() {
    return static::getSort(static::getReviewsAcceptOrder(), static::getReviewsAcceptBy());
  }

  public static function getReviewsSortList() {
    return static::getSortList(IBlockFilters::getReviewsAcceptOrder(), IBlockFilters::getReviewsAcceptBy(), 'REVIEWS_'.SORT_VARIABLE.'_');
  }

  public static function getReviewsAcceptOrder() {
    return array(
      'ID',
    );
  }

  public static function getReviewsAcceptBy() {
    return array(
      'ASC',
      'DESC'
    );
  }

  public static function getProductsSortList() {
    return static::getSortList(IBlockFilters::getProductsAcceptOrder(), IBlockFilters::getProductsAcceptBy(), 'PRODUCTS_'.SORT_VARIABLE.'_');
  }


  public static function getProductsSort() {
    return static::getSort(static::getProductsAcceptOrder(), static::getProductsAcceptBy());
  }

  public static function getProductsAcceptOrder() {
    return array(
      'PROPERTY_NEWPRODUCT',
      'PROPERTY_MINIMUM_PRICE',
      'SHOWS'
    );
  }

  public static function getProductsAcceptBy() {
    return array(
      'DESC',
      'ASC',
    );
  }
}
