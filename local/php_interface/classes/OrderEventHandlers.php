<?php

use \Bitrix\Sale\Delivery\ExtraServices\Manager;
use Bitrix\Sale;

CModule::includeModule('iblock');
CModule::includeModule('sale');

AddEventHandler("sale", "OnOrderSave", Array("CMwiOrder", "implodeDeliveryProps"));
AddEventHandler("sale", "OnOrderSave", Array("CMwiOrder", "customMailSaleNewOrder"));

class CMwiOrder
{
    function implodeDeliveryProps($orderID, $arFields)
    {
        if (!function_exists('getPropertyByCode')) {
            function getPropertyByCode($propertyCollection, $code)
            {
                foreach ($propertyCollection as $property) {
                    if ($property->getField('CODE') == $code)
                        return $property;
                }
            }
        }
        $order = Sale\Order::load($orderID);

        $propertyCollection = $order->getPropertyCollection();
        //проверяем заполненность свойства, где хранится улица
        $street = getPropertyByCode($propertyCollection, 'STREET');
        $streetValue = $street->getValue();

        $house = getPropertyByCode($propertyCollection, 'HOUSE');
        $houseValue = $house->getValue();

        $build = getPropertyByCode($propertyCollection, 'HOUSING');
        $buildValue = $build->getValue();

        $apartament = getPropertyByCode($propertyCollection, 'AP_NUMBER');
        $apartamentValue = $apartament->getValue();

        $location = getPropertyByCode($propertyCollection, 'LOCATION');
        $locationValue = $location->getViewHtml();

        $zip = getPropertyByCode($propertyCollection, 'ZIP');
        $zipValue = $zip->getValue();

        $deliveryTimeFrom = getPropertyByCode($propertyCollection, 'DELIVERY_TIME_FROM');
        $timeFromValue = $deliveryTimeFrom->getValue();

        $deliveryTimeTo = getPropertyByCode($propertyCollection, 'DELIVERY_TIME_TO');
        $timeToValue = $deliveryTimeTo->getValue();

        $propValue = $locationValue . ' ' . $streetValue . ' д ' . $houseValue . ' корп/стр ' . $buildValue . ' кв/офис ' . $apartamentValue . ' индекс ' . $zipValue . ' Время доставки с ' . $timeFromValue . ' до ' . $timeToValue;

        if (!empty($streetValue) || !empty($housevalue)) {
            $propCode = "ADDRESS";
            $arOrder = CSaleOrder::getList(
                array(),
                array("ID" => $orderID)
            )->fetch();
            $op = CSaleOrderProps::GetList(
                array(),
                array(
                    "PERSON_TYPE_ID" => $arOrder['PERSON_TYPE_ID'],
                    "CODE" => $propCode)
            )->Fetch();

            if ($op) {
                $arFields = array(
                    "ORDER_ID" => $orderID,
                    "ORDER_PROPS_ID" => 28,
                    "NAME" => 'Адрес доставки',
                    "CODE" => $propCode,
                    "VALUE" => preg_replace("/\"/", "", $propValue)
                );

                $dbOrderProp = CSaleOrderPropsValue::GetList(
                    array(),
                    array(
                        "ORDER_PROPS_ID" => 28,
                        "CODE" => $propCode,
                        "ORDER_ID" => $orderID
                    )
                );

                if ($existProp = $dbOrderProp->Fetch())
                    CSaleOrderPropsValue::Update($existProp["ID"], $arFields);
                else
                    CSaleOrderPropsValue::Add($arFields);
            }
        }
    }

    function customMailSaleNewOrder($id, $arFields, $arOrder)
    {

        if ($arOrder["CANCELED"] != "Y") {

            $event_id = "SALE_NEW_ORDER_CUSTOM_MAIL";

            CModule::IncludeModule("catalog");

//Имя
            if (isset($arOrder["PAYER_NAME"]) && !empty($arOrder["PAYER_NAME"])) {
                $name = $arOrder["PAYER_NAME"];
            } else {
                $name = $arOrder["ORDER_PROP"][2];
            }
//Email
            if (isset($arOrder["USER_EMAIL"]) && !empty($arOrder["USER_EMAIL"])) {
                $email = $arOrder["USER_EMAIL"];
                $email = $arOrder["USER_EMAIL"];
            } else {
                $email = $arOrder["ORDER_PROP"][1];
            }
//Телефон
            if (isset($arOrder["USER_PHONE"]) && !empty($arOrder["USER_PHONE"])) {
                $phone = $arOrder["USER_PHONE"];
            } else {
                $phone = $arOrder["ORDER_PROP"][4];
            }

            //Адрес доставки
            $address = $arOrder["ADDRESS"];

//Total price
            $price = $arFields["PRICE"];
            $delivery_id = $arFields["DELIVERY_ID"];
            $pay_system_id = $arFields["PAY_SYSTEM_ID"];

//Получаем название способа доставки
            $db_dtype = CSaleDelivery::GetList(
                array(
                    "SORT" => "ASC",
                    "NAME" => "ASC"
                ),
                array(),
                false,
                false,
                array("ID", "NAME")
            );
            $arDeliveries = array();
            while ($ar_dtype = $db_dtype->Fetch()) {
                $arDeliveries[$ar_dtype["ID"]] = $ar_dtype["NAME"];
            }
            $delivery_name = $arDeliveries[$delivery_id];

//Получаем название способа оплаты
            $db_ptype = CSalePaySystem::GetList(Array("SORT" => "ASC", "PSA_NAME" => "ASC"), Array("LID" => SITE_ID, "CURRENCY" => "RUB", "ACTIVE" => "Y"));
            $arPaySystems = array();
            while ($ar_paytype = $db_ptype->Fetch()) {
                $arPaySystems[$ar_paytype["ID"]] = $ar_paytype["NAME"];
            }
            $paysystem_name = $arPaySystems[$pay_system_id];
            $order = Sale\Order::load($id);

            $shipmentCollection = $order->getShipmentCollection();
            foreach ($shipmentCollection as $shipment) {

                if ($shipment->isSystem()) {
                    continue;
                }
                $extraServices = $shipment->getExtraServices();

            }

       /*     $extra_params = Bitrix\Sale\Delivery\ExtraServices\Manager::getExtraServicesList($delivery_id, false);
            $arResServices = array();
            foreach ($extraServices as $serviceId => $value) {
                if ($value == "Y") {
                    $arResServices[$serviceId]["NAME"] = $extra_params[$serviceId]["NAME"];
                }
            }*/

            $tableGoods = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0 0 23px; border-top: 1px dotted #969696;\">
                        <tr>
                            <td style = \"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">Название</td>
                            <td style = \"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">Количество</td>
                            <td style = \"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">Цена за шт.</td>
                            <td style = \"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">Вес</td>
                        </tr>";
            foreach ($arOrder["BASKET_ITEMS"] as $item) {
                $tableGoods .= "<tr>
                                                        <td style = \"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">
                                                            <div style=\"padding: 0 0 7px;\">" . $item["NAME"] . "</div>
                                                        </td>
                                                        <td style=\"border-bottom: 1px dotted #969696; padding: 20px 20px 20px 0;\">
                                                            <div style=\"font-size: 14px;\">" . $item["QUANTITY"] . "</div>
                                                        </td>
                                                        <td style=\"border-bottom: 1px dotted #969696; padding: 20px 0 20px 0;\">
                                                            <div style=\"font-size: 14px; color: #ff7e00;\">" . substr($item["PRICE"], 0, strlen($item["PRICE"]) - 5) . " руб.</div>
                                                        </td>
                                                         <td style=\"border-bottom: 1px dotted #969696; padding: 20px 0 20px 0;\">
                                                            <div style=\"font-size: 14px;\">" . $item["WEIGHT"] . " " . $item["PROPERTIES"]["WEIGHT"] . "</div>
                                                        </td>
                                                    </tr>";
            }
            $tableGoods .= "</table>";

            /*if (!empty($arResServices)) {
                $dop_uslugi = 'Доп. услуги:<br>';
                foreach ($arResServices as $arResService) {
                    $dop_uslugi .= $arResService["NAME"] . "<br>";
                }
            }*/
            // $arFields, $arOrder, $isNew
            //Проверяем на отмену заказа, если заказ отменен - сменяем шаблон
            /*if ($arOrder["CANCELED"] == "Y") {

                $event_id = "SALE_CANCEL_ORDER_CUSTOM_MAIL";
            } else {
                $event_id = "SALE_NEW_ORDER_CUSTOM_MAIL";
            }*/
            //Желаемая дата доставки
            $date = $arOrder["ORDER_PROP"][18];
            $arEventFields = array(
                'ORDER_ID' => $id,
                'USER_NAME' => $name,
                'USER_MAIL' => $email,
                'ADDRESS' => $address,
                'PRICE' => $price,
                'ORDER_LIST' => $tableGoods,
                'PAY_SYSTEM' => $paysystem_name,
                'DELIVERY' => $delivery_name,
                //'DOP_USLUGI' => $dop_uslugi,
                'DATE' => $date,
                'USER_PHONE' => $phone,
            );
            $res = CEvent::Send($event_id, SITE_ID, $arEventFields, 107);
            //mail('karpovich@mwi.me','test',$res);
        }
    }
}