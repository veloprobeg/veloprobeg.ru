<?

AddEventHandler("main", "OnBeforeUserRegister", array(
  "UserEventHandlers",
  "OnBeforeUserRegisterHandler"
));
AddEventHandler("main", "OnBeforeUserUpdate", array(
  "UserEventHandlers",
  "OnBeforeUserRegisterHandler"
));
AddEventHandler("main", "OnAfterUserUpdate", array(
  "UserEventHandlers",
  "OnAfterUserUpdateHandler"
));
AddEventHandler("main", "OnAfterUserLogin", array(
  "UserEventHandlers",
  "OnAfterUserLoginHandler"
));

class UserEventHandlers {
  public static function OnBeforeUserRegisterHandler(&$arFields) {
    if ($arFields['LOGIN'] != 'admin') {
      $arFields['LOGIN'] = $arFields['EMAIL'];
    }
  }

  public static function OnAfterUserLoginHandler(&$arFields) {
    define('PRELOADER', true);
  }

  public static function OnAfterUserUpdateHandler(&$arFields) {
    global $APPLICATION;

    if (!empty($arFields['UF_CITY'])) {
      $APPLICATION->set_cookie('CURRENT_CITY', $arFields['UF_CITY']);
    }
  }
}