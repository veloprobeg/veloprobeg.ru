<?php
class XMLexport
{
    private $dom;
    private $root;
    private $date;

    public function __construct()
    {
        $this->date = date("d:m:Y H:m:i");
        CModule::IncludeModule('iblock');
        $this->dom = new domDocument("1.0", "utf-8");
        $this->root = $this->dom->createElement("Выгрузка"); // Создаём корневой элемент
        $this->dom->appendChild($this->root);
        $this->root->setAttribute("Дата", $this->date);

    }

    public function exportUsers($resultFilePath){
        $arUsers = array();
        $filter = Array("GROUPS_ID" => Array(GROUP_BONUS__ID,GROUP_OLD__ID,GROUP_VIP__ID));
        $order = array('sort' => 'asc');
        $tmp = 'sort';
        $arParams = array("SELECT"=>array("UF_*"));
        $rsUsers = CUser::GetList($order,$tmp,$filter,$arParams);
        while ($arUser = $rsUsers->Fetch()) {
            $arUsers[$arUser["ID"]] = $arUser;
        }

        foreach ($arUsers as $usrId=>$arUser) {
            $user = $this->dom->createElement("Пользователь");
            if(!empty($arUser["XML_ID"])) {
                $user->setAttribute("GUID", $arUser["XML_ID"]);
            }

            //Имя
            $name = $this->dom->createElement("Имя");
            $name->setAttribute("type", "Строка");
            $name->setAttribute("Наименование", $arUser["NAME"]);
            $nameValue = $this->dom->createElement("Значение", $arUser["NAME"]);
            $user->appendChild($name);
            $name->appendChild($nameValue);

            //Фамилия
            $name = $this->dom->createElement("Фамилия");
            $name->setAttribute("type", "Строка");
            $name->setAttribute("Наименование", $arUser["LAST_NAME"]);
            $nameValue = $this->dom->createElement("Значение", $arUser["LAST_NAME"]);
            $user->appendChild($name);
            $name->appendChild($nameValue);

            //Отчество
            $name = $this->dom->createElement("Отчество");
            $name->setAttribute("type", "Строка");
            $name->setAttribute("Наименование", $arUser["SECOND_NAME"]);
            $nameValue = $this->dom->createElement("Значение", $arUser["SECOND_NAME"]);
            $user->appendChild($name);
            $name->appendChild($nameValue);

            //Количество баллов
            $name = $this->dom->createElement("Баллы");
            $name->setAttribute("type", "Число");
            $nameValue = $this->dom->createElement("Значение", $arUser["UF_LOGICTIM_BONUS"]);
            $user->appendChild($name);
            $name->appendChild($nameValue);

            $this->root->appendChild($user);
        }
        $this->dom->save($resultFilePath); // Сохраняем полученный XML-документ в файл*/
    }
}