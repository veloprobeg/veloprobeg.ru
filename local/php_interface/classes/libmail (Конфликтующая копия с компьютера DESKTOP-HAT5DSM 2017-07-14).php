<?php

namespace CakeLabs;

class Mail
{
  /**
   * массивы адресов кому отправить
   *
   * @var  array
   */

  protected $sendto = array();
  /**
   * @var  array
   */
  protected $cc = array();
  /**
   * @var  array
   */
  protected $bcc = array();
  /**
   * прикрепляемые файлы
   *
   * @var array
   */
  protected $attach = array();
  /**
   * массив заголовков
   *
   * @var array
   */
  protected $xHeaders = array();
  /**
   * приоритеты
   *
   * @var array
   */
  protected $priorities = array(
      '1 (Highest)',
      '2 (High)',
      '3 (Normal)',
      '4 (Low)',
      '5 (Lowest)'
  );
  /**
   * кодировка по умолчанию
   *
   * @var string
   */
  protected $charset = "UTF-8";
  protected $ctEncoding = "8bit";
  protected $receipt = 0;
  protected $text_html = "text/plain"; // формат письма. по умолчанию текстовый
  protected $smtpOn = false;    // отправка через smtp. по умолчанию выключена
  protected $namesEmail = array(); // имена для email адресов, чтобы делать вид ("Антон" <te@gg.ru>)
  protected $boundary = array(); // Идентификатор частей письма
  protected $headers = ''; // Заголовки
  protected $fullBody = '';
  protected $checkAddress;
  protected $smtpSendTo;
  protected $body; // Тело письма
  protected $smtpLog;
  protected $smtpServer;
  protected $smtpLogin;
  protected $smtpPassword;
  protected $smtpTimeout;
  protected $smtpPort;
  protected $strTo;
  protected $webiFilename;
  protected $acType;
  protected $adispo;
  protected $errors = array();

  /**
   * @param string $charset - Кодивровка письма
   */
  function __construct($charset = "")
  {
    $this->autoCheck(true);

    $this->boundary = "--" . md5(time());

    if ($charset != "") {
      $this->charset = strtolower($charset);
      if ($this->charset == "us-ascii") {
        $this->ctEncoding = "7bit";
      }
    }

    $this->errors = array();
  }

  /**
   * включение/выключение проверки валидности email
   * по умолчанию проверка включена
   *
   * @param bool $bool
   */
  function autoCheck($bool)
  {
    if ($bool) {
      $this->checkAddress = true;
    } else {
      $this->checkAddress = false;
    }
  }

  public static function OnBeforeMailSendHandler($mailParams)
  {
    $m = new \CakeLabs\Mail();
    $to = explode(',', $mailParams['TO']);

    foreach ($to as &$t) {
      $t = trim($t);
    }

    $userName = 'send-mail@it-rkomi.ru';
    $userPassword = '9wbZE5agZnjhxbAmehO6';
    $host = "ssl://smtp.yandex.ru";
    $from = 'send-mail@it-rkomi.ru';
    $port = 465;

    $m->From($from); // от кого отправляется почта
    $m->To($to); // кому адресованно
    $m->Subject($mailParams['SUBJECT'], true);
    $m->Body($mailParams['BODY'], $mailParams['CONTENT_TYPE']);
    $m->Priority(3); // приоритет письма
    $m->smtpOn($host, $userName, $userPassword, $port);

    if (is_array($mailParams['ATTACHMENT'])) {
      foreach ($mailParams['ATTACHMENT'] as $attach) {
        $m->Attach($attach['PATH']);
      }
    }

    $m->Send();

    \Debug::d($m->Get(), 'SMTP_LOG');
  }



  /**
   *
   * от кого
   *
   * @param string $from
   */
  function From($from)
  {
    if (!is_string($from)) {
      $this->errors[] = "Ошибка: From должен быть строкой";
    }

    $temp_mass = explode(';', $from); // разбиваем по разделителю для выделения имени

    if (count($temp_mass) == 2) { // если удалось разбить на два элемента
      $this->namesEmail['from'] = $temp_mass[0]; // имя первая часть
      $this->xHeaders['From'] = $temp_mass[1]; // адрес вторая часть
    } else { // и если имя не определено
      $this->namesEmail['from'] = '';
      $this->xHeaders['From'] = $from;
    }
  }

  /**
   * set the mail recipient
   *
   * @param string $to - Email address, accept both a single address or an array of addresses
   */
  function To($to)
  {
    // если это массив
    if (is_array($to)) {
      foreach ($to as $key => $value) { // перебираем массив и добавляем в массив для отправки через smtp
        $tempArray = explode(';', $value); // разбиваем по разделителю для выделения имени

        if (count($tempArray) == 2) { // если удалось разбить на два элемента
          $this->smtpSendTo[$tempArray[1]] = $tempArray[1]; // ключи и значения одинаковые, чтобы исключить дубли адресов
          $this->namesEmail['To'][$tempArray[1]] = $tempArray[0]; // имя первая часть
          $this->sendto[] = $tempArray[1];
        } else { // и если имя не определено
          $this->smtpSendTo[$value] = $value; // ключи и значения одинаковые, чтобы исключить дубли адресов
          $this->namesEmail['To'][$value] = ''; // имя первая часть
          $this->sendto[] = $value;
        }
      }
    } else {
      $tempArray = explode(';', $to); // разбиваем по разделителю для выделения имени

      if (count($tempArray) == 2) { // если удалось разбить на два элемента

        $this->sendto[] = $tempArray[1];
        $this->smtpSendTo[$tempArray[1]] = $tempArray[1]; // ключи и значения одинаковые, чтобы исключить дубли адресов
        $this->namesEmail['To'][$tempArray[1]] = $tempArray[0]; // имя первая часть
      } else { // и если имя не определено

        $this->sendto[] = $to;
        $this->smtpSendTo[$to] = $to; // ключи и значения одинаковые, чтобы исключить дубли адресов

        $this->namesEmail['To'][$to] = ''; // имя первая часть
      }
    }

    if ($this->checkAddress == true) {
      $this->CheckAddresses($this->sendto);
    }
  }

  /**
   * Тема письма
   *
   * @param string $subject
   * @param bool $encode
   */
  function Subject($subject, $encode = true)
  {
    if ($encode) {
      $this->xHeaders['Subject'] = "=?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($subject, "\r\n", "  ")))) . "?=";
    } else {
      $this->xHeaders['Subject'] = $subject;
    }
  }

  /**
   * Установка тела письма
   *
   * @param string $body - Тело письма
   * @param string $textHtml - В каком формате будет письмо, в тексте или html. по умолчанию стоит текст
   */
  function Body($body, $textHtml = "")
  {
    $this->body = $body;

    if ($textHtml == "html") {
      $this->text_html = "text/html";
    }
  }

  /**
   * set the mail priority
   * ex: $mail->Priority(1) ; => Highest
   *
   * @param int $priority - integer taken between 1 (highest) and 5 ( lowest )
   *
   * @return bool
   */
  function Priority($priority)
  {
    if (!intval($priority)) {
      return false;
    }

    if (!isset($this->priorities[$priority - 1])) {
      return false;
    }

    $this->xHeaders["X-Priority"] = $this->priorities[$priority - 1];

    return true;
  }

  /**
   * включение отправки через smtp используя сокеты
   * после запуска этой функции отправка через smtp включена
   * для отправки через защищенное соединение сервер нужно указывать с добавлением "ssl://" например так "ssl://smtp.gmail.com"
   *
   * @param string $smtpServer
   * @param string $login
   * @param string $pass
   * @param int $port
   * @param int $timeout
   */
  function smtpOn($smtpServer, $login, $pass, $port = 25, $timeout = 5)
  {
    $this->smtpOn = true; // включаем отправку через smtp

    $this->smtpServer = $smtpServer;
    $this->smtpLogin = $login;
    $this->smtpPassword = $pass;
    $this->smtpPort = $port;
    $this->smtpTimeout = $timeout;
  }

  /**
   * прикрепленные файлы
   *
   * @param string $filename - путь к файлу, который надо отправить
   * @param string $originalFilename - реальное имя файла. если вдруг вставляется файл временный, то его имя будет хрен пойми каким..
   * @param string $fileType - MIME-тип файла. по умолчанию 'application/x-unknown-content-type'
   * @param string $disposition - инструкция почтовому клиенту как отображать прикрепленный файл ("inline") как часть письма или ("attachment")
   *                                 как прикрепленный файл
   */
  function Attach($filename, $originalFilename = "", $fileType = "", $disposition = "inline")
  {
    // если типа файла не указан, ставим неизвестный тип
    if ($fileType == "") {
      $fileType = "application/x-unknown-content-type";
    }

    $this->attach[] = $filename;
    $this->webiFilename[] = $originalFilename;
    $this->acType[] = $fileType;
    $this->adispo[] = $disposition;
  }

  /**
   * отправка письма
   */
  function Send()
  {
    $this->BuildMail();
    $this->strTo = implode(", ", $this->sendto);

    if (!empty($this->errors)) {
      $this->smtpLog .= implode("\n", $this->errors);
      return false;
    }

    // если отправка без использования smtp
    if (!$this->smtpOn) {
      $res = @mail($this->strTo, $this->xHeaders['Subject'], $this->fullBody, $this->headers);
    } else { // если через smtp
      if (!$this->smtpServer OR !$this->smtpLogin OR !$this->smtpPassword OR !$this->smtpPort) {
        return false;
      } // если нет хотя бы одного из основных данных для коннекта, выходим с ошибкой

      // разбиваем (FROM - от кого) на юзера и домен. юзер понадобится в приветсвии с сервом
      $userDomain = explode('@', $this->xHeaders['From']);

      $this->smtpLog = '';
      $smtpConn = fsockopen($this->smtpServer, $this->smtpPort, $errno, $errstr, $this->smtpTimeout);

      if (!$smtpConn) {
        $this->smtpLog .= "соединение с сервером не прошло\n\n";
        fclose($smtpConn);

        return false;
      }

      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";

      fputs($smtpConn, "EHLO " . $userDomain[0] . "\r\n");
      $this->smtpLog .= "Я: EHLO " . $userDomain[0] . "\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
      $code = substr($data, 0, 3); // получаем код ответа

      if ($code != 250) {
        $this->smtpLog .= "ошибка приветсвия EHLO \n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, "AUTH LOGIN\r\n");
      $this->smtpLog .= "Я: AUTH LOGIN\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
      $code = substr($data, 0, 3);

      if ($code != 334) {
        $this->smtpLog .= "сервер не разрешил начать авторизацию \n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, base64_encode($this->smtpLogin) . "\r\n");
      $this->smtpLog .= "Я: " . base64_encode($this->smtpLogin) . "\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";

      $code = substr($data, 0, 3);
      if ($code != 334) {
        $this->smtpLog .= "ошибка доступа к такому юзеру\n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, base64_encode($this->smtpPassword) . "\r\n");
      $this->smtpLog .= "Я: " . base64_encode($this->smtpPassword) . "\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";

      $code = substr($data, 0, 3);
      if ($code != 235) {
        $this->smtpLog .= "не правильный пароль\n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, "MAIL FROM:<" . $this->xHeaders['From'] . "> SIZE=" . strlen($this->headers . "\r\n" . $this->fullBody) . "\r\n");
      $this->smtpLog .= "Я: MAIL FROM:<" . $this->xHeaders['From'] . "> SIZE=" . strlen($this->headers . "\r\n" . $this->fullBody) . "\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";

      $code = substr($data, 0, 3);
      if ($code != 250) {
        $this->smtpLog .= "сервер отказал в команде MAIL FROM\n";
        fclose($smtpConn);

        return false;
      }

      foreach ($this->smtpSendTo as $keywebi => $valuewebi) {
        fputs($smtpConn, "RCPT TO:<" . $valuewebi . ">\r\n");
        $this->smtpLog .= "Я: RCPT TO:<" . $valuewebi . ">\n";
        $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
        $code = substr($data, 0, 3);

        if ($code != 250 AND $code != 251) {
          $this->smtpLog .= "Сервер не принял команду RCPT TO\n";
          fclose($smtpConn);

          return false;
        }
      }

      fputs($smtpConn, "DATA\r\n");
      $this->smtpLog .= "Я: DATA\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
      $code = substr($data, 0, 3);

      if ($code != 354) {
        $this->smtpLog .= "сервер не принял DATA\n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, $this->headers . "\r\n" . $this->fullBody . "\r\n.\r\n");
      $this->smtpLog .= "Я: " . $this->headers . "\r\n" . $this->fullBody . "\r\n.\r\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
      $code = substr($data, 0, 3);

      if ($code != 250) {
        $this->smtpLog .= "ошибка отправки письма\n";
        fclose($smtpConn);

        return false;
      }

      fputs($smtpConn, "QUIT\r\n");
      $this->smtpLog .= "QUIT\r\n";
      $this->smtpLog .= $data = $this->getData($smtpConn) . "\n";
      fclose($smtpConn);
    }
  }

  /**
   * @param $smtpConn
   *
   * @return string
   */
  function getData($smtpConn)
  {
    $data = "";
    while ($str = fgets($smtpConn, 515)) {
      $data .= $str;
      if (substr($str, 3, 1) == " ") {
        break;
      }
    }

    return $data;
  }

  /**
   * на какой адрес отвечать
   *
   * @param string $address
   *
   * @return bool
   */
  function ReplyTo($address)
  {
    if (!is_string($address)) {
      return false;
    }

    $tempArray = explode(';', $address); // разбиваем по разделителю для выделения имени

    if (count($tempArray) == 2) { // если удалось разбить на два элемента
      $this->namesEmail['Reply-To'] = $tempArray[0]; // имя первая часть
      $this->xHeaders['Reply-To'] = $tempArray[1]; // адрес вторая часть
    } else { // и если имя не определено
      $this->namesEmail['Reply-To'] = '';
      $this->xHeaders['Reply-To'] = $address;
    }
  }

  /**
   * Добавление заголовка для получения уведомления о прочтении. обратный адрес берется из "From" (или из "ReplyTo" если указан)
   */
  function Receipt()
  {
    $this->receipt = 1;
  }

  /**
   * установка заголдовка CC (открытая копия, все получатели будут видеть куда ушла копия)
   *
   * @param string $cc - email address(es), accept both array and string
   */
  function Cc($cc)
  {
    if (is_array($cc)) {
      $this->cc = $cc;

      foreach ($cc as $key => $value) { // перебираем массив и добавляем в массив для отправки через smtp
        $this->smtpSendTo[$value] = $value; // ключи и значения одинаковые, чтобы исключить дубли адресов
      }
    } else {
      $this->cc[] = $cc;
      $this->smtpSendTo[$cc] = $cc; // ключи и значения одинаковые, чтобы исключить дубли адресов
    }

    if ($this->checkAddress == true) {
      $this->CheckAddresses($this->cc);
    }
  }

  /**
   *
   * проверка массива адресов
   *
   * @param array $addresses
   */
  function CheckAddresses($addresses)
  {
    for ($i = 0; $i < count($addresses); $i++) {
      if (!$this->ValidEmail($addresses[$i])) {
        $this->errors[] = "Ошибка: не верный email " . $addresses[$i];
      }
    }
  }

  /**
   * проверка мыла
   *
   * @param string $address
   *
   * @return bool
   */
  function ValidEmail($address)
  {
    // если существует современная функция фильтрации данных, то проверять будем этой функцией. появилась в php 5.2
    if (function_exists('filter_list')) {
      $validEmail = filter_var($address, FILTER_VALIDATE_EMAIL);

      if ($validEmail !== false) {
        return true;
      } else {
        return false;
      }
    } else { // а если php еще старой версии, то проверка валидности пойдет старым способом
      if (preg_match(".*<(.+)>", $address, $regs)) {
        $address = $regs[1];
      }

      if (preg_match("^[^@  ]+@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-]{2}|net|com|gov|mil|org|edu|int)\$", $address)) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Скрытая копия. Не будет помещать заголовок кому ушло письмо
   *
   * @param string $bcc - email address(es), accept both array and string
   */
  function Bcc($bcc)
  {
    if (is_array($bcc)) {
      $this->bcc = $bcc;
      foreach ($bcc as $key => $value) { // перебираем массив и добавляем в массив для отправки через smtp
        $this->smtpSendTo[$value] = $value; // ключи и значения одинаковые, чтобы исключить дубли адресов
      }
    } else {
      $this->bcc[] = $bcc;
      $this->smtpSendTo[$bcc] = $bcc; // ключи и значения одинаковые, чтобы исключить дубли адресов
    }

    if ($this->checkAddress == true) {
      $this->CheckAddresses($this->bcc);
    }
  }

  /**
   * Установка организации
   *
   * @param string $org - set the Organization header
   */
  function Organization($org)
  {
    if (trim($org != "")) {
      $this->xHeaders['Organization'] = $org;
    }
  }

  /**
   * Показывает что было отправлено
   */
  function Get()
  {
    if (isset($this->smtpLog)) {
      if ($this->smtpLog) {
        return $this->smtpLog; // если есть лог отправки smtp выведем его
      }
    }

    $this->BuildMail();
    $mail = $this->headers . "\n\n";
    $mail .= $this->fullBody;

    return $mail;
  }

  /**
   * Собираем письмо
   */
  function BuildMail()
  {
    $this->errors = array();
    $this->headers = "";

    $tempArray = array();
    // создание заголовка TO.
    // добавление имен к адресам
    foreach ($this->sendto as $key => $value) {
      if (strlen($this->namesEmail['To'][$value])) {
        $tempArray[] = "=?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($this->namesEmail['To'][$value], "\r\n", "  ")))) . "?= <" . $value . ">";
      } else {
        $tempArray[] = $value;
      }
    }

    $this->xHeaders['To'] = implode(", ", $tempArray); // этот заголовок будет не нужен при отправке через mail()

    if (count($this->cc) > 0) {
      $this->xHeaders['CC'] = implode(", ", $this->cc);
    }

    if (count($this->bcc) > 0) {
      $this->xHeaders['BCC'] = implode(", ", $this->bcc);
    }  // этот заголовок будет не нужен при отправке через smtp

    if ($this->receipt) {
      if (isset($this->xHeaders["Reply-To"])) {
        $this->xHeaders["Disposition-Notification-To"] = $this->xHeaders["Reply-To"];
      } else {
        $this->xHeaders["Disposition-Notification-To"] = $this->xHeaders['From'];
      }
    }

    if ($this->charset != "") {
      $this->xHeaders["Mime-Version"] = "1.0";
      $this->xHeaders["Content-Type"] = $this->text_html . "; charset=$this->charset";
      $this->xHeaders["Content-Transfer-Encoding"] = $this->ctEncoding;
    }

    $this->xHeaders["X-Mailer"] = "Bitrix V" . SM_VERSION;

    // вставаляем файлы
    if (count($this->attach) > 0) {
      $this->_buildAttachment();
    } else {
      $this->fullBody = $this->body;
    }

    // создание заголовков если отправка идет через smtp
    if ($this->smtpOn) {
      // разбиваем (FROM - от кого) на юзера и домен. домен понадобится в заголовке
      $userDomain = explode('@', $this->xHeaders['From']);

      $this->headers = "Date: " . date("D, j M Y G:i:s") . " +0300\r\n";
      $this->headers .= "Message-ID: <" . rand() . "." . date("YmjHis") . "@" . $userDomain[1] . ">\r\n";

      reset($this->xHeaders);
      while (list($hdr, $value) = each($this->xHeaders)) {
        if ($hdr == "From" and strlen($this->namesEmail['from'])) {
          $this->headers .= $hdr . ": =?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($this->namesEmail['from'], "\r\n", "  ")))) . "?= <" . $value . ">\r\n";
        } elseif ($hdr == "Reply-To" and strlen($this->namesEmail['Reply-To'])) {
          $this->headers .= $hdr . ": =?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($this->namesEmail['Reply-To'], "\r\n", "  ")))) . "?= <" . $value . ">\r\n";
        } elseif ($hdr != "BCC") {
          $this->headers .= $hdr . ": " . $value . "\r\n";
        } // пропускаем заголовок для отправки скрытой копии
      }
    } else { // создание заголовоков, если отправка идет через mail()
      reset($this->xHeaders);

      while (list($hdr, $value) = each($this->xHeaders)) {
        if ($hdr == "From" and strlen($this->namesEmail['from'])) {
          $this->headers .= $hdr . ": =?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($this->namesEmail['from'], "\r\n", "  ")))) . "?= <" . $value . ">\r\n";
        } elseif ($hdr == "Reply-To" and strlen($this->namesEmail['Reply-To'])) {
          $this->headers .= $hdr . ": =?" . $this->charset . "?Q?" . str_replace("+", "_", str_replace("%", "=", urlencode(strtr($this->namesEmail['Reply-To'], "\r\n", "  ")))) . "?= <" . $value . ">\r\n";
        } elseif ($hdr != "Subject" and $hdr != "To") {
          $this->headers .= "$hdr: $value\n";
        } // пропускаем заголовки кому и тему... они вставятся сами
      }
    }
  }

  /**
   * сборка файлов для отправки
   */
  function _buildAttachment()
  {
    $this->xHeaders["Content-Type"] = "multipart/mixed;\n boundary=\"$this->boundary\"";

    $this->fullBody = "This is a multi-part message in MIME format.\n--$this->boundary\n";
    $this->fullBody .= "Content-Type: " . $this->text_html . "; charset=$this->charset\nContent-Transfer-Encoding: $this->ctEncoding\n\n" . $this->body . "\n";

    $sep = chr(13) . chr(10);

    $ata = array();
    $k = 0;

    // перебираем файлы
    for ($i = 0; $i < count($this->attach); $i++) {
      $filename = $this->attach[$i];
      $originalFilename = $this->webiFilename[$i]; // имя файла, которое может приходить в класс, и имеет другое имя файла

      if (strlen($originalFilename)) {
        $basename = basename($originalFilename);
      } else { // если есть другое имя файла, то оно будет таким
        $basename = basename($filename);
      } // а если нет другого имени файла, то имя будет выдернуто из самого загружаемого файла

      $cType = $this->acType[$i]; // content-type
      $disposition = $this->adispo[$i];

      if (!file_exists($filename)) {
        $this->errors[] = "Ошибка прикрепления файла: файл $filename не существует";
      }

      $subHeader = "--$this->boundary\nContent-type: $cType;\n name=\"$basename\"\nContent-Transfer-Encoding: base64\nContent-Disposition: $disposition;\n  filename=\"$basename\"\n";
      $ata[$k++] = $subHeader;
      // non encoded line length
      $lineSZ = filesize($filename) + 1;
      $fp = fopen($filename, 'r');
      $ata[$k++] = chunk_split(base64_encode(fread($fp, $lineSZ)));
      fclose($fp);
    }

    $this->fullBody .= implode($sep, $ata);
  }
}

// class Mail

\CakeLabs\Mail::OnBeforeMailSendHandler(array(
  'TO' => 'test.tester@mail.ru',
  'SUBJECT' => 'TEST',
  'BODY' => "TESTER",
  'CONTENT_TYPE' => 'html'
));

?>


