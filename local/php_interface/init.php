<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
define('IBLOCK_NEWS_ID', 1);
define('IBLOCK_ACTIONS_ID', 7);
//define('IBLOCK_CATALOG_ID', 2);
define('IBLOCK_CATALOG_ID', 79);
define('IBLOCK_CATALOG_SKU_ID', 80);
define('IBLOCK_VACANCY_ID', 8);
define('IBLOCK_REVIEWS_ID', 9);
define('IBLOCK_APPLY_JOB_ID', 13);

define("HL_IBLOCK_VIDY_KART__ID",6);

define('DEFAULT_DELIVERY_TIME_FROM', '10:00');
define('DEFAULT_DELIVERY_TIME_TO', '20:00');

define('DELIVERY_COURIER_ID', 2);

define('SORT_VARIABLE', 'ORDER_BY');
define('BONUS_RATIO', 0.1);

define('GROUP_VIP__ID', 9);
define('GROUP_OLD__ID', 10);
define('GROUP_BONUS__ID', 11);
define('GROUP_PRODAZHI_BEFORE_10000', 12);
define('GROUP_PRODAZHI_FROM_10000', 13);
define('GROUP_PRODAZHI_FROM_500000', 14);

$debugPath = __DIR__.'/Debug.php';
if (file_exists($debugPath)) {
  include_once($debugPath);
}

// Class IBlock event handlers
//$iBlockEventHandlersPath = __DIR__.'/classes/IBlockEventHandlers.php';
//if (file_exists($iBlockEventHandlersPath)) {
//  include_once($iBlockEventHandlersPath);
//}
// Class XMLexport event handlers
$xmlexport = __DIR__.'/classes/XMLexport.php';
if (file_exists($xmlexport)) {
    include_once($xmlexport);
}

// Class IBlock filters
$iBlockFiltersPath = __DIR__.'/classes/IBlockFilters.php';
if (file_exists($iBlockFiltersPath)) {
  include_once($iBlockFiltersPath);
}

// Class User event handlers
$userEventHandlersPath = __DIR__.'/classes/UserEventHandlers.php';
if (file_exists($userEventHandlersPath)) {
  include_once($userEventHandlersPath);
}

// Class Order event handlers
$orderEventHandlersPath = __DIR__.'/classes/OrderEventHandlers.php';
if (file_exists($orderEventHandlersPath)) {
    include_once($orderEventHandlersPath);
}

// Class highload event handlers
$orderEventHandlersPath = __DIR__.'/classes/HighloadEventHandlers.php';
if (file_exists($orderEventHandlersPath)) {
    include_once($orderEventHandlersPath);
}

// Class Agents
$agentsEventHandlersPath = __DIR__.'/classes/Agents.php';
if (file_exists($agentsEventHandlersPath)) {
  include_once($agentsEventHandlersPath);
}

// Class send email from SMTP-server
$libMailPath = __DIR__.'/classes/libmail.php';
if (file_exists($libMailPath)) {
  include_once($libMailPath);
}

//region Custom mail
define('USE_SMTP_MAIL_SEND', true);
if (USE_SMTP_MAIL_SEND) {
  // See classes/libmail.php
  AddEventHandler("main", "OnBeforeMailSend", Array(
    "\CakeLabs\Mail",
    "OnBeforeMailSendHandler"
  ));
}
// Не переносить из init.php. Необходим для прерывания обычной отправки письма
if (!function_exists('custom_mail') && USE_SMTP_MAIL_SEND) {
  /**
   * @param string $to
   * @param string $subject
   * @param string $message
   * @param string $additional_headers
   * @param string $additional_parameters
   *
   * @return bool
   */
  function custom_mail($to, $subject, $message, $additional_headers, $additional_parameters) {
    // Send email
    return true;
  }
}
//endregion

AddEventHandler("catalog", "OnProductAdd","UpdateProductQuantity");
AddEventHandler("catalog", "OnProductUpdate","UpdateProductQuantity");

function UpdateProductQuantity($id, $arFields)
{   
    $quantity = $arFields['QUANTITY'];

    if (CModule::IncludeModule('catalog') && CModule::IncludeModule('iblock')) {
        $arProductInfo = CCatalogSKU::GetProductInfo($id);
        if (is_array($arProductInfo)) {
            $arOffersInfo = CCatalogSKU::GetInfoByProductIBlock($arProductInfo['IBLOCK_ID']);
            $arFilter = array(
                'IBLOCK_ID' => OFFERS_IBLOCK_ID,
                "PROPERTY_CML2_LINK" => $arProductInfo['ID'],
                "!ID" => $id,
            );

            $obOffersList = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, array("CATALOG_QUANTITY"));
            while ($arOffers = $obOffersList->Fetch()) {
                $quantity += $arOffers["CATALOG_QUANTITY"];
            }

            $arFieldsProduct = array(
                "QUANTITY" => $quantity,
            ); 
			if ( $quantity > 0 )
				$is_avalible = 1;
			else
				$is_avalible = 0;
				
			$arSetProps["IS_AVAILABLE"] = $is_avalible;
			
			CIBlockElement::SetPropertyValuesEx($arProductInfo['ID'], false, $arSetProps);
            CCatalogProduct::Update($arProductInfo['ID'], $arFieldsProduct);
        }
    }
}


AddEventHandler("main", "OnAfterUserRegister", "OnBeforeUserRegisterHandler");

function OnBeforeUserRegisterHandler(&$arFields)
{
    //создаём профиль
    //PERSON_TYPE_ID - идентификатор типа плательщика, для которого создаётся профиль
    if (!empty($arFields['UF_STREET'])) {
        $name = $arFields['UF_STREET'];
    }
    else {
        $name = "Профиль покупателя";
    }
    $arProfileFields = array(
        "NAME" => $name,
        "USER_ID" => $arFields['USER_ID'],
        "PERSON_TYPE_ID" => 1
    );
    $PROFILE_ID = CSaleOrderUserProps::Add($arProfileFields);

    //если профиль создан
    if ($PROFILE_ID) {
        //формируем массив свойств
        $PROPS = Array(
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 1,
                "NAME" => "Фамилия",
                "VALUE" => $arFields['LAST_NAME']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 2,
                "NAME" => "E-Mail",
                "VALUE" => $arFields['EMAIL']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 3,
                "NAME" => "Телефон",
                "VALUE" => $arFields['PERSONAL_PHONE']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 4,
                "NAME" => "Индекс",
                "VALUE" => $arFields['UF_INDEX']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 6,
                "NAME" => "Город",
                "VALUE" => $arFields['UF_CITY']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 7,
                "NAME" => "Улица",
                "VALUE" => $arFields['UF_STREET']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 20,
                "NAME" => "Имя",
                "VALUE" => $arFields['NAME']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 22,
                "NAME" => "Удобное время доставки с",
                "VALUE" => DEFAULT_DELIVERY_TIME_FROM
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 23,
                "NAME" => "Удобное время доставки до",
                "VALUE" => DEFAULT_DELIVERY_TIME_TO
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 25,
                "NAME" => "Дом",
                "VALUE" => $arFields['UF_HOUSE']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 26,
                "NAME" => "Корпус",
                "VALUE" => $arFields['UF_HOUSING']
            ),
            Array(
                "USER_PROPS_ID" => $PROFILE_ID,
                "ORDER_PROPS_ID" => 27,
                "NAME" => "Квартира / Офис",
                "VALUE" => $arFields['UF_AP_NUMBER']
            ),
        );
        //добавляем значения свойств к созданному ранее профилю
        foreach ($PROPS as $prop) {
            CSaleOrderUserPropsValue::Add($prop);
        }
    }
}


AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "BXIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "BXIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "BXIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "BXIBlockAfterSave");

function BXIBlockAfterSave($arg1, $arg2 = false) {
    $ELEMENT_ID = false;
    $IBLOCK_ID = false;
    $OFFERS_IBLOCK_ID = false;
    $OFFERS_PROPERTY_ID = false;
    CModule::IncludeModule('sale');
    CModule::IncludeModule('catalog');
    if (!class_exists('CCurrency')) return true;
    $CURRENCY = CCurrency::GetBaseCurrency();

    if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
    {
        $rsPriceElement = CIBlockElement::GetList(
            array(),
            array(
                "ID" => $arg2["PRODUCT_ID"],
            ),
            false,
            false,
            array("ID", "IBLOCK_ID")
        );
        if($arPriceElement = $rsPriceElement->Fetch())
        {
            $arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
            if(is_array($arCatalog))
            {
                if($arCatalog["OFFERS"] == "Y")
                {
                    $rsElement = CIBlockElement::GetProperty(
                        $arPriceElement["IBLOCK_ID"],
                        $arPriceElement["ID"],
                        "sort",
                        "asc",
                        array("ID" => $arCatalog["SKU_PROPERTY_ID"])
                    );
                    $arElement = $rsElement->Fetch();
                    if($arElement && $arElement["VALUE"] > 0)
                    {
                        $ELEMENT_ID = $arElement["VALUE"];
                        $IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
                        $OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
                        $OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
                    }
                }
                elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
                    $OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
                }
                else
                {
                    $ELEMENT_ID = $arPriceElement["ID"];
                    $IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
                    $OFFERS_IBLOCK_ID = false;
                    $OFFERS_PROPERTY_ID = false;
                }
            }
        }
    }    
    elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
    {
        $arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
        if(is_array($arOffers))
        {
            $ELEMENT_ID = $arg1["ID"];
            $IBLOCK_ID = $arg1["IBLOCK_ID"];
            $OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
            $OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
        }
    }

    if($ELEMENT_ID)
    {
        static $arPropCache = array();
        if(!array_key_exists($IBLOCK_ID, $arPropCache))
        {
            $rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
            $arProperty = $rsProperty->Fetch();
            if($arProperty)
                $arPropCache[$IBLOCK_ID] = $arProperty["ID"];
            else
                $arPropCache[$IBLOCK_ID] = false;
        }

        if($arPropCache[$IBLOCK_ID])
        {
            $arProductID = array($ELEMENT_ID);
            if($OFFERS_IBLOCK_ID)
            {
                $rsOffers = CIBlockElement::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => $OFFERS_IBLOCK_ID,
                        "PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
                    ),
                    false,
                    false,
                    array("ID")
                );
                while($arOffer = $rsOffers->Fetch())
                    $arProductID[] = $arOffer["ID"];
            }

            $minPrice = false;
            $maxPrice = false;

            $rsPrices = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $arProductID,
					"CATALOG_GROUP_ID" => Array(10, 11)
                )
            );
			$i = 0;
            while($arPrice = $rsPrices->Fetch())
            {
                if (!intval($arPrice["PRICE"])) continue;

				if ( $arPrice["CATALOG_GROUP_ID"] == 11 ){
					$PRICE = $arPrice["PRICE"];

					if($minPrice === false || $minPrice > $PRICE)
						$minPrice = $PRICE;

					if($maxPrice === false || $maxPrice < $PRICE)
						$maxPrice = $PRICE;
					$i++;
				}
				else {
					$PRICE_RRP = $arPrice["PRICE"];
				}
            }
			$DISCOUNT = "";
			if ( $PRICE_RRP > $minPrice ) {
				$OLD_PRICE = $PRICE_RRP;
				$DISCOUNT = 1;
			}

            if($minPrice !== false)
            {
                CIBlockElement::SetPropertyValuesEx(
                    $ELEMENT_ID,
                    $IBLOCK_ID,
                    array(
                        "MINIMUM_PRICE" =>  $minPrice,
                        "MAXIMUM_PRICE" => $maxPrice,
						"OLD_PRICE" => $OLD_PRICE,
						"DISCOUNT" => $DISCOUNT,
                    )
                );
            }
        }
    }
}


AddEventHandler("catalog", "OnSuccessCatalogImport1C", "Add1CAgent");

function Add1CAgent($arg1, $arg2 = false){
    CAgent::AddAgent("OnSuccessCatalogImport1C();",'','N',300);
}

function OnSuccessCatalogImport1C($arg1, $arg2 = false){

			CModule::IncludeModule('iblock');
			$arFilter = array(
                'IBLOCK_ID' => 79
            );

            $obElList = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, array("ID"));
            while ($arEl = $obElList->Fetch()) {
				$quantity = 0;
				
				$arFilter = array(
					'IBLOCK_ID' => IBLOCK_CATALOG_SKU_ID,
					"PROPERTY_CML2_LINK" => $arEl['ID'],
				);
				
				$obOffersList = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, array("CATALOG_QUANTITY"));
				while ($arOffers = $obOffersList->Fetch()) {
					$quantity += $arOffers["CATALOG_QUANTITY"];
				}
				
				if ( $quantity > 0 )
					$is_avalible = 1;
				else
					$is_avalible = 0;
					
				$arSetProps["IS_AVAILABLE"] = $is_avalible;
				
				CIBlockElement::SetPropertyValuesEx($arEl['ID'], false, $arSetProps);
				
            }
} 



/**
 * @return array - structure iBlocks with types
 */
function getStructureIBlocks() {
  $phpCache = new \CPHPCache();
  $iBlocks = array();

  if ($phpCache->InitCache(36000, 'iblocks')) {
    $vars = $phpCache->GetVars();
    $iBlocks = $vars['IBLOCKS'];
  } elseif ($phpCache->StartDataCache()) {
    $res = \CIBlock::GetList(array(), array());

    while ($iBLock = $res->Fetch()) {
      $iBlocks[$iBLock['IBLOCK_TYPE_ID']][$iBLock['ID']] = $iBLock;
    }

    $phpCache->EndDataCache(array(
      'IBLOCKS' => $iBlocks
    ));
  }

  return $iBlocks;
}

/**
 * Возвращает число для инициализации слайдера времени доставки
 *
 * @param string $time
 *
 * @return int
 */
function getTimeSliderJS($time) {
  if (is_numeric($time)) {
    return $time;
  }

  $timeSourceJS = explode(':', $time);

  $addTimeJS = 0;
  if ($timeSourceJS[1] == 30) {
    $addTimeJS++;
  }

  return $timeSourceJS[0] * 2 + $addTimeJS;
}

/**
 * *Как посчитать стоимость товара или предложения со всеми скидками
 *
 * @param int $item_id
 * @param string string $sale_currency
 * @return mixed
 */
function getFinalPriceInCurrency($item_id, $sale_currency = 'RUB') {
  CModule::IncludeModule("iblock");
  CModule::IncludeModule("catalog");
  CModule::IncludeModule("sale");
  global $USER;

  $currency_code = 'RUB';
  $final_price = false;

  // Проверяем, имеет ли товар торговые предложения?
  if (CCatalogSku::IsExistOffers($item_id)) {

    // Пытаемся найти цену среди торговых предложений
    $res = CIBlockElement::GetByID($item_id);

    if ($ar_res = $res->GetNext()) {
      if (isset($ar_res['IBLOCK_ID']) && $ar_res['IBLOCK_ID']) {
        // Ищем все тогровые предложения
        $offers = CIBlockPriceTools::GetOffersArray(array(
          'IBLOCK_ID' => $ar_res['IBLOCK_ID'],
          'HIDE_NOT_AVAILABLE' => 'Y',
          'CHECK_PERMISSIONS' => 'Y'
        ), array($item_id), null, null, null, null, null, null, array('CURRENCY_ID' => $sale_currency), $USER->getId(), null);

        foreach ($offers as $offer) {
          $price = CCatalogProduct::GetOptimalPrice($offer['ID'], 1, $USER->GetUserGroupArray(), 'N');

          if (isset($price['PRICE'])) {
            $final_price = $price['PRICE']['PRICE'];
            $currency_code = $price['PRICE']['CURRENCY'];

            // Ищем скидки и высчитываем стоимость с учетом найденных
            $arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, $USER->GetUserGroupArray(), "N");

            if (is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
              $final_price = CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
            }

            // Конец цикла, используем найденные значения
            break;
          }
        }
      }
    }
  } else {
    // Простой товар, без торговых предложений (для количества равному 1)
    $price = CCatalogProduct::GetOptimalPrice($item_id, 1, $USER->GetUserGroupArray(), 'N');

    // Получили цену?
    if (!$price || !isset($price['PRICE'])) {
      return false;
    }

    // Меняем код валюты, если нашли
    if (isset($price['CURRENCY'])) {
      $currency_code = $price['CURRENCY'];
    }

    if (isset($price['PRICE']['CURRENCY'])) {
      $currency_code = $price['PRICE']['CURRENCY'];
    }

    // Получаем итоговую цену
    $final_price = $price['PRICE']['PRICE'];

    // Ищем скидки и пересчитываем цену товара с их учетом
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, $USER->GetUserGroupArray(), "N", 2);

    if (is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
      $final_price = CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
    }
  }

  // Если необходимо, конвертируем в нужную валюту
  if ($currency_code != $sale_currency) {
    $final_price = CCurrencyRates::ConvertCurrency($final_price, $currency_code, $sale_currency);
  }

  return $final_price;
  return number_format($final_price, 2, '.', ' ');
}

/**
 * Функция меняет значения элементов массива $key и $key2 местами
 * @param array $array исходный массив
 * @param $key ключ элемента массива
 * @param $key2 ключ элемента массива
 * @return bool true замена произошла, false замена не произошла
 */
function array_swap(array &$array, $key, $key2)
{
    if (isset($array[$key]) && isset($array[$key2])) {
        list($array[$key], $array[$key2]) = array($array[$key2], $array[$key]);
        return true;
    }

    return false;
}

/**
 * Функция выводит данные $data в тег <pre> с классом $class, значение css свойства display которого равно $display
 * @param $data Выводимые данные
 * @param string $class Класс блока
 * @param string $display Значение css свойства
 */
function printData($data, $class = 'test', $display = 'none')
{
    echo "<pre class='$class' style='display:$display'>";
    print_r($data);
    echo '</pre>';
}

function dump($arr){
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}
/**
 * @param string $str
 * @return string
 * Обработка входящих данных из POST
 */
function post_data_processing($str){
    return htmlspecialcharsbx(trim($str));
}