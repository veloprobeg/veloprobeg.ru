(function (window) {

  if (!!window.JCCatalogCompareList) {
    return;
  }

  window.JCCatalogCompareList = function (params) {
    this.obCompare = null;
    this.obAdminPanel = null;
    this.visual = params.VISUAL;
    this.ajax = params.AJAX;

    BX.ready(BX.proxy(this.init, this));
  };

  window.JCCatalogCompareList.prototype.init = function () {
    this.obCompare = BX(this.visual.ID);
    if (!!this.obCompare) {
      BX.addCustomEvent(window, "OnCompareChange", BX.proxy(this.reload, this));
    }
  };

  window.JCCatalogCompareList.prototype.reload = function () {
    BX.showWait(this.obCompare);
    BX.ajax.post(
      this.ajax.url,
      this.ajax.params,
      BX.proxy(this.reloadResult, this)
    );
  };

  window.JCCatalogCompareList.prototype.reloadResult = function (result) {
    BX.closeWait();
    this.obCompare.innerHTML = result;
    var link = BX.findChildren(this.obCompare, {
      tagName: 'A'
    });

    if (!!link && link.length > 0) {
      BX.style(this.obCompare, 'display', 'flex');
    } else {
      BX.style(this.obCompare, 'display', 'none');
    }
  };

})(window);