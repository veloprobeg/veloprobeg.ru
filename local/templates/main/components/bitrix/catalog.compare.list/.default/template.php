<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$itemCount = count($arResult);
$isAjax = (isset($_REQUEST["ajax_action"]) && $_REQUEST["ajax_action"] == "Y");
$idCompareCount = 'compareList'.$this->randString();
$obCompare = 'ob'.$idCompareCount;

use \Bitrix\Main\Localization\Loc;

?>
<div class="header__compare" id="<? echo $idCompareCount; ?>"<?= $itemCount == 0 ? ' style="display:none;"' : '' ?>>
  <?
  if ($isAjax) {
    $APPLICATION->RestartBuffer();
  }
  $frame = $this->createFrame($idCompareCount)->begin('');
  if ($itemCount > 0) {
    ?>
    <a href="<? echo $arParams["COMPARE_URL"]; ?>" title="<?= Loc::getMessage("CP_BCCL_TPL_MESS_COMPARE_PAGE"); ?>"
       class="header__compare-link"><?= Loc::getMessage("CP_BCCL_TPL_MESS_COMPARE_COUNT", array(
        '#COUNT#' => $itemCount
      )); ?></a>
  <? } ?>
  <?
  $frame->end();
  if ($isAjax) {
    die();
  }
  $currentPath = CHTTP::urlDeleteParams(
    $APPLICATION->GetCurPageParam(),
    array(
      $arParams['PRODUCT_ID_VARIABLE'],
      $arParams['ACTION_VARIABLE'],
      'ajax_action'
    ),
    array("delete_system_params" => true)
  );

  $jsParams = array(
    'VISUAL' => array(
      'ID' => $idCompareCount,
    ),
    'AJAX' => array(
      'url' => $currentPath,
      'params' => array(
        'ajax_action' => 'Y'
      )
    )
  );
  ?>
</div>
<script type="text/javascript">
  var <? echo $obCompare; ?> =
  new JCCatalogCompareList(<? echo CUtil::PhpToJSObject($jsParams, false, true); ?>)
</script>
