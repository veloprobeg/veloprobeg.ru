<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
  $templateLibrary[] = 'currency';
  $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
  'TEMPLATE_LIBRARY' => $templateLibrary,
  'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
  'ID' => $strMainID,
  'PICT' => $strMainID.'_pict',
  'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
  'STICKER_ID' => $strMainID.'_sticker',
  'BIG_SLIDER_ID' => $strMainID.'_big_slider',
  'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
  'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
  'SLIDER_LIST' => $strMainID.'_slider_list',
  'SLIDER_LEFT' => $strMainID.'_slider_left',
  'SLIDER_RIGHT' => $strMainID.'_slider_right',
  'OLD_PRICE' => $strMainID.'_old_price',
  'PRICE' => $strMainID.'_price',
  'BONUSES' => $strMainID.'_bonuses',
  'DISCOUNT_PRICE' => $strMainID.'_price_discount',
  'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
  'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
  'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
  'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
  'QUANTITY' => $strMainID.'_quantity',
  'QUANTITY_DOWN' => $strMainID.'_quant_down',
  'QUANTITY_UP' => $strMainID.'_quant_up',
  'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
  'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
  'BASIS_PRICE' => $strMainID.'_basis_price',
  'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
  'PROP' => $strMainID.'_prop_',
  'PROP_DIV' => $strMainID.'_skudiv',
  'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
  'OFFER_GROUP' => $strMainID.'_set_group_',
  'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
  'ONE_CLICK_ID' => $strMainID.'_one_click_id',
  'ONE_CLICK_NAME_ID' => $strMainID.'_one_click_name',
  'ONE_CLICK_EMAIL_ID' => $strMainID.'_one_click_email',
  'ONE_CLICK_PHONE_ID' => $strMainID.'_one_click_phone',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
  ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
  : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
  ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
  : $arResult['NAME']
);
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
  foreach ($arResult['SKU_PROPS'] as &$arProp) {
    if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']])) {
      continue;
    }
    $arSkuProps[] = array(
      'ID' => $arProp['ID'],
      'CODE' => $arProp['CODE'],
      'SHOW_MODE' => $arProp['SHOW_MODE'],
      'VALUES_COUNT' => $arProp['VALUES_COUNT']
    );
  }

  foreach ($arResult['JS_OFFERS'] as &$arOneJS) {
    if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE']) {
      $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
      $arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
    }
    $strProps = '';
    if ($arResult['SHOW_OFFERS_PROPS']) {
      if (!empty($arOneJS['DISPLAY_PROPERTIES'])) {
        foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp) {
          $strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
            is_array($arOneProp['VALUE'])
              ? implode(' / ', $arOneProp['VALUE'])
              : $arOneProp['VALUE']
            ).'</dd>';
        }
      }
    }
    $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
  }
  if (isset($arOneJS)) {
    unset($arOneJS);
  }
  $arJSParams = array(
    'CONFIG' => array(
      'USE_CATALOG' => $arResult['CATALOG'],
      'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
      'SHOW_PRICE' => true,
      'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
      'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
      'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
      'OFFER_GROUP' => false,
      ///$arResult['OFFER_GROUP'],
      'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
      'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
      'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
      'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
      'USE_STICKERS' => true,
    ),
    'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    'ONE_CLICK_URL' => '/local/tools/ajax/get_one_click_product.php',
    'VISUAL' => array(
      'ID' => $arItemIDs['ID'],
    ),
    //'DEFAULT_PICTURE' => array(
    //  'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
    //  'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
    //),
    'PRODUCT' => array(
      'ID' => $arResult['ID'],
      'NAME' => $arResult['~NAME']
    ),
    'BASKET' => array(
      'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
      'BASKET_URL' => $arParams['BASKET_URL'],
      'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
      'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
      'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
    ),
    'OFFERS' => $arResult['JS_OFFERS'],
    'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
    'TREE_PROPS' => $arSkuProps,
    'ONE_CLICK_NAME' => $arItemIDs['ONE_CLICK_NAME_ID'],
    'ONE_CLICK_EMAIL' => $arItemIDs['ONE_CLICK_EMAIL_ID'],
    'ONE_CLICK_PHONE' => $arItemIDs['ONE_CLICK_PHONE_ID'],
  );
} else {
  $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
  if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
    ?>
    <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
      <?
      if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])) {
        foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
          ?>
          <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                 value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
          <?
          if (isset($arResult['PRODUCT_PROPERTIES'][$propID])) {
            unset($arResult['PRODUCT_PROPERTIES'][$propID]);
          }
        }
      }
      $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
      if (!$emptyProductProperties) {
        ?>
        <table>
          <?
          foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
            ?>
            <tr>
              <td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
              <td>
                <?
                if (
                  'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
                  && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
                ) {
                  foreach ($propInfo['VALUES'] as $valueID => $value) {
                    ?><label><input type="radio"
                                    name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                                    value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                    </label><br><?
                  }
                } else {
                  ?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                  foreach ($propInfo['VALUES'] as $valueID => $value) {
                    ?>
                    <option
                    value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
                  }
                  ?></select><?
                }
                ?>
              </td>
            </tr>
            <?
          }
          ?>
        </table>
        <?
      }
      ?>
    </div>
    <?
  }
  if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE']) {
    $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
    $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
  }
  $arJSParams = array(
    'CONFIG' => array(
      'USE_CATALOG' => $arResult['CATALOG'],
      'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
      'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
      'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
      'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
      'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
      'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
      'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
      'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
      'USE_STICKERS' => true,
    ),
    'VISUAL' => array(
      'ID' => $arItemIDs['ID'],
    ),
    'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    'ONE_CLICK_URL' => '/local/tools/ajax/get_one_click_product.php',
    'PRODUCT' => array(
      'ID' => $arResult['ID'],
      'PICT' => $arFirstPhoto,
      'NAME' => $arResult['~NAME'],
      'PRICE' => $arResult['MIN_PRICE'],
      'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
      'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
      'SLIDER' => $arResult['MORE_PHOTO'],
      'CAN_BUY' => $arResult['CAN_BUY'],
      'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
      'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
      'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
      'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
    ),
    'BASKET' => array(
      'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
      'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
      'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
      'EMPTY_PROPS' => $emptyProductProperties,
      'BASKET_URL' => $arParams['BASKET_URL'],
      'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
      'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
    ),
    'ONE_CLICK_NAME' => $arItemIDs['ONE_CLICK_NAME_ID'],
    'ONE_CLICK_EMAIL' => $arItemIDs['ONE_CLICK_EMAIL_ID'],
    'ONE_CLICK_PHONE' => $arItemIDs['ONE_CLICK_PHONE_ID'],
  );
  unset($emptyProductProperties);
}

$minPrice = false;
if (isset($arResult['MIN_PRICE']) || isset($arResult['RATIO_PRICE'])) {
  $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
}

$isDiscount = $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'];
$isNew = !empty($arResult['PROPERTIES']['NEWPRODUCT']['VALUE']);

$productClass = '';
if ($isDiscount) {
  $productClass .= ' card--sale';
}

if ($isHid) {
  $productClass .= ' card--hit';
}

if ($isNew) {
  $productClass .= ' card--new';
}

?>
  <script src="<?= $templateFolder.'/script.js' ?>"></script>
  <div class="popup__card" id="<? echo $arItemIDs['ID']; ?>">
    <div class="popup__card-main">
      <h3 class="popup__card-name"><?= $arResult['NAME'] ?></h3>
	  <img id="<?= $arItemIDs['PICT'] ?>" style="display: none;">
      <div class="popup__card-image"><img src="<?= $arResult['PICTURE']['SRC'] ?>" 
                                          alt=""></div>
    </div>
    <div class="popup__card-sub" id="<? echo $arItemIDs['PROP_DIV']; ?>">
      <? if ($arResult['COLOR_PROP']): ?>
        <div class="popup__card-types"><?= $arResult['COLOR_PROP']['NAME'] ?>
          <?
          $i = 0;
          foreach ($arResult['COLOR_PROP']['VALUES'] as $k => $value):
            if ($value['ID'] == 0) {
              continue;
            }
            ?>
            <span
                class="popup__card-type"><?= $value['NAME'] ?><?= count($arResult['COLOR_PROP']['VALUES']) > $i + 1 ? '/' : '' ?></span>
            <?
            $i++;
          endforeach ?>
        </div>

        <div class="popup__card-switchs" id="<?= $arItemIDs['PROP'].$arResult['COLOR_PROP']['ID'].'_list' ?>">
          <!--image 55x55-->
          <?
          foreach ($arResult['COLOR_PROP']['VALUES'] as $k => $value):
            if ($value['ID'] == 0) {
              continue;
            }
            ?>
            <a href="javascript:void(0)" style="background-image: url(<?= $value['PICT']['SRC'] ?>)"
               class="popup__card-switcher" data-onevalue="<?= $value['ID'] ?>"
               data-treevalue="<?= $arResult['COLOR_PROP']['ID'].'_'.$value['ID'] ?>">
              <input type="checkbox" name="color" value="red" class="popup__card-switcher-value"></a>
          <? endforeach ?>
        </div>
      <? endif ?>
      <div class="popup__card-sizes-wrapper">
        <?
        foreach ($arResult['SKU_PROPS'] as &$arProp):
          if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]) || $arProp['CODE'] == 'COLOR_REF') {
            continue;
          }
          ?>
          <div class="popup__card-sizes-head"><?= $arProp['NAME'] ?></div>
          <div class="popup__card-sizes" id="<?= $arItemIDs['PROP'].$arProp['ID'].'_list' ?>">
            <? foreach ($arProp['VALUES'] as $value):
              if ($value['NAME'] == '-') {
                continue;
              }
              ?>
              <div class="popup__card-size">
                <a href="javascript:void(0)" class="popup__card-size-link" data-onevalue="<?= $value['ID'] ?>"
                   data-treevalue="<?= $arProp['ID'].'_'.$value['ID'] ?>"><?= $value['NAME'] ?>
                  <input type="checkbox" value="m" name="size" class="popup__card-size-value"></a>
                <div class="popup__card-size-count">
                  <div class="popup__card-size-count-pin is-active"></div>
                  <div class="popup__card-size-count-pin is-active"></div>
                  <div class="popup__card-size-count-pin is-active"></div>
                  <div class="popup__card-size-count-pin"></div>
                  <div class="popup__card-size-count-pin"></div>
                </div>
                <div class="popup__card-size-desc">в наличии</div>
              </div>
            <? endforeach ?>
          </div>
        <? endforeach ?>

        <input type="text" class="default-input default-input--normal" name="<?= $arItemIDs['ONE_CLICK_NAME_ID'] ?>" id="<?= $arItemIDs['ONE_CLICK_NAME_ID'] ?>"
               value="" placeholder="Имя">
        <input type="text" class="default-input default-input--normal mask-phone" name="<?= $arItemIDs['ONE_CLICK_PHONE_ID'] ?>" id="<?= $arItemIDs['ONE_CLICK_PHONE_ID'] ?>"
               value="" placeholder="Телефон">
		
		<textarea class="default-input default-input--normal" name="text" placeholder="Комментарий"></textarea>
			   
        <? $isViewSecondPrice = ('Y' == $arParams['SHOW_OLD_PRICE'] && $isDiscount); ?>
        <div class="popup__card-get<?= $isViewSecondPrice ? ' card__about--disc' : '' ?>">
          <div class="popup__card-price">
            <? if ($isViewSecondPrice): ?>
              <div class="popup__card-disc" id="<?= $arItemIDs['OLD_PRICE']; ?>"><?= $minPrice['PRINT_VALUE'] ?></div>
            <? endif ?>
            <div class="popup__card-cost"
                 id="<?= $arItemIDs['PRICE']; ?>"><?= $minPrice['PRINT_DISCOUNT_VALUE'] ?></div>
			<?/*
		   <div class="popup__card-bonus"
                 id="<?= $arItemIDs['BONUSES']; ?>"><?= round($minPrice['DISCOUNT_VALUE'] * $arParams['BONUS_RATIO']) ?>
              бонусов
            </div>
			*/?>
          </div>

          <button type="submit" class="button button--orange popup__card-submit"
                  id="<?= $arItemIDs['ONE_CLICK_ID'] ?>" onclick="return false;">
            Купить
          </button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    BX.ready(function () {
      var <? echo $strObName; ?> =
      new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
      console.log(' <? echo $strObName; ?>:',  <? echo $strObName; ?>);
      $('.mask-phone').mask('+7 (999) - 999 - 99 - 99');
    });
    BX.message({
      ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
      BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
      TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
      TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
      BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
      BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
      BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
      BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
      BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
      TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
      PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
      SITE_ID: '<? echo SITE_ID; ?>'
    });
  </script>
<? \Debug::lm($arResult['OFFERS'][0], 'FIRST_OFFER'); ?>
<? \Debug::dtc($arResult['OFFERS_PROP'], 'OFFERS_PROP'); ?>
<? //\Debug::d($arResult, 'arResult'); ?>