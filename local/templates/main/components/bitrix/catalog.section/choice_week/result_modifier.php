<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

if (!empty($arResult['ITEMS'])) {
  $arEmptyPreview = false;
  $strEmptyPreview = $this->GetFolder().'/images/no_photo.png';

  if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview)) {
    $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);

    if (!empty($arSizes)) {
      $arEmptyPreview = array(
        'SRC' => $strEmptyPreview,
        'WIDTH' => intval($arSizes[0]),
        'HEIGHT' => intval($arSizes[1])
      );
    }

    unset($arSizes);
  }

  unset($strEmptyPreview);

  $strBaseCurrency = '';
  $boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

  $arNewItemsList = array();
  foreach ($arResult['ITEMS'] as $key => $arItem) {
    $arItem['CHECK_QUANTITY'] = false;
	
	foreach ($arItem['OFFERS'] as &$arOffer) {
  
		if ( is_array($arOffer["PREVIEW_PICTURE"]) ){
			$arItem['PREVIEW_PICTURE'] = $arOffer["PREVIEW_PICTURE"];
			continue;
		}
		
	}
	
	
	

    if (!isset($arItem['CATALOG_MEASURE_RATIO'])) {
      $arItem['CATALOG_MEASURE_RATIO'] = 1;
    }

    if (!isset($arItem['CATALOG_QUANTITY'])) {
      $arItem['CATALOG_QUANTITY'] = 0;
    }

    $arItem['CATALOG_QUANTITY'] = (
    0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
      ? floatval($arItem['CATALOG_QUANTITY'])
      : intval($arItem['CATALOG_QUANTITY'])
    );

    $arItem['CATALOG'] = false;

    if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION']) {
      $arItem['CATALOG_SUBSCRIPTION'] = 'N';
    }

    if ($arResult['MODULES']['catalog']) {
      $arItem['CATALOG'] = true;

      if (!isset($arItem['CATALOG_TYPE'])) {
        $arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
      }

      if ((CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE']) && !empty($arItem['OFFERS'])) {
        $arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
      }

      switch ($arItem['CATALOG_TYPE']) {
        case CCatalogProduct::TYPE_SET:
          $arItem['OFFERS'] = array();
          $arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
          break;
        case CCatalogProduct::TYPE_SKU:
          break;
        case CCatalogProduct::TYPE_PRODUCT:
        default:
          $arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
          break;
      }
    } else {
      $arItem['CATALOG_TYPE'] = 0;
      $arItem['OFFERS'] = array();
    }

    if ($arItem['CATALOG'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
      $arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromOffers(
        $arItem['OFFERS'],
        $boolConvert ? $arResult['CONVERT_CURRENCY']['CURRENCY_ID'] : $strBaseCurrency
      );
    }

    if (!empty($arItem['DISPLAY_PROPERTIES'])) {
      foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp) {
        if ('F' == $arDispProp['PROPERTY_TYPE']) {
          unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
        }
      }
    }

    $arItem['LAST_ELEMENT'] = 'N';
    $arNewItemsList[$key] = $arItem;
  }

  $arResult['ITEMS'] = $arNewItemsList;
  $arResult['DEFAULT_PICTURE'] = $arEmptyPreview;
}

foreach ($arResult["ITEMS"] as &$arItem) {
  $srcPic = false;

  if (is_array($arItem['PREVIEW_PICTURE'])) {
    $srcPic = $arItem['PREVIEW_PICTURE'];
  } elseif (is_array($arItem['DETAIL_PICTURE'])) {
    $srcPic = $arItem['DETAIL_PICTURE'];
  } elseif (is_array($arEmptyPreview)) {
    $srcPic = $arEmptyPreview;
  }

  if ($srcPic['ID']) {
    $resPic = CFile::ResizeImageGet($srcPic['ID'], array(
      'height' => $arParams['IMAGE_HEIGHT'],
      'width' => $arParams['IMAGE_WIDTH']
    ), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);

    if ($resPic) {
      $arItem['PREVIEW_PICTURE']['SRC'] = $resPic['src'];
      $arItem['PREVIEW_PICTURE']['WIDTH'] = $resPic['width'];
      $arItem['PREVIEW_PICTURE']['HEIGHT'] = $resPic['height'];
    }
  }
}