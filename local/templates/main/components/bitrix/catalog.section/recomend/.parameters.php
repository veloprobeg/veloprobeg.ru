<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters['IMAGE_WIDTH'] = array(
    'NAME' => GetMessage('CP_BCS_TPL_IMAGE_WIDTH'),
    'TYPE' => 'STRING',
    'DEFAULT' => GetMessage('CP_BCS_TPL_IMAGE_WIDTH_DEFAULT')
);
$arTemplateParameters['IMAGE_HEIGHT'] = array(
    'NAME' => GetMessage('CP_BCS_TPL_IMAGE_HEIGHT'),
    'TYPE' => 'STRING',
    'DEFAULT' => GetMessage('CP_BCS_TPL_IMAGE_HEIGHT_DEFAULT')
);
?>