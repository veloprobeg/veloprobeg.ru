<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
    ?>
    <div class="spec">
        <div class="spec__header"><?= Loc::getMessage("CT_BCS_SPECIAL"); ?></div>
        <div class="spec__items">
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="spec__item"
                   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div style="background-image: url('<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>')"
                         class="spec__image"></div>
                    <div class="spec__text"><?= Loc::getMessage("CT_BCS_PRODUCT_ON_STOCK", array(
                            '#PRODUCT_NAME#' => $arItem['NAME']
                        )); ?></div>
                </a>
            <? endforeach ?>
        </div>
        <a href="/catalog/veloprobeg_ru/filter/specialoffer-is-have/apply/" class="spec__more"><?= Loc::getMessage("CT_BCS_VIEW_ALL_PRODUCTS"); ?></a>
    </div>
    <?
} ?>
