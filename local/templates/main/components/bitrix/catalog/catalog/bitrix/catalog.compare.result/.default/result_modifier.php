<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
use Bitrix\Main\Type\Collection;

$arResult['ALL_FIELDS'] = array();
$existShow = !empty($arResult['SHOW_FIELDS']);
$existDelete = !empty($arResult['DELETED_FIELDS']);
if ($existShow || $existDelete) {
  if ($existShow) {
    foreach ($arResult['SHOW_FIELDS'] as $propCode) {
      $arResult['SHOW_FIELDS'][$propCode] = array(
        'CODE' => $propCode,
        'IS_DELETED' => 'N',
        'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_FIELD_TEMPLATE']),
        'SORT' => $arResult['FIELDS_SORT'][$propCode]
      );
    }
    unset($propCode);
    $arResult['ALL_FIELDS'] = $arResult['SHOW_FIELDS'];
  }
  if ($existDelete) {
    foreach ($arResult['DELETED_FIELDS'] as $propCode) {
      $arResult['ALL_FIELDS'][$propCode] = array(
        'CODE' => $propCode,
        'IS_DELETED' => 'Y',
        'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_FIELD_TEMPLATE']),
        'SORT' => $arResult['FIELDS_SORT'][$propCode]
      );
    }
    unset($propCode, $arResult['DELETED_FIELDS']);
  }
  Collection::sortByColumn($arResult['ALL_FIELDS'], array('SORT' => SORT_ASC));
}

$arResult['ALL_PROPERTIES'] = array();
$existShow = !empty($arResult['SHOW_PROPERTIES']);
$existDelete = !empty($arResult['DELETED_PROPERTIES']);
if ($existShow || $existDelete) {
  if ($existShow) {
    foreach ($arResult['SHOW_PROPERTIES'] as $propCode => $arProp) {
      $arResult['SHOW_PROPERTIES'][$propCode]['IS_DELETED'] = 'N';
      $arResult['SHOW_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_PROPERTY_TEMPLATE']);
    }
    $arResult['ALL_PROPERTIES'] = $arResult['SHOW_PROPERTIES'];
  }
  unset($arProp, $propCode);
  if ($existDelete) {
    foreach ($arResult['DELETED_PROPERTIES'] as $propCode => $arProp) {
      $arResult['DELETED_PROPERTIES'][$propCode]['IS_DELETED'] = 'Y';
      $arResult['DELETED_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_PROPERTY_TEMPLATE']);
      $arResult['ALL_PROPERTIES'][$propCode] = $arResult['DELETED_PROPERTIES'][$propCode];
    }
    unset($arProp, $propCode, $arResult['DELETED_PROPERTIES']);
  }
  Collection::sortByColumn($arResult["ALL_PROPERTIES"], array(
    'SORT' => SORT_ASC,
    'ID' => SORT_ASC
  ));
}

$arResult["ALL_OFFER_FIELDS"] = array();
$existShow = !empty($arResult["SHOW_OFFER_FIELDS"]);
$existDelete = !empty($arResult["DELETED_OFFER_FIELDS"]);
if ($existShow || $existDelete) {
  if ($existShow) {
    foreach ($arResult["SHOW_OFFER_FIELDS"] as $propCode) {
      $arResult["SHOW_OFFER_FIELDS"][$propCode] = array(
        "CODE" => $propCode,
        "IS_DELETED" => "N",
        "ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_FIELD_TEMPLATE']),
        'SORT' => $arResult['FIELDS_SORT'][$propCode]
      );
    }
    unset($propCode);
    $arResult['ALL_OFFER_FIELDS'] = $arResult['SHOW_OFFER_FIELDS'];
  }
  if ($existDelete) {
    foreach ($arResult['DELETED_OFFER_FIELDS'] as $propCode) {
      $arResult['ALL_OFFER_FIELDS'][$propCode] = array(
        "CODE" => $propCode,
        "IS_DELETED" => "Y",
        "ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_FIELD_TEMPLATE']),
        'SORT' => $arResult['FIELDS_SORT'][$propCode]
      );
    }
    unset($propCode, $arResult['DELETED_OFFER_FIELDS']);
  }
  Collection::sortByColumn($arResult['ALL_OFFER_FIELDS'], array('SORT' => SORT_ASC));
}

$arResult['ALL_OFFER_PROPERTIES'] = array();
$existShow = !empty($arResult["SHOW_OFFER_PROPERTIES"]);
$existDelete = !empty($arResult["DELETED_OFFER_PROPERTIES"]);
if ($existShow || $existDelete) {
  if ($existShow) {
    foreach ($arResult['SHOW_OFFER_PROPERTIES'] as $propCode => $arProp) {
      $arResult["SHOW_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "N";
      $arResult["SHOW_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_PROPERTY_TEMPLATE']);
    }
    unset($arProp, $propCode);
    $arResult['ALL_OFFER_PROPERTIES'] = $arResult['SHOW_OFFER_PROPERTIES'];
  }
  if ($existDelete) {
    foreach ($arResult['DELETED_OFFER_PROPERTIES'] as $propCode => $arProp) {
      $arResult["DELETED_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "Y";
      $arResult["DELETED_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_PROPERTY_TEMPLATE']);
      $arResult['ALL_OFFER_PROPERTIES'][$propCode] = $arResult["DELETED_OFFER_PROPERTIES"][$propCode];
    }
    unset($arProp, $propCode, $arResult['DELETED_OFFER_PROPERTIES']);
  }
  Collection::sortByColumn($arResult['ALL_OFFER_PROPERTIES'], array(
    'SORT' => SORT_ASC,
    'ID' => SORT_ASC
  ));
}

$arResult['SECTIONS'] = array();
$sectionIds = array();
$fileIds = array();
$files = array();

foreach ($arResult['ITEMS'] as &$arItem) {
  if (intval($arItem['IBLOCK_SECTION_ID']) > 0) {
    $sectionIds[] = $arItem['IBLOCK_SECTION_ID'];
  }

  if (is_array($arItem['DETAIL_PICTURE'])) {
    $arItem['PICTURE'] = $arItem['DETAIL_PICTURE'];
  } elseif (is_array($arItem['PREVIEW_PICTURE'])) {
    $arItem['PICTURE'] = $arItem['PREVIEW_PICTURE'];
  } elseif (intval($arItem['PROPERTIES']['MORE_PHOTO']['VALUE'][0]) > 0) {
    $fileIds[$arItem['ID']] = $arItem['PROPERTIES']['MORE_PHOTO']['VALUE'][0];
  }

  if ($arItem['PICTURE']) {
    $resize = CFile::ResizeImageget($arItem['PICTURE'], array(
      'height' => 52,
      'width' => 90,
    ), BX_RESIZE_IMAGE_EXACT, true);

    if ($resize) {
      $arItem['PICTURE']['SRC'] = $resize['src'];
      $arItem['PICTURE']['WIDTH'] = $resize['width'];
      $arItem['PICTURE']['HEIGHT'] = $resize['height'];
      $arItem['PICTURE']['SIZE'] = $resize['size'];
    }
  }
}

if (!empty($fileIds)) {
  $fileRes = CFile::GetList(array(), array(
    '@ID' => implode(',', array_values($fileIds))
  ));

  while ($file = $fileRes->Fetch()) {
    $resize = CFile::ResizeImageget($file, array(
      'height' => 90,
      'width' => 90
    ), BX_RESIZE_IMAGE_EXACT, true);

    if ($resize) {
      $file['SRC'] = $resize['src'];
      $file['WIDTH'] = $resize['width'];
      $file['HEIGHT'] = $resize['height'];
      $file['SIZE'] = $resize['size'];
    }

    $files[$file['ID']] = $file;
  }

  unset($file);
} else {

}

unset($fileIds);

foreach ($arResult['ITEMS'] as &$arItem) {
  if (isset($files[$arItem['PICTURE']])) {
    $arItem['PICTURE'] = $files[$arItem['PICTURE']];
  }
}

unset($files);

$sectionIds = array_unique($sectionIds);

$sectionRes = \CIBlockSection::GetList(array(), array(
  'ID' => $sectionIds
), false, false, array());

while ($section = $sectionRes->GetNext()) {
  $arResult['SECTIONS'][] = $section;
}

unset($sectionIds);
?>