BX.namespace("BX.Iblock.Catalog");

var CompareClass = function (params) {
  this.selectClass = params.SELECT_CLASS || 'select--compare';
  this.parentSelectClass = params.PARENT_SELECT_CLASS || 'compare__select';
  this.ajaxUrl = params.AJAX_URL || null;
  this.actionVariable = params.ACTION_VARIABLE || 'action_ccl';
  this.productIdVariable = params.PRODUCT_ID_VARIABLE || 'id';

  this.obWrapper = null;
  if (!!params.WRAPPER_ID) {
    this.obWrapper = BX(params.WRAPPER_ID);
  }

  this.obListWrapper = null;
  if (!!params.LIST_WRAPPER_ID) {
    this.obListWrapper = BX(params.LIST_WRAPPER_ID);
  }

  this.obHiddenWrapper = null;
  if (!!params.HIDDEN_LIST_WRAPPER_ID) {
    this.obHiddenWrapper = BX(params.HIDDEN_LIST_WRAPPER_ID);
  }

  this.isInitSlider = false;
  this.showedElements = [];

  this.init();
};
BX.Iblock.Catalog.CompareClass = (function () {

  CompareClass.prototype = {
    init: function () {
      this.initSelect();

      setTimeout(BX.proxy(function () {
        this.selectSection();
      }, this), 1000);
    },

    initSelect: function () {
      this.obSelectSection = $('.' + this.selectClass).select2({
        dropdownCssClass: this.selectClass,
        dropdownParent: $('.' + this.parentSelectClass)
      }).on('select2:select', BX.proxy(this.selectSection, this));
    },

    destroySelect: function () {
      this.obSelectSection.select2('destroy');
    },

    initSlider: function () {
      if ($('.compare__items').length > 0) {
        this.slider = $('.compare__items').owlCarousel({
          loop: true,
          nav: true,

          responsive: {

            0: {
              items: 1
            },

            768: {
              items: 2

            },

            1024: {
              items: 3
            },

            1280: {
              items: 4
            },

            1500: {
              items: 5
            }
          }
        });
      }

      this.isInitSlider = true;
    },

    destroySlider: function () {
      if (this.isInitSlider) {
        this.slider.data('owlCarousel').destroy();
        this.isInitSlider = false;
      }
    },

    selectSection: function () {
      console.info('selectSection');
      // Destroy slider
      this.destroySlider();

      var select = this.obSelectSection,
        sectionId = select.val();

      // Move showed items to hidden wrapper
      if (!!this.showedElements) {
        for (var j in this.showedElements) {
          if (this.showedElements.hasOwnProperty(j)) {
            this.obHiddenWrapper.appendChild(this.showedElements[j]);
          }
        }
      }

      this.showedElements = [];

      if (Number(sectionId) > 0) {
        // Move need items to showed wrapper
        var sectionElements = BX.findChildren(this.obHiddenWrapper, {
          attr: {
            'data-section-id': sectionId
          }
        }, true);

        if (!!sectionElements) {
          for (var k in sectionElements) {
            if (sectionElements.hasOwnProperty(k)) {
              this.showedElements.push(sectionElements[k]);
              this.obListWrapper.appendChild(sectionElements[k]);
            }
          }
        }
      }

      // Init slider
      this.initSlider();
    },

    MakeAjaxAction: function (url) {
      BX.showWait(this.obWrapper);
      BX.ajax.post(
        url,
        {
          ajax_action: 'Y'
        },
        BX.proxy(function (result) {
          BX.closeWait();
          this.obWrapper.innerHTML = result;
        }, this)
      );
    },

    removeAjax: function (id) {
      BX.showWait(this.obWrapper);
      var params = {
        ajax_action: 'Y'
      };

      params[this.productIdVariable] = id;
      params[this.actionVariable] = 'DELETE_FROM_COMPARE_LIST';

      BX.ajax.loadJSON(
        this.ajaxUrl,
        params,
        BX.proxy(function (result) {
          BX.closeWait();

          BX.onCustomEvent('OnCompareChange');

          if (result.STATUS == 'ERROR') {
            // Show error
            console.error(result.MESSAGE);
            return;
          }

          this.destroySlider();
          var removeItem = BX.findChild(this.obListWrapper, {
            attr: {
              'data-id': result.ID
            }
          }, true);

          if (!!removeItem) {
            var sectionId = BX.data(removeItem, 'section-id');
            BX.remove(removeItem);

            for (var i in this.showedElements) {
              if (this.showedElements.hasOwnProperty(i)) {
                var productId = BX.data(this.showedElements[i], 'id');
                if (productId == id) {
                  this.showedElements.splice(Number(i), 1);
                }
              }
            }

            if (Number(sectionId) > 0) {
              var sectionItems = BX.findChildren(this.obListWrapper, {
                attr: {
                  'data-section-id': sectionId
                }
              });

              if (!sectionItems || sectionItems.length == 0) {
                this.destroySelect();
                this.obSelectSection.find('[value=' + sectionId + ']').remove();

                if (this.obSelectSection.find('option').length == 0) {
                  BX.adjust(this.obWrapper, {
                    html: '<p class="note-text">Список сравниваемых элементов пуст.</p>'
                  });
                } else {
                  this.initSelect();
                }
              }

              this.selectSection();
            }
          }
        }, this)
      );
    },

    add2Basket: function(id, url) {
      BX.showWait(this.obListWrapper);
      var params = {
        ajax_basket: 'Y',
        action: 'ADD2BASKET'
      };
      params[this.productIdVariable] = id;

      BX.ajax.loadJSON(
        url,
        params,
        BX.proxy(this.add2BasketHandler, this),
        function() {
          BX.closeWait();
        }
      );
    },

    add2BasketHandler: function(result) {
      BX.closeWait();

      if (result.STATUS == 'OK') {
        BX.onCustomEvent('OnBasketChange');
      }
    },
  };

  return CompareClass;
})();