<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

use Bitrix\Main\Localization\Loc;

$fields = array();
$fieldNames = array();
if (!empty($arResult["SHOW_OFFER_FIELDS"])) {
  foreach ($arResult["SHOW_OFFER_FIELDS"] as $code => $arProp) {
    $showRow = true;

    if ($arResult['DIFFERENT']) {
      $arCompare = array();

      foreach ($arResult["ITEMS"] as $arElement) {
        $Value = $arElement["OFFER_FIELDS"][$code];

        if (is_array($Value)) {
          sort($Value);
          $Value = implode(" / ", $Value);
        }
        $arCompare[] = $Value;
      }

      unset($arElement);
      $showRow = (count(array_unique($arCompare)) > 1);
    }

    if ($showRow) {
      $fieldNames[] = '<li class="compare__param">'.Loc::getMessage("IBLOCK_FIELD_".$code).'</li>';

      foreach ($arResult["ITEMS"] as $arElement) {
        $fields[$arElement['ID']] .= '<div class="c-item__param">'.(is_array($arElement["OFFER_FIELDS"][$code]) ? implode("/ ", $arElement["OFFER_FIELDS"][$code]) : $arElement["OFFER_FIELDS"][$code]).'</div>';
      }

      unset($arElement);
    }
  }
}

if (!empty($arResult["SHOW_PROPERTIES"])) {
  foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty) {
    $showRow = true;

    if ($arResult['DIFFERENT']) {
      $arCompare = array();

      foreach ($arResult["ITEMS"] as $arElement) {
        $arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];

        if (is_array($arPropertyValue)) {
          sort($arPropertyValue);
          $arPropertyValue = implode(" / ", $arPropertyValue);
        }

        $arCompare[] = $arPropertyValue;
      }

      unset($arElement);
      $showRow = (count(array_unique($arCompare)) > 1);
    }

    if ($showRow) {
      $fieldNames[] = '<li class="compare__param">'.$arProperty['NAME'].'</li>';

      foreach ($arResult["ITEMS"] as $arElement) {
        $value = is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) : $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"];

        if ($code == 'BRAND') {
          $value = $arElement['DISPLAY_PROPERTIES'][$code]['LINK_ELEMENT_VALUE'][$arElement['DISPLAY_PROPERTIES'][$code]['VALUE']]['NAME'];
        }

        $fields[$arElement['ID']] .= '<div class="c-item__param">'.$value.'</div>';
      }

      unset($arElement);
    }
  }
}

if (!empty($arResult["SHOW_OFFER_PROPERTIES"])) {
  foreach ($arResult["SHOW_OFFER_PROPERTIES"] as $code => $arProperty) {
    $showRow = true;

    if ($arResult['DIFFERENT']) {
      $arCompare = array();

      foreach ($arResult["ITEMS"] as $arElement) {
        $arPropertyValue = $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["VALUE"];

        if (is_array($arPropertyValue)) {
          sort($arPropertyValue);
          $arPropertyValue = implode(" / ", $arPropertyValue);
        }

        $arCompare[] = $arPropertyValue;
      }

      unset($arElement);
      $showRow = (count(array_unique($arCompare)) > 1);
    }

    if ($showRow) {
      $fieldNames[] = '<li class="compare__param">'.$arProperty['NAME'].'</li>';

      foreach ($arResult["ITEMS"] as $arElement) {
        $fields[$arElement['ID']] .= '<div class="c-item__param">'.(is_array($arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ? implode("/ ", $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) : $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]).'</div>';
      }

      unset($arElement);
    }
  }
}
?>

  <div class="compare" id="compare">
    <div class="compare__nav">
      <div class="compare__select">
        <select class="select select--compare">
          <? foreach ($arResult['SECTIONS'] as $section): ?>
            <option value="<?= $section['ID'] ?>"><?= $section['NAME'] ?></option>
          <? endforeach ?>
        </select>
      </div>
      <ul class="compare__params">
        <li class="compare__param">Изображение</li>
        <li class="compare__param">Название</li>
        <? foreach ($fieldNames as $fieldName) {
          echo $fieldName;
        } ?>
        <li class="compare__param">Цена</li>
      </ul>
    </div>
    <div class="compare__items slider slider--navs" id="compare-list-wrapper"></div>
    <div class="hidden" id="compare-hidden-list-wrapper">
      <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <div class="c-item" data-section-id="<?= $arItem['IBLOCK_SECTION_ID'] ?>" data-id="<?= $arItem['ID'] ?>">
          <div class="c-item__inner">
            <? if ($arItem["CAN_BUY"]):
              // TODO Make ajax
              ?>
              <a class="c-item__add" href="javascript:void(0)" onclick="CatalogCompareObj.add2Basket(<?= $arItem['ID'] ?>, '<?= $arItem['DETAIL_PAGE_URL'] ?>')"
                 rel="nofollow"><?= Loc::getMessage("CATALOG_COMPARE_ADD"); ?></a>
              <?
            elseif (!empty($arResult["PRICES"]) || is_array($arItem["PRICE_MATRIX"])):
              echo Loc::getMessage("CATALOG_NOT_AVAILABLE");
            endif;
            ?>
            <a href="javascript:void(0)" class="c-item__remove"
               onclick="CatalogCompareObj.removeAjax('<?= $arItem['ID'] ?>');"><?= Loc::getMessage("CATALOG_REMOVE_PRODUCT"); ?></a>
            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="c-item__image"><img
                  src="<?= $arItem['PICTURE']['SRC'] ?>" alt=""></a>
            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="c-item__name"><?= $arItem['NAME'] ?></a>
            <?
            echo $fields[$arItem['ID']];

            $price = '&nbsp;';

            if (isset($arItem['MIN_PRICE']) && is_array($arItem['MIN_PRICE'])) {
              $price = $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
            } ?>
            <div class="c-item__param"><?= $price ?></div>
          </div>
        </div>
      <? endforeach ?>
    </div>
  </div>
  <?
  $jsParams = array(
    'SELECT_CLASS' => 'select--compare',
    'PARENT_SELECT_CLASS' => 'compare__select',
    'SELECT_SECTION_ID' => '',
    'WRAPPER_ID' => 'compare',
    'LIST_WRAPPER_ID' => 'compare-list-wrapper',
    'HIDDEN_LIST_WRAPPER_ID' => 'compare-hidden-list-wrapper',
    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'].'_ccl',
    'AJAX_URL' => $APPLICATION->GetCurDir()
  );
  ?>
  <script type="text/javascript">
    var CatalogCompareObj;
    BX.ready(function() {
      CatalogCompareObj = new BX.Iblock.Catalog.CompareClass(<?= CUtil::PhpToJSObject($jsParams) ?>);
    });
  </script>
<? \Debug::dtc($arResult, 'result'); ?>
<? \Debug::dtc($arParams, 'params'); ?>