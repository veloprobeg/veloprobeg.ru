<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;
global $USER;
$arUserGroups = $USER->GetUserGroupArray();
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
  
  

}






/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
$displayPreviewTextMode = array(
  'H' => true,
  'E' => true,
  'S' => true
);
$detailPictMode = array(
  'IMG' => true,
  'POPUP' => true,
  'MAGNIFIER' => true,
  'GALLERY' => true
);

$arDefaultParams = array(
  'TEMPLATE_THEME' => 'blue',
  'ADD_PICT_PROP' => '-',
  'LABEL_PROP' => '-',
  'OFFER_ADD_PICT_PROP' => '-',
  'OFFER_TREE_PROPS' => array('-'),
  'DISPLAY_NAME' => 'Y',
  'DETAIL_PICTURE_MODE' => 'IMG',
  'ADD_DETAIL_TO_SLIDER' => 'N',
  'DISPLAY_PREVIEW_TEXT_MODE' => 'E',
  'PRODUCT_SUBSCRIPTION' => 'N',
  'SHOW_DISCOUNT_PERCENT' => 'N',
  'SHOW_OLD_PRICE' => 'N',
  'SHOW_MAX_QUANTITY' => 'N',
  'SHOW_BASIS_PRICE' => 'N',
  'ADD_TO_BASKET_ACTION' => array('BUY'),
  'SHOW_CLOSE_POPUP' => 'N',
  'MESS_BTN_BUY' => '',
  'MESS_BTN_ADD_TO_BASKET' => '',
  'MESS_BTN_SUBSCRIBE' => '',
  'MESS_BTN_COMPARE' => '',
  'MESS_NOT_AVAILABLE' => '',
  'USE_VOTE_RATING' => 'N',
  'VOTE_DISPLAY_AS_RATING' => 'rating',
  'USE_COMMENTS' => 'N',
  'BLOG_USE' => 'N',
  'BLOG_URL' => 'catalog_comments',
  'BLOG_EMAIL_NOTIFY' => 'N',
  'VK_USE' => 'N',
  'VK_API_ID' => '',
  'FB_USE' => 'N',
  'FB_APP_ID' => '',
  'BRAND_USE' => 'N',
  'BRAND_PROP_CODE' => ''
);
$arParams = array_merge($arDefaultParams, $arParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME']) {
  $arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
  if ('site' == $arParams['TEMPLATE_THEME']) {
    $templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
    $templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
    $arParams['TEMPLATE_THEME'] = COption::GetOptionString('main', 'wizard_'.$templateId.'_theme_id', 'blue', SITE_ID);
  }
  if ('' != $arParams['TEMPLATE_THEME']) {
    if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css')) {
      $arParams['TEMPLATE_THEME'] = '';
    }
  }
}
if ('' == $arParams['TEMPLATE_THEME']) {
  $arParams['TEMPLATE_THEME'] = 'blue';
}

$arParams['ADD_PICT_PROP'] = trim($arParams['ADD_PICT_PROP']);
if ('-' == $arParams['ADD_PICT_PROP']) {
  $arParams['ADD_PICT_PROP'] = '';
}
$arParams['LABEL_PROP'] = trim($arParams['LABEL_PROP']);
if ('-' == $arParams['LABEL_PROP']) {
  $arParams['LABEL_PROP'] = '';
}
$arParams['OFFER_ADD_PICT_PROP'] = trim($arParams['OFFER_ADD_PICT_PROP']);
if ('-' == $arParams['OFFER_ADD_PICT_PROP']) {
  $arParams['OFFER_ADD_PICT_PROP'] = '';
}
if (!is_array($arParams['OFFER_TREE_PROPS'])) {
  $arParams['OFFER_TREE_PROPS'] = array($arParams['OFFER_TREE_PROPS']);
}
foreach ($arParams['OFFER_TREE_PROPS'] as $key => $value) {
  $value = (string)$value;
  if ('' == $value || '-' == $value) {
    unset($arParams['OFFER_TREE_PROPS'][$key]);
  }
}
if (empty($arParams['OFFER_TREE_PROPS']) && isset($arParams['OFFERS_CART_PROPERTIES']) && is_array($arParams['OFFERS_CART_PROPERTIES'])) {
  $arParams['OFFER_TREE_PROPS'] = $arParams['OFFERS_CART_PROPERTIES'];
  foreach ($arParams['OFFER_TREE_PROPS'] as $key => $value) {
    $value = (string)$value;
    if ('' == $value || '-' == $value) {
      unset($arParams['OFFER_TREE_PROPS'][$key]);
    }
  }
}
if ('N' != $arParams['DISPLAY_NAME']) {
  $arParams['DISPLAY_NAME'] = 'Y';
}
if (!isset($detailPictMode[$arParams['DETAIL_PICTURE_MODE']])) {
  $arParams['DETAIL_PICTURE_MODE'] = 'IMG';
}
if ('Y' != $arParams['ADD_DETAIL_TO_SLIDER']) {
  $arParams['ADD_DETAIL_TO_SLIDER'] = 'N';
}
if (!isset($displayPreviewTextMode[$arParams['DISPLAY_PREVIEW_TEXT_MODE']])) {
  $arParams['DISPLAY_PREVIEW_TEXT_MODE'] = 'E';
}
if ('Y' != $arParams['PRODUCT_SUBSCRIPTION']) {
  $arParams['PRODUCT_SUBSCRIPTION'] = 'N';
}
if ('Y' != $arParams['SHOW_DISCOUNT_PERCENT']) {
  $arParams['SHOW_DISCOUNT_PERCENT'] = 'N';
}
if ('Y' != $arParams['SHOW_OLD_PRICE']) {
  $arParams['SHOW_OLD_PRICE'] = 'N';
}
if ('Y' != $arParams['SHOW_MAX_QUANTITY']) {
  $arParams['SHOW_MAX_QUANTITY'] = 'N';
}
if ($arParams['SHOW_BASIS_PRICE'] != 'Y') {
  $arParams['SHOW_BASIS_PRICE'] = 'N';
}
if (!is_array($arParams['ADD_TO_BASKET_ACTION'])) {
  $arParams['ADD_TO_BASKET_ACTION'] = array($arParams['ADD_TO_BASKET_ACTION']);
}
$arParams['ADD_TO_BASKET_ACTION'] = array_filter($arParams['ADD_TO_BASKET_ACTION'], 'CIBlockParameters::checkParamValues');
if (empty($arParams['ADD_TO_BASKET_ACTION']) || (!in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']) && !in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']))) {
  $arParams['ADD_TO_BASKET_ACTION'] = array('BUY');
}
if ($arParams['SHOW_CLOSE_POPUP'] != 'Y') {
  $arParams['SHOW_CLOSE_POPUP'] = 'N';
}

$arParams['MESS_BTN_BUY'] = trim($arParams['MESS_BTN_BUY']);
$arParams['MESS_BTN_ADD_TO_BASKET'] = trim($arParams['MESS_BTN_ADD_TO_BASKET']);
$arParams['MESS_BTN_SUBSCRIBE'] = trim($arParams['MESS_BTN_SUBSCRIBE']);
$arParams['MESS_BTN_COMPARE'] = trim($arParams['MESS_BTN_COMPARE']);
$arParams['MESS_NOT_AVAILABLE'] = trim($arParams['MESS_NOT_AVAILABLE']);
if ('Y' != $arParams['USE_VOTE_RATING']) {
  $arParams['USE_VOTE_RATING'] = 'N';
}
if ('vote_avg' != $arParams['VOTE_DISPLAY_AS_RATING']) {
  $arParams['VOTE_DISPLAY_AS_RATING'] = 'rating';
}
if ('Y' != $arParams['USE_COMMENTS']) {
  $arParams['USE_COMMENTS'] = 'N';
}
if ('Y' != $arParams['BLOG_USE']) {
  $arParams['BLOG_USE'] = 'N';
}
if ('Y' != $arParams['VK_USE']) {
  $arParams['VK_USE'] = 'N';
}
if ('Y' != $arParams['FB_USE']) {
  $arParams['FB_USE'] = 'N';
}
if ('Y' == $arParams['USE_COMMENTS']) {
  if ('N' == $arParams['BLOG_USE'] && 'N' == $arParams['VK_USE'] && 'N' == $arParams['FB_USE']) {
    $arParams['USE_COMMENTS'] = 'N';
  }
}
if ('Y' != $arParams['BRAND_USE']) {
  $arParams['BRAND_USE'] = 'N';
}
if ($arParams['BRAND_PROP_CODE'] == '') {
  $arParams['BRAND_PROP_CODE'] = array();
}
if (!is_array($arParams['BRAND_PROP_CODE'])) {
  $arParams['BRAND_PROP_CODE'] = array($arParams['BRAND_PROP_CODE']);
}

$arEmptyPreview = false;
$strEmptyPreview = $this->GetFolder().'/images/no_photo.png';
if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview)) {
  $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);
  if (!empty($arSizes)) {
    $arEmptyPreview = array(
      'SRC' => $strEmptyPreview,
      'WIDTH' => (int)$arSizes[0],
      'HEIGHT' => (int)$arSizes[1]
    );
  }
  unset($arSizes);
}
unset($strEmptyPreview);

$arSKUPropList = array();
$arSKUPropIDs = array();
$arSKUPropKeys = array();
$boolSKU = false;
$strBaseCurrency = '';
$boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

if ($arResult['MODULES']['catalog']) {
  if (!$boolConvert) {
    $strBaseCurrency = CCurrency::GetBaseCurrency();
  }

  $arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
  $boolSKU = !empty($arSKU) && is_array($arSKU);

  if ($boolSKU && !empty($arParams['OFFER_TREE_PROPS'])) {
    $arSKUPropList = CIBlockPriceTools::getTreeProperties(
      $arSKU,
      $arParams['OFFER_TREE_PROPS'],
      array(
        'PICT' => $arEmptyPreview,
        'NAME' => '-'
      )
    );
    $arSKUPropIDs = array_keys($arSKUPropList);
  }
}



$arResult['CHECK_QUANTITY'] = false;
if (!isset($arResult['CATALOG_MEASURE_RATIO'])) {
  $arResult['CATALOG_MEASURE_RATIO'] = 1;
}
if (!isset($arResult['CATALOG_QUANTITY'])) {
  $arResult['CATALOG_QUANTITY'] = 0;
}
$arResult['CATALOG_QUANTITY'] = (
0 < $arResult['CATALOG_QUANTITY'] && is_float($arResult['CATALOG_MEASURE_RATIO'])
  ? (float)$arResult['CATALOG_QUANTITY']
  : (int)$arResult['CATALOG_QUANTITY']
);
$arResult['CATALOG'] = false;
if (!isset($arResult['CATALOG_SUBSCRIPTION']) || 'Y' != $arResult['CATALOG_SUBSCRIPTION']) {
  $arResult['CATALOG_SUBSCRIPTION'] = 'N';
}

CIBlockPriceTools::getLabel($arResult, $arParams['LABEL_PROP']);

$productSlider = CIBlockPriceTools::getSliderForItem($arResult, $arParams['ADD_PICT_PROP'], 'Y' == $arParams['ADD_DETAIL_TO_SLIDER']);
if (empty($productSlider)) {

}



$productSliderCount = count($productSlider);
$arResult['SHOW_SLIDER'] = true;
//$arResult['MORE_PHOTO'] = $productSlider;
$arResult['MORE_PHOTO_COUNT'] = count($productSlider);




if ($arResult['MODULES']['catalog']) {
  $arResult['CATALOG'] = true;
  if (!isset($arResult['CATALOG_TYPE'])) {
    $arResult['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
  }
  if (
    (CCatalogProduct::TYPE_PRODUCT == $arResult['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arResult['CATALOG_TYPE'])
    && !empty($arResult['OFFERS'])
  ) {
    $arResult['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
  }
  switch ($arResult['CATALOG_TYPE']) {
    case CCatalogProduct::TYPE_SET:
      $arResult['OFFERS'] = array();
      $arResult['CHECK_QUANTITY'] = ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO']);
      break;
    case CCatalogProduct::TYPE_SKU:
      break;
    case CCatalogProduct::TYPE_PRODUCT:
    default:
      $arResult['CHECK_QUANTITY'] = ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO']);
      break;
  }
} else {
  $arResult['CATALOG_TYPE'] = 0;
  $arResult['OFFERS'] = array();
}



//работа с таблицами размеров
$arFilter = array('IBLOCK_ID' => $arResult['IBLOCK_ID'], 'ID' => $arResult['IBLOCK_SECTION_ID']);
$rsSections = CIBlockSection::GetList(array(), $arFilter, false, Array("ID", "UF_SIZES") );
if ($arSection = $rsSections->Fetch()){
	$sizeTable = $arSection['UF_SIZES']; //элемент с таблицей размеров
}

if ( $sizeTable ){
	
	$arFilter = Array(
		"IBLOCK_CODE"	=>	"sizes_table", 
		"ID" 		=> 	$sizeTable,
		"ACTIVE"	=>	"Y", 
	 );
	$res = CIBlockElement::GetList(Array(), $arFilter, Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE"));
	if($ar_fields = $res->GetNext()){
	
	
		$arResult['SHOW_SIZE_TABLE'] = "Y";
		
		if ( $ar_fields["PREVIEW_PICTURE"] ){
			$arResult['SIZE_TABLE']  = '<img src="'. CFile::GetPath($ar_fields["PREVIEW_PICTURE"]) . '" alt="">';
		}
		elseif ( $ar_fields["PREVIEW_TEXT"] )  {
			$arResult['SIZE_TABLE'] = $ar_fields["PREVIEW_TEXT"];
		}
		else {
			$arResult['SHOW_SIZE_TABLE'] = "N";
		}

	}
	
	

}




if ($arResult['CATALOG'] && isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
  $boolSKUDisplayProps = false;

  $arResultSKUPropIDs = array();
  $arFilterProp = array();
  $arNeedValues = array();
  

	$arColorPics = Array();
	$arStoresName = Array();
	$arStoreGrupp = Array();
	
	//получаем данные о группах складов
	$arStoreGP = Array();
	$arFilter = Array(
		"IBLOCK_CODE"	=>	"stores", 
		"ACTIVE"	=>	"Y", 
	 );
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "PROPERTY_PRIORITY"=>"ASC"), $arFilter, Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_euro"));
	while($ar_fields = $res->GetNext())
	{
		if ( $ar_fields["PROPERTY_EURO_VALUE"] ){
			$ar_fields["EURO"] = "Y";
			$ar_fields["CODE"] = 'EURO';
		}
		else {
			$ar_fields["CODE"] = $ar_fields["ID"];
		}
		$arStoreGP[$ar_fields["ID"]] = $ar_fields;
	}
	
	$arResult['STORES_GROUP'] = $arStoreGP;
	
	
  
	$filter = Array("ACTIVE" => "Y");
	$select_fields = Array("ID", "ADDRESS", "DESCRIPTION", "UF_STORE" );
	$resStore = CCatalogStore::GetList(array("sort" => "asc"),$filter,false,false,$select_fields);
	while($sklad = $resStore->Fetch())
	{
	
		if ( $sklad["UF_STORE"] ) {
			if ( $arStoreGP[$sklad["UF_STORE"]]["EURO"] ){
				$arStoreGrupp['STORE_EURO'][] = $sklad["ID"];
			}
			else {
				$arStoreGrupp[$sklad["UF_STORE"]][] = $sklad["ID"];
			}
		
		}

	
		$stores[] = $sklad["ID"];
		$arStoresName[ $sklad["ID"] ] = $sklad["ADDRESS"];
		$arDescription[ $sklad["ID"] ] = $sklad["DESCRIPTION"];
	}
	
	$arResult['STORES'] = $arStoresName;
	$arResult['STORES_DESCROPTION'] = $arDescription;
			
			
    $arColors = Array();
  	$arDopPics = Array();
    foreach ($arResult['OFFERS'] as $key => &$arOffer) {
      foreach ($arSKUPropIDs as &$strOneCode) {
	
		$arFilter = Array("PRODUCT_ID"=>$arOffer["ID"],"STORE_ID"=>$stores);
		$res = CCatalogStoreProduct::GetList(Array(),$arFilter,false,false,Array());
		while($arRes = $res->GetNext()){
			$arOfferStore[ $arRes['STORE_ID'] ] = $arRes['AMOUNT'];
		}
		$arResult['OFFERS'][$key]["STORE"] = $arOfferStore;

				
      if (isset($arOffer['DISPLAY_PROPERTIES'][$strOneCode])) {
        $arResultSKUPropIDs[$strOneCode] = true;
        if (!isset($arNeedValues[$arSKUPropList[$strOneCode]['ID']])) {
          $arNeedValues[$arSKUPropList[$strOneCode]['ID']] = array();
        }
        $valueId = (
        $arSKUPropList[$strOneCode]['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_LIST
          ? $arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE_ENUM_ID']
          : $arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']
        );
        $arNeedValues[$arSKUPropList[$strOneCode]['ID']][$valueId] = $valueId;
        unset($valueId);
        if (!isset($arFilterProp[$strOneCode])) {
          $arFilterProp[$strOneCode] = $arSKUPropList[$strOneCode];
        }
      }
     }
	
	$colorID = $arOffer["PROPERTIES"]["TSVET_BAZOVYY"]["VALUE_ENUM_ID"];

	if ( !in_array($colorID, $arColors ) )
		$arColors[] = $colorID;
	
	if ( is_array($arOffer["PREVIEW_PICTURE"]) ){
		
		$file = CFile::ResizeImageGet($arOffer["PREVIEW_PICTURE"], array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
        $file2 = CFile::ResizeImageGet($arOffer["PREVIEW_PICTURE"], array('width'=>600, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
		$arColorPics[ $colorID ] = $file['src']; 
		$arColorBigPics[ $colorID ] = Array(
			"SMALL_SRC" => $file['src'],
			"MIDDLE_SRC" => $file2['src'],
			"SRC" => $arOffer["DETAIL_PICTURE"]["SRC"],
			"ID" => $colorID
		);
		
	}
	
	//добавляем доп картинки предложений

	if ( count ( $arOffer["PROPERTIES"]["MORE_PHOTO"]["VALUE"] ) > 0 && count($arDopPics) == 0 ){
		foreach ( $arOffer["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $key => $picture ){
			if ( $key ){
				$file = CFile::ResizeImageGet($picture, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$file2 = CFile::ResizeImageGet($picture, array('width'=>600, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
				$arDopPics[] = Array(
					"SMALL_SRC" => $file['src'],
					"MIDDLE_SRC" => $file2['src'],
					"SRC" => CFile::GetPath($picture),
					"ID" => $picture
				);
			}
		}
	
	}
	
	
    unset($strOneCode);
  }
  unset($arOffer);
  

  
  foreach ( $arColors as $colorID){
	if ( !$arColorPics[ $colorID ] ) {
		$arColorPics[ $colorID ] = '/upload/no_photo_small.jpg';
		
		$arColorBigPics[ $colorID ] = Array(
			"SMALL_SRC" => '/upload/no_photo_small.jpg',
			"MIDDLE_SRC" => '/upload/no_photo_big.jpg',
			"SRC" => '/upload/no_photo_big.jpg',
			"ID" => $colorID,
			"SHOW_PREVIEW" => "N"
		);
	}
  }
  


  CIBlockPriceTools::getTreePropertyValues($arSKUPropList, $arNeedValues);
  $arSKUPropIDs = array_keys($arSKUPropList);
  $arSKUPropKeys = array_fill_keys($arSKUPropIDs, false);



  
  $arMatrixFields = $arSKUPropKeys;
  $arMatrix = array();

  $arNewOffers = array();

  $arIDS = array($arResult['ID']);
  $arOfferSet = array();
  $arResult['OFFER_GROUP'] = false;
  $arResult['OFFERS_PROP'] = false;

  $arDouble = array();
  
  foreach ($arResult['OFFERS'] as $keyOffer => $arOffer) {
    $arOffer['ID'] = (int)$arOffer['ID'];
    if (isset($arDouble[$arOffer['ID']])) {
      continue;
    }
    $arIDS[] = $arOffer['ID'];
    $boolSKUDisplayProperties = false;
    $arOffer['OFFER_GROUP'] = false;
    $arRow = array();
    foreach ($arSKUPropIDs as $propkey => $strOneCode) {
      $arCell = array(
        'VALUE' => 0,
        'SORT' => PHP_INT_MAX,
        'NA' => true
      );
      
      
      if (isset($arOffer['DISPLAY_PROPERTIES'][$strOneCode])) {
        $arMatrixFields[$strOneCode] = true;
        $arCell['NA'] = false;
        if ('directory' == $arSKUPropList[$strOneCode]['USER_TYPE']) {
          $intValue = $arSKUPropList[$strOneCode]['XML_MAP'][$arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']];
          $arCell['VALUE'] = $intValue;
        } elseif ('L' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE']) {
          $arCell['VALUE'] = (int)$arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE_ENUM_ID'];
        } elseif ('E' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE']) {
          $arCell['VALUE'] = (int)$arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE'];
        }
        $arCell['SORT'] = $arSKUPropList[$strOneCode]['VALUES'][$arCell['VALUE']]['SORT'];
      }
      $arRow[$strOneCode] = $arCell;
    }
    $arMatrix[$keyOffer] = $arRow;

    CIBlockPriceTools::setRatioMinPrice($arOffer, false);

    $arOffer['MORE_PHOTO'] = array();
    $arOffer['MORE_PHOTO_COUNT'] = 0;
    $offerSlider = CIBlockPriceTools::getSliderForItem($arOffer, $arParams['OFFER_ADD_PICT_PROP'], $arParams['ADD_DETAIL_TO_SLIDER'] == 'Y');
    if (empty($offerSlider)) {
      $offerSlider = $productSlider;
    }

	$offerSlider = Array();
	foreach ( $arColorBigPics as $keyPic => $valPic ){
		$offerSlider[] = $valPic;
	}  
	


	$arOffer['MORE_PHOTO'] = $offerSlider;
    $arOffer['MORE_PHOTO_COUNT'] = count($offerSlider);

    if (CIBlockPriceTools::clearProperties($arOffer['DISPLAY_PROPERTIES'], $arParams['OFFER_TREE_PROPS'])) {
      $boolSKUDisplayProps = true;
    }

    $arDouble[$arOffer['ID']] = true;
    $arNewOffers[$keyOffer] = $arOffer;
	
	if ( $arOffer["DETAIL_TEXT"] ){
		$arResult["DETAIL_TEXT"] = $arOffer["DETAIL_TEXT"];
	}
	
	
  }
  $arResult['OFFERS'] = $arNewOffers;
  $arResult['SHOW_OFFERS_PROPS'] = $boolSKUDisplayProps;

  $arUsedFields = array();
  $arSortFields = array();
//dump($arResult['OFFERS']);


  foreach ($arSKUPropIDs as $propkey => $strOneCode) {
      //echo $strOneCode;
    $boolExist = $arMatrixFields[$strOneCode];

    

    
    foreach ($arMatrix as $keyOffer => $arRow) {
      if ($boolExist) {
        if (!isset($arResult['OFFERS'][$keyOffer]['TREE'])) {
          $arResult['OFFERS'][$keyOffer]['TREE'] = array();
        }
        $arResult['OFFERS'][$keyOffer]['TREE']['PROP_'.$arSKUPropList[$strOneCode]['ID']] = $arMatrix[$keyOffer][$strOneCode]['VALUE'];
        $arResult['OFFERS'][$keyOffer]['SKU_SORT_'.$strOneCode] = $arMatrix[$keyOffer][$strOneCode]['SORT'];
        $arUsedFields[$strOneCode] = true;
        $arSortFields['SKU_SORT_'.$strOneCode] = SORT_NUMERIC;
      } else {
        unset($arMatrix[$keyOffer][$strOneCode]);
      }
    }
  }
  

  
  
  $arResult['OFFERS_PROP'] = $arUsedFields;
  $arResult['OFFERS_PROP_CODES'] = (!empty($arUsedFields) ? base64_encode(serialize(array_keys($arUsedFields))) : '');

  
  
  Collection::sortByColumn($arResult['OFFERS'], $arSortFields);

  $offerSet = array();
  if (!empty($arIDS) && CBXFeatures::IsFeatureEnabled('CatCompleteSet')) {
    $offerSet = array_fill_keys($arIDS, false);
    $rsSets = CCatalogProductSet::getList(
      array(),
      array(
        '@OWNER_ID' => $arIDS,
        '=SET_ID' => 0,
        '=TYPE' => CCatalogProductSet::TYPE_GROUP
      ),
      false,
      false,
      array(
        'ID',
        'OWNER_ID'
      )
    );
    while ($arSet = $rsSets->Fetch()) {
      $arSet['OWNER_ID'] = (int)$arSet['OWNER_ID'];
      $offerSet[$arSet['OWNER_ID']] = true;
      $arResult['OFFER_GROUP'] = true;
    }
    if ($offerSet[$arResult['ID']]) {
      foreach ($offerSet as &$setOfferValue) {
        if ($setOfferValue === false) {
          $setOfferValue = true;
        }
      }
      unset($setOfferValue);
      unset($offerSet[$arResult['ID']]);
    }
    if ($arResult['OFFER_GROUP']) {
      $offerSet = array_filter($offerSet);
      $arResult['OFFER_GROUP_VALUES'] = array_keys($offerSet);
    }
  }

  $arMatrix = array();
  $intSelected = -1;
  $arResult['MIN_PRICE'] = false;
  $arResult['MIN_BASIS_PRICE'] = false;
  foreach ($arResult['OFFERS'] as $keyOffer => $arOffer) {
  
 
    if (empty($arResult['MIN_PRICE'])) {
      if ($arResult['OFFER_ID_SELECTED'] > 0) {
        $foundOffer = ($arResult['OFFER_ID_SELECTED'] == $arOffer['ID']);
      } else {
        $foundOffer = $arOffer['CAN_BUY'];
      }
      if ($foundOffer) {
        $intSelected = $keyOffer;
        $arResult['MIN_PRICE'] = (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']);
        $arResult['MIN_BASIS_PRICE'] = $arOffer['MIN_PRICE'];
      }
      unset($foundOffer);
    }

    $arSKUProps = false;
    if (!empty($arOffer['DISPLAY_PROPERTIES'])) {
      $boolSKUDisplayProps = true;
      $arSKUProps = array();
      foreach ($arOffer['DISPLAY_PROPERTIES'] as &$arOneProp) {
        if ('F' == $arOneProp['PROPERTY_TYPE']) {
          continue;
        }
        $arSKUProps[] = array(
          'NAME' => $arOneProp['NAME'],
          'VALUE' => $arOneProp['DISPLAY_VALUE']
        );
      }
      unset($arOneProp);
    }
    if (isset($arOfferSet[$arOffer['ID']])) {
      $arOffer['OFFER_GROUP'] = true;
      $arResult['OFFERS'][$keyOffer]['OFFER_GROUP'] = true;
    }
    //reset($arOffer['MORE_PHOTO']);
    $firstPhoto = current($arOffer['MORE_PHOTO']);
	
	
    $arOneRow = array(
      'ID' => $arOffer['ID'],
      'NAME' => $arOffer['~NAME'],
      'TREE' => $arOffer['TREE'],
      'PRICE' => (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']),
      'BASIS_PRICE' => $arOffer['MIN_PRICE'],
      'DISPLAY_PROPERTIES' => $arSKUProps,
      'PREVIEW_PICTURE' => $firstPhoto,
      'DETAIL_PICTURE' => $firstPhoto,
      'CHECK_QUANTITY' => $arOffer['CHECK_QUANTITY'],
      'MAX_QUANTITY' => $arOffer['CATALOG_QUANTITY'],
      'STEP_QUANTITY' => $arOffer['CATALOG_MEASURE_RATIO'],
      'QUANTITY_FLOAT' => is_double($arOffer['CATALOG_MEASURE_RATIO']),
      'MEASURE' => $arOffer['~CATALOG_MEASURE_NAME'],
      'OFFER_GROUP' => (isset($offerSet[$arOffer['ID']]) && $offerSet[$arOffer['ID']]),
      'CAN_BUY' => $arOffer['CAN_BUY'],
      'SLIDER' => $arOffer['MORE_PHOTO'],
      'SLIDER_COUNT' => $arOffer['MORE_PHOTO_COUNT'],
    );
    $arMatrix[$keyOffer] = $arOneRow;
  }
  if (-1 == $intSelected) {
    $intSelected = 0;
    $arResult['MIN_PRICE'] = (isset($arResult['OFFERS'][0]['RATIO_PRICE']) ? $arResult['OFFERS'][0]['RATIO_PRICE'] : $arResult['OFFERS'][0]['MIN_PRICE']);
    $arResult['MIN_BASIS_PRICE'] = $arResult['OFFERS'][0]['MIN_PRICE'];
  }
  $arResult['JS_OFFERS'] = $arMatrix;
  $arResult['OFFERS_SELECTED'] = $intSelected;
  if ($arMatrix[$intSelected]['SLIDER_COUNT'] > 0) {
    $arResult['MORE_PHOTO'] = $arMatrix[$intSelected]['SLIDER'];
    $arResult['MORE_PHOTO_COUNT'] = $arMatrix[$intSelected]['SLIDER_COUNT'];
  }

  $arResult['OFFERS_IBLOCK'] = $arSKU['IBLOCK_ID'];
}



$arPropNames = Array();
$arStoreTree = Array();
foreach ($arResult['OFFERS'] as $keyOffer => $arOffer) {

		$allCount = 0;
		$arStore = Array();
		
		foreach ( $arOffer["STORE"] as $key => $val){
			if ( $key > 49 ){
				$arStore["STORE_".$key] = $val;
			}
			//$allCount += $val;
		}
		
		foreach ( $arStoreGrupp as $key => $arStores ){
			
		
			if ( $key == "STORE_EURO" ){
				foreach ( $arStores as $store ){	
					$arStore["STORE_EURO"] += $arOffer["STORE"][$store];
				}
			}
			else {
				foreach ( $arStores as $store ){	
					$arStore["STORE_".$key] += $arOffer["STORE"][$store];
				}
				$allCount += $arOffer["STORE"][$store];
			}
		}
		

		

		$colorID = $arOffer['PROPERTIES']["TSVET_BAZOVYY"]["VALUE_ENUM_ID"];
		if ( $colorID ){
			$arStoreTree[$colorID][$arOffer['PROPERTIES']["RAZMER_3"]["VALUE_ENUM_ID"]] = $arStore;
		
			$arPropNames[$arOffer["ID"]] = Array(
				"NAME" => $arOffer['NAME'],
				"COLOR_ID" => $colorID,
				"COLOR_NAME" => $arOffer['PROPERTIES']["TSVET_POLNOSTYU_1"]["VALUE"],
				"SIZE_NAME" => $arOffer['PROPERTIES']["RAZMER_3"]["VALUE"],
				"KOD" => $arResult['PROPERTIES']["KOD"]["VALUE"],
				"SIZE_ID" => $arOffer['PROPERTIES']["RAZMER_3"]["VALUE_ENUM_ID"],
				"ARTICULE" => $arOffer['PROPERTIES']["ARTIKUL_1"]["VALUE"],
				"STORE"   => $arStore,
				"AMOUNT"   => $allCount
			);

            if(in_array(GROUP_VIP__ID,$arUserGroups)){//VIP юзер
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_10000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"];
                }
            }elseif(in_array(GROUP_OLD__ID,$arUserGroups)){
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_10000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_500000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_500000"]["VALUE"];
                }
            }elseif(in_array(GROUP_BONUS__ID,$arUserGroups)){
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_10000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_500000"]["VALUE"])){
                    $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_500000"]["VALUE"];
                }
            }else{
                $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"];
            }

			/*if(in_array(GROUP_BONUS__ID,$arUserGroups)){
                $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS"]["VALUE"];
            }elseif(in_array(GROUP_OLD__ID,$arUserGroups)){
                $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD"]["VALUE"];
            }else{
                $arPropNames[$arOffer["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP"]["VALUE"];
            }*/

			
		}
		else {
			
			$index = count($arSKUPropIDs) - 1;

			$arStoreTree[$arOffer['PROPERTIES'][$arSKUPropIDs[$index]]["VALUE_ENUM_ID"]] = $arStore;
			$arPropNames[$arOffer["ID"]] = Array(
				"NAME" => $arOffer['NAME'],
				"COLOR_NAME" => $arOffer['PROPERTIES']["TSVET_POLNOSTYU_1"]["VALUE"],
				"ARTICULE" => $arOffer['PROPERTIES']["ARTIKUL_1"]["VALUE"],
				"STORE"   => $arStore,
				"KOD" => $arResult['PROPERTIES']["KOD"]["VALUE"],
				"AMOUNT"   => $allCount,
			);


            if(in_array(GROUP_VIP__ID,$arUserGroups)){//VIP юзер
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_10000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"];
                }
            }elseif(in_array(GROUP_OLD__ID,$arUserGroups)){
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_10000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_500000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_OLD_FROM_500000"]["VALUE"];
                }
            }elseif(in_array(GROUP_BONUS__ID,$arUserGroups)){
                if(in_array(GROUP_PRODAZHI_BEFORE_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS"]["VALUE"];
                    }elseif(in_array(GROUP_PRODAZHI_FROM_10000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_10000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_10000"]["VALUE"];
                }elseif(in_array(GROUP_PRODAZHI_FROM_500000,$arUserGroups) && !empty($arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_500000"]["VALUE"])){
                    $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_FROM_500000"]["VALUE"];
                }
            }else{
                $arPropNames[$arPropNames["ID"]]["BALLS"] = $arOffer['PROPERTIES']["LOGICTIM_BONUS_BALLS_VIP_FROM_500000"]["VALUE"];
            }
		}	



		
		/*
		$arStoreTree[$colorID][$arOffer['PROPERTIES']["RAZMER_3"]["VALUE_ENUM_ID"]] = Array(
			"RUS" => $arOffer["STORE"][44] + $arOffer["STORE"][45],
			"EUR" => $arOffer["STORE"][46],
			"SK_RUS" => $arOffer["STORE"][47] + $arOffer["STORE"][49],
		);
		
		*/
		

}




$arSortStoreTree = Array();
foreach ( $arStoreTree as $idColor => $arStoreColor ){
	foreach ( $arStoreColor as $idRazmer => $valRazmer ){
		$arSortStoreTree[$idColor][] = $idRazmer;
	}
}

$arResult['STORE_TREE'] = $arStoreTree;
$arResult['STORE_TREE_SORT'] = $arSortStoreTree;



$countOffers = 0;

foreach ( $arResult['JS_OFFERS'] as $key => $arOffer ){
	$offerNames = $arPropNames[$arOffer["ID"]];
	if ( is_array($offerNames) ){
		$colorID = $offerNames["COLOR_ID"];
		$arResult['JS_OFFERS'][$key]["NAME"] = $offerNames["NAME"];
		$arResult['OFFER_NAME'] = $offerNames["NAME"];
		$arResult['JS_OFFERS'][$key]["COLOR_NAME"] = $offerNames["COLOR_NAME"];
		$arResult['JS_OFFERS'][$key]["SIZE_NAME"]  = $offerNames["SIZE_NAME"];
		$arResult['JS_OFFERS'][$key]["SIZE_ID"]  = $offerNames["SIZE_ID"];
		$arResult['JS_OFFERS'][$key]["KOD"]  = $offerNames["KOD"];
		$arResult['JS_OFFERS'][$key]["ARTICULE"]   = $offerNames["ARTICULE"];
		$arResult['JS_OFFERS'][$key]["BALLS"]   = $offerNames["BALLS"];
		$arResult['JS_OFFERS'][$key]["COLOR_ID"]   = $offerNames["COLOR_ID"];
		$arResult['JS_OFFERS'][$key]["STORE"]   = $offerNames["STORE"];
		$arResult['JS_OFFERS'][$key]["AMOUNT"]   = $offerNames["AMOUNT"];
		$arResult['JS_OFFERS'][$key]["PICTURE"]   = $arColorBigPics[ $colorID ]["SRC"];
	}
	
	if ( $offerNames["SIZE_ID"] && $offerNames["COLOR_ID"] ){
		$countOffers++;
	}
}





if ( $countOffers == 0 ){
	$arResult['SIMPLE_PRODUCT'] = 'Y';
}






foreach ($arResult['MORE_PHOTO'] as $key => $photo){

	if ( !$photo['SMALL_SRC'] && $photo['SMALL_SRC'] !== 'null' ){
			$resize = CFile::ResizeImageget($photo["ID"], array(
				'width' => 100,
				'height' => 100
			), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			
			$arResult['MORE_PHOTO'][$key]['SMALL_SRC'] = $resize['src'];
			
			$resize = CFile::ResizeImageget($photo["ID"], array(
				'width' => 600,
				'height' => 400
			), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arResult['MORE_PHOTO'][$key]['MIDDLE_SRC'] = $resize['src'];
			
	}
}

if(isset($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"]) && is_array($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])){
	foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $FILE){
		$FILE = CFile::GetFileArray($FILE);
		$resize = CFile::ResizeImageget($FILE["ID"], array(
				'width' => 100,
				'height' => 100
			), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$FILE['SMALL_SRC'] = $resize['src'];
		
		$resize = CFile::ResizeImageget($FILE["ID"], array(
				'width' => 600,
				'height' => 400
			), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$FILE['MIDDLE_SRC'] = $resize['src'];

		
		if(is_array($FILE))
			$arResult["MORE_PHOTO"][]=$FILE;
	}
}





if ($arResult['MODULES']['catalog'] && $arResult['CATALOG']) {
  if ($arResult['CATALOG_TYPE'] == CCatalogProduct::TYPE_PRODUCT || $arResult['CATALOG_TYPE'] == CCatalogProduct::TYPE_SET) {
    CIBlockPriceTools::setRatioMinPrice($arResult, false);
    $arResult['MIN_BASIS_PRICE'] = $arResult['MIN_PRICE'];
  }
  if (
    CBXFeatures::IsFeatureEnabled('CatCompleteSet')
    && (
      $arResult['CATALOG_TYPE'] == CCatalogProduct::TYPE_PRODUCT
      || $arResult['CATALOG_TYPE'] == CCatalogProduct::TYPE_SET
    )
  ) {
    $rsSets = CCatalogProductSet::getList(
      array(),
      array(
        '@OWNER_ID' => $arResult['ID'],
        '=SET_ID' => 0,
        '=TYPE' => CCatalogProductSet::TYPE_GROUP
      ),
      false,
      false,
      array(
        'ID',
        'OWNER_ID'
      )
    );
    if ($arSet = $rsSets->Fetch()) {
      $arResult['OFFER_GROUP'] = true;
    }
  }
}

if (!empty($arResult['DISPLAY_PROPERTIES'])) {
  foreach ($arResult['DISPLAY_PROPERTIES'] as $propKey => $arDispProp) {
    if ('F' == $arDispProp['PROPERTY_TYPE']) {
      unset($arResult['DISPLAY_PROPERTIES'][$propKey]);
    }
  }
}


if ( count( $arSKUPropList['TSVET_BAZOVYY']['VALUES'] ) ){

	foreach ( $arSKUPropList['TSVET_BAZOVYY']['VALUES'] as $key => $color ){
	
		if ( $arColorPics[$color["ID"]] ){
			$arSKUPropList['TSVET_BAZOVYY']['VALUES'][$key]["PICT"]["SRC"] = $arColorPics[$color["ID"]];
		}
	}
	$arSKUPropList['TSVET_BAZOVYY']['SHOW_MODE'] = 'PICT';
}


$arResult['SKU_PROPS'] = $arSKUPropList;


if ( $arResult['DETAIL_PICTURE']['SRC'] ){
	$arResult['DEFAULT_DETAIL_PICTURE'] = Array(
		'SRC' => $arResult['DETAIL_PICTURE']['SRC'],
		'HEIGHT' => $arResult['DETAIL_PICTURE']['HEIGHT'],
		'WIDTH' => $arResult['DETAIL_PICTURE']['WIDTH'],
	);
}
else {
	$arResult['DEFAULT_DETAIL_PICTURE'] = $arEmptyPreview;
}
$arResult['DEFAULT_PICTURE'] = $arEmptyPreview;

$arResult['CURRENCIES'] = array();
if ($arResult['MODULES']['currency']) {
  if ($boolConvert) {
    $currencyFormat = CCurrencyLang::GetFormatDescription($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);
    $arResult['CURRENCIES'] = array(
      array(
        'CURRENCY' => $arResult['CONVERT_CURRENCY']['CURRENCY_ID'],
        'FORMAT' => array(
          'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
          'DEC_POINT' => $currencyFormat['DEC_POINT'],
          'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
          'DECIMALS' => $currencyFormat['DECIMALS'],
          'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
          'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
        )
      )
    );
    unset($currencyFormat);
  } else {
    $currencyIterator = CurrencyTable::getList(array(
      'select' => array('CURRENCY')
    ));
    while ($currency = $currencyIterator->fetch()) {
      $currencyFormat = CCurrencyLang::GetFormatDescription($currency['CURRENCY']);
      $arResult['CURRENCIES'][] = array(
        'CURRENCY' => $currency['CURRENCY'],
        'FORMAT' => array(
          'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
          'DEC_POINT' => $currencyFormat['DEC_POINT'],
          'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
          'DECIMALS' => $currencyFormat['DECIMALS'],
          'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
          'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
        )
      );
    }
    unset($currencyFormat, $currency, $currencyIterator);
  }
}

// Get Brand info


	
      if ( $arResult['PROPERTIES']['CML2_MANUFACTURER']['VALUE'] ) {
        $arResult['BRAND'] = \CIBlockElement::GetList(array(), array(
          'NAME' => $arResult['PROPERTIES']['CML2_MANUFACTURER']['VALUE']
        ), false, false, array(
          'ID',
          'NAME',
          'PREVIEW_PICTURE',
          'DETAIL_PICTURE',
          'PREVIEW_TEXT',
          'DETAIL_TEXT',
          'PROPERTY_LINK'
        ))->Fetch();

        if ($arResult['BRAND']['DETAIL_PICTURE']) {
          $arResult['BRAND']['PICTURE'] = $arResult['BRAND']['DETAIL_PICTURE'];
        } elseif ($arResult['BRAND']['PREVIEW_PICTURE']) {
          $arResult['BRAND']['PICTURE'] = $arResult['BRAND']['PREVIEW_PICTURE'];
        }

        if (!empty($arResult['BRAND']['PICTURE'])) {
          $file = CFile::GetList(array(), array(
            'ID' => $arResult['BRAND']['PICTURE']
          ))->Fetch();

          $resize = CFile::ResizeImageget($file, array(
            'width' => 59,
            'height' => 59
          ), BX_RESIZE_IMAGE_PROPORTIONAL, true);

          if ($resize) {
            $arResult['BRAND']['PICTURE'] = array(
              'ID' => $arResult['BRAND']['PICTURE']
            );
            $arResult['BRAND']['PICTURE']['SRC'] = $resize['src'];
            $arResult['BRAND']['PICTURE']['WIDTH'] = $resize['width'];
            $arResult['BRAND']['PICTURE']['HEIGHT'] = $resize['height'];
            $arResult['BRAND']['PICTURE']['SIZE'] = $resize['size'];
          }
        }
      }
	  
		//видео
		if ( count( $arResult['PROPERTIES']['VIDEO']['VALUE'] )) {
			$arVideo = Array();
			foreach ( $arResult['PROPERTIES']['VIDEO']['VALUE'] as $video ){
				
				$code = str_replace('https://youtu.be/', '', $video);
				$arVideo[] = $code;
				
			}
			unset( $arResult['DISPLAY_PROPERTIES']['VIDEO'] );
			$arResult['VIDEO'] = $arVideo;
		}
	
		//рекомендованные товары
		global $arRecomend;
		if ( count( $arResult['PROPERTIES']['RECOMEND']['VALUE'] )) {
			$arRecomend = $arResult['PROPERTIES']['RECOMEND']['VALUE'];
		}
		
		//невыводимые свойства
		$arNoShowProp = Array('RECOMEND', 'OLD_PRICE');
		foreach ($arNoShowProp as $prop ){
			unset( $arResult['DISPLAY_PROPERTIES'][$prop] );
		}


// Get product reviews
$arResult['REVIEWS'] = array();
$arParams['REVIEWS_COUNT'] = isset($arParams['REVIEWS_COUNT']) ? $arParams['REVIEWS_COUNT'] : 2;

$reviewsRes = \CIBlockElement::GetList(array(), array(
  'IBLICK_ID' => IBLOCK_REVIEWS_ID,
  'PROPERTY_PRODUCT_ID' => $arResult['ID'],
  'ACTIVE' => 'Y'
), false, array(
  'nTopCount' => $arParams['REVIEWS_COUNT']
), array(
  'ID',
  'IBLOCK_ID',
  'NAME',
  'PREVIEW_TEXT',
  'DETAIL_TEXT',
  'PROPERTY_NAME'
));

$arResult['REVIEWS_SHOW_MORE'] = true;//$reviewsRes->NavRecordCount > $arParams['REVIEWS_COUNT'];
//$arResult['COUNT_REVIEWS'] = $reviewsRes->NavRecordCount;

while($review = $reviewsRes->Fetch()) {
  $arResult['REVIEWS'][] = $review;
}




// Delivery systems
if (\Bitrix\Main\Loader::includeModule('sale')) {


$db_dtype = CSaleDelivery::GetList(
    array(
            "SORT" => "ASC",
            "NAME" => "ASC"
        ),
    array(
            "LID" => SITE_ID,
            "ACTIVE" => "Y",
            "LOCATION" => $DELIVERY_LOCATION
        ),
    false,
    false,
    array()
);

  // Delivery list
  $arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();


  $restrictions = \Bitrix\Sale\Delivery\Restrictions\Manager::getList(array(
    'filter' => array(
      'SERVICE_TYPE' => \Bitrix\Sale\Delivery\Restrictions\Manager::SERVICE_TYPE_SHIPMENT,
    )
  ))->fetchAll();
  
  
  

  global $CITY_CODE;
  global $BASKETPRICE;
  
  $BASKETPRICE = $BASKETPRICE + $arResult['MIN_PRICE']["VALUE"];
  
   
   if ( $CITY_CODE == '0000073738' ){
	unset($arResult['DELIVERY'][55]);
	unset($arResult['DELIVERY'][60]);
   }
   
   //находим доступное предлоение для расчета доставки
   $realOffer = 0;
   foreach ( $arResult['OFFERS'] as $keyOffer => $arOffer ){
	if ( $arOffer['CATALOG_QUANTITY'] > 0 ){
		$realOffer = $keyOffer;
		continue;
	}
   }
   
	$nav = CIBlockSection::GetNavChain(false, $arResult['IBLOCK_SECTION_ID'], Array("ID"));
	while($arSectionPath = $nav->GetNext()){
         
		$arParentSection[] = $arSectionPath["ID"];
            
	}
	
	if ( $arParentSection[1] == 24435 ){
		unset($arResult['DELIVERY'][60]);
	}
   
  foreach ($arResult['DELIVERY'] as $deliveryId => &$delivery) {
  
	$arResult['DELIVERY'][$deliveryId]['PRICE'] = $delivery['CONFIG']['MAIN']['PRICE'];

	if ( $delivery['PRICE'] == 0 ){
		$arResult['DELIVERY'][$deliveryId]['PRICE_FORMATED'] = 'Бесплатно';
	}
	else {
		$arResult['DELIVERY'][$deliveryId]['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($delivery['PRICE'], 'RUB');
	}
  
	if ( $arResult['DELIVERY'][$deliveryId]['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Services\Group'){
		unset($arResult['DELIVERY'][$deliveryId]);
		
	}
	else {
  
		foreach ($restrictions as $restriction) {
		
			$break = false;
			
			if ($deliveryId == $restriction['SERVICE_ID']) {
			
				
				$restriction['PARAMS'] = is_array($restriction['PARAMS']) ? $restriction['PARAMS'] : array();

				if ($restriction['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Restrictions\ByPublicMode') {
					$checked = $restriction['CLASS_NAME']::check('', $restriction['PARAMS'], $deliveryId);
					if (!$checked) {
						unset($arResult['DELIVERY'][$deliveryId]);
						$break = true;
					}
				}
				
				if ($restriction['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Restrictions\ByDimensions') {
					$checked = $restriction['CLASS_NAME']::check('', $restriction['PARAMS'], $arResult['IBLOCK_SECTION_ID']);
					if (!$checked) {
						unset($arResult['DELIVERY'][$deliveryId]);
						$break = true;
					}
				}
				
				if ($restriction['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Restrictions\ByLocation') {
					$checked = $restriction['CLASS_NAME']::check($CITY_CODE, $restriction['PARAMS'], $deliveryId);
					if (!$checked) {
						unset($arResult['DELIVERY'][$deliveryId]);
						$break = true;
					}
				}
				
				if ($restriction['CLASS_NAME'] == '\Bitrix\Sale\Delivery\Restrictions\ByPrice') {
					if ( ($restriction['PARAMS']['MIN_PRICE'] && $BASKETPRICE < $restriction['PARAMS']['MIN_PRICE']) || ($restriction['PARAMS']['MAX_PRICE'] && $BASKETPRICE > $restriction['PARAMS']['MAX_PRICE'])) {
						unset($arResult['DELIVERY'][$deliveryId]);
						$break = true;
					}
				}
				
				
			
			
			}
		}	
			
			
			if ( is_array( $arResult['DELIVERY'][$deliveryId] ) && !$arResult['DELIVERY'][$deliveryId]['PRICE'] ){
			
				//$obBasket = \Bitrix\Sale\Basket::create(SITE_ID);
				$obBasket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
				$obItem = $obBasket->createItem("catalog", $arResult["OFFERS"][$realOffer]['ID']);
				$arProductFields = array(
				    'NAME' => $arResult['NAME'],
				    'PRICE' => $arResult['MIN_PRICE']["VALUE"],
				    'CURRENCY' => 'RUB',
				    'QUANTITY' => 1,
				    'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
				    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				);
				$obItem->setFields($arProductFields);
				
				
				
				

				$obOrder = \Bitrix\Sale\Order::create(SITE_ID, 1);
				$obOrder->setPersonTypeId(1);
				$obOrder->setBasket($obBasket);
				
				
				
				$propertyCollection = $obOrder->getPropertyCollection();
				$locPropValue   = $propertyCollection->getDeliveryLocation();
				$locPropValue->setValue($CITY_CODE);
				//Следующие 26 строк не работают на тестовом
				$obShipmentCollection = $obOrder->getShipmentCollection();
				$shipment = $obShipmentCollection->createItem();
				$shipmentItemCollection = $shipment->getShipmentItemCollection();
				$shipment->setField('CURRENCY', $obOrder->getCurrency());
				$shipment->setField('DELIVERY_ID', $deliveryId);
				foreach ($obOrder->getBasket() as $basketItem)
				{
				    $item = $shipmentItemCollection->createItem($basketItem);
				    $item->setQuantity($basketItem->getQuantity());
			    
				}
				
				$obShipmentCollection->calculateDelivery();
				
				//echo $obOrder->getDeliveryPrice() . '-' . $deliveryId . "<br>";

				
				$arDelivery = \Bitrix\Sale\Delivery\Services\Manager::calculateDeliveryPrice($shipment, $deliveryId, array());

				if (count($arDelivery->getErrorMessages()) > 0) {
					foreach ($arDelivery->getErrorMessages() as $message) {
					    //echo $message . '<br>';
					}
				}
				

				$priceDev = $arDelivery->getDeliveryPrice();
				$arResult['DELIVERY'][$deliveryId]['PRICE'] = $priceDev;
				if ( $priceDev == 0 )
					$arResult['DELIVERY'][$deliveryId]['PRICE_FORMATED'] = 'Бесплатно';
				else
					$arResult['DELIVERY'][$deliveryId]['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($priceDev, 'RUB');

			
			}	
		
	}



  }
  
  
  
}

if ( count( $arSKUPropList['TSVET_BAZOVYY']['VALUES'] ) ){

	//пересортируем цветовые предложения чтобы была одинаковая последовательность с фотками в слайдере
	$arColorValues = $arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'];
	
	
	
	$arColorNewValues = Array();

	foreach ( $arResult['MORE_PHOTO'] as $key => $arPhoto ){
		if ( $arColorValues[$arPhoto["ID"]] ){
			$arColorNewValues[] = $arColorValues[$arPhoto["ID"]];
		}
		if ( !$arPhoto["SMALL_SRC"] || $arPhoto["SMALL_SRC"] == 'null' ){
			unset( $arResult['MORE_PHOTO'][$key] );
		}
	}
	


	$arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'] = $arColorNewValues;



	$firstColor = $arResult['JS_OFFERS'][0]['TREE']['PROP_1462'];
	$arNewValues = Array();
	foreach ( $arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'] as $key => $value ){
		if ( $value['ID'] == $firstColor )
			$firstColorID = $key;
	}


	$arNewValues[$firstColor] = $arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'][$firstColorID];
	foreach ( $arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'] as $key => $value ){

		if ( $value['ID'] !== $firstColor ){
			$arNewValues[$value['ID']] = $value;
		}
	}
	$arResult['SKU_PROPS']['TSVET_BAZOVYY']['VALUES'] = $arNewValues;
}




foreach ( $arDopPics as $arImg ){
	$arResult['MORE_PHOTO'][] = $arImg;
}


//удаляем ненужные свойства из вывода
$arNoShowProp = Array("DISCOUNT", "SPECIALOFFER", "NEWPRODUCT", "S_WORKS");
foreach ($arNoShowProp as $prop ){
	unset($arResult["DISPLAY_PROPERTIES"][$prop]);
}






// Current city
$arResult['CURRENT_CITY'] = $_SESSION['CURRENT_CITY'];

global $APPLICATION;

$comparePath = CHTTP::urlDeleteParams(
  $APPLICATION->GetCurPageParam(),
  array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE'], ''),
  array('delete_system_params' => true)
);
$comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');

$arResult['~COMPARE_URL_TEMPLATE'] = $comparePath . $arParams["PRODUCT_ID_VARIABLE"] . "=#ID#";