<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$productId = isset($_REQUEST['productId']) ? intval($_REQUEST['productId']) : 0;
$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 0;
$pageSize = isset($_REQUEST['pageSize']) ? intval($_REQUEST['pageSize']) : 2;

$page++;

$result = array(
  'items' => array()
);

global $DB;

try {
  if (!\Bitrix\Main\Loader::includeModule('iblock')) {
    throw new \Exception(Loc::getMessage("NOT_INCLUDE_IB_MODULE"));
  }

  $phpCache = new CPHPCache();
  $allCount = 0;

  $result['page'] = $page;
  $currentPage = $page;
  $showButton = false;

  if ($phpCache->InitCache(3600, serialize(array_merge((array)$page, (array)$productId, (array)$pageSize)))) {
    $vars = $phpCache->GetVars();
    $result['items'] = $vars['ITEMS'];
    $allCount = $vars['ALL_COUNT'];
    $showButton = $vars['SHOW_BUTTON'];
    $currentPage = $vars['CURRENT_PAGE'];
  } elseif ($phpCache->StartDataCache()) {
    $res = \CIBlockElement::GetList(array(), array(
      'IBLOCK_ID' => IBLOCK_REVIEWS_ID,
      'PROPERTY_PRODUCT_ID' => $productId
    ), false, array(
      'iNumPage' => $page,
      'nPageSize' => $pageSize
    ), array(
      'ID',
      'IBLOCK_ID',
      'NAME',
      'PREVIEW_TEXT',
      'PROPERTY_NAME',
      'PROPERTY_PRODUCT_ID',
    ));


    while ($product = $res->Fetch()) {
      $result['items'][$product['ID']] = $product;
    }

    $allCount = $res->NavRecordCount;
    $showButton = $page * $pageSize < $allCount;
    $currentPage = $res->NavPageNomer;

    $phpCache->EndDataCache(array(
      'ITEMS' => $result['items'],
      'ALL_COUNT' => $allCount,
      'SHOW_BUTTON' => $showButton,
      'CURRENT_PAGE' => $currentPage
    ));
  }

  $result['total_count'] = $allCount;
  $result['items'] = array_values($result['items']);
  $result['page'] = $showButton ? $currentPage : 0;
} catch (\Exception $ex) {
  $result['STATUS'] = 'ERROR';
  $result['MESSAGE'] = $ex->getMessage();
}

echo json_encode($result);