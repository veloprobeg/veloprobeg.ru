<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}


/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;

global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME'])) {
  $APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY'])) {
  $loadCurrency = false;
  if (!empty($templateData['CURRENCIES'])) {
    $loadCurrency = Loader::includeModule('currency');
  }
  CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
  if ($loadCurrency) {
    ?>
    <script type="text/javascript">
      BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
    </script>
    <?
  }
}


if(array_key_exists("IS_AJAX", $_REQUEST) && $_REQUEST["IS_AJAX"] == "Y")
{
    	die();
}


$nav = CIBlockSection::GetNavChain(false, $arResult["ID"]); 
      
while($arSectionPath = $nav->GetNext()){
	if ( $arSectionPath["DEPTH_LEVEL"] > 1 ){
   	$APPLICATION->AddChainItem($arSectionPath["NAME"], $arSectionPath["SECTION_PAGE_URL"]);
	}
}



CJSCore::Init(array('popup'));
?>