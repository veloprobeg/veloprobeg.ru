<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//\Debug::d($arResult['ITEMS'], 'ITEMS');
use \Bitrix\Main\Localization\Loc;
?>

		

<?
if (!empty($arResult['ITEMS'])) {
  $templateLibrary = array('popup');
  $currencyList = '';
  if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
  }
  $templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
  );
  unset($currencyList, $templateLibrary);

  $skuTemplate = array();
  if (!empty($arResult['SKU_PROPS'])) {
    foreach ($arResult['SKU_PROPS'] as $arProp) {
      $propId = $arProp['ID'];
      $skuTemplate[$propId] = array(
        'SCROLL' => array(
          'START' => '',
          'FINISH' => '',
        ),
        'FULL' => array(
          'START' => '',
          'FINISH' => '',
        ),
        'ITEMS' => array()
      );
      $templateRow = '';
      if ('TEXT' == $arProp['SHOW_MODE']) {
        $skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_size full" id="#ITEM#_prop_'.$propId.'_cont">'.
          '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
          '<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
        $skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
          '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
          '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
          '</div></div>';

        $skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_size" id="#ITEM#_prop_'.$propId.'_cont">'.
          '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
          '<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
        $skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
          '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
          '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
          '</div></div>';
        foreach ($arProp['VALUES'] as $value) {
          $value['NAME'] = htmlspecialcharsbx($value['NAME']);
          $skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
            '" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#;" title="'.$value['NAME'].'"><i></i><span class="cnt">'.$value['NAME'].'</span></li>';
        }
        unset($value);
      } elseif ('PICT' == $arProp['SHOW_MODE']) {
        $skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_scu full" id="#ITEM#_prop_'.$propId.'_cont">'.
          '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
          '<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
        $skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
          '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
          '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
          '</div></div>';

        $skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_scu" id="#ITEM#_prop_'.$propId.'_cont">'.
          '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
          '<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
        $skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
          '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
          '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
          '</div></div>';
        foreach ($arProp['VALUES'] as $value) {
          $value['NAME'] = htmlspecialcharsbx($value['NAME']);
          $skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
            '" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#; padding-top: #WIDTH#;"><i title="'.$value['NAME'].'"></i>'.
            '<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$value['PICT']['SRC'].'\');" title="'.$value['NAME'].'"></span></span></li>';
        }
        unset($value);
      }
    }
    unset($templateRow, $arProp);
  }

  if ($arParams["DISPLAY_TOP_PAGER"]) {
    ?><? echo $arResult["NAV_STRING"]; ?><?
  }

  $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
  $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
  $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
  global $specPage;
  ?>
  <?if(!array_key_exists("IS_AJAX", $_REQUEST) && !$_REQUEST["IS_AJAX"] == "Y"):?>
  <div class="catalog__cards <?if ($specPage):?>spec<?endif;?>">
  <?endif;?>
    <?
    global $USER;
    $arUserGroups = $USER->GetUserGroupArray();
    CModule::IncludeModule('logictim.balls');
    $arBonus = cHelperCalc::CatalogBonusCustom($arResult,$arUserGroups);
    foreach ($arResult['ITEMS'] as $key => $arItem) {
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
      $strMainID = $this->GetEditAreaId($arItem['ID']);

      $arItemIDs = array(
        'ID' => $strMainID,
        'PICT' => $strMainID.'_pict',
        'SECOND_PICT' => $strMainID.'_secondpict',
        'STICKER_ID' => $strMainID.'_sticker',
        'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
        'QUANTITY' => $strMainID.'_quantity',
        'QUANTITY_DOWN' => $strMainID.'_quant_down',
        'QUANTITY_UP' => $strMainID.'_quant_up',
        'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
        'BUY_LINK' => $strMainID.'_buy_link',
        'BASKET_ACTIONS' => $strMainID.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
        'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
        'COMPARE_LINK' => $strMainID.'_compare_link',

        'PRICE' => $strMainID.'_price',
        'DSC_PERC' => $strMainID.'_dsc_perc',
        'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
        'PROP_DIV' => $strMainID.'_sku_tree',
        'PROP' => $strMainID.'_prop_',
        'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
        'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
        'ONE_CLICK' => $strMainID.'_one_click',
        'ONE_CLICK_POPUP_ID' => $strMainID.'_card',
      );

      $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

      $productTitle = (
      isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
        : $arItem['NAME']
      );
      $imgTitle = (
      isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
        : $arItem['NAME']
      );

      $minPrice = false;
      if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
        $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
      }

      $isDiscount = $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'];
      $isNew = !empty($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']);

      $productClass = '';
      if ($isDiscount) {
        $productClass .= ' card--sale';
      }

      if ($isHid) {
        $productClass .= ' card--hit';
      }

      if ($isNew) {
        $productClass .= ' card--new';
      }
              $inStock = 0;

              if (empty($arItem['OFFERS'])) {
                $inStock = $arItem['CATALOG_QUANTITY'];

              } else {
                foreach ($arItem['OFFERS'] as $arOffer) {
					
                  $inStock += $arOffer['CATALOG_QUANTITY'];
                }
              }

			  
      ?>

	  
      <div class="card<?= $productClass ?> <? if ($inStock == 0 && !$arItem["BANNER"]): ?>no_valiable<?endif;?>" id="<? echo $strMainID; ?>">
        
		<?if ($arItem["BANNER"] ):?>
			<a href="<?=$arItem['LINK']; ?>" class="card__link">
				<img alt="<?=$arItem['NAME']?>" class="card__banner" src="<?=$arItem['PICTURE']; ?>">
			</a>
		<?else:?>
			<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" class="card__link"></a>
			<div class="card__inner">
			  <div class="abs_block">
				<?if ( $arItem['PROPERTIES']['DISCOUNT']["VALUE"] ):?>
					<div>
						<img src="/local/templates/main/tpl/images/static/sale_new.png" alt="Супер вариант" title="Супер вариант">
					</div>
				<?endif;?>
				
				<?if ( $arItem['PROPERTIES']['SPECIALOFFER']["VALUE"] ):?>
					<div>
					  <img src="/local/templates/main/tpl/images/static/fire_new.png" alt="Надо брать" title="Надо брать">
					</div>
				<?endif;?>
				
				<?if ( $arItem['PROPERTIES']['NEWPRODUCT']["VALUE"] ):?>
					<div>
					  <img src="/local/templates/main/tpl/images/static/new_new.png" alt="Новинка" title="Новинка">
					</div>
				<?endif;?>
				
			  </div>
			  
			  <div class="card__content">
				<?/*
				<div class="card__header">
				  <a href="javascript:void(0)" class="card__compare" id="<?= $arItemIDs['COMPARE_LINK']; ?>"><span
						class="card__compare-add">к сравнению</span><span
						class="card__compare-added">в сравнении</span></a>
				  <div class="card__type"></div>
				</div>
				*/?>
				<div class="card__image">
					<?if ( $arItem['PREVIEW_PICTURE']['SRC'] ):?>
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="" id="<?= $arItemIDs['PICT'] ?>">
					<?else:?>
						<img src="/upload/no_photo_small.jpg" alt="" id="<?= $arItemIDs['PICT'] ?>">
					<?endif;?>
				</div>
				<div class="card__name"><?= $productTitle ?></div>
				<div class="card__params">
					<?if ( $arItem['IS_COLOR'] ):?>
						<div class="card__param">Цвета</div>
					<?endif?>
					<?if ( $arItem['IS_SIZE'] ):?>
						<div class="card__param">Размеры</div>
					<?endif?>	
				</div>
				<div class="card__desc">

				  <div class="card__count">
					<? if ($inStock > 0): ?>
					  <div class="card__count-sqs">
						<? for ($i = 1; $i <= 5; $i++): ?>
						  <div class="card__count-sq<?= $i <= $inStock ? ' card__count-sq--active' : '' ?>"></div>
						<? endfor ?>
					  </div>
					  <span class="card__stats">в наличии</span>
					<? else: ?>
					  <span class="card__stats">отсутствует</span>
					<? endif ?>
				  </div>
				  <div class="card__delivery"><?if ( $arItem["FREE_DELIVERY"] ):?>Бесплатно<?endif;?></div>
				</div>
			  </div>
			  

			  
			  <?
			  $compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE'));
			  if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
				\Debug::dtc('NO_OFFERS');
				?>
				<div class="bx_catalog_item_controls" style="display: none;"><?
				  if ($arItem['CAN_BUY']) {
					if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) {
					  ?>
					  <div class="bx_catalog_item_controls_blockone">
						<div style="display: inline-block;position: relative;">
						  <a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)"
							 class="bx_bt_button_type_2 bx_small" rel="nofollow">-
						  </a>
						  <input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>"
								 name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
								 value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
						  <a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)"
							 class="bx_bt_button_type_2 bx_small" rel="nofollow">+
						  </a>
						  <span
							  id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo $arItem['CATALOG_MEASURE_NAME']; ?></span>
						</div>
					  </div>
					  <?
					}
					if ($arParams['DISPLAY_COMPARE']) {
					  ?>
					  <div class="bx_catalog_item_controls_blocktwo">
					  <a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" class="bx_bt_button_type_2 bx_medium"
						 href="javascript:void(0)"><? echo $compareBtnMessage; ?></a>
					  </div><?
					}
				  } else {

					?>
				  <div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_catalog_item_controls_blockone"><span
						class="bx_notavailable"><?
					  echo('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
					  ?></span></div><?
					if ($arParams['DISPLAY_COMPARE']) {
					  ?>
					  <div class="bx_catalog_item_controls_blocktwo"><?
					  if ($arParams['DISPLAY_COMPARE']) {
						?>
						<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" class="bx_bt_button_type_2 bx_medium"
						   href="javascript:void(0)"><? echo $compareBtnMessage; ?></a><?
					  } ?>
					  </div><?
					}
				  }
				  ?>
				</div><?
			  if (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']))
			  {
			  ?>
				<div class="bx_catalog_item_articul">
				  <?
				  foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp) {
					?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
					echo(
					is_array($arOneProp['DISPLAY_VALUE'])
					  ? implode('<br>', $arOneProp['DISPLAY_VALUE'])
					  : $arOneProp['DISPLAY_VALUE']
					);
				  }
				  ?>
				</div>
			  <?
			  }
			  $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
			  if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
			  {
			  ?>
				<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
				  <?
				  if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
					foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
					  ?>
					  <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
							 value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
					  <?
					  if (isset($arItem['PRODUCT_PROPERTIES'][$propID])) {
						unset($arItem['PRODUCT_PROPERTIES'][$propID]);
					  }
					}
				  }
				  $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
				  if (!$emptyProductProperties) {
					?>
					<table>
					  <?
					  foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
						?>
						<tr>
						  <td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
						  <td>
							<?
							if (
							  'L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE']
							  && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']
							) {
							  foreach ($propInfo['VALUES'] as $valueID => $value) {
								?><label><input type="radio"
												name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
												value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
								</label><br><?
							  }
							} else {
							  ?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
							  foreach ($propInfo['VALUES'] as $valueID => $value) {
								?>
								<option
								value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>><? echo $value; ?></option><?
							  }
							  ?></select><?
							}
							?>
						  </td>
						</tr>
						<?
					  }
					  ?>
					</table>
					<?
				  }
				  ?>
				</div>
			  <?
			  }
			  $arJSParams = array(
				'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
				'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
				'SHOW_ADD_BASKET_BTN' => false,
				'SHOW_BUY_BTN' => true,
				'SHOW_ABSENT' => true,
				'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
				'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'IN_COMPARE_URL' => '/local/tools/ajax/in_compare.php',
				'ONE_CLICK_URL' => '/local/tools/ajax/get_one_click_product.php',
				'PRODUCT' => array(
				  'ID' => $arItem['ID'],
				  'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				  'NAME' => $productTitle,
				  'PICT' => $arItem['PREVIEW_PICTURE'],
				  'CAN_BUY' => $arItem["CAN_BUY"],
				  'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
				  'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
				  'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
				  'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
				  'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
				  'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
				  'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
				),
				'BASKET' => array(
				  'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
				  'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				  'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				  'EMPTY_PROPS' => $emptyProductProperties,
				  'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				  'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
				),
				'VISUAL' => array(
				  'ID' => $arItemIDs['ID'],
				  'PICT_ID' => $arItemIDs['PICT'],
				  'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				  'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				  'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				  'PRICE_ID' => $arItemIDs['PRICE'],
				  'BUY_ID' => $arItemIDs['BUY_LINK'],
				  'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
				  'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				  'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				  'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
				  'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
				  'ONE_CLICK' => $arItemIDs['ONE_CLICK'],
				  'ONE_CLICK_POPUP_ID' => $arItemIDs['ONE_CLICK_POPUP_ID'],
				),
				'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
			  );
			  if ($arParams['DISPLAY_COMPARE']) {
				$arJSParams['COMPARE'] = array(
				  'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
				  'COMPARE_PATH' => $arParams['COMPARE_PATH'],
				  'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
				  'COMPARE_NAME' => $arParams['COMPARE_NAME'],
				);
			  }
			  unset($emptyProductProperties);
			  ?>
				<script type="text/javascript">
				  var <? echo $strObName; ?>;
				  BX.ready(function () {
					<? echo $strObName; ?> =
					  new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
				  });
				</script><?
			  } else {
			  \Debug::dtc('HAS_OFFERS');
			  \Debug::dtc($arParams['PRODUCT_DISPLAY_MODE'], '$arParams[PRODUCT_DISPLAY_MODE]');
			  if ('Y' == $arParams['PRODUCT_DISPLAY_MODE']) {
			  $canBuy = $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['CAN_BUY'];
			  ?>
				<div class="bx_catalog_item_controls no_touch" style="display: none;">
				  <?
				  if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) {
					?>
					<div class="bx_catalog_item_controls_blockone">
					  <a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)"
						 class="bx_bt_button_type_2 bx_small" rel="nofollow">-
					  </a>
					  <input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>"
							 name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
							 value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
					  <a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)"
						 class="bx_bt_button_type_2 bx_small" rel="nofollow">+
					  </a>
					  <span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"></span>
					</div>
					<?
				  }

				  ?>
				  <div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_catalog_item_controls_blockone"
					   style="display: <? echo($canBuy ? 'none' : ''); ?>;"><span class="bx_notavailable"><?
					  echo('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
					  ?></span></div>
				  <?
				  if ($arParams['DISPLAY_COMPARE']) {
					?>
					<div class="bx_catalog_item_controls_blocktwo">
					<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" class="bx_bt_button_type_2 bx_medium"
					   href="javascript:void(0)"><? echo $compareBtnMessage; ?></a>
					</div><?
				  } ?>
				</div>
				<?
				unset($canBuy);
			  } else {
				?>
				<div class="bx_catalog_item_controls no_touch" style="display: none;">
				  <a class="bx_bt_button_type_2 bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
					echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
					?></a>
				</div>
			  <?
			  }
			  ?>
				<div class="bx_catalog_item_controls touch" style="display: none;">
				  <a class="bx_bt_button_type_2 bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
					echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
					?></a>
				</div>
			  <?
			  $boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
			  $boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
			  if ($boolShowProductProps || $boolShowOfferProps) {
			  ?>
				<div class="bx_catalog_item_articul" style="display: none;">
				  <?
				  if ($boolShowProductProps) {
					foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp) {
					  ?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
					  echo(
					  is_array($arOneProp['DISPLAY_VALUE'])
						? implode(' / ', $arOneProp['DISPLAY_VALUE'])
						: $arOneProp['DISPLAY_VALUE']
					  );
					}
				  }
				  if ($boolShowOfferProps) {
					?>
					<span id="<? echo $arItemIDs['DISPLAY_PROP_DIV']; ?>" style="display: none;"></span>
					<?
				  }
				  ?>
				</div>
			  <?
			  }

			  if ('Y' == $arParams['PRODUCT_DISPLAY_MODE']) {
			  if (!empty($arItem['OFFERS_PROP'])) {
			  $arSkuProps = array();
			  ?>
				<div class="bx_catalog_item_scu" id="<? echo $arItemIDs['PROP_DIV']; ?>" style="display: none;"><?
				  foreach ($skuTemplate as $propId => $propTemplate) {
					if (!isset($arItem['SKU_TREE_VALUES'][$propId])) {
					  continue;
					}
					$valueCount = count($arItem['SKU_TREE_VALUES'][$propId]);
					if ($valueCount > 5) {
					  $fullWidth = ($valueCount * 20).'%';
					  $itemWidth = (100 / $valueCount).'%';
					  $rowTemplate = $propTemplate['SCROLL'];
					} else {
					  $fullWidth = '100%';
					  $itemWidth = '20%';
					  $rowTemplate = $propTemplate['FULL'];
					}
					unset($valueCount);
					echo '<div>', str_replace(array(
					  '#ITEM#_prop_',
					  '#WIDTH#'
					), array(
					  $arItemIDs['PROP'],
					  $fullWidth
					), $rowTemplate['START']);
					foreach ($propTemplate['ITEMS'] as $value => $valueItem) {
					  if (!isset($arItem['SKU_TREE_VALUES'][$propId][$value])) {
						continue;
					  }
					  echo str_replace(array(
						'#ITEM#_prop_',
						'#WIDTH#'
					  ), array(
						$arItemIDs['PROP'],
						$itemWidth
					  ), $valueItem);
					}
					unset($value, $valueItem);
					echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $rowTemplate['FINISH']), '</div>';
				  }
				  unset($propId, $propTemplate);
				  foreach ($arResult['SKU_PROPS'] as $arOneProp) {
					if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']])) {
					  continue;
					}
					$arSkuProps[] = array(
					  'ID' => $arOneProp['ID'],
					  'SHOW_MODE' => $arOneProp['SHOW_MODE'],
					  'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
					);
				  }
				  foreach ($arItem['JS_OFFERS'] as &$arOneJs) {
					if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT']) {
					  $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
					  $arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
					}
				  }
				  unset($arOneJs);
				  ?></div><?
			  if ($arItem['OFFERS_PROPS_DISPLAY']) {
				foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer) {
				  $strProps = '';
				  if (!empty($arJSOffer['DISPLAY_PROPERTIES'])) {
					foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp) {
					  $strProps .= '<br>'.$arOneProp['NAME'].' <strong>'.(
						is_array($arOneProp['VALUE'])
						  ? implode(' / ', $arOneProp['VALUE'])
						  : $arOneProp['VALUE']
						).'</strong>';
					}
				  }
				  $arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
				}
			  }
			  $arJSParams = array(
				'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
				'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
				'SHOW_ADD_BASKET_BTN' => false,
				'SHOW_BUY_BTN' => true,
				'SHOW_ABSENT' => true,
				'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
				'SECOND_PICT' => $arItem['SECOND_PICT'],
				'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
				'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'IN_COMPARE_URL' => '/local/tools/ajax/in_compare.php',
				'ONE_CLICK_URL' => '/local/tools/ajax/get_one_click_product.php',
				'DEFAULT_PICTURE' => array(
				  'PICTURE' => $arItem['PRODUCT_PREVIEW'],
				  'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
				),
				'VISUAL' => array(
				  'ID' => $arItemIDs['ID'],
				  'PICT_ID' => $arItemIDs['PICT'],
				  'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
				  'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				  'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				  'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				  'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
				  'PRICE_ID' => $arItemIDs['PRICE'],
				  'TREE_ID' => $arItemIDs['PROP_DIV'],
				  'TREE_ITEM_ID' => $arItemIDs['PROP'],
				  'BUY_ID' => $arItemIDs['BUY_LINK'],
				  'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
				  'DSC_PERC' => $arItemIDs['DSC_PERC'],
				  'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
				  'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
				  'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				  'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				  'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
				  'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
				  'ONE_CLICK' => $arItemIDs['ONE_CLICK'],
				  'ONE_CLICK_POPUP_ID' => $arItemIDs['ONE_CLICK_POPUP_ID'],
				),
				'BASKET' => array(
				  'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				  'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				  'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
				  'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				  'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
				),
				'PRODUCT' => array(
				  'ID' => $arItem['ID'],
				  'NAME' => $productTitle,
				  'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				),
				'OFFERS' => $arItem['JS_OFFERS'],
				'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
				'TREE_PROPS' => $arSkuProps,
				'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
			  );
			  if ($arParams['DISPLAY_COMPARE']) {
				$arJSParams['COMPARE'] = array(
				  'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
				  'COMPARE_PATH' => $arParams['COMPARE_PATH'],
				  'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
				  'COMPARE_NAME' => $arParams['COMPARE_NAME'],
				);
			  }
			  ?>
				<script type="text/javascript">
				  var <? echo $strObName; ?>;
				  BX.ready(function () {
					<? echo $strObName; ?> =
					  new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
				  });
				</script>
			  <?
			  }
			  } else {
			  $arJSParams = array(
				'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
				'SHOW_QUANTITY' => false,
				'SHOW_ADD_BASKET_BTN' => false,
				'SHOW_BUY_BTN' => false,
				'SHOW_ABSENT' => false,
				'SHOW_SKU_PROPS' => false,
				'SECOND_PICT' => $arItem['SECOND_PICT'],
				'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
				'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'COMPARE_NAME' => $arParams['COMPARE_NAME'],
				'IN_COMPARE_URL' => '/local/tools/ajax/in_compare.php',
				'ONE_CLICK_URL' => '/local/tools/ajax/get_one_click_product.php',
				'DEFAULT_PICTURE' => array(
				  'PICTURE' => $arItem['PRODUCT_PREVIEW'],
				  'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
				),
				'VISUAL' => array(
				  'ID' => $arItemIDs['ID'],
				  'PICT_ID' => $arItemIDs['PICT'],
				  'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
				  'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				  'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				  'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				  'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
				  'PRICE_ID' => $arItemIDs['PRICE'],
				  'TREE_ID' => $arItemIDs['PROP_DIV'],
				  'TREE_ITEM_ID' => $arItemIDs['PROP'],
				  'BUY_ID' => $arItemIDs['BUY_LINK'],
				  'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
				  'DSC_PERC' => $arItemIDs['DSC_PERC'],
				  'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
				  'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
				  'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				  'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				  'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
				  'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
				  'ONE_CLICK' => $arItemIDs['ONE_CLICK'],
				  'ONE_CLICK_POPUP_ID' => $arItemIDs['ONE_CLICK_POPUP_ID'],
				),
				'BASKET' => array(
				  'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				  'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				  'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
				  'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				  'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
				),
				'PRODUCT' => array(
				  'ID' => $arItem['ID'],
				  'NAME' => $productTitle,
				  'IBLOCK_ID' => $arItem['IBLOCK_ID'],
				),
				'OFFERS' => array(),
				'OFFER_SELECTED' => 0,
				'TREE_PROPS' => array(),
				'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
			  );
			  if ($arParams['DISPLAY_COMPARE']) {
				$arJSParams['COMPARE'] = array(
				  'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
				  'COMPARE_PATH' => $arParams['COMPARE_PATH'],
				  'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
				  'COMPARE_NAME' => $arParams['COMPARE_NAME'],
				);
			  }
			  ?>
				<script type="text/javascript">
				  var <? echo $strObName; ?>;
				  BX.ready(function () {
					<? echo $strObName; ?> =
					  new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
				  });
				</script>
				<?
			  }
			  }
			  ?></div>
			<? $isViewSecondPrice = ('Y' == $arParams['SHOW_OLD_PRICE'] && $isDiscount); ?>
			<div class="card__about<?= $isViewSecondPrice ? ' card__about--disc' : '' ?>">
			  <? if (!empty($minPrice)): ?>
				<div class="card__price" id="<?= $arItemIDs['PRICE']; ?>">
				  <? if ( $arItem['PROPERTIES']['OLD_PRICE']["VALUE"] ): ?>
					<div class="card__disc"><?=number_format( $arItem['PROPERTIES']['OLD_PRICE']["VALUE"], 0, '', ' ') ?> <span class="icon-rouble">i</span></div>
				  <? endif ?>
				  <div class="card__cost <? if ( $arItem['PROPERTIES']['OLD_PRICE']["VALUE"] ): ?>discount<?endif;?>"><?= $minPrice['PRINT_DISCOUNT_VALUE'] ?></div>
				</div>
			  <? endif; ?>
                <??>
                <?if($arBonus[$arItem["ID"]]["VIEW_BONUS"]>0){?>
			  <div class="card__bonus"><?if(!in_array(9,$arUserGroups) && !in_array(10,$arUserGroups) && !in_array(11,$arUserGroups)){
                      echo "до ";
                  }?><?=$arBonus[$arItem["ID"]]["VIEW_BONUS"];?><?/*= round($minPrice['DISCOUNT_VALUE'] * $arParams['BONUS_RATIO']) */?> бонусов</div>
                <?}?>
			  <? unset($minPrice); ?>
			</div>
			<div class="card__nav" id="<?= $arItemIDs['BASKET_ACTIONS']; ?>">
				<? if ($inStock > 0): ?>
				  <a href="javascript:void(0)" data-modal-open="#card" data-id="<?= $arItem['ID'] ?>"
					 class="card__fastbuy button button--yel-transp button--hover" id="<?= $arItemIDs['ONE_CLICK'] ?>">Купить в
					1 клик
				  </a>
				<?endif;?>
			  <?
			  // TODO it`s simple mode -> only detail page link
			  if ($arItem['CAN_BUY']) { ?>
				<a href="javascript:void(0)" id="<? echo $arItemIDs['BUY_LINK']; ?>"
				   class="card__add button button--orange">
				  <? if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
					echo('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
				  } else {
					echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
				  }
				  ?>
				</a>
			  <? } else { ?>
				<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
				   class="card__add button button--orange"><?= GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL') ?></a>
			  <? }
			  ?>
			</div>
		
		<?endif;?>
		
      </div>
      <div id="<?= $arItemIDs['ONE_CLICK_POPUP_ID'] ?>" class="popup">
        <form class="popup__inner">
          <a href="javascript:void(0)" class="popup__closer"></a>
          <div class="popup__card-wrapper">
            <!-- AJAX INSERT PRODUCT -->
          </div>
        </form>
      </div>
      <?
    }

    if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
      ?><? echo $arResult["NAV_STRING"]; ?><?
    }
    ?>
<?if(!array_key_exists("IS_AJAX", $_REQUEST) && !$_REQUEST["IS_AJAX"] == "Y"):?>
  </div>
 <?endif;?>
  <script type="text/javascript">
    BX.message({
      BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
      BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
      ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
      TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
      TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
      TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
      BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
      BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
      BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
      BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
      COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
      COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
      COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
      BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
      SITE_ID: '<? echo SITE_ID; ?>'
    });
  </script>
  <?
}

\Debug::dtc($arResult['ITEMS'], 'ITEMS');