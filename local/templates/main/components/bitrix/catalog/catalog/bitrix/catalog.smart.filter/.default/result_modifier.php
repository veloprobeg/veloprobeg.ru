<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array(
    "left",
    "right"
  ))) ? $arParams["POPUP_POSITION"] : "left";

$arResult['ORDER_BY'] = IBlockFilters::getProductsSortList();

$countCheckBoxes = 0;

foreach ($arResult['ITEMS'] as $arItem) {
  if ($arItem['CODE'] == 'BRAND') {
    $arResult['BRAND'] = $arItem;

    foreach ($arResult['BRAND']['VALUES'] as $brand) {
      $arResult['BRAND']['CONTROL_NAME'] = $brand['CONTROL_NAME_ALT'];
      break;
    }
  }

  if ($arItem['DISPLAY_TYPE'] == 'F') {
    if ($countCheckBoxes % 2 == 0) {
      $arItem['LEFT_CHECKBOXES'][] = $arItem;
    } else {
      $arItem['RIGHT_CHECKBOXES'][] = $arItem;
    }

    $countCheckBoxes++;
  }

  if ($arItem['PRICE']) {
    $arItem['SLIDER_ID'] = 'price-slider';
    $arResult['JS_FILTER_PARAMS']['PRICE'] = $arItem;
  }
}

foreach($arResult["ITEMS"] as $key=>$arItem){

	if ( $arItem["CODE"] == 'SPECIALOFFER'){
		$arSpec = $arItem;
		unset( $arResult["ITEMS"][$key] );
	}
	if ( $arItem["CODE"] == 'TSVET_BAZOVYY'){
		$arColor = $arItem;
		unset( $arResult["ITEMS"][$key] );
	}
	
}
$arResult["ITEMS"][] = $arColor;
$arResult["ITEMS"][] = $arSpec;




$arResult['CURRENT_'.SORT_VARIABLE] = IBlockFilters::getProductsSort();

$arResult['JS_FILTER_PARAMS']['SORT_VARIABLE'] = $arParams['SORT_VARIABLE'];
