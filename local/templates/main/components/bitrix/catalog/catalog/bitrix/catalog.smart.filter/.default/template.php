<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;
global $specPage;
if ( !$specPage):
?>


  <a href="javascript:void(0)" class="button button--yellow catalog__filter-button">Фильтр<span>/ <span
          id="filter-count"><?=$arResult["ELEMENT_COUNT"]?></span> выбрано</span></a>
  <div class="catalog__filter">
    <div id="modef" style="display: none;">
      <a href=""></a>
      <span id="modef_num"></span></div>
    <form name="<? echo $arResult["FILTER_NAME"]."_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get"
          class="filter">
      <div class="filter__selects">

	
	<div class="filter__section">
          <div class="filter__section-name">Сортировка</div>
          

	  <div class="filter__select filter__select--first">
            <select class="select select--filter select--catalog select--filter-first select2-hidden-accessible" name="ORDER_BY" tabindex="-1" aria-hidden="true">
              <? foreach ($arResult['ORDER_BY'] as $sort):
                // exclude sort items
                if (in_array($sort['CODE'], array(
                  'PROPERTY_NEWPRODUCT-ASC',
                  'SHOWS-DESC'
                ))) {
                  continue;
                }
                ?>
                <option
                    value="<?= $sort['CODE'] ?>"<?= $sort['CODE'] == $arResult['CURRENT_'.SORT_VARIABLE][0].'-'.$arResult['CURRENT_'.SORT_VARIABLE][1] ? ' selected' : '' ?>><?= Loc::getMessage($sort['NAME']); ?></option>
              <? endforeach ?>
            </select>
          </div>

	  
	  
	  
        </div>
	
		
		<? foreach ($arResult["ITEMS"] as $key => $arItem):
			if ( $arItem["CODE"] !== 'CML2_MANUFACTURER')
				continue;
			
	
				
			$checkedItemExist = false;
			$arCur = current($arItem["VALUES"]);
            ?>

				<div class="filter__section">
					<div class="filter__section-name"><?= $arItem['NAME'] ?></div>
										<div class="bx-filter-select-block bx-filter-select-container" data-id="<?=CUtil::JSEscape($key)?>" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text">
											
												<span data-role="currentOption" class="select2-selection__rendered" id="select2-ORDER_BY-m0-container" title="">
													<?
													foreach ($arItem["VALUES"] as $val => $ar)
													{
														if ($ar["CHECKED"])
														{
															echo $ar["VALUE"];
															$checkedItemExist = true;
														}
													}
													if (!$checkedItemExist)
													{
														echo GetMessage("CT_BCSF_FILTER_ALL");
													}
													?>
												</span>
												<span class="selection__arrow" role="presentation"><b role="presentation"></b></span>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<li>
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
					
				</div>
				  
		<?endforeach;?>
		
      </div>
      <div class="filter__options">
		
        <? foreach ($arResult["ITEMS"] as $key => $arItem)//prices
        {
          $key = $arItem["ENCODED_ID"];
          if (isset($arItem["PRICE"])):
            if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) {
              continue;
            }
            ?>
            <div class="filter__section">
              <div class="filter__section-name">Цена, руб</div>
              <div class="filter-slider">
                <div class="reg__timer">
                  <div class="reg__time"><span class="reg__desc">от</span>
                    <input name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                           id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                           value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                           class="default-input default-input--small"
                           size="5"
                           onkeyup="smartFilter.keyup(this)"><span class="reg__desc">До</span>
                    <input class="default-input default-input--small"
                           name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                           id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                           value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                           size="5"
                           onkeyup="smartFilter.keyup(this)">
                  </div>
                  <div id="price-slider"></div>
                </div>
              </div>
            </div>
          <?endif;
        } ?>
		
				<?
				
			
				
				//not prices
				$found = false;
				foreach($arResult["ITEMS"] as $key=>$arItem)
				{
				
				
					if(
						empty($arItem["VALUES"])
						|| isset($arItem["PRICE"])
					)
						continue;
						
					if ( $arItem["CODE"] == 'CML2_MANUFACTURER')
						continue;

					if (
						$arItem["DISPLAY_TYPE"] == "A"
						&& (
							$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
						)
					)
						continue;
						
					$found = true;
					?>
		
					<div class="filter__section">
						<div class="filter__section-name"><?=$arItem["NAME"]?></div>

						<?
							$arCur = current($arItem["VALUES"]);
							switch ($arItem["DISPLAY_TYPE"])
							{
								case "P"://DROPDOWN
									$checkedItemExist = false;
									?>
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text" data-role="currentOption">
												<?
												foreach ($arItem["VALUES"] as $val => $ar)
												{
													if ($ar["CHECKED"])
													{
														echo $ar["VALUE"];
														$checkedItemExist = true;
													}
												}
												if (!$checkedItemExist)
												{
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<li>
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
									<?
									break;
								
								default://CHECKBOXES
									?>
									
									<?
									$num = ceil(count($arItem["VALUES"])/2);
									$i = 0;
									if ( $arItem["CODE"] == "DISCOUNT"){$arItem["VALUES"][1]["VALUE"] = 'да';}
									?>
									<div class="filter__check-area">
										
											<?foreach($arItem["VALUES"] as $val => $ar):?>
												
												<?if ( !$i || $i == $num ):?>
												
													<?if ( $i > 0 ):?>
														</div>
													<?endif;?>
														<div class="filter__checks">
												<?endif;?>
												
											<div class="checkbox-area">
															<input
																type="checkbox"
																class="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
																<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
																onclick="smartFilter.click(this)"
															/>
												
												
												<label data-role="label_<?=$ar["CONTROL_ID"]?>" for="<? echo $ar["CONTROL_ID"] ?>"><?=$ar["VALUE"];?></label>
											</div>
											<?$i++;?>
											<?endforeach;?>
										</div>
									</div>
							<?
							}
							?>
						</div>
				<?}?>
						<div class="reset_div">
						
							
						
							<input
								class="button button--white button--hover"
								type="submit"
								id="del_filter"
								name="del_filter"
								value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
							/>
						
						</div>
        </div>
      </div>
	
		<input type="hidden" id="set_filter" name="set_filter" value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>">
	
    </form>

  <script>
    var smartFilter;
    BX.ready(function () {
      smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
    })
  </script>
<?endif;?>
<? \Debug::dtc($arResult, 'result'); ?>