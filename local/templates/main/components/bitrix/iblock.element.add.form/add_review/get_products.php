<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$name = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';

use \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$result = array(
  'items' => array()
);

global $DB;

try {
  if (!\Bitrix\Main\Loader::includeModule('iblock')) {
    throw new \Exception(Loc::getMessage("IB_MODULE_NOT_INCLUDED"));
  }

  if (empty($name)) {
    throw new \Exception('EMPTY_SEARCH_REQUEST');
  }

  $phpCache = new CPHPCache();

  if ($phpCache->InitCache(86400, serialize(array_merge((array)$name)))) {
    $vars = $phpCache->GetVars();
    $result['items'] = $vars['ITEMS'];
  } elseif ($phpCache->StartDataCache()) {
    $res = \CIBlockElement::GetList(array(), array(
      'IBLOCK_ID' => IBLOCK_CATALOG_ID,
      '%NAME' => $DB->ForSql($name)
    ), false, false, array(
      'ID',
      'IBLOCK_ID',
      'NAME',
      'PREVIEW_PICTURE',
      'DETAIL_PICTURE'
    ));

    $fileIds = array();

    while ($product = $res->Fetch()) {
      $product['text'] = $product['NAME'];
      $product['id'] = $product['ID'];
      if ($product['DETAIL_PICTURE']) {
        $product['PICTURE'] = $product['DETAIL_PICTURE'];
      } elseif ($product['PREVIEW_PICTURE']) {
        $product['PICTURE'] = $product['PREVIEW_PICTURE'];
      }

      if (intval($product['PICTURE']) > 0) {
        $fileIds[] = $product['PICTURE'];
      }

      $result['items'][$product['ID']] = $product;
    }

    if (!empty($fileIds)) {
      $fileRes = CFile::GetList(array(), array(
        '@ID' => implode(',', $fileIds)
      ));

      while ($file = $fileRes->Fetch()) {
        $resize = CFile::ResizeImageget($file, array(
          'width' => 80,
          'height' => 80
        ), BX_RESIZE_IMAGE_EXACT, true);

        if ($resize) {
          $file['SRC'] = $resize['src'];
          $file['WIDTH'] = $resize['width'];
          $file['HEIGHT'] = $resize['height'];
          $file['SIZE'] = $resize['size'];
        }

        $files[$file['ID']] = $file;
      }
    }

    foreach ($result['items'] as &$product) {
      if (isset($files[$product['PICTURE']])) {
        $product['PICTURE'] = $files[$product['PICTURE']];
      }

      $product['PRICE'] = getFinalPriceInCurrency($product['ID']);
    }

    $phpCache->EndDataCache(array(
      'ITEMS' => $result['items']
    ));
  }

  $result['total_count'] = count($result['items']);
  $result['items'] = array_values($result['items']);
} catch (\Exception $ex) {
  $result['STATUS'] = 'ERROR';
  $result['MESSAGE'] = $ex->getMessage();
}

echo json_encode($result);