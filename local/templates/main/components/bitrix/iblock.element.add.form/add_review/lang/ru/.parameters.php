<?
$MESS["IBLOCK_PROPERTY"] = "Свойства, выводимые на редактирование";
$MESS["IBLOCK_PROPERTY_REQUIRED"] = "Свойства, обязательные для заполнения";
$MESS["IBLOCK_ADD_NAME"] = "* наименование *";
$MESS["IBLOCK_ADD_TAGS"] = "* теги *";
$MESS["IBLOCK_ADD_ACTIVE_FROM"] = "* дата начала *";
$MESS["IBLOCK_ADD_ACTIVE_TO"] = "* дата завершения *";
$MESS["IBLOCK_ADD_IBLOCK_SECTION"] = "* раздел инфоблока *";
$MESS["IBLOCK_ADD_PREVIEW_TEXT"] = "* текст анонса *";
$MESS["IBLOCK_ADD_PREVIEW_PICTURE"] = "* картинка анонса *";
$MESS["IBLOCK_ADD_DETAIL_TEXT"] = "* подробный текст *";
$MESS["IBLOCK_ADD_DETAIL_PICTURE"] = "* подробная картинка *";