<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

global $USER;

// Get user info and set default values
if ($USER->IsAuthorized()) {
  $user = $USER->GetList($order = '', $by = '', array(
    'ID' => $USER->GetID()
  ), array(
    'FIELDS' => array(
      'ID',
      'NAME',
      'LAST_NAME',
      'EMAIL',
      'PERSONAL_PHONE'
    )
  ))->Fetch();

  if ($user) {
    foreach ($arResult['PROPERTY_LIST_FULL'] as $id => $arProperty) {
      if ($arProperty['CODE'] == 'NAME') {
        $arResult['PROPERTY_LIST_FULL'][$id]['VALUE'] = $user['NAME'].' '.$user['LAST_NAME'];
      }

      if ($arProperty['CODE'] == 'EMAIL') {
        $arResult['PROPERTY_LIST_FULL'][$id]['VALUE'] = $user['EMAIL'];
      }

      if ($arProperty['CODE'] == 'PHONE') {
        $arResult['PROPERTY_LIST_FULL'][$id]['VALUE'] = $user['PERSONAL_PHONE'];
      }
    }
  }
}

// split on visual parts
foreach ($arResult['PROPERTY_LIST_FULL'] as $id => $arProperty) {
  if ($arProperty['CODE'] == 'REVIEW_OF') {
    $arResult['REVIEW_ABOUT'][$id] = $arProperty;
  }

  if (in_array($arProperty['CODE'], array(
    'NAME',
    'EMAIL',
    'PHONE',
  ))) {
    $arResult['FIRST_ROW'][$id] = $arProperty;
  }

  if ($id == 'NAME') {
    $arResult['SECOND_ROW'][$id] = $arProperty;
  }

  if ($id == 'PREVIEW_TEXT') {
    $arResult['THIRD_ROW'][$id] = $arProperty;
  }

  if ($arProperty['CODE'] == 'PRODUCT_ID') {
    $arResult['PRODUCT'][$id] = $arProperty;
  }
}

// Get last buy products
if ($USER->IsAuthorized() && \Bitrix\Main\Loader::includeModule('sale')) {
  $productIds = array();
  $basketRes = \CSaleBasket::GetList(array(
    'ID' => 'DESC'
  ), array(
    '!ORDER_ID' => false,
    'FUSER_ID' => \CSaleBasket::GetBasketUserID()
  ), array(
    'PRODUCT_ID',
    'FUSER_ID',
    'ORDER_ID'
  ), array(
    'nTopCount' => 10
  ));

  $basketIds = array();

  while ($basketItem = $basketRes->Fetch()) {
    $arResult['BASKET_ITEMS'][] = $basketItem;
    $basketIds[] = $basketItem['PRODUCT_ID'];
  }

  $basketIds = array_unique($basketIds);
  $productsInfo = \CCatalogSKU::getProductList($basketIds);

  foreach ($basketIds as $k => $basketId) {
    if (isset($productsInfo[$basketId])) {
      unset($basketIds[$k]);
    }
  }

  $productIds = $basketIds;

  foreach ($productsInfo as $product) {
    $productIds[] = $product['ID'];
  }

  // Make slice to two products
  $productIds = array_slice(array_unique($productIds), 0, 2);
  $products = array();

  if (!empty($productIds)) {
    $productsRes = \CIBlockElement::GetList(array(), array(
      'ID' => $productIds
    ), false, false, array(
      'ID',
      'NAME',
      'IBLOCK_ID',
      'PREVIEW_PICTURE',
      'DETAIL_PICTURE',
      'PROPERTY_CML2_LINK'
    ));

    $fileIds = array();
    // get picture
    while ($product = $productsRes->Fetch()) {
      $product['PICTURE'] = false;

      if ($product['DETAIL_PICTURE']) {
        $product['PICTURE'] = $product['DETAIL_PICTURE'];
      } elseif ($product['PREVIEW_PICTURE']) {
        $product['PICTURE'] = $product['PREVIEW_PICTURE'];
      }

      if (intval($product['PICTURE']) > 0) {
        $fileIds[] = $product['PICTURE'];
      }

      $arResult['LAST_PRODUCTS'][$product['ID']] = $product;
    }

    $files = array();

    if (!empty($fileIds)) {
      $fileRes = CFile::GetList(array(), array(
        '@ID' => implode(',', $fileIds)
      ));

      while ($file = $fileRes->Fetch()) {
        $resize = CFile::ResizeImageget($file, array(
          'width' => 80,
          'height' => 80
        ), BX_RESIZE_IMAGE_EXACT, true);

        if ($resize) {
          $file['SRC'] = $resize['src'];
          $file['WIDTH'] = $resize['width'];
          $file['HEIGHT'] = $resize['height'];
          $file['SIZE'] = $resize['size'];
        }

        $files[$file['ID']] = $file;
      }
    }

    foreach ($arResult['LAST_PRODUCTS'] as &$product) {
      if (isset($files[$product['PICTURE']])) {
        $product['PICTURE'] = $files[$product['PICTURE']];
      }

      $product['PRICE'] = getFinalPriceInCurrency($product['ID']);
      $product['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($product['PRICE'], 'RUB');
    }
  }
}

// Rules and messages for validate fields
$arResult['RULES'] = $arResult['RULES_MESSAGES'] = array();

foreach ($arParams['PROPERTY_CODES_REQUIRED'] as $required) {
  $arResult['RULES']['PROPERTY['.$required.'][0]'] = array(
    'required' => true
  );

  $arResult['RULES_MESSAGES']['PROPERTY['.$required.'][0]'] = '';
}

// Process request productId
$arResult['SELECTED_PRODUCT'] = false;
$productId = isset($_REQUEST['productId']) ? intval($_REQUEST['productId']) : 0;

if ($productId > 0) {
  $productR = \CIBlockElement::GetList(array(), array(
    'ID' => $productId
  ), false, false, array(
    'ID',
    'NAME'
  ))->Fetch();

  if ($productR) {
    $arResult['SELECTED_PRODUCT'] = $productR;
  }
}