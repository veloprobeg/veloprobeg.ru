BX.debugEnable(true);

(function (window) {
  if (!!window.CJSAddReview) {
    return;
  }

  window.CJSAddReview = function (params) {
    this.obProduct = null;
    this.select = null;
    this.reviewOf = null;

    this.minSearchText = params.MIN_SEARCH_TEXT || 3;
    this.ajaxUrl = params.URL || null;
    this.rules = params.RULES;
    this.rulesMessages = params.RULES_MESSAGES;

    this.selectedProduct = params.SELECTED_PRODUCT;
    this.reviewOfProudct = params.REVIEW_OF_PRODUCT;

    if (!!params.PRODUCT_ID) {
      this.obProduct = BX(params.PRODUCT_ID);
    }

    if (!!params.LAST_PRODUCTS_WRAPPER_ID) {
      this.obLastProductsWrapper = BX(params.LAST_PRODUCTS_WRAPPER_ID);
    }

    this.params = params;
    this.init();
  };

  window.CJSAddReview.prototype = {
    init: function () {
      if (!!this.obLastProductsWrapper) {
        BX.bindDelegate(this.obLastProductsWrapper, 'click', {
          attrs: {
            className: 'i-prew--transp'
          }
        }, BX.proxy(this.selectLastProduct, this));
      }

      $('.add-comment').validate({
        rules: this.rules,
        messages: this.rulesMessages
      });

      this.initReviewOf();
      this.initSelect();

      if (this.selectedProduct && this.selectedProduct.hasOwnProperty('ID')) {
        this.select.val(this.selectedProduct['ID']).trigger('change');
        this.setReviewOf(this.reviewOfProudct);
      }
    },

    initSelect: function () {
      this.select = $('.select--product').select2({
        dropdownCssClass: 'select--product',
        dropdownParent: $('.add-comment__input--item-name'),
        debug: true,
        ajax: {
          url: this.ajaxUrl,
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            console.log('data:', data);
            params.page = params.page || 1;

            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) {
          return markup;
        },
        minimumInputLength: this.minSearchText,
        templateResult: this.formatRepo,
        templateSelection: this.formatRepoSelection
      }).on('select2:select', BX.proxy(function () {
        $('.i-prew--transp').removeClass('is-active').children('input[type=radio]').attr('checked', false);
        this.setReviewOf(this.reviewOfProudct);
      }, this));
    },

    initReviewOf: function() {
      this.reviewOf = $('.select--comment').select2({
        dropdownCssClass: 'select--comment',
        dropdownParent: $('.add-comment__select'),
        minimumResultsForSearch: Infinity // Скрывет поле поиска
      });
    },

    setReviewOf: function(reviewOf) {
      console.info('setReviewOf');
      this.reviewOf.val(reviewOf).trigger('change');
    },

    formatRepo: function (repo) {
      if (repo.loading) {
        return repo.text;
      }

      var picture = '';

      if (repo.hasOwnProperty('PICTURE') && !!repo.PICTURE && repo.PICTURE.hasOwnProperty('SRC')) {
        picture += '<img src="' + repo.PICTURE.SRC + '" class="i-prew__img" alt="">';
      }

      if (repo.hasOwnProperty('PREVIEW_PICTURE') && !!repo.PREVIEW_PICTURE && repo.PREVIEW_PICTURE.hasOwnProperty('SRC')) {
        picture += '<img src="' + repo.PREVIEW_PICTURE.SRC + '" class="i-prew__img" alt="">';
      }

      if (repo.hasOwnProperty('DETAIL_PICTURE') && !!repo.DETAIL_PICTURE && repo.DETAIL_PICTURE.hasOwnProperty('SRC')) {
        picture += '<img src="' + repo.DETAIL_PICTURE.SRC + '" class="i-prew__img" alt="">';
      }

      return '<a href="#" class="i-prew i-prew--transp" data-id="' + repo.ID + '">' +
        '<div class="i-prew__ok"></div>' +
        '<div class="i-prew__image">' + picture + '</div>' +
        '<div class="i-prew__about">' +
        '<div class="i-prew__name">' + repo.NAME + '</div>' +
        '<div class="i-prew__price">' + repo.PRICE + ' <span class="icon-rouble">i</span></div>' +
        '</div>' +
        '</a>';
    },

    formatRepoSelection: function (repo) {
      return repo.NAME || repo.text;
    },

    selectLastProduct: function (e) {
      var product = e.target;

      if (!BX.hasClass(product, 'i-prew--transp')) {
        product = BX.findParent(product, {
          className: 'i-prew--transp'
        }, 1);
      }

      if (!!product) {
        this.select.val(BX.data(product, 'id')).trigger('change');

        $(product).addClass('is-active').children('input[type=radio]').attr('checked', true);
        $(product).siblings().removeClass('is-active').children('input[type=radio]').attr('checked', false);
      }

      e.stopPropagation();
    }
  }
})(window);