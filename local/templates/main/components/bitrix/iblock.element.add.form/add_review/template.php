<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

use \Bitrix\Main\Localization\Loc;

if (!empty($arResult["ERRORS"])):?>
  <? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
  <div id="success">
    <? ShowNote($arResult["MESSAGE"]) ?>
  </div>
  <script>
    setTimeout(function () {
      BX.remove(BX('success'));
    }, 5000);
  </script>
<? endif ?>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data"
      class="add-comment">
  <?= bitrix_sessid_post() ?>
  <div class="add-comment__head">
    <div class="add-comment__navs">
      <div class="add-comment__area">
        <? foreach ($arResult['REVIEW_ABOUT'] as $id => $about): ?>
          <div class="add-comment__area-name"><?= $about['NAME'] ?>:</div>
          <div class="add-comment__select">
            <select class="select default-input select--comment" name="PROPERTY[<?= $id ?>][0]">
              <? foreach ($about['ENUM'] as $element): ?>
                <option value="<?= $element['ID'] ?>"><?= $element['VALUE'] ?></option>
              <? endforeach ?>
            </select>
          </div>
        <? endforeach; ?>
      </div>
      <? foreach ($arResult['PRODUCT'] as $id => $product): ?>
        <div class="add-comment__area add-comment__area--item">
          <div class="add-comment__area-name"><?= $product['NAME'] ?>:</div>
          <div class="add-comment__input add-comment__input--item-name">
            <select class="select default-input select--product" name="PROPERTY[<?= $id ?>][0]"
                    id="<?= $product['CODE'] ?>">
              <option value=""><?= Loc::getMessage("CHOICE_PRODUCT"); ?></option>
              <? foreach ($arResult['LAST_PRODUCTS'] as $lastProduct): ?>
                <option value="<?= $lastProduct['ID'] ?>"><?= $lastProduct['NAME'] ?></option>
              <? endforeach ?>
              <? if ($arResult['SELECTED_PRODUCT']): ?>
                <option value="<?= $arResult['SELECTED_PRODUCT']['ID'] ?>"><?= $arResult['SELECTED_PRODUCT']['NAME'] ?></option>
              <? endif ?>
            </select>
          </div>
        </div>
      <? endforeach ?>
    </div>
    <? if ($arResult['LAST_PRODUCTS']): ?>
      <div class="add-comment__area add-comment__area--items">
        <div class="add-comment__area-name"><?= Loc::getMessage("CHOICE_IN_LAST_BUY_PRODUCTS"); ?></div>
        <div class="add-comment__area-items" id="add-comment__area-items">
          <? foreach ($arResult['LAST_PRODUCTS'] as $product): ?>
            <a href="javascript:void(0)" class="i-prew i-prew--transp" data-id="<?= $product['ID'] ?>">
              <div class="i-prew__ok"></div>
              <div class="i-prew__image"><img src="<?= $product['PICTURE']['SRC'] ?>" class="i-prew__img" alt="">
              </div>
              <div class="i-prew__about">
                <div class="i-prew__name"><?= $product['NAME'] ?></div>
                <div class="i-prew__price"><?= $product['PRICE_FORMATED']; ?></div>
              </div>
              <input type="radio" name="type" value="1" class="i-prew__radio"></a>
          <? endforeach ?>
        </div>
      </div>
    <? endif ?>
  </div>
  <div class="add-comment__body">
    <div class="add-comment__row">
      <? foreach ($arResult['FIRST_ROW'] as $id => $field): ?>
        <div class="add-comment__area add-comment__area--info">
          <div class="add-comment__area-name"><?= $field['NAME'] ?></div>
          <div class="add-comment__input">
            <input type="text" value="<?= $field['VALUE'] ?>" name="PROPERTY[<?= $id ?>][0]"
                   class="default-input<?= $field['CODE'] == 'PHONE' ? ' mask-phone' : '' ?>">
          </div>
        </div>
      <? endforeach ?>
    </div>
    <div class="add-comment__row">
      <? foreach ($arResult['SECOND_ROW'] as $id => $field): ?>
        <div class="add-comment__area add-comment__area--head">
          <div class="add-comment__area-name"><?= $id == 'NAME' ? Loc::getMessage("TITLE") : $field['NAME'] ?></div>
          <div class="add-comment__input">
            <input type="text" value="<?= $field['VALUE'] ?>" name="PROPERTY[<?= $id ?>][0]" class="default-input">
          </div>
        </div>
      <? endforeach ?>
    </div>
    <div class="add-comment__row">
      <? foreach ($arResult['THIRD_ROW'] as $id => $field): ?>
        <div class="add-comment__area add-comment__area--text">
          <div
              class="add-comment__area-name"><?= $id == 'PREVIEW_TEXT' ? Loc::getMessage("REVIEW_TEXT") : $field['NAME'] ?></div>
          <div class="add-comment__textarea">
            <textarea name="PROPERTY[<?= $id ?>][0]" class="add-comment__text-area"><?= $field['VALUE'] ?></textarea>
          </div>
        </div>
      <? endforeach ?>
    </div>
    <div class="add-comment__row add-comment__row--center">
      <button type="submit" class="button button--orange"><?= Loc::getMessage("IBLOCK_FORM_SUBMIT") ?></button>
    </div>
  </div>
  <input type="hidden" name="iblock_submit" value="<?= Loc::getMessage("IBLOCK_FORM_SUBMIT") ?>"/>
</form>
<?
$jsParams = array(
  'PRODUCT_ID' => 'PRODUCT_ID',
  'LAST_PRODUCTS_WRAPPER_ID' => 'add-comment__area-items',
  'MIN_SEARCH_TEXT' => 3,
  'URL' => $templateFolder.'/get_products.php',
  'RULES' => $arResult['RULES'],
  'RULES_MESSAGES' => $arResult['RULES_MESSAGES'],
  'SELECTED_PRODUCT' => $arResult['SELECTED_PRODUCT'],
  'REVIEW_OF_PRODUCT' => 20,
);
?>
<script>
  var cCJSAddReview = null;
  BX.ready(function () {
    cCJSAddReview = new CJSAddReview(<?= CUtil::PhpToJSObject($jsParams)?>);
  });
</script>
