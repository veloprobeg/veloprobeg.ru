<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */


$newArMap = array(
  array(
    "DESCRIPTION" => "",
    "FULL_PATH" => "/",
    "ID" => md5($_SERVER['DOCUMENT_ROOT']),
    "IS_DIR" => "Y",
    "LEVEL" => "0",
    "NAME" => "Главная",
    "PATH" => "/wwwroot/sites/veloprobeg/",
    "SEARCH_PATH" => "/"
  )
);

$isCatalogAdd = false;
$incLevel = false;
$countCatalogElements = $countSecondLevel = 0;
$countByLevel = array();

foreach ($arResult['arMap'] as $item) {
  if (preg_match('/^\/catalog/', $item['FULL_PATH'])) {
    $item['IS_CATALOG'] = true;
    $countCatalogElements++;

    if ($item['LEVEL'] == 0) {
      $countSecondLevel++;
    }

    $countByLevel[$item['LEVEL']]++;

    if (!$isCatalogAdd) {
      $newArMap[] = array(
        "DESCRIPTION" => "",
        "FULL_PATH" => "/catalog/",
        "ID" => md5($_SERVER['DOCUMENT_ROOT'].'/catalog/'),
        "IS_DIR" => "Y",
        "LEVEL" => "0",
        "NAME" => "Каталог",
        "PATH" => "/wwwroot/sites/veloprobeg/catalog/",
        "SEARCH_PATH" => "/catalog"
      );

      $isCatalogAdd = true;
      $incLevel = true;
    } else {
      if ($isCatalogAdd && $incLevel && !preg_match('/^\/catalog/', $item['FULL_PATH'])) {
        $incLevel = false;
      }
    }

    if ($isCatalogAdd && $incLevel) {
      $item['LEVEL']++;
      $newArMap[] = $item;
    }
  } else {
    $newArMap[] = $item;
  }
}

$arResult['newArMap'] = $newArMap;
$arResult['arMap'] = $newArMap;
$arResult['countCatalogElement'] = $countCatalogElements;
$arResult['countSecondLevel'] = $countSecondLevel;
$arResult['countByLevel'] = $countByLevel;