<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

if (!is_array($arResult["arMap"]) || count($arResult["arMap"]) < 1) {
  return;
}

// Открыт ли тег обертки
$isWrapperAdd = false;
// Отерыт ли тег обертки-каталога
$isCatalogWrapperAdd = false;
// Зактыр ли тег обертки-каталога
$isCatalogWrapperClosed = false;

// Открыта ли колонка
$isColumnAdd = false;
// Открыт ли второй уровень
$isSecondLevelAdd = false;
// Отерыт ли третий уровень
$isThirdLevelAdd = false;
$isFirstCol = true;
// Нужно ли делать колонку
$needColumn = false;
// Нужно ли использовать "Делать колонку"
$needColumnUse = false;
// Количество элементов до первого элемента каталога
$noCatalog = 0;
?>
<div class="sitemap">
  <? foreach ($arResult['arMap'] as $index => $arItem): ?>
  <?
    if (!$arItem['IS_CATALOG']) {
      $noCatalog++;
    }

    if ($needColumnUse) {
      $needColumn = false;
        $needColumnUse = false;
    }

    if (($index - $noCatalog) % (round($arResult['countCatalogElement'] / 3))== 0) {
      $needColumn = true;
    }

    if ($arItem['LEVEL'] == 0): ?>
      <? if ($isColumnAdd): ?>
        </div>
        <!--close sitemap__list-col -->
        <? $isColumnAdd = false; ?>
      <? endif ?>
      <? if ($isCatalogWrapperAdd && !$isCatalogWrapperClosed): ?>
        </div>
        <!-- close sitemap__lists -->
        <? $isCatalogWrapperClosed = true; ?>
      <? endif ?>
      <a href="<?= $arItem['FULL_PATH'] ?>" class="sitemap__link"><?= $arItem['NAME'] ?></a>
    <? else: ?>
      <? if ($arItem['IS_CATALOG']): ?>
        <? if ($arItem['LEVEL'] == 1): ?>
          <? if (!$isCatalogWrapperAdd && !$isCatalogWrapperClosed): ?>
            <!-- open sitemap__lists -->
            <div class="sitemap__lists">
            <? $isCatalogWrapperAdd = true ?>
          <? endif ?>
          <? if ($needColumn): ?>
            <? if (!$isFirstCol): ?>
              </div>
              <!--close sitemap__list-col -->
            <? endif ?>
            <? $isFirstCol = false; ?>
            <!-- open sitemap__list-col -->
            <div class="sitemap__list-col">
            <? $isColumnAdd = true; $needColumnUse = true; ?>
          <? endif ?>
          <? if (!$isSecondLevelAdd): ?>
            <!-- open sitemap__list-wrapper -->
            <div class="sitemap__list-wrapper">
              <a href="<?= $arItem['FULL_PATH'] ?>" class="sitemap__list-head"><?= $arItem['NAME'] ?></a>
            <? $isSecondLevelAdd = true;?>
            <? if (isset($arResult['arMap'][$index + 1]) && $arItem['LEVEL'] == $arResult['arMap'][$index + 1]['LEVEL']): ?>
              </div>
              <!-- close sitemap__list-wrapper -->
              <? $isSecondLevelAdd = false;?>
            <? endif; ?>
          <? endif ?>
        <? endif ?>
        <? if ($arItem['LEVEL'] == 2): ?>
          <? if (!$isThirdLevelAdd): ?>
            <!-- open sitemap__list -->
            <ul class="sitemap__list">
            <? $isThirdLevelAdd = true ?>
          <? endif ?>
          <li class="sitemap__list-item">
            <a href="<?= $arItem['FULL_PATH'] ?>" class="sitemap__list-link"><?= $arItem['NAME'] ?></a>
          </li>
          <? if ($isThirdLevelAdd && isset($arResult['arMap'][$index + 1]) && $arItem['LEVEL'] > $arResult['arMap'][$index + 1]['LEVEL']): ?>
            </ul>
            <!-- close sitemap__list -->
            <? $isThirdLevelAdd = false ?>
          <? endif; ?>
          <? if (isset($arResult['arMap'][$index + 1]) && $arItem['LEVEL'] > $arResult['arMap'][$index + 1]['LEVEL'] && $isSecondLevelAdd): ?>
            </div>
            <!-- close sitemap__list-wrapper -->
            <? $isSecondLevelAdd = false ?>
          <? endif; ?>
        <? endif ?>
      <? else: ?>
        <? if (!$isWrapperAdd): ?>
          <!-- open sitemap__inner-list -->
          <ul class="sitemap__inner-list">
          <? $isWrapperAdd = true ?>
        <? endif ?>
        <li class="sitemap__inner-list-item">
          <a href="<?= $arItem['FULL_PATH'] ?>" class="sitemap__inner-list-link"><?= $arItem['NAME'] ?></a>
        </li>
        <? if ($isWrapperAdd && isset($arResult['arMap'][$index + 1]) && $arResult['arMap'][$index + 1]['LEVEL'] == 0): ?>
          </ul>
          <!-- close sitemap__inner-list -->
        <? endif ?>
      <? endif ?>
    <? endif ?>
  <? endforeach ?>
</div>
