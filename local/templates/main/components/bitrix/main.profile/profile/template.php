<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
?>
<form class="data__form data__form--main" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
      enctype="multipart/form-data">
  <? ShowError($arResult["strProfileError"]); ?>
  <? if ($arResult['DATA_SAVED'] == 'Y'): ?>
    <div class="data__form--success" id="data__form--success"><?= Loc::getMessage('PROFILE_DATA_SAVED') ?></div>
    <script>
      setTimeout(function () {
        BX.remove(BX('data__form--success'));
      }, 5000);
    </script>
  <? endif ?>
  <?= $arResult["BX_SESSION_CHECK"] ?>
  <input type="hidden" name="lang" value="<?= LANG ?>"/>
  <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>">
  <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>">
  <div class="reg__section reg__section--data">
    <div class="reg__row">
      <div class="reg__block">
        <div class="reg__name"><?= Loc::getMessage('LAST_NAME') ?></div>
        <input type="text" name="LAST_NAME" value="<?= $arResult["arUser"]["LAST_NAME"] ?>"
               class="default-input default-input--normal">
      </div>
      <div class="reg__block">
        <div class="reg__name"><?= Loc::getMessage('NAME') ?></div>
        <input type="text" name="NAME" value="<?= $arResult["arUser"]["NAME"] ?>"
               class="default-input default-input--normal">
      </div>
      <div class="reg__block reg__block--gender">
        <div class="reg__name"><?= Loc::getMessage('USER_GENDER') ?></div>
        <div class="reg__radio">
          <input type="radio" name="PERSONAL_GENDER" value="M" id="male"
                 class="radio"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " checked" : "" ?>>
          <label for="male"><?= Loc::getMessage("USER_MALE") ?></label>
        </div>
        <div class="reg__radio">
          <input type="radio" name="PERSONAL_GENDER" value="F" id="female"
                 class="radio"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " checked" : "" ?>>
          <label for="female"><?= Loc::getMessage("USER_FEMALE") ?></label>
        </div>
      </div>
      <div class="reg__block reg__block--bday">
        <div class="reg__name"><?= Loc::getMessage("USER_BIRTHDAY_DT") ?></div>
        <input type="text" name="PERSONAL_BIRTHDAY" value="<?= $arResult["arUser"]["PERSONAL_BIRTHDAY"] ?>"
               class="default-input default-input--small mask-bday">
      </div>
    </div>
    <div class="reg__row">
      <div class="reg__block">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_BONUS_CARD']['EDIT_FORM_LABEL'] ?></div>
        <input type="text" value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_BONUS_CARD']['VALUE'] ?>"
               class="default-input default-input--normal mask-card" name="UF_BONUS_CARD">
      </div>
      <div class="reg__block">
        <div class="reg__name"><?= Loc::getMessage('EMAIL') ?></div>
        <input type="email" name="EMAIL" value="<? echo $arResult["arUser"]["EMAIL"] ?>"
               class="default-input default-input--normal">
      </div>
      <div class="reg__block">
        <div class="reg__name"><?= Loc::getMessage('USER_PHONE') ?></div>
        <input type="text" name="PERSONAL_PHONE" value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"
               class="default-input default-input--normal mask-phone">
      </div>
    </div>
    <input type="hidden" name="save" value="<?= Loc::getMessage("MAIN_SAVE") ?>">
    <button type="submit" class="button button--orange data__submit"><?= Loc::getMessage("MAIN_SAVE") ?></button>
  </div>
</form>