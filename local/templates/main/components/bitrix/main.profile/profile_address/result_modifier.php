<?
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

$startTime = $endTime = 0;
if ($arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_FINISH']['VALUE'] > 0) {
  $endTime = $arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_FINISH']['VALUE'];
}

if ($arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_START']['VALUE'] > 0) {
  $startTime = $arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_START']['VALUE'];
}

$arResult['START_DELIVERY_TIME_JS'] = getTimeSliderJS($startTime);
$arResult['END_DELIVERY_TIME_JS'] = getTimeSliderJS($endTime);

if (!empty($arResult['USER_PROPERTIES']['DATA']['UF_CITY']['VALUE'])) {
  $arResult['USER_PROPERTIES']['DATA']['UF_CITY']['CITY'] = \Bitrix\Sale\Location\LocationTable::getList(array(
    'select' => array(
      'ID' => 'ID',
      'CODE' => 'CODE',
      'CITY_NAME' => 'NAME.NAME',
    ),
    'filter' => array(
      'TYPE.CODE' => 'CITY',
      'NAME.LANGUAGE_ID' => 'ru',
      'CODE' => $arResult['USER_PROPERTIES']['DATA']['UF_CITY']['VALUE']
    )
  ))->fetch();
}