<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use \Bitrix\Main\Localization\Loc;
?>
<form class="data__form data__form--address" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
      enctype="multipart/form-data">
  <? ShowError($arResult["strProfileError"]); ?>
  <? if ($arResult['DATA_SAVED'] == 'Y'): ?>
    <div class="data__form--success" id="data__form--success"><?= Loc::getMessage('PROFILE_DATA_SAVED') ?></div>
    <script>
      setTimeout(function () {
        BX.remove(BX('data__form--success'));
      }, 5000);
    </script>
  <? endif ?>
  <?= $arResult["BX_SESSION_CHECK"] ?>
  <input type="hidden" name="lang" value="<?= LANG ?>"/>
  <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>">
  <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>">
  <input type="hidden" name="EMAIL" value="<?= $arResult["arUser"]["EMAIL"] ?>">
  <input type="hidden" name="PERSONAL_GENDER" value="<?= $arResult["arUser"]["PERSONAL_GENDER"] ?>">
  <div class="reg__section reg__section--data">
    <div class="reg__head"><?= Loc::getMessage('ADDRESS') ?></div>
    <div class="reg__row">
      <div class="reg__block">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_CITY']['EDIT_FORM_LABEL'] ?></div>
        <?
        $APPLICATION->IncludeComponent(
          "bitrix:sale.location.selector.search",
          "choice_location",
          array(
            "COMPONENT_TEMPLATE" => "choice_location",
            "ID" => "",
            "CODE" => $arResult['USER_PROPERTIES']['DATA']['UF_CITY']['CITY']['CODE'],
            "INPUT_NAME" => 'UF_CITY',
            "PROVIDE_LINK_BY" => "code",
            "FILTER_BY_SITE" => "N",
            "SHOW_DEFAULT_LOCATIONS" => "Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "JS_CALLBACK" => "",
            "SUPPRESS_ERRORS" => "N",
            "INITIALIZE_BY_GLOBAL_EVENT" => "",
            "FILTER_SITE_ID" => "current",
            'JS_CONTROL_GLOBAL_ID' => 'REGISTER_CITY_SEARCH'
          ),
          false
        );
        ?>
      </div>
      <div class="reg__block">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_STREET']['EDIT_FORM_LABEL'] ?></div>
        <input type="text" name="UF_STREET" value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_STREET']['VALUE'] ?>" class="default-input default-input--normal">
      </div>
      <div class="reg__block reg__block--small">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_HOUSE']['EDIT_FORM_LABEL'] ?></div>
        <input type="text" name="UF_HOUSE" value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_HOUSE']['VALUE'] ?>" class="default-input default-input--small">
      </div>
      <div class="reg__block reg__block--small">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_HOUSING']['EDIT_FORM_LABEL'] ?></div>
        <input type="text" name="UF_HOUSING" value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_HOUSING']['VALUE'] ?>" class="default-input default-input--small">
      </div>
    </div>
    <div class="reg__row">
      <div class="reg__block reg__block--small">
        <div class="reg__name"><?= $arResult['USER_PROPERTIES']['DATA']['UF_INDEX']['EDIT_FORM_LABEL'] ?></div>
        <input type="text" name="UF_INDEX" value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_INDEX']['VALUE'] ?>" class="default-input default-input--small">
      </div>
    </div>
    <div class="reg__row">
      <div class="reg__block reg__block--timer">
        <div class="reg__name"><?= Loc::getMessage('DELIVERY_TIME') ?></div>
        <div class="reg__timer">
          <div class="reg__time"><span class="reg__desc"><?= $arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_START']['EDIT_FORM_LABEL'] ?></span>
            <input id="startTime" class="default-input default-input--small" value="<?= $startTime ?>" name="UF_DELIV_TIME_START"><span
                class="reg__desc"><?= $arResult['USER_PROPERTIES']['DATA']['UF_DELIV_TIME_FINISH']['EDIT_FORM_LABEL'] ?></span>
            <input id="endTime" class="default-input default-input--small" value="<?= $endTime ?>" name="UF_DELIV_TIME_FINISH">
          </div>
          <div id="reg__timer"></div>
        </div>
      </div>
    </div>
    <input type="hidden" name="save" value="<?= Loc::getMessage("MAIN_SAVE") ?>">
    <button type="submit" class="button button--orange data__submit"><?= Loc::getMessage("MAIN_SAVE") ?></button>
  </div>
  <script>
    $(function() {
      regTimer(<?= $arResult['START_DELIVERY_TIME_JS'] ?>, <?= $arResult['END_DELIVERY_TIME_JS'] ?>);
    });
  </script>
</form>