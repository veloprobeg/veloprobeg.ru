<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use \Bitrix\Main\Localization\Loc;
?>
<form class="data__form data__form--password" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
      enctype="multipart/form-data">
  <? ShowError($arResult["strProfileError"]); ?>
  <? if ($arResult['DATA_SAVED'] == 'Y'): ?>
    <div class="data__form--success" id="data__form--success"><?= Loc::getMessage('PROFILE_DATA_SAVED') ?></div>
    <script>
      setTimeout(function () {
        BX.remove(BX('data__form--success'));
      }, 5000);
    </script>
  <? endif ?>
  <?= $arResult["BX_SESSION_CHECK"] ?>
  <input type="hidden" name="lang" value="<?= LANG ?>"/>
  <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>">
  <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>">
  <input type="hidden" name="EMAIL" value="<?= $arResult["arUser"]["EMAIL"] ?>">
  <input type="hidden" name="PERSONAL_GENDER" value="<?= $arResult["arUser"]["PERSONAL_GENDER"] ?>">
  <div class="reg__section reg__section--no-border">
    <div class="reg__head"></div>
    <div class="reg__row">
      <div class="reg__block">
        <a href="#" class="reg__showpw showpw"></a>
        <div class="reg__name"><?= Loc::getMessage('NEW_PASSWORD_REQ') ?></div>
        <input type="password" name="NEW_PASSWORD" value="" id="password"
               class="default-input default-input--normal default-input--pw">
      </div>
      <div class="reg__block">
        <a href="#" class="reg__showpw showpw"></a>
        <div class="reg__name"><?= Loc::getMessage('NEW_PASSWORD_CONFIRM') ?></div>
        <input type="password" name="NEW_PASSWORD_CONFIRM" value=""
               class="default-input default-input--normal default-input--pw">
      </div>
    </div>
    <div class="data__checks">
      <!--<div class="checkbox-area">
        <input type="checkbox" name="UF_PROC_PERS_DATA" id="UF_PROC_PERS_DATA_LABEL"
               class="checkbox"<?/*= $arResult['USER_PROPERTIES']['DATA']['UF_PROC_PERS_DATA']['VALUE'] == 1 ? ' checked' : '' */?>
               value="<?/*= $arResult['USER_PROPERTIES']['DATA']['UF_PROC_PERS_DATA']['VALUE'] */?>">
        <label for="UF_PROC_PERS_DATA_LABEL"><?/*= $arResult['USER_PROPERTIES']['DATA']['UF_PROC_PERS_DATA']['EDIT_FORM_LABEL'] */?></label>
        <input type="hidden" name="UF_PROC_PERS_DATA" id="UF_PROC_PERS_DATA"
               value="<?/*= $arResult['USER_PROPERTIES']['DATA']['UF_PROC_PERS_DATA']['VALUE'] */?>">
        <script>
          BX.bind(BX('UF_PROC_PERS_DATA_LABEL'), 'change', function(e) {
            BX('UF_PROC_PERS_DATA').value = e.target.checked ? '1' : '0';
          });
        </script>
      </div>-->
      <div class="checkbox-area">
        <input type="checkbox" name="" id="UF_SUBSCRIBE_LABEL"
               class="checkbox"<?= $arResult['USER_PROPERTIES']['DATA']['UF_SUBSCRIBE']['VALUE'] == 1 ? ' checked' : '' ?>>
        <label for="UF_SUBSCRIBE_LABEL"><?= $arResult['USER_PROPERTIES']['DATA']['UF_SUBSCRIBE']['EDIT_FORM_LABEL'] ?></label>
        <input type="hidden" name="UF_SUBSCRIBE" id="UF_SUBSCRIBE"
               value="<?= $arResult['USER_PROPERTIES']['DATA']['UF_SUBSCRIBE']['VALUE'] ?>">
        <script>
          BX.bind(BX('UF_SUBSCRIBE_LABEL'), 'change', function(e) {
            BX('UF_SUBSCRIBE').value = e.target.checked ? '1' : '0';
          });
        </script>
      </div>
    </div>
    <input type="hidden" name="save" value="<?= Loc::getMessage("MAIN_SAVE") ?>">
    <button type="submit" class="button button--orange data__submit"><?= Loc::getMessage("MAIN_SAVE") ?></button>
  </div>
</form>