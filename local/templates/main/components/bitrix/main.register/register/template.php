<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
 CJSCore::Init(array(
                'fx',
                'popup',
                'window',
                'ajax',
                'easing',
		'jquery',
            ));
?>
<? if ($USER->IsAuthorized()): ?>

    <p><? echo Loc::getMessage("MAIN_REGISTER_AUTH") ?></p>

<? else: ?>
    <?
    if (count($arResult["ERRORS"]) > 0):
	echo "<div id='reg_errors' style='display: none;'>";
        foreach ($arResult["ERRORS"] as $key => $error) {
            if (intval($key) == 0 && $key !== 0) {
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . Loc::getMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
            }
        }

        ShowError(implode("<br />", $arResult["ERRORS"]));
	echo "</div>";
    elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
        ?>
        <p><? echo Loc::getMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
    <? endif ?>
    <?
    $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "register",
        array(
            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            "AUTH_URL" => $arResult["AUTH_URL"],
            "POST" => $arResult["POST"],
            "POPUP" => "Y",
            "SUFFIX" => "form",
        ),
        $component,
        array("HIDE_ICONS" => "Y")
    );
    $checkBoxes = $personalDataFirstRow = $personalDataSecondRow = $personalDataSecondRowEnd = $addressSectionFirstRow = $addressSectionSecondRow = $passwordFields = '';

    ?>
    <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" id="reg_form" enctype="multipart/form-data"
          class="reg">
        <?
        if ($arResult["BACKURL"] <> ''):
            ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
            <?
        endif;
        ?>
        <input type="hidden" name="REGISTER[LOGIN]" value="<?= $arResult["VALUES"]['LOGIN'] ?>"/>
        <? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
            <?
            if (in_array($FIELD, array(
                'PASSWORD',
                'CONFIRM_PASSWORD'
            ))) {
                $passwordFields .= '<div class="reg__block">
          <a href="#" class="reg__showpw showpw"></a>
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . '<span class="req-orange"> * </span></div>
          <input type="password" name="REGISTER[' . $FIELD . ']" id="' . $FIELD . '" value="' . $arResult["VALUES"][$FIELD] . '"
                 class="default-input default-input--normal default-input--pw" autocomplete="off">
        </div>';
            }

            if (in_array($FIELD, array(
                'LAST_NAME',
                'NAME',
            ))) {
                $personalDataFirstRow .= '<div class="reg__block">
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? ' <span class=\'req-orange\'> * </span>' : '') . '</div>
          <input type="text" name="REGISTER[' . $FIELD . ']" placeholder="' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . '" class="default-input default-input--normal" id="' . $FIELD . '" value="' . $arResult["VALUES"][$FIELD] . '">
        </div>';
            }

            if ($FIELD == 'PERSONAL_GENDER') {
                $personalDataFirstRow .= '<div class="reg__block reg__block--gender">
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . '</div><div class="reg__radio">
            <input type="radio" name="REGISTER[' . $FIELD . ']" value="M" id="male" class="radio"' . ($arResult['VALUES'][$FIELD] == 'M' ? ' checked' : '') . '>
            <label for="male">' . Loc::getMessage("USER_MALE") . '</label>
          </div>
          <div class="reg__radio">
            <input type="radio" name="REGISTER[' . $FIELD . ']" value="F" id="female" class="radio"' . ($arResult['VALUES'][$FIELD] == 'F' ? ' checked' : '') . '>
            <label for="female">' . Loc::getMessage("USER_FEMALE") . '</label>
          </div></div>';
            }

            if ($FIELD == 'PERSONAL_BIRTHDAY') {
                $personalDataFirstRow .= '<div class="reg__block reg__block--bday">
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . '</div>
          <input type="text" name="REGISTER[' . $FIELD . ']" placeholder="08.03.1990"
                 class="default-input default-input--small mask-bday" value="' . $arResult["VALUES"][$FIELD] . '">
          </div>';
            }

            if (in_array($FIELD, array(
                'PERSONAL_PHONE'
            ))) {
                $personalDataSecondRow .= '<div class="reg__block">
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? ' <span class=\'req-orange\'> * </span>' : '') . '</div>
          <input type="text" name="REGISTER[' . $FIELD . ']" placeholder="+7 777 777 666"
                 class="default-input default-input--normal mask-phone" value="' . $arResult["VALUES"][$FIELD] . '">
        </div>';
            }

            if (in_array($FIELD, array(
                'EMAIL',
            ))) {
                $personalDataSecondRowEnd .= '<div class="reg__block">
          <div class="reg__name">' . Loc::getMessage("REGISTER_FIELD_" . $FIELD) . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y" ? ' <span class=\'req-orange\'> * </span>' : '') . '</div>
          <input type="text" name="REGISTER[' . $FIELD . ']" placeholder="info@veloprobeg.ru"
                 class="default-input default-input--normal" value="' . $arResult["VALUES"][$FIELD] . '">
        </div>';
            }

            ?>
        <? endforeach ?>
        <? // ********************* User properties ***************************************************?>

        <? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
            <? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                <?
                if (in_array($FIELD_NAME, array(
                    'UF_SUBSCRIBE',
                    'UF_PROC_PERS_DATA'
                ))) {
                    $checkBoxes .= '
            <div class="checkbox-area">
              <input type="checkbox" name="' . $FIELD_NAME . '" id="' . $FIELD_NAME . '" class="checkbox"' . ($arResult["VALUES"][$FIELD_NAME] == 'on' ? ' checked' : '') . ' value="1">
              <label for="' . $FIELD_NAME . '">' . $arUserField['EDIT_FORM_LABEL'] . '</label>
            </div>
            ';
                }

                if (in_array($FIELD_NAME, array(
                    'UF_CITY',
                    'UF_STREET',
                    'UF_HOUSE',
                    'UF_HOUSING',
                    'UF_INDEX',
                    'UF_AP_NUMBER'
                ))) {
                    $addressSectionFirstRow .= '<div class="reg__block' . (!in_array($FIELD_NAME, array(
                            'UF_CITY',
                            'UF_STREET'
                        )) ? ' reg__block--smallest' : '') . '">
              <div class="reg__name">' . $arUserField['EDIT_FORM_LABEL'] . '</div>';
                    if ($FIELD_NAME == 'UF_CITY') {
                        $currentCityCookie = $APPLICATION->get_cookie('CURRENT_CITY');
                        $this->SetViewTarget('SELECT_CITY');
                        $APPLICATION->IncludeComponent(
                            "bitrix:sale.location.selector.search",
                            "choice_location",
                            array(
                                "COMPONENT_TEMPLATE" => "choice_location",
                                "ID" => "",
                                "CODE" => $currentCityCookie,
                                "INPUT_NAME" => 'UF_CITY',
                                "PROVIDE_LINK_BY" => "code",
                                "FILTER_BY_SITE" => "N",
                                "SHOW_DEFAULT_LOCATIONS" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "JS_CALLBACK" => "",
                                "SUPPRESS_ERRORS" => "N",
                                "INITIALIZE_BY_GLOBAL_EVENT" => "",
                                "FILTER_SITE_ID" => "current",
                                'JS_CONTROL_GLOBAL_ID' => 'REGISTER_CITY_SEARCH'
                            ),
                            false
                        );
                        $this->EndViewTarget();
                        $addressSectionFirstRow .= $APPLICATION->GetViewContent('SELECT_CITY');
                    } else {
                        $addressSectionFirstRow .= '<input type="text" name="' . $FIELD_NAME . '" value="' . $arResult["VALUES"][$FIELD_NAME] . '" class="default-input default-input--' . (in_array($FIELD_NAME, array(
                                'UF_CITY',
                                'UF_STREET'
                            )) ? 'normal' : 'small') . '" placeholder="' . $arUserField['EDIT_FORM_LABEL'] . '">';
                    }

                    $addressSectionFirstRow .= '</div>';
                }

                /*if (in_array($FIELD_NAME, array(
                  'UF_DELIV_TIME_START',
                  'UF_DELIV_TIME_FINISH',
                ))) {
                  $addressSectionSecondRow .= '<span class="reg__desc">'.$arUserField['EDIT_FORM_LABEL'].'</span><input name="'.$FIELD_NAME.'" id="'.($FIELD_NAME == 'UF_DELIV_TIME_START' ? 'startTime' : 'endTime').'" class="default-input default-input--small" value="'.$arResult["VALUES"][$FIELD_NAME].'">';
                }*/

                if ($FIELD_NAME == 'UF_BONUS_CARD') {
                    $personalDataSecondRowEnd .= '<div class="reg__block">
          <div class="reg__name">' . $arUserField['EDIT_FORM_LABEL'] . ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD_NAME] == "Y" ? ' <span class="req-orange"> * </span>' : '') . '</div>
          <input type="text" name="' . $FIELD_NAME . '" placeholder=""
                 class="default-input default-input--normal" value="' . $arResult["VALUES"][$FIELD_NAME] . '">
        </div>';
                }
                ?>
            <? endforeach; ?>
        <? endif; ?>
        <? // ******************** /User properties ***************************************************?>
        <?
        /* CAPTCHA */
        if ($arResult["USE_CAPTCHA"] == "Y") {
            ?>
            <tr>
                <td colspan="2"><b><?= GetMessage("REGISTER_CAPTCHA_TITLE") ?></b></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"
                         width="180" height="40" alt="CAPTCHA"/>
                </td>
            </tr>
            <tr>
                <td><?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>:<span class="starrequired">*</span></td>
                <td><input type="text" name="captcha_word" maxlength="50" value=""/></td>
            </tr>
            <?
        }
        /* !CAPTCHA */
        ?>
        <div class="reg__section">
            <div class="reg__head"><?= Loc::getMessage("PERSONAL_DATA"); ?></div>
            <div class="reg__row">
                <?= $personalDataFirstRow ?>
            </div>
            <div class="reg__row">
                <?= $personalDataSecondRow . $personalDataSecondRowEnd ?>
            </div>
        </div>
        <div class="reg__section">
            <div class="reg__head"><?= Loc::getMessage("ADDRESS"); ?></div>
            <div class="reg__row">
                <?= $addressSectionFirstRow ?>
            </div>
            <? /*
      <div class="reg__row">
        <div class="reg__block reg__block--timer">
          <div class="reg__name"><?= Loc::getMessage("DELIVERY_TIME"); ?></div>
          <div class="reg__timer">
            <div class="reg__time">
              <?= $addressSectionSecondRow ?>
            </div>
            <div id="reg__timer"></div>
          </div>
          <div class="reg__info"><?= Loc::getMessage("DELIVERY_TIME_COMMENT"); ?></div>
        </div>
      </div>
        */ ?>
        </div>
        <div class="reg__section">
            <div class="reg__head"><?= Loc::getMessage("REGISTER_FIELD_PASSWORD"); ?></div>
            <div class="reg__row">
                <?= $passwordFields ?>
            </div>
        </div>
        <div class="reg__confirm">

            <div class="reg__confirm-check">
                <?= $checkBoxes ?>
            </div>
	    
	    <div class="reg__confirm-warn"><?= Loc::getMessage("AUTH_REQ"); ?></div>
            <div id="errors"></div>
	    
	   <div id="popup-errors" class="popup">
                <div class="popup__inner">
                    <a href="javascript:void(0)" class="popup__closer"></a>
                    <div class="popup__content-wrapper">
                        <div id="main-errors">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    
	    
            <div class="reg__confirm-buttons">
                <button type="submit" id="reg_button"
                        class="button button--orange reg__submit"><?= Loc::getMessage("AUTH_REGISTER") ?></button>
                <input type="hidden" name="register_submit_button" value="<?= Loc::getMessage("AUTH_REGISTER") ?>">
            </div>

        </div>
    </form>
    <script>
        $(function () {
            //regTimer();
        });
    </script>
    
    
<? endif ?>

<?
    $jsParams = array(
        'FORM_ID' => 'bx-soa-order-form',
        'BUTTON_ID' => 'submit-form',
        'AJAX_URL' => $APPLICATION->GetCurPage(),
        'MAIN_ERRORS_BLOCK' => 'main-errors',
        'POPUP_ERRORS_BLOCK' => 'popup-errors',
        "PUBLIC_OFFER" => 'public_offer',
        "DELIVERY_INFO" => 'reg__address',
    );
    ?>
    <?$messages = Loc::loadLanguageFile(__FILE__);?>
    <script>
        var registerAjax;
        // Пробрасываем языковые фразы в JS
        BX.message(<?= CUtil::PhpToJsObject($messages) ?>);

        BX.ready(function () {
            registerAjax = new CJSRegisterAjax(<?= CUtil::PhpToJSObject($jsParams) ?>);

        });
    </script>


<? \Debug::dtc($arResult, 'result'); ?>
