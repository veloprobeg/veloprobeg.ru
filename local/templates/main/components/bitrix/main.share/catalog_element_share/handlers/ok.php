<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/ok.php");
$name = "ok";
$title = GetMessage("BOOKMARK_HANDLER_OK");

$icon_url_template = "<a href=\"javascript:void(0)\" target=\"_blank\" class=\"item__share-link item__share-link--ok\" title=\"".$title."\"  id=\"ok_shareWidget\" onclick=\"window.open('https://connect.ok.ru/offer?url=#PAGE_URL#','sharer','toolbar=0,status=0,width=626,height=436'); \nreturn false;\"></a>";
$sort = 200;

?>