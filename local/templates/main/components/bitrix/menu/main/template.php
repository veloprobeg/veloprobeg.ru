<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"])) {
    return;
}
?>


<div class="header__menu">
    <a href="javascript:void(0)" class="header__menu-closer"></a>
    <div class="header__menu-mobile">
        <div class="header__menu-name"><?= Loc::getMessage('MM_CATALOG') ?></div>
        <a href="/pick-up-the-bike/" class="header__menu-get-bike"><?= Loc::getMessage('MM_PICK_UP_BAKE') ?></a>
    </div>
    <ul class="header__menu-list">
        <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>     <!-- first level-->
            <li class="header__menu-item <?= empty($arResult["ALL_ITEMS"][$itemID]['PARAMS']['class']) ? 'header__menu-item--dd' : $arResult['ALL_ITEMS'][$itemID]['PARAMS']['class'] ?>">
                <a href="<?= (!is_array($arColumns) || empty($arColumns)) ? $arResult["ALL_ITEMS"][$itemID]["LINK"] : 'javascript:void(0)' ?>"
                   class="header__menu-link <?= empty($arResult["ALL_ITEMS"][$itemID]['PARAMS']['classLink']) ? 'header__menu-link--dd' : $arResult['ALL_ITEMS'][$itemID]['PARAMS']['classLink'] ?>"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></a>
                <? if (is_array($arColumns) && count($arColumns) > 0): ?>
                    <div class="header__menu-dd">
                        <a href="javascript:void(0)" class="header__menu-dd-closer">
                            <div class="header__menu-dd-closer-inner"></div>
                        </a>
                        <h2 class="header__menu-dd-name"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></h2>
                        <? foreach ($arColumns as $key => $arRow): ?>
                            <div class="header__menu-dd-inner <?if ( $itemID == 3114178683 ):?>fullscreen<?endif;?> <?if ( $itemID == 1654725533 ):?>velocolumn<?endif;?>">
                                
				<?if ( $itemID == 1654725533 ):?>
					<?
					$count = 0;
					$arNewColumn = Array();
					foreach ($arRow as $itemIdLevel_2 => $arLevel_3){
						if ( $count++ >	4 ){
							$arNewColumn[$itemIdLevel_2] = $arLevel_3;
							unset($arRow[$itemIdLevel_2]);
						}
					}
					?>
				<?endif;?>
				
				<? foreach ($arRow as $itemIdLevel_2 => $arLevel_3): ?>  <!-- second level-->
				    <div class="header__menu-dd-block">
                                        <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>"
                                           class="header__menu-dd-head <?if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] == 'S-WORKS'):?>SWORK<?endif;?>"><?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?></a>
                                        <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                                            <ul class="header__menu-dd-list">
                                                <? foreach ($arLevel_3 as $itemIdLevel_3): ?>  <!-- third level-->
                                                    <li class="header__menu-dd-item">
                                                        <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>"
                                                           class="header__menu-dd-link"><?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"] ?></a>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        <? endif ?>
                                    </div>
                                <? endforeach; ?>
				
				<?if ( $itemID == 1654725533 ):?>
					</div>
					<div class="header__menu-dd-inner">
						<? foreach ($arNewColumn as $itemIdLevel_2 => $arLevel_3): ?>
							<div class="header__menu-dd-block">
								 <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>" class="header__menu-dd-head "><?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?></a>
							</div>
						<? endforeach; ?>
				<?endif;?>
                            </div>
                            <div class="menu-picture_wrap">
                                <?
                                if (!empty($arResult["ALL_ITEMS"][$itemID]['PARAMS']['PICTURE'])) {
                                    ?>
                                    <img src="<?= $arResult["ALL_ITEMS"][$itemID]['PARAMS']['PICTURE'];?>" class="menu-picture"/>
                                    <?
                                }
                                ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif ?>
            </li>
        <? endforeach; ?>
    </ul>
</div><a href="javascript:void(0)" class="header__catalog-button"><?= Loc::getMessage("MM_CATALOG"); ?></a>