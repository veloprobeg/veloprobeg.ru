<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

foreach ($arResult['ITEMS'] as &$arItem) {
    if (intval($arItem['PROPERTIES']['IMAGE_SVG']['VALUE']) > 0) {
        $arItem['PROPERTIES']['IMAGE_SVG'] = CFile::GetFileArray($arItem['PROPERTIES']['IMAGE_SVG']['VALUE']);
    }
}