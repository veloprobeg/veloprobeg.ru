<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

$infoBoxes = '';
$shops = array();
?>
<div class="contacts__contacts">
	
  <? foreach ($arResult['ITEMS'] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    $phones = '';
    $phone = '';
    ?>
		<div class="contacts__how-to">
		  <div class="contact__head"><?= $arItem['NAME'] ?></div>
			  <? if (!empty($arItem['PROPERTIES']['PHONE']['VALUE'])): ?>
				<div class="contact__row">
					<div class="contacts__how-to-inner">
					  <? foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $k => $v): ?>
						<div class="contacts__how-to-section">
							<a href="tel:<?= $v ?>" class="contact__link contact__link--phone">
							  <div class="contact__disc"><?= $arItem['PROPERTIES']['PHONE']['DESCRIPTION'][$k] ?></div>
							  <div class="contact__target"><?= $v ?></div>
							</a>
							<?
							$phones .= '<a href="tel:'.$v.'">'.$v.'</a>';
							if (empty($phone)) {
							  $phone = $v;
							}
							?>
						</div>
					  <? endforeach ?>
					</div> 
				</div>
			  <? endif ?>
		  <div class="contact__row">
			<div class="contacts__how-to-inner">
				<div class="contacts__how-to-section">
					<a href="mailto:<?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?>" class="contact__link contact__link--email">
					  <div class="contact__disc"><?= Loc::getMessage("EMAIL");?></div>
					  <div class="contact__target"><?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?></div>
					</a>
				</div>
				<div class="contacts__how-to-section">
					<div class="contact__link contact__link--worktime">
						<div class="contact__disc"><?= Loc::getMessage("WORK_TIME");?></div>
						<div class="contact__target"><?= $arItem['PROPERTIES']['WORK_TIME']['VALUE'] ?></div>
					</div>
				</div>
			</div>
		  </div>
		  <div class="contact__row">
			<a href="#" class="contact__link contact__link--address"> 
			  <div class="contact__disc"><?= Loc::getMessage("ADDRESS"); ?></div>
			  <div class="contact__target"><?= $arItem['PREVIEW_TEXT'] ?></div>
			</a>
		  </div>
	</div>
    <?
    $infoBoxes .= '<div id="point_'.$arItem['ID'].'" class="infobox point_'.$arItem['ID'].'">
      <a href="#" class="infobox-btn-close infobox_close_'.$arItem['ID'].'"></a>
      <div class="infobox-txt">
        <p class="infobox-name">'.$arItem['NAME'].'</p>
        <p class="infobox-addr">'.$arItem['PREVIEW_TEXT'].(!empty($arItem['PROPERTIES']['WORK_TIME']['VALUE']) ? ', '.$arItem['PROPERTIES']['WORK_TIME']['VALUE'] : '').'</p>
        <p class="infobox-tel">'.$phones.'</p>
      </div>
    </div>';

    $coordinates = explode(',', $arItem['PROPERTIES']['MAP']['VALUE']);

    $shops[] = array(
      'ID' => $arItem['ID'],
      'LAT' => $coordinates[0],
      'LNG' =>  $coordinates[1],
      'ADDRESS' => $arItem['PREVIEW_TEXT'],
      'NAME' => $arItem['NAME'],
      'PHONE' => $phone
    );
    ?>
  <? endforeach ?>
</div>
<div id="contacts__map"></div>
<div class="infoboxes">
  <?= $infoBoxes ?>
</div>
<script>
  var shops = <?= json_encode($shops)?>,
    mapMarker = '<?= SITE_TEMPLATE_PATH ?>/tpl/images/static/map-pin.png';
</script>