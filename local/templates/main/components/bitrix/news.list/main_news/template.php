<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

?>
<? if (!empty($arResult['ITEMS'])): ?>
    <?
    $first = array_pop($arResult['ITEMS']);
    $this->AddEditAction($first['ID'], $first['EDIT_LINK'], CIBlock::GetArrayByID($first["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($first['ID'], $first['DELETE_LINK'], CIBlock::GetArrayByID($first["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="blog-sc">
        <div style="background-image: url('<?= SITE_TEMPLATE_PATH ?>/tpl/images/content/blog__bg.jpg')"
             class="blog-sc__bg-image"></div>
        <div class="blog-sc__bg-color"></div>
        <div class="blog-sc__inner">
            <div class="wrapper">
                <div class="blog-sc__head"><?= Loc::getMessage("CT_BNL_BLOG"); ?></div>
                <div class="blog-sc__wrapper">

                    <a href="<?= $first['DETAIL_PAGE_URL'] ?>" class="blog-sc__main"
                       id="<?= $this->GetEditAreaId($first['ID']); ?>">
                        <div style="background-image: url('<?= $first['PREVIEW_PICTURE']['SRC'] ?>')"
                             class="blog-sc__main-image"></div>
                        <div class="blog-sc__main-head"><?= $first['NAME'] ?></div>
                        <div class="blog-sc__main-text"><?= $first['PREVIEW_TEXT'] ?></div>
                    </a>
                    <div class="blog-sc__list">
                        <div class="blog-sc__list-name"><?= Loc::getMessage("CT_BNL_NEWS"); ?></div>
                        <div class="blog-sc__items">
                            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="blog-sc__item"
                                   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                    <div class="blog-sc__item-name"><?= $arItem['NAME'] ?></div>
                                    <div class="blog-sc__item-text"><?= $arItem['PREVIEW_TEXT'] ?></div>
                                </a>
                            <? endforeach ?>
                        </div>
                        <a href="/news/"
                           class="button button--white button--hover"><?= Loc::getMessage("CT_BNL_READ_NEWS"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif ?>