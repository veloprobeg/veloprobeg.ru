<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

?>
<? if (!empty($arResult['ITEMS'])): ?>
    <div class="slider slider--full slider--index slider--navs">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="slider__slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="slider__filter"></div>
                <div style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)" class="slider__wrapper">
                    <div class="wrapper wrapper--v-center wrapper--content wrapper--flex">
                        <div class="slider__inner">
                            <h2 class="slider__name"><?= $arItem['NAME'] ?></h2>
                            <div class="slide__description"><?= $arItem['PREVIEW_TEXT'] ?></div>
                            <? if (mb_strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0): ?>
                                <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"
                                   class="button button--black button--hover">
                                    <?= empty($arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']) ? Loc::getMessage("CT_BNL_BUTTON_TEXT") : $arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']; ?>
                                </a>
                            <? endif ?>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>
<? endif ?>