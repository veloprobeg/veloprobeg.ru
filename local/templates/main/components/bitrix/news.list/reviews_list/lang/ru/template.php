<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS['REVIEWS_ORDER_BY'] = 'Сортировать';
$MESS['REVIEWS_ORDER_BY_ID_ASC'] = 'По дате добавления по возрастанию';
$MESS['REVIEWS_ORDER_BY_ID_DESC'] = 'По дате добавления по убыванию';
$MESS['REVIEWS_ORDER_BY_ACTIVE_FROM_ASC'] = 'По началу активности по возрастанию';
$MESS['REVIEWS_ORDER_BY_ACTIVE_FROM_DESC'] = 'По началу активности по убыванию';
$MESS['REVIEWS_ORDER_BY_SORT_ASC'] = 'По сортировке по возрастанию';
$MESS['REVIEWS_ORDER_BY_SORT_DESC'] = 'По сортировке по убыванию';
$MESS['REVIEWS_ADD_REVIEW'] = 'Написать отзыв';
$MESS['REVIEWS_SHOW_REVIEWS'] = 'Показать отзывы';
$MESS['REVIEWS_ANSWER'] = 'Ответ администратора:';
?>