<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

if (is_array($arResult['ITEMS'])) {
  $productIds = array(-1);

  foreach ($arResult['ITEMS'] as $k => $arItem) {
    if (!empty($arItem['PROPERTIES']['PRODUCT_ID']['VALUE'])) {
      $productIds[] = $arItem['PROPERTIES']['PRODUCT_ID']['VALUE'];
    }
  }

  $productIds = array_unique($productIds);

  $productRes = \CIBlockElement::GetList(array(), array(
    'ID' => $productIds
  ), false, false, array(
    'ID',
    'IBLOCK_ID',
    'NAME',
    'PREVIEW_PICTURE',
    'DETAIL_PICTURE',
    'DETAIL_PAGE_URL'
  ));

  $products = array();
  $fileIds = array();

  while ($product = $productRes->GetNext()) {
    if ($product['DETAIL_PICTURE']) {
      $product['PICTURE'] = $product['DETAIL_PICTURE'];
    } elseif ($product['PREVIEW_PICTURE']) {
      $product['PICTURE'] = $product['PREVIEW_PICTURE'];
    }

    $products[$product['ID']] = $product;

    if (intval($product['PICTURE']) > 0) {
      $fileIds[] = $product['PICTURE'];
    }
  }

  $files = array();

  if (!empty($fileIds)) {
    $fileRes = CFile::GetList(array(), array(
      '@ID' => implode(',', $fileIds)
    ));

    while ($file = $fileRes->Fetch()) {
      $resize = CFile::ResizeImageget($file, array(
        'width' => 80,
        'height' => 80
      ), BX_RESIZE_IMAGE_EXACT, true);

      if ($resize) {
        $file['SRC'] = $resize['src'];
        $file['WIDTH'] = $resize['width'];
        $file['HEIGHT'] = $resize['height'];
        $file['SIZE'] = $resize['size'];
      }

      $files[$file['ID']] = $file;
    }
  }

  foreach ($products as &$product) {
    if (isset($files[$product['PICTURE']])) {
      $product['PICTURE'] = $files[$product['PICTURE']];
    }
  }

  if (\Bitrix\Main\Loader::includeModule('sale')) {
    foreach ($arResult['ITEMS'] as $k => &$arItem) {
      if (isset($products[$arItem['PROPERTIES']['PRODUCT_ID']['VALUE']])) {
        $arItem['PRODUCT'] = $products[$arItem['PROPERTIES']['PRODUCT_ID']['VALUE']];
      }

      $arItem['DATE'] = date(\Bitrix\Main\Type\Date::getFormat(), strtotime($arItem['TIMESTAMP_X']));

      if ($arItem['PRODUCT']) {
        $arItem['PRODUCT']['PRICE'] = getFinalPriceInCurrency($arItem['PRODUCT']['ID']);
        $arItem['PRODUCT']['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($arItem['PRODUCT']['PRICE'], 'RUB');
      }

      $arResult['COLUMNS'][$k % 2][] = $arItem;
    }
  }

  unset($products);
}

// Sort list
$arResult['SORT'] = IBlockFilters::getReviewsSortList();

// reviewOf property values
$resProp = \CIBlockPropertyEnum::GetList(
  array(
    "SORT" => "ASC",
    "VALUE" => "ASC"
  ),
  array(
    'IBLOCK_ID' => IBLOCK_REVIEWS_ID,
    'CODE' => 'REVIEW_OF'
  )
);
$arResult['REVIEW_OF'][] = array(
  'ID' => 0,
  'VALUE' => '--Все--'
);
while($propValue = $resProp->Fetch()) {
  $arResult['REVIEW_OF'][] = $propValue;
}