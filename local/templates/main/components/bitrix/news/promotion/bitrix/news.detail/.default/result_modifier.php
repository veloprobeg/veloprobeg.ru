<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

$res = CIBlockElement::GetList(array(), array(
  '!ID' => $arResult['ID'],
  'IBLOCK_ID' => $arResult['IBLOCK_ID']
), false, array(
  'nTopCount' => $arParams['COUNT_VIEW_MORE']
), array(
  'ID',
  'NAME',
  'PREVIEW_TEXT',
  'PREVIEW_PICTURE',
  'DETAIL_PICTURE',
  'DETAIL_PAGE_URL'
));

$arResult['VIEW_MORE'] = array();

while ($arItem = $res->GetNext()) {
  $srcPic = false;

  if (intval($arItem['DETAIL_PICTURE']) > 0) {
    $srcPic = $arItem['DETAIL_PICTURE'];
  } elseif (intval($arItem['PREVIEW_PICTURE']) > 0) {
    $srcPic = $arItem['PREVIEW_PICTURE'];
  }

  if ($srcPic) {
    $resPic = CFile::ResizeImageGet($srcPic, array(
      'width' => 500,
      'height' => 280
    ), BX_RESIZE_IMAGE_EXACT, true);

    if ($resPic) {
      $arItem['PREVIEW_PICTURE'] = array(
        'SRC' => $resPic['src'],
        'WIDTH' => $resPic['width'],
        'HEIGHT' => $resPic['height'],
        'ID' => $srcPic
      );
    }
  }

  $arResult['VIEW_MORE'][] = $arItem;
}

$arResult['DETAIL_TEXT'] = preg_replace('/#SITE_TEMPLATE_PATH#/', SITE_TEMPLATE_PATH, $arResult['DETAIL_TEXT']);