<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

?>
<div class="styles">
  <div style="background-image: url(<?= $arResult['DETAIL_PICTURE']['SRC'] ?>)" class="styles__welcome"></div>
  <div class="styles__inner">
    <div class="styles__content">
      <h2><?= $arResult['NAME'] ?></h2>
      <div class="styles__main"><?= $arResult['DETAIL_TEXT'] ?></div>
    </div>
    <div class="styles__promos" id="styles__promos">
      <? foreach ($arResult['VIEW_MORE'] as $arView): ?>
        <a href="<?= $arView['DETAIL_PAGE_URL'] ?>" class="styles__more">
          <? if (is_array($arView['PREVIEW_PICTURE'])): ?>
            <div style="background-image: url(<?= $arView['PREVIEW_PICTURE']['SRC'] ?>)"
                 class="styles__more-image"></div>
          <? endif ?>
          <div class="styles__more-header">
            <div class="styles__more-look"><?= Loc::getMessage("VIEW_MORE"); ?></div>
          </div>
          <div class="styles__more-inner">
            <div class="styles__more-name"><?= $arView['NAME'] ?></div>
            <div class="styles__more-text"><?= $arView['PREVIEW_TEXT'] ?></div>
          </div>
        </a>
      <? endforeach ?>
    </div>
  </div>
</div>