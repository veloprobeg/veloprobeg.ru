<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

?>
<div class="blog">
  <? foreach ($arResult['ITEMS'] as $k => $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

    $type = $arResult['TYPES'][$k];
    $classWrapper = 'article';
    $prefixClass = 'article__';
    $classContent = $prefixClass.'content';
    $classWrapperImage = $prefixClass.'image-wrapper';
    $classImage = $prefixClass.'image';
    $viewText = false;
    $viewImage = true;

    switch ($type) {
      case 'big':
        $classWrapper = 'bl-item bl-item--big';
        $prefixClass = 'bl-item__';
        $classContent = $prefixClass.'content';
        $classWrapperImage = $prefixClass.'backface';
        $classImage = $prefixClass.'image';
        $viewText = true;
        break;
      case 'white':
        $classWrapper = 'bl-item bl-item--white';
        $prefixClass = 'bl-item__';
        $classContent = $prefixClass.'inner';
        $viewText = true;
        $viewImage = false;
        break;
    }
    ?>
    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="<?= $classWrapper ?>"
       id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
      <? if ($viewImage && is_array($arItem['PREVIEW_PICTURE'])): ?>
        <div class="<?= $classWrapperImage ?>">
          <div style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)" class="<?= $classImage ?>"></div>
        </div>
      <? endif ?>
      <div class="<?= $classContent ?>">
        <div class="<?= $prefixClass ?>date"><?= $arItem['DATE'] ?></div>
        <div class="<?= $prefixClass ?>name"><?= $arItem['NAME'] ?></div>
        <? if ($viewText): ?>
          <div class="<?= $prefixClass ?>desc"><?= $arItem['PREVIEW_TEXT'] ?></div>
        <? endif ?>
      </div>
    </a>
  <? endforeach ?>
  <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
  <? endif; ?>
</div>