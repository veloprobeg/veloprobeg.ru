<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
use \Bitrix\Main\Localization\Loc;
global $BASKETPRICE;
$BASKETPRICE = (int)str_replace('&nbsp;', '', $arResult['TOTAL_PRICE']);
$cartStyle = 'header__basket';
$cartId = "bx_basket".$component->randString();
$arParams['cartId'] = $cartId;
// TODO Ничего из шаблона не удаляем, а то обновление суммы полетит при добавлении товара в корзину
?>

<script>
  var <?=$cartId?> =
  new BitrixSmallCart;
</script>
<div id="<?= $cartId ?>" class="<?= $cartStyle ?>"><?
  /** @var \Bitrix\Main\Page\FrameBuffered $frame */
  $frame = $this->createFrame($cartId, false)->begin();
  require(realpath(dirname(__FILE__)).'/ajax_template.php');
  $frame->beginStub();
  $arResult['COMPOSITE_STUB'] = 'Y';
  require(realpath(dirname(__FILE__)).'/top_template.php');
  unset($arResult['COMPOSITE_STUB']);
  $frame->end();
  ?></div>
<script type="text/javascript">
  <?= $cartId ?>.siteId = '<?= SITE_ID ?>';
  <?= $cartId ?>.cartId = '<?= $cartId ?>';
  <?= $cartId ?>.ajaxPath = '<?= $componentPath ?>/ajax.php';
  <?= $cartId ?>.templateName = '<?= $templateName ?>';
  <?= $cartId ?>.arParams =  <?= CUtil::PhpToJSObject($arParams) ?>; // TODO \Bitrix\Main\Web\Json::encode
  <?= $cartId ?>.closeMessage = '<?= Loc::getMessage('TSB1_COLLAPSE') ?>';
  <?= $cartId ?>.openMessage = '<?= Loc::getMessage('TSB1_EXPAND') ?>';
  <?= $cartId ?>.activate();
</script>
