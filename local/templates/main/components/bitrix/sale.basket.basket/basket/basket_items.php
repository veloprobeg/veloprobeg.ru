<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $USER;
/** @var array $arParams */

use Bitrix\Sale\DiscountCouponsManager;
use \Bitrix\Main\Localization\Loc;

if (!empty($arResult["ERROR_MESSAGE"])) {
    ShowError($arResult["ERROR_MESSAGE"]);
}

$bDelayColumn = false;
$bDeleteColumn = true;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

?>

    <div id="basket_items_list">
        <?
        if ($normalCount > 0):
            ?>
            <div class="basket__items" id="basket_items">
                <?
                foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
                    $arHeaders[] = $arHeader["id"];
                }

                foreach ($arResult["GRID"]["ROWS"] as $k => $arItem) {

                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                        ?>
                        <div class="basket__item" id="<?= $arItem["ID"] ?>">
                            <?
                            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
                                if (in_array($arHeader["id"], array(
                                    "PROPS",
                                    "DELAY",
                                    "DELETE",
                                    "TYPE"
                                ))) // some values are not shown in the columns in this template
                                {
                                    continue;
                                }

                                if ($arHeader["name"] == '') {
                                    $arHeader["name"] = Loc::getMessage("SALE_" . $arHeader["id"]);
                                }

                                if ($arHeader["id"] == "NAME") {
                                    ?>
                                    <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?>
                                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="basket__item-link"><? endif; ?>
                                    <?
                                    if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                        $url = $arItem["PREVIEW_PICTURE_SRC"];
                                    elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                        $url = $arItem["DETAIL_PICTURE_SRC"];
                                    else:
                                        $url = $templateFolder . "/images/no_photo.png";
                                    endif;
                                    ?>
                                    <!--80x51-->
                                    <div style="background-image: url(<?= $url ?>" class="basket__item-image"></div>
                                    <div class="basket__item-name"><?= $arItem['NAME'] ?></div>
                                    <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?></a><? endif; ?>
                                    <?
                                } elseif ($arHeader["id"] == "QUANTITY") {
                                    ?>
                                    <?
                                    $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                    $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $arItem["AVAILABLE_QUANTITY"] . "\"" : "";
                                    $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                    $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                                    ?>
                                    <div class="basket__item-counter">
                                        <div class="counter">
                                            <?
                                            if (!isset($arItem["MEASURE_RATIO"])) {
                                                $arItem["MEASURE_RATIO"] = 1;
                                            }

                                            if (
                                                floatval($arItem["MEASURE_RATIO"]) != 0
                                            ):  
                                                ?>
                                                <span class="counter__minus"
                                                      onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">- </span>
                                            <? endif ?>
                                            <input type="text" value="<?= $arItem["QUANTITY"] ?>"
                                                   class="counter__result"
                                                   id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                                   name="QUANTITY_INPUT_<?= $arItem["ID"] ?>" size="3" maxlength="18"
                                                   min="0"
                                                   onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                                                <?= $max ?>>
                                            <? if (floatval($arItem["MEASURE_RATIO"]) != 0): ?>
                                                <span class="counter__plus"
                                                      onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">+</span>
                                            <? endif ?>
                                            <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>"
                                                   name="QUANTITY_<?= $arItem['ID'] ?>"
												   data-max="<?=$arItem["AVAILABLE_QUANTITY"]?>"
                                                   value="<?= $arItem["QUANTITY"] ?>"/>
                                        </div>
                                    </div>
                                    <?
                                } elseif ($arHeader["id"] == "PRICE") {
                                    ?>
                                    <div class="basket__item-price">
                                        <? if ($arItem['fullPrice'] != $arItem['price']): ?>
                                            <div class="basket__item-old-price"
                                                 id="old_price_<?= $arItem["ID"] ?>"><?= $arItem['FULL_PRICE_FORMATED'] ?></div>
                                            <div class="basket__item-bonus"
                                                 id="current_price_<?= $arItem["ID"] ?>"><?= $arItem['PRICE_FORMATED'] ?></div>
                                        <? else: ?>
                                            <div class="basket__item-current-price"
                                                 id="current_price_<?= $arItem["ID"] ?>"><?= $arItem['PRICE_FORMATED'] ?></div>
                                        <? endif ?>
                                    </div>
                                    <?
                                } elseif ($arHeader['id'] == 'DISCOUNT') {
                                } elseif ($arHeader['id'] == 'PREVIEW_PICTURE') {

                                } else {
                                    ?>
                                    <div class="basket__item-cost">
                                        <div class="basket__item-cost-name"><?= Loc::getMessage("SALE_SUM"); ?></div>
                                        <div class="basket__item-cost-count"><?
                                            if ($arHeader["id"] == "SUM"):
                                            ?>
                                            <div id="sum_<?= $arItem["ID"] ?>">
                                                <?
                                                endif;

                                                echo $arItem[$arHeader["id"]];

                                                if ($arHeader["id"] == "SUM"):
                                                ?>
                                            </div>
                                        <?
                                        endif;
                                        ?></div>
                                    </div>
                                    <?
                                }
                            }

                            if ($bDelayColumn || $bDeleteColumn):
                                ?>
                                <td class="control">
                                    <?
                                    if ($bDeleteColumn):
                                        ?>
                                        <a href="#" class="basket__item-remove"
                                           onclick="deleteBasketItem(<?= $arItem['ID'] ?>); return false;"><?= Loc::getMessage("SALE_DELETE") ?></a>
                                        <?
                                    endif;
                                    if ($bDelayColumn):
                                        ?>
                                        <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delay"]) ?>"><?= Loc::getMessage("SALE_DELAY") ?></a>
                                        <?
                                    endif;
                                    ?>
                                </td>
                                <?
                            endif;
                            ?>
                            <td class="margin"></td>
                        </div>
                        <?
                    endif;
                }
                ?>
            </div>
            <?
            $deliveryIds = array_keys($arResult['DELIVERY']);
            $firstDeliveryId = $deliveryIds[0];
            ?>
            <div class="basket__delivery" style="display: none;">
                <div class="basket__select">
                    <select class="select select--basket" name="DELIVERY_ID" onchange="changeData()" id="deliveryId">
                        <? foreach ($arResult['DELIVERY'] as $delivery): ?>
                            <?
                            if (!$firstDeliveryId) {
                                $firstDeliveryId = $arResult['DELIVERY_ID'];
                            }
                            ?>
                            <option value="<?= $delivery['ID'] ?>"<?= $delivery['ID'] == $arResult['DELIVERY_ID'] ? ' selected' : '' ?>
                                    data-price="<?= $delivery['CONFIG']['MAIN']['PRICE'] ?>"><?= $delivery['NAME'] ?></option>
                        <? endforeach ?>
                    </select>
                    <script>
                        $('.select--basket').on('select2:select', function (evt) {
                            // View price, if exists
                            var price = $(evt.target).find(':selected').data('price');

                            if (price !== '' && price >= 0) {
                                $('.basket__item-cost-count', $(evt.target).parents('.basket__delivery')).html(BX.message('SALE_VIEW_PRICE').replace('#PRICE#', $(evt.target).find(':selected').data('price')));
                            } else {
                                $('.basket__item-cost-count', $(evt.target).parents('.basket__delivery')).html('');
                            }
                        });
                    </script>
                </div>
                <div class="basket__item-cost">
                    <div class="basket__item-cost-count"><?= Loc::getMessage("SALE_VIEW_PRICE", array(
                            '#PRICE#' => $arResult['DELIVERY'][$firstDeliveryId]['CONFIG']['MAIN']['PRICE']
                        )); ?>
                    </div>
                </div>
            </div>
            <div class="basket__end">

                <div class="basket__end-promo">

                    <?/*
        <?
        if ($arParams["HIDE_COUPON"] != "Y") {
          $couponClass = '';
          $coupon = false;

          foreach ($arResult['COUPON_LIST'] as $coupon) {
            if ($coupon['JS_STATUS'] != 'APPLYED') {
              $coupon = false;
              continue;
            }

            $couponClass = '';
            switch ($coupon['STATUS']) {
              case DiscountCouponsManager::STATUS_NOT_FOUND:
              case DiscountCouponsManager::STATUS_FREEZE:
                $couponClass = 'bad';
                break;
              case DiscountCouponsManager::STATUS_APPLYED:
                $couponClass = 'good';
                break;
            }

            break;
          }
          ?>
          <div class="basket__promo" id="coupons_block">
            <div class="basket__end-gray"><?= Loc::getMessage("SALE_I_HAVE_PROMO"); ?></div>
            <div class="basket__promo-input <?= $couponClass ?>">
              <input type="text" class="default-input default-input--full" onchange="changeData();" name="COUPON"
                     id="coupon" value="<?= $coupon['COUPON'] ?>">
            </div>
            <? if ($couponClass): ?>
              <div class="basket__end-gray-small"><?= $coupon['DISCOUNT_NAME'] ?></div>
            <? endif ?>
          </div>
          <?
        }

        if ($USER->IsAuthorized()):
          ?>
          <div class="basket__doit">
            <div class="basket__doit-input">
              <div class="basket__end-gray"><?= Loc::getMessage("SALE_AVAILABLE_BONUSES", array(
                  '#BONUSES#' => $arResult['USER_ACCOUNT']['CURRENT_BUDGET']
                )); ?></div>
              <input type="text" class="default-input default-input--full" name="BONUSES" id="bonuses"
                     value="<?= $arResult['USED_BONUSES'] > 0 ? $arResult['USED_BONUSES'] : '' ?>">
            </div>
            <a href="javascript:void(0)" class="button button--yel-transp button--hover" id="write-off-bonuses"
               onclick="changeData()"><?= Loc::getMessage("SALE_USE_BONUSES"); ?>
            </a>
          </div>
        <? else: ?>
          <input type="hidden" value="" name="BONUSES" id="bonuses">
        <? endif ?>
		*/
                    ?>
                </div>

                <div class="basket__total">
		    <?/*
                    <div class="basket__total-money">
                        <div class="basket__total-name"><?= Loc::getMessage("SALE_TOTAL"); ?></div>
                        <div class="basket__total-num"
                             id="allSum_wVAT_FORMATED"><?= $arResult['allPriceWithoutDiscount'] ?>
                        </div>
                    </div>
		    */?>
                    <? ?>
                    <? if ($arResult['DISCOUNT_PRICE_ALL'] > 0): ?>
                        <div class="basket__total-bonus">
                            <div class="basket__total-name"><?= Loc::getMessage("SALE_DISCOUNT_BY_PROMO"); ?></div>
                            <div class="basket__total-num" id="allDiscountPriceAll">
                                -<?= $arResult['DISCOUNT_PRICE_ALL_FORMATED'] ?></div>
                        </div>
                    <? endif ?>
                    <?/* if ($arResult['USED_BONUSES'] > 0): */?>
                    <?
					/*
					CModule::IncludeModule('logictim.balls');
                    $arUserGroups = $USER->GetUserGroupArray();

                    $arBonus = cHelperCalc::CartBonusCustom($arResult["ITEMS"]["AnDelCanBuy"],$arUserGroups);
                    if(!empty($arBonus["ALL_BONUS"])){
                        $totalPrice = $arResult["allSum"] - ceil($arBonus["ALL_BONUS"]);
                        $totalPrice = number_format($totalPrice,0,',',' ').' <span class="icon-rouble">i</span>';
                    }else{
                        $totalPrice = $arResult['allSum_FORMATED'];
                    }
					*/
                    //dump($arResult);
                    ?>
                       <!-- <div class="basket__total-bonus">
                            <div class="basket__total-name"><?/*= Loc::getMessage("SALE_DISCOUNT_BY_BONUSES"); */?></div>
                            <div class="basket__total-num" id="allBonusesPrice">-<?/*=ceil($arBonus["ALL_BONUS"]);*/?></div>
                        </div>-->
                    <?/* endif */?>
		    
                    <div class="basket__total-price">
                        <div class="basket__total-name"><?= Loc::getMessage("SALE_SUM_TO_PAY"); ?></div>
                        <div class="basket__total-num" id="allSum_FORMATED"><?=$totalPrice?></div>
                    </div>
                    <div class="basket__total-submit">
                        <button type="submit" class="button button--orange"
                                onclick="/*checkOut();*/"><?= Loc::getMessage("SALE_ORDER") ?></button>
                    </div>
                </div>
            </div>
            <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
            <input type="hidden" id="offers_props"
                   value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
            <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
            <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
            <input type="hidden" id="count_discount_4_all_quantity"
                   value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
            <input type="hidden" id="price_vat_show_value"
                   value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
            <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
            <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>
            <input type="hidden" id="auto_calculation"
                   value="<?= ($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y" ?>"/>
            <?
        else:
            ?>
            <table>
                <tbody>
                <tr>
                    <td style="text-align:center">
                        <div class=""><?= Loc::getMessage("SALE_NO_ITEMS"); ?></div>
                    </td>
                </tr>
                </tbody>
            </table>
            <?
        endif;
        ?>
    </div>
    <script>
        var basketPoolQuantity = null,
            countDecimals = <?= $arParams['COUNT_DECIMALS'] ?>;
        // Пробрасываем языковые фразы в JS
        BX.message(<?= CUtil::PhpToJsObject($messages) ?>);
    </script>
<? \Debug::dtc($arResult, 'result'); ?>