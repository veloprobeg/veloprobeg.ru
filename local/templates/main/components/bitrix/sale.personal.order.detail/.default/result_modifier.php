<?
foreach ($arResult['ORDER_PROPS'] as $prop) {
  $arResult['PROPS'][$prop['CODE']] = $prop;
}

$arResult['PRODUCT_IDS'] = array();
foreach ($arResult['BASKET'] as $basketItem) {
  if ($basketItem['PRODUCT_ID'] > 0) {
    $arResult['PRODUCT_IDS'][] = $basketItem['PRODUCT_ID'];
  }
}

$arResult['PRODUCT_IDS'] = array_unique($arResult['PRODUCT_IDS']);

$noPhoto = __DIR__.'/images/no_photo.png';

$arResult['PRODUCTS'] = array();
if (\Bitrix\Main\Loader::includeModule('iblock')) {
  $res = \CIBlockElement::GetList(array(), array(
    'ID' => $arResult['PRODUCT_IDS']
  ), false, false, array(
    'ID',
    'IBLOC_ID',
    'NAME',
    'PREVIEW_PICTURE',
    'DETAIL_PICTURE'
  ));

  $arResult['FILE_IDS'] = array();
  while($product = $res->Fetch()) {
    $arResult['PRODUCTS'][$product['ID']] = $product;

    if ($product['PREVIEW_PICTURE']) {
      $arResult['FILE_IDS'][$product['ID']] = $product['PREVIEW_PICTURE'];
    } elseif ($product['DETAIL_PICTURE']) {
      $arResult['FILE_IDS'][$product['ID']] = $product['DETAIL_PICTURE'];
    }
  }

  $arResult['FILES'] = array();

  $noPhotoInfo = pathinfo($noPhoto);

  $dir = $_SERVER['DOCUMENT_ROOT'].'/upload/resize_cache/basket/';

  if (!file_exists($dir)) {
    mkdir($dir);
  }

  $dir .= '80_80/';

  if (!file_exists($dir)) {
    mkdir($dir);
  }

  $path = $dir.md5($noPhoto).'.'.$noPhotoInfo['extension'];

  $resized = \CFile::ResizeImageFile($noPhoto, $path, array(
    'width' => 80,
    'height' => 80
  ), BX_RESIZE_IMAGE_EXACT, true);

  if ($resized) {
    $arResult['FILES'][0] = array(
      'SRC' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $path),
      'WIDTH' => 80,
      'HEIGHT' => 80
    );
  }

  if (!empty($arResult['FILE_IDS'])) {
    $fileRes = CFile::GetList(array(), array(
      '@ID' => array_values($arResult['FILE_IDS'])
    ));

    while ($file = $fileRes->Fetch()) {
      $resize = \CFile::ResizeImageGet($file, array(
        'width' => 80,
        'height' => 80
      ), BX_RESIZE_IMAGE_EXACT, true);

      $arResult['FILES'][$file['ID']] = $resize;
    }
  }

  foreach ($arResult['BASKET'] as &$basketItem) {
   
	/*
   if (isset($arResult['FILES'][$arResult['PRODUCTS'][$basketItem['PRODUCT_ID']]['PREVIEW_PICTURE']])) {
      $basketItem['PICTURE'] = $arResult['FILES'][$arResult['PRODUCTS'][$basketItem['PRODUCT_ID']]['PREVIEW_PICTURE']];
    }

    if (isset($arResult['FILES'][$arResult['PRODUCTS'][$basketItem['PRODUCT_ID']]['DETAIL_PICTURE']])) {
      $basketItem['PICTURE'] = $arResult['FILES'][$arResult['PRODUCTS'][$basketItem['PRODUCT_ID']]['DETAIL_PICTURE']];
    }
	*/

    //if (!$basketItem['PICTURE']) {
	
		foreach ( $basketItem['PROPS'] as $arProp ){
			if ( $arProp["CODE"] == "" ){
				$color = $basketItem['PROPS']['SKU_VALUE']['ID'];
			}
		}
		
	
		$arFilter = Array(
			"ID" => $basketItem['PRODUCT_ID']
		);
		$res = CIBlockElement::GetList(Array(), $arFilter, Array("PROPERTY_CML2_LINK"));
		if($ar_fields = $res->GetNext()){
			$elementID = $ar_fields["PROPERTY_CML2_LINK_VALUE"];
		}
		
		$arFilter = Array(
			"PROPERTY_CML2_LINK" => $elementID,
			"!PREVIEW_PICTURE" => false,
			"PROPERTY_TSVET_BAZOVYY_VALUE" => $color
		);
		
	
		$res = CIBlockElement::GetList(Array(), $arFilter, Array("PREVIEW_PICTURE"));
		if($ar_fields = $res->GetNext()){
			$basketItem['PICTURE']['SRC'] = CFile::GetPath($ar_fields["PREVIEW_PICTURE"]);
		}

	// }
  }
}

// First shipment
foreach ($arResult['SHIPMENT'] as $k => $shipment) {
  if (!empty($shipment['TRACKING_NUMBER'])) {
    $arResult['TRACKING_NUMBER_URL'][$k] = preg_replace('/#TRACK_NUMBER#/', $shipment['TRACKING_NUMBER'], $arParams['TRACK_NUMBER_URL']);
  }
}

if (!empty($arResult['COMMENTS'])) {
	$arResult['WARNING_MESSAGE'] = $arResult['COMMENTS'];
}
?>