<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc,
  Bitrix\Main\Page\Asset;

if ($arParams['GUEST_MODE'] !== 'Y') {
  Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
  Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
}
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");

CJSCore::Init(array(
  'clipboard',
  'fx'
));

$APPLICATION->SetTitle("");

if (!empty($arResult['ERRORS']['FATAL'])) {
  foreach ($arResult['ERRORS']['FATAL'] as $error) {
    ShowError($error);
  }

  $component = $this->__component;

  if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
    $APPLICATION->AuthForm('', false, false, 'N', false);
  }
} else {
  if (!empty($arResult['ERRORS']['NONFATAL'])) {
    foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
      ShowError($error);
    }
  }
  $bonusPrice = 0;
  /*dump($arResult);*/
    foreach ($arResult["PAYMENT"] as $key=>$payment) {
        if($payment["PAY_SYSTEM_NAME"] == 'Бонусный счет'){
            $bonusPriceFormated = $payment["PRICE_FORMATED"];
            $bonusPrice = $payment["SUM"];
        }
    }
  ?>
  <div class="detail">
    <div class="detail__head">
      <div class="detail__info">
        <div class="detail__info-block">
          <div class="detail__text"><?= $arResult["DATE_INSERT_FORMATED"] ?></div>
          <div class="detail__text">N <?= $arResult['ACCOUNT_NUMBER'] ?></div>
        </div>
        <div class="detail__info-block">
          <div class="detail__text"><?= htmlspecialcharsbx($arResult["STATUS"]["NAME"]) ?></div>
          <? foreach ($arResult['TRACKING_NUMBER_URL'] as $url): ?>
            <a target="_blank" href="<?= $url ?>" class="detail__text"><?= Loc::getMessage("SPOD_TRACK_ORDER"); ?></a>
          <? endforeach ?>
        </div>
      </div>
      <div class="detail__nav"><a href="<?= $arResult['URL_TO_COPY'] ?>" class="detail__repeat"><?= Loc::getMessage("SPOD_ORDER_REPEAT"); ?></a><a
            href="<?= $arResult['URL_TO_CANCEL'] ?>" class="detail__remove"><?= Loc::getMessage("SPOD_ORDER_CANCEL"); ?></a></div>
    </div>
    <div class="detail__body">
      <div class="detail__comp">
        <div class="detail__name"><?= Loc::getMessage("SPOD_ORDER_BASKET"); ?></div>
        <div class="detail__order">
          <? foreach ($arResult['BASKET'] as $basketItem): ?>
            <a href="<?= $basketItem['DETAIL_PAGE_URL'] ?>" class="i-prew">
              <div class="i-prew__image"><img src="<?= $basketItem['PICTURE']['SRC'] ?>" class="i-prew__img"></div>
              <div class="i-prew__about">
                <div class="i-prew__name"><?= $basketItem['NAME'] ?></div>
                <div class="i-prew__price"><?= $basketItem['PRICE_FORMATED'] ?></div>
              </div>
            </a>
          <? endforeach ?>
        </div>
        <div class="detail__desc">
          <div class="detail__price">
            <div class="detail__cost">
              <div class="detail__desc-name"><?= Loc::getMessage("SPOD_SUMMARY"); ?></div>
              <div class="detail__desc-count"><?= $arResult['PRODUCT_SUM_FORMATED'] ?></div>
            </div>
		<?if ( $arResult['PRICE_DELIVERY_FORMATED'] ):?>
			    <div class="detail__cost">
			      <div class="detail__desc-name">Стоимость доставки:</div>
			      <div class="detail__desc-count"><?= $arResult['PRICE_DELIVERY_FORMATED'] ?></div>
			    </div>
		<?endif;?>
	    
            <? if (floatval($arResult['PROPS']['']) > 0 || floatval($arResult['DISCOUNT_VALUE']) > 0): ?>
              <div class="detail__bonuses">
                <? if (floatval($arResult['DISCOUNT_VALUE']) > 0): ?>
                  <div class="detail__bonus">
                    <div class="detail__desc-name"><?= Loc::getMessage("SPOD_DISCOUNT_PROMO_CODE"); ?></div>
                    <div
                        class="detail__desc-count"><?= CCurrencyLang::CurrencyFormat(floatval($arResult['DISCOUNT_VALUE']), 'RUB') ?></div>
                  </div>
                <? endif ?>
                <? if (floatval($arResult['PROPS']['']['VALUE']) > 0): ?>
                  <div class="detail__bonus">
                    <div class="detail__desc-name"><?= Loc::getMessage("SPOD_BONUSES_IN_PAYMENT"); ?></div>
                    <div
                        class="detail__desc-count"><?= CCurrencyLang::CurrencyFormat(floatval($arResult['PROPS']['']['VALUE']), 'RUB') ?></div>
                  </div>
                <? endif ?>
              </div>
            <? endif ?>


              <? if ($bonusPrice > 0): ?>
                  <div class="detail__bonuses">
                          <div class="detail__bonus">
                              <div class="detail__desc-name">Бонусы в счет оплаты:</div>
                              <div class="detail__desc-count"><?= CCurrencyLang::CurrencyFormat($bonusPrice, 'RUB') ?></div>
                          </div>
                  </div>
              <? endif ?>


          </div>

	  
          <div class="detail__total">
		
	  
	  
		<div class="detail__total-name"><?= Loc::getMessage("SPOD_ORDER_PRICE"); ?></div>
		<div class="detail__total-count"><?= CCurrencyLang::CurrencyFormat($arResult['PRICE']-$bonusPrice, 'RUB') ?></div>
          </div>
        </div>
      </div>
      <div class="detail__about">
        <? if ($arResult['WARNING_MESSAGE']): ?>
          <div class="detail__about-row">
            <div class="detail__warning"><?= $arResult['WARNING_MESSAGE'] ?></div>
            <!-- Внимание! Оператор не смог с вами связаться. Позвоните нам для подтверждения жаты и времени доставки. -->
          </div>
        <? endif ?>
        <div class="detail__about-row">
          <div class="detail__about-info">
            <?
            $address = '';
            $empty = true;

            if ($arResult['PROPS']['LOCATION']['VALUE']) {
              $address .= $arResult['PROPS']['LOCATION']['VALUE'];
              $empty = false;
            }

            if ($arResult['PROPS']['STREET']['VALUE']) {
              if (!$empty) {
                $address .= ', ';
              }

              $address .= $arResult['PROPS']['STREET']['VALUE'];
              $empty = false;
            }

            if ($arResult['PROPS']['HOUSE']['VALUE']) {
              if (!$empty) {
                $address .= ', ';
              }

              $address .= $arResult['PROPS']['HOUSE']['VALUE'];
              $empty = false;
            }

            if ($arResult['PROPS']['HOUSING']['VALUE']) {
              if (!$empty) {
                $address .= '-';
              }

              $address .= $arResult['PROPS']['HOUSING']['VALUE'];
              $empty = false;
            }

            if ($arResult['PROPS']['AP_NUMBER']['VALUE']) {
              if (!$empty) {
                $address .= '-';
              }

              $address .= $arResult['PROPS']['AP_NUMBER']['VALUE'];
              $empty = false;
            }
            ?>
            <div class="detail__about-param"><?= Loc::getMessage("SPOD_ADDRESS"); ?></div>
            <div class="detail__about-value"><?= $address ?></div>
          </div>
          <? if ($arResult['DELIVERY_ID'] == 2): ?>
            <div class="detail__about-info">
              <div class="detail__about-param"><?= Loc::getMessage("SPOD_DATE"); ?></div>
              <div
                  class="detail__about-value"><?= date('d.m.Y', strtotime($arResult["DATE_INSERT_FORMATED"]) + 86400) ?></div>
            </div>
          <? endif ?>
        </div>
        <div class="detail__about-row">
          <div class="detail__about-info">
            <div class="detail__about-param"><?= Loc::getMessage("SPOD_RECIPIENT"); ?></div>
            <div class="detail__about-value"><?= $arResult['USER']['LAST_NAME'] ?> <?= $arResult['USER']['NAME'] ?></div>
          </div>
          <div class="detail__about-info">
            <div class="detail__about-param"><?= Loc::getMessage("SPOD_DESIRED_DELIVERY_TIME"); ?></div>
            <div class="detail__about-value">с <?= $arResult['PROPS']['DELIVERY_TIME_FROM']['VALUE'] ?>
              до <?= $arResult['PROPS']['DELIVERY_TIME_TO']['VALUE'] ?></div>
          </div>
        </div>
        <div class="detail__about-row">
          <div class="detail__about-info">
            <div class="detail__about-param"><?= Loc::getMessage("SPOD_ORDER_PAY_SYSTEM"); ?></div>
            <? foreach ($arResult['PAYMENT'] as $payment): ?>
              <div class="detail__about-value"><?= $payment['PAY_SYSTEM_NAME'] ?></div>
            <? endforeach ?>
          </div>
          <div class="detail__about-info">
            <div class="detail__about-param"><?= Loc::getMessage("SPOD_PHONE"); ?></div>
            <div class="detail__about-value"><?= $arResult['PROPS']['PHONE']['VALUE'] ?></div>
          </div>
        </div>
        <? if (!empty($arResult['USER_DESCRIPTION'])): ?>
          <div class="detail__about-row">
            <div class="detail__about-info detail__about-info--comment">
              <div class="detail__about-param"><?= Loc::getMessage("SPOD_COMMENT"); ?></div>
              <div class="detail__about-value"><?= $arResult['USER_DESCRIPTION'] ?></div>
            </div>
          </div>
        <? endif ?>
      </div>
    </div>
  </div>
  <?
}

\Debug::dtc($arResult, 'result');
?>