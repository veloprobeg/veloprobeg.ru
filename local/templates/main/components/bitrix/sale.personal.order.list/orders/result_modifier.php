<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
$arResult['STATUSES'] = array();
$res = \CSaleStatus::GetList();

while ($status = $res->Fetch()) {
  $arResult['STATUSES'][$status['ID']] = $status;
}

foreach ($arResult['ORDERS'] as &$arOrder) {
  $arOrder['ORDER']['TRACKING_NUMBER_URL'] = array();

  // First shipment
  foreach ($arOrder['SHIPMENT'] as $k => $shipment) {
    if (!empty($shipment['TRACKING_NUMBER'])) {
      $arOrder['ORDER']['TRACKING_NUMBER_URL'][$k] = preg_replace('/#TRACK_NUMBER#/', $shipment['TRACKING_NUMBER'], $arParams['TRACK_NUMBER_URL']);
    }
  }

  $arOrder["ORDER"]["URL_TO_DETAIL"] .= '?back_url=/personal/order/';
}

\Debug::dtc($arResult, 'result');
\Debug::dtc($arParams['TRACK_NUMBER_URL'], 'TRACK_NUMBER_URL');