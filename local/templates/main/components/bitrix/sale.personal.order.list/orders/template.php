<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

use Bitrix\Main\Localization\Loc;
if (!empty($arResult['ERRORS']['NONFATAL']))
{
  foreach($arResult['ERRORS']['NONFATAL'] as $error)
  {
    ShowError($error);
  }
}
?>
  <div class="data__history">
    <? foreach ($arResult['ORDERS'] as $arOrder): ?>
      <div class="data__order">
        <div class="data__order-info">
          <div class="data__order-block">
            <div class="data__order-date"><?= $arOrder['ORDER']['DATE_INSERT_FORMATED'] ?></div>
            <div class="data__order-num"><?= Loc::getMessage("SPOL_ORDER_NUMBER", array(
                '#ORDER_NUMBER#' => $arOrder['ORDER']['ID']
              )); ?></div>
          </div>
          <div class="data__order-block">
            <div class="data__order-status"><?= $arResult['STATUSES'][$arOrder['ORDER']['STATUS_ID']]['NAME'] ?></div>
            <? foreach ($arOrder['ORDER']['TRACKING_NUMBER_URL'] as $url): ?>
              <a href="<?= $url ?>" target="_blank"
                 class="data__order-trace-it"><?= Loc::getMessage("SPOL_ORDER_TRACK"); ?></a>&nbsp;
            <? endforeach ?>
          </div>
        </div>
        <a href="<?= htmlspecialcharsbx($arOrder["ORDER"]["URL_TO_DETAIL"]) ?>" class="data__order-link"></a>
        <div class="data__order-nav">
          <a href="<?= htmlspecialcharsbx($arOrder["ORDER"]["URL_TO_COPY"]) ?>"
             class="data__order-repeat"> <?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?></a>
        </div>
      </div>
    <? endforeach ?>
  </div>
<? \Debug::dtc($arResult, 'result'); ?>
<? \Debug::dtc($arParams, 'params'); ?>