<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;

$dbProps = CSaleOrderProps::GetList(
    array("SORT" => "ASC"),
    array(
        "PROPS_GROUP_ID" => 2,
        "USER_PROPS" => "Y"
    ),
    false,
    false,
    array()
);

$arProps = Array();

$dbUserProfiles = CSaleOrderUserProps::GetList(
    array("DATE_UPDATE" => "DESC"),
    array(
        "USER_ID" => $USER->GetID()
    )
);

while ($arUserProfiles = $dbUserProfiles->GetNext()) {
    $arResult["ORDER_PROP"]["USER_PROFILES"][$arUserProfiles["ID"]] = $arUserProfiles;
}

// Get user profile values for view name
$arOrder = Array(
    "ID" => "ASC"
);
$arFilter = Array(
    'USER_PROPS_ID' => array_keys($arResult['ORDER_PROP']['USER_PROFILES'])
);
$arSelect = Array(
    "*"
);

$res = \CSaleOrderUserPropsValue::GetList(
    $arOrder,
    $arFilter,
    false,
    false,
    $arSelect
);

while ($prop = $dbProps->GetNext()) {
    if ($prop['CODE'] != 'CITY') {
        $prop['VALUE'] = '';
        $arProps[$prop['CODE']] = $prop;
    }
}

if (empty($arProps['DELIVERY_TIME_FROM']['VALUE'])) {
    $arProps['DELIVERY_TIME_FROM']['VALUE'] = DEFAULT_DELIVERY_TIME_FROM;
}
if (empty($arProps['DELIVERY_TIME_TO']['VALUE'])) {
    $arProps['DELIVERY_TIME_TO']['VALUE'] = DEFAULT_DELIVERY_TIME_FROM;
}

$first = true;
foreach ($arResult['ORDER_PROP']['USER_PROFILES'] as $key => $profile) {
    if ($first) {
        $arResult['ORDER_PROP']['USER_PROFILES'][$key]['CHECKED'] = 'Y';
        $first = false;
    }
    $arResult['ORDER_PROP']['USER_PROFILES'][$key]['VALUES'] = $arProps;
}

while ($userPropValue = $res->Fetch()) {
    if ($userPropValue['PROP_IS_LOCATION'] == 'Y') {
        // Get city name
        $city = \Bitrix\Sale\Location\LocationTable::getList(array(
            'select' => array(
                'ID' => 'ID',
                'CODE' => 'CODE',
                'CITY_NAME' => 'NAME.NAME',
                'CITY_SHORT_NAME' => 'NAME.SHORT_NAME'
            ),
            'filter' => array(
                'CITY_ID' => $userPropValue['VALUE'],
            )
        ))->fetch();

        $userPropValue['CITY'] = $city;
    }

    if (!empty($arResult['ORDER_PROP']['USER_PROFILES'][$userPropValue['USER_PROPS_ID']]['VALUES'][$userPropValue['PROP_CODE']])) {
        $arResult['ORDER_PROP']['USER_PROFILES'][$userPropValue['USER_PROPS_ID']]['VALUES'][$userPropValue['PROP_CODE']] = $userPropValue;
    }
}

foreach ($arResult['ORDER_PROP']['USER_PROFILES'] as &$userProfile) {
    $newName = '';

    if (!empty($userProfile['VALUES'])) {
        if (isset($userProfile['VALUES']['LOCATION']) && !empty($userProfile['VALUES']['LOCATION']['CITY']['CITY_NAME'])) {
            $newName .= $userProfile['VALUES']['LOCATION']['CITY']['CITY_NAME'];
        }

        if (isset($userProfile['VALUES']['STREET']) && !empty($userProfile['VALUES']['STREET']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', ').$userProfile['VALUES']['STREET']['VALUE'];
        }

        if (isset($userProfile['VALUES']['HOUSE']) && !empty($userProfile['VALUES']['HOUSE']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', кв ').$userProfile['VALUES']['HOUSE']['VALUE'];
        }

        if (isset($userProfile['VALUES']['HOUSING']) && !empty($userProfile['VALUES']['HOUSING']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', к ').$userProfile['VALUES']['HOUSING']['VALUE'];
        }

        if (isset($userProfile['VALUES']['DELIVERY_TIME_FROM']) && !empty($userProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', ').$userProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE'];

            if (isset($userProfile['VALUES']['DELIVERY_TIME_TO']) && !empty($userProfile['VALUES']['DELIVERY_TIME_TO']['VALUE'])) {
                $newName .= (empty($newName) ? '' : ' - ').$userProfile['VALUES']['DELIVERY_TIME_TO']['VALUE'];
            }
        }
    }

    if (!empty($newName)) {
        $userProfile['NAME'] = $newName;
    }
}