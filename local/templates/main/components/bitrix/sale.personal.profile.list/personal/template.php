<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Sale;

if(strlen($arResult["ERROR_MESSAGE"])>0)
{
	ShowError($arResult["ERROR_MESSAGE"]);
}
if(strlen($arResult["NAV_STRING"]) > 0)
{
	?>
	<p><?=$arResult["NAV_STRING"]?></p>
	<?
}

/*foreach ($arResult['PROFILES'] as $key => $profile) {
    $profileData = Sale\OrderUserProperties::getProfileValues((int)($profile['ID']));

    // Get city name
    if (!empty($profileData[6])) {
        printData($profileData[6],'','block');
        $city = \Bitrix\Sale\Location\LocationTable::getList(array(
            'select' => array(
                'ID' => 'ID',
                'CODE' => 'CODE',
                'CITY_NAME' => 'NAME.NAME',
                'CITY_SHORT_NAME' => 'NAME.SHORT_NAME'
            ),
            'filter' => array(
                'CITY_ID' => $profileData[6],
            )
        ))->fetch();

        printData($city,'testcity','block');
        $profileData['CITY_NAME'] = $city;
    }

    $arResult['PROFILES'][$key]['INFO'] = $profileData;
}*/

if (count($arResult['ORDER_PROP']['USER_PROFILES'])) {
    ?>
    <form id="bx-soa-profiles-form">
        <?= bitrix_sessid_post() ?>
        <div class="order__section order__section--address" id="profile-wrapper">
            <? foreach ($arResult['ORDER_PROP']['USER_PROFILES'] as $profile): ?>
                <div class="order__radio order__radio--old">
                    <input type="radio" name="PROFILE_ID" value="<?= $profile['ID'] ?>"
                           id="address_<?= $profile['ID'] ?>"
                           class="radio radio-js"<?= $profile['CHECKED'] == 'Y' ? ' checked' : '' ?>>
                    <label for="address_<?= $profile['ID'] ?>"><?= $profile['NAME'] ?></label>
                </div>
            <? endforeach ?>
            <div class="order__radio order__radio--new">
                <input type="radio" name="PROFILE_ID" value="0" id="address_new"
                       class="radio radio-js"<?= empty($arResult['ORDER_PROP']['USER_PROFILES']) ? ' checked' : '' ?>>
            </div>
        </div>

        <div class="order__row">
            <? foreach (current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES'] as $key => $prop) { ?>
                <? $isBig = in_array($key, array(
                    'LOCATION',
                    'STREET'
                )); ?>
                <div class="order__block<?= $isBig ? '' : ' order__block--smallest' ?>">
                    <?
                    if ($prop['CODE'] == 'LOCATION') {
                        ?>
                        <div class="order__name"><?= $prop['NAME'] ?></div>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:sale.location.selector.search",
                            "choice_location",
                            array(
                                "COMPONENT_TEMPLATE" => "choice_location",
                                "ID" => "",
                                "CODE" => $prop['CITY']['CODE'],
                                "INPUT_NAME" => 'LOCATION',
                                "PROVIDE_LINK_BY" => "code",
                                "FILTER_BY_SITE" => "N",
                                "SHOW_DEFAULT_LOCATIONS" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "JS_CALLBACK" => "",
                                "SUPPRESS_ERRORS" => "N",
                                "INITIALIZE_BY_GLOBAL_EVENT" => "",
                                "FILTER_SITE_ID" => "current",
                                'JS_CONTROL_GLOBAL_ID' => 'ORDER_CITY_SEARCH'
                            ),
                            false
                        );
                    } elseif (!in_array($prop['CODE'], Array('DELIVERY_TIME_FROM', 'DELIVERY_TIME_TO'))) {
                        ?>
                        <div class="order__name"><?= $prop['NAME'] ?></div>
                        <input type="text" name="ORDER_PROP_<?= $prop['ID'] ?>"
                               placeholder="<?= $prop['DEFAULT_VALUE'] ?>"
                               id="<?= $prop['CODE'] ?>"
                               class="<?= $isBig ? 'default-input default-input--normal' : 'default-input default-input--smallest' ?>"
                               value="<?= $prop['VALUE'] ?>">
                    <? } ?>
                </div>
            <? } ?>
        </div>

        <div class="order__row">
            <div class="order__block order__block--timer">
                <div class="reg__timer">
                    <div class="reg__time"><span
                                class="reg__desc"><?= Loc::getMessage("SALE_DELIVERY_TIME_FROM"); ?></span>
                        <input id="startTime" class="default-input default-input--small"
                               name="ORDER_PROP_<?= current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_FROM']['PROP_ID'] ?>"
                               value="<?=
                               current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_FROM']['VALUE']
                               ?>"
                        >
                        <span class="reg__desc"><?= Loc::getMessage("SALE_DELIVERY_TIME_TO"); ?></span>
                        <input id="endTime" class="default-input default-input--small"
                               name="ORDER_PROP_<?= current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_TO']['PROP_ID'] ?>"
                               value="<?=
                               current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_TO']['VALUE']
                               ?>"
                        >
                    </div>
                    <div id="reg__timer"></div>
                </div>
            </div>
        </div>
        <button type="submit" id="save_profile" class="button button--orange data__submit">Сохранить</button>
    </form>
    <?php
    $jsParams = array(
        'FORM_ID' => 'bx-soa-profiles-form',
        'BUTTON_ID' => 'save_profile',
        'AJAX_URL' => '/local/tools/ajax/profile.php',
        'USER_PROFILES' => $arResult['ORDER_PROP']['USER_PROFILES'],
        'REFRESH_USER_PROFILE_FIELDS' => array(
            'LOCATION',
            'STREET',
            'HOUSE',
            'HOUSING',
            'AP_NUMBER',
            'ZIP',
            'DELIVERY_TIME_FROM',
            'DELIVERY_TIME_TO'
        ),
        'NAME_PROFILE' => 'radio-js',
        'DEFAULT_DELIVERY_TIME_FROM' => DEFAULT_DELIVERY_TIME_FROM,
        'DEFAULT_DELIVERY_TIME_TO' => DEFAULT_DELIVERY_TIME_TO,
        'PROFILE_CHANGE_ID' => 'profile_change',
        'JS_CONTROL_GLOBAL_ID' => 'ORDER_CITY_SEARCH',
        'PROFILE_WRAPPER_ID' => 'profile-wrapper',
        'RESULT' => $arResult,
        'PROFILE_NAME' => 'PROFILE_ID',
        'MAIN_ERRORS_BLOCK' => 'main-errors'
    );
    ?>

    <script>
        var orderAjax;
        // Пробрасываем языковые фразы в JS
        //BX.message(<?= CUtil::PhpToJsObject($messages) ?>);

        BX.ready(function () {
            var delivery = <?= json_encode($arResult['DELIVERY']) ?>;
            orderAjax = new CJSUserProfiles(<?= CUtil::PhpToJSObject($jsParams) ?>);
            // Init delivery time slider
            regTimer(
                <?= getTimeSliderJS(current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_FROM']['VALUE']) ?>,
                <?= getTimeSliderJS(current($arResult['ORDER_PROP']['USER_PROFILES'])['VALUES']['DELIVERY_TIME_TO']['VALUE']) ?>
            );
        });
    </script>
    <?
}
else
{
	?>
	<h3><?=Loc::getMessage("STPPL_EMPTY_PROFILE_LIST") ?></h3>
	<?
}
?>
