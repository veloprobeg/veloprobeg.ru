<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/*
$elementIds = array();
$elSearch = array();

foreach ($arResult['SEARCH'] as $k => $item) {
  if (!empty($item['PARAM1']) && $item['PARAM2'] > 0 && $item['ITEM_ID'] > 0) {
    $elementIds[$item['PARAM2']][] = $item['ITEM_ID'];
    $elSearch[$item['ITEM_ID']] = $k;
  }

  $arResult['SEARCH'][$k]['PRICE'] = getFinalPriceInCurrency($item['ITEM_ID']);
  $arResult['SEARCH'][$k]['PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($arResult['SEARCH'][$k]['PRICE'], 'RUB');
}

$elementIds = array_unique($elementIds);

$elements = array();
$fileIds = array();

foreach ($elementIds as $iBlockId => $itemIds) {
  $res = CIBlockELement::GetList(array(), array(
    'ID' => $itemIds
  ), false, false, array(
    'ID',
    'IBLOCK_ID',
    'PREVIEW_PICTURE',
    'DETAIL_PICTURE',
    'PROPERTY_MORE_PHOTO'
  ));

  while ($element = $res->GetNext()) {
    if (intval($element['PREVIEW_PICTURE']) > 0) {
      $fileIds[] = $element['PREVIEW_PICTURE'];
    } elseif (intval($element['DETAIL_PICTURE']) > 0) {
      $fileIds[] = $element['DETAIL_PICTURE'];
    } elseif (intval($element['PROPERTY_MORE_PHOTO_VALUE']) > 0) {
      $fileIds[] = $element['PROPERTY_MORE_PHOTO_VALUE'];
    }

    $elements[] = $element;
  }
}

if (!empty($fileIds)) {
  $file = new CFile();

  $files = array();
  $fileRes = $file->GetList(array(), array(
    'ID' => $fileIds
  ));

//  TODO Move to .parameters.php
  $size = array(
    'width' => 164,
    'height' => 120
  );

  while ($f = $fileRes->Fetch()) {
    $resized = $file->ResizeImageGet($f, $size, BX_RESIZE_IMAGE_EXACT, true);

    if ($resized) {
      $f['SRC'] = $resized['src'];
      $f['WIDTH'] = $resized['width'];
      $f['HEIGHT'] = $resized['height'];
    }

    $files[$f['ID']] = $f;
  }
}

foreach ($elements as $el) {
  if (isset($files[$el['PREVIEW_PICTURE']])) {
    $arResult['SEARCH'][$elSearch[$el['ID']]]['IMG'] = $files[$el['PREVIEW_PICTURE']];
  } elseif (isset($files[$el['DETAIL_PICTURE']])) {
    $arResult['SEARCH'][$elSearch[$el['ID']]]['IMG'] = $files[$el['DETAIL_PICTURE']];
  } elseif (isset($files[$el['PROPERTY_MORE_PHOTO_VALUE']])) {
    $arResult['SEARCH'][$elSearch[$el['ID']]]['IMG'] = $files[$el['PROPERTY_MORE_PHOTO_VALUE']];
  }
}

$arResult['IBLOCKS'] = getStructureIBlocks();
*/