<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Localization\Loc;

?>
<div class="search">
  <form class="search__head" action="/search/index.php">
    <input type="search" class="search__input" name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>">
    <div class="search__checks">
      <div class="search__checks-head"><?= Loc::getMessage('SEARCH_BY_TYPE') ?></div>
      <div class="search__checks-wrapper">
        <div class="checkbox-area">
          <input type="checkbox" name="show_goods" id="show_goods"
                 class="checkbox"<?= $_REQUEST['show_goods'] == 'on' ? ' checked' : '' ?>>
          <label for="show_goods"><?= Loc::getMessage('SEARCH_PRODUCTS') ?></label>
        </div>
        <div class="checkbox-area">
          <input type="checkbox" name="show_except_goods" id="show_except_goods"
                 class="checkbox"<?= $_REQUEST['show_except_goods'] == 'on' ? ' checked' : '' ?>>
          <label for="show_except_goods"><?= Loc::getMessage('SEARCH_TEXT') ?></label>
        </div>
      </div>
    </div>
    <button type="submit" class="button button--orange"><?= Loc::getMessage('RELOAD_SEARCH') ?></button>
  </form>
  <? $type = Loc::getMessage('SEARCH_PRODUCT'); ?>
  <? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
  <? elseif ($arResult["ERROR_CODE"] != 0): ?>
    <div style="background-color: #fff;padding: 40px 50px;margin-top: 15px;">
      <p><?= Loc::getMessage("SEARCH_ERROR") ?></p>
      <? ShowError($arResult["ERROR_TEXT"]); ?>
      <p><?= Loc::getMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
      <br/><br/>
      <p><?= Loc::getMessage("SEARCH_SINTAX") ?><br/><b><?= Loc::getMessage("SEARCH_LOGIC") ?></b></p>
      <table border="0" cellpadding="5">
        <tr>
          <td align="center" valign="top"><?= Loc::getMessage("SEARCH_OPERATOR") ?></td>
          <td valign="top"><?= Loc::getMessage("SEARCH_SYNONIM") ?></td>
          <td><?= Loc::getMessage("SEARCH_DESCRIPTION") ?></td>
        </tr>
        <tr>
          <td align="center" valign="top"><?= Loc::getMessage("SEARCH_AND") ?></td>
          <td valign="top">and, &amp;, +</td>
          <td><?= Loc::getMessage("SEARCH_AND_ALT") ?></td>
        </tr>
        <tr>
          <td align="center" valign="top"><?= Loc::getMessage("SEARCH_OR") ?></td>
          <td valign="top">or, |</td>
          <td><?= Loc::getMessage("SEARCH_OR_ALT") ?></td>
        </tr>
        <tr>
          <td align="center" valign="top"><?= Loc::getMessage("SEARCH_NOT") ?></td>
          <td valign="top">not, ~</td>
          <td><?= Loc::getMessage("SEARCH_NOT_ALT") ?></td>
        </tr>
        <tr>
          <td align="center" valign="top">( )</td>
          <td valign="top">&nbsp;</td>
          <td><?= Loc::getMessage("SEARCH_BRACKETS_ALT") ?></td>
        </tr>
      </table>
    </div>
  <? elseif (count($arResult['SEARCH']) > 0): ?>
    <? foreach ($arResult["SEARCH"] as $arItem): ?>
      <? if (empty($arItem['PARAM1']) && empty($arItem['PARAM2'])) : ?>
        <? $type = ''; ?>
        <div class="search__text">
          <a href="<?= $arItem['URL'] ?>" class="search__text-header"><?= $arItem['TITLE'] ?></a>
          <div class="search__text-desc"><?= $type ?></div>
          <div class="search__text-content"><?= $arItem['BODY_FORMATED'] ?></div>
        </div>
      <? else: ?>
        <div class="search__item">
          <? if (!empty($arItem['IMG']['SRC'])): ?>
            <a href="<?= $arItem['URL'] ?>" class="search__item-image"><img src="<?= $arItem['IMG']['SRC'] ?>"></a>
          <? endif ?>
          <div class="search__item-about">
            <a href="<?= $arItem['URL'] ?>" class="search__item-name"><?= $arItem['TITLE'] ?></a>
            <div
                class="search__item-type"><?= $arResult['IBLOCKS'][$arItem['PARAM1']][$arItem['PARAM2']]['NAME'] ?></div>
            <? if ($arItem['PARAM1'] == 'catalog' && $arItem['PRICE']): ?>
              <div class="search__item-price"><?= Loc::getMessage('SEARCH_PRICE_FROM', array(
                  '#PRICE#' => $arItem['PRICE_FORMATED']
                )) ?></div>
            <? endif ?>
          </div>
        </div>
      <? endif ?>
    <? endforeach ?>
  <? else: ?>
    <? ShowNote(Loc::getMessage("SEARCH_NOTHING_TO_FOUND")); ?>
  <? endif ?>
</div>