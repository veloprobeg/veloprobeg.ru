<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

$arAuthServices = $arPost = array();

if (is_array($arParams["~AUTH_SERVICES"])) {
    $arAuthServices = $arParams["~AUTH_SERVICES"];
}

if (is_array($arParams["~POST"])) {
    $arPost = $arParams["~POST"];
}

$arResult['AUTH_SERVICES_ASSOC'] = array(
    'Facebook' => 'fb',
    'VKontakte' => 'vk'
);

if (!empty($arParams['AUTH_SERVICES'])) {
    if ($arParams["POPUP"]) {
        //only one float div per page
        if (defined("BX_SOCSERV_REG_POPUP")) {
            return;
        }

        define("BX_SOCSERV_REG_POPUP", true);
        ?>
        <div class="reg reg--social">
            <div class="reg__head"><?= Loc::getMessage("REGISTER_FROM_SOCIAL") ?></div>
            <div class="reg__head-links">
                <? foreach ($arAuthServices as $service): ?>
                    <a href="javascript:void(0)"
                       onclick="<?= $service['ONCLICK'] ?>"
                       id="bx_auth_href_<?= $arParams["SUFFIX"] ?><?= $service["ID"] ?>"
                       class="reg__social reg__social--<?= $arResult['AUTH_SERVICES_ASSOC'][$service['ID']] ?>"></a>
                <? endforeach ?>
            </div>
        </div>
        <?
    }
}