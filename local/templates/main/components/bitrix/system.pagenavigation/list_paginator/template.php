<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
  if (0 == $arResult["NavRecordCount"] || (1 == $arResult["NavPageCount"] && false == $arResult["NavShowAll"])) {
    return;
  }
}
if ('' != $arResult["NavTitle"]) {
  $arResult["NavTitle"] .= ' ';
}

$strSelectPath = $arResult['sUrlPathParams'].($arResult["bSavePage"] ? '&PAGEN_'.$arResult["NavNum"].'='.(true !== $arResult["bDescPageNumbering"] ? 1 : '').'&' : '').'SHOWALL_'.$arResult["NavNum"].'=0&SIZEN_'.$arResult["NavNum"].'=';

?>
<?if ( $arResult['NavPageNomer'] < $arResult['NavPageCount'] ):?>
	<input type="hidden" id="next_page" style="display: block;" value="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>&SIZEN_<?= $arResult["NavNum"] ?>=<?= $arResult['NavPageSize']; ?>">
<div id="page_preloader" style="display: none;">
		<img src="/images/30.gif">
	</div>
<?endif;?>
