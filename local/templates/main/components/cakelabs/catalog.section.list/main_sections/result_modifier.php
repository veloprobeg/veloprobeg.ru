<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['SECTIONS'] as &$arSection) {
    $srcPic = false;

    if (is_array($arSection['PICTURE'])) {
        $srcPic = $arSection['PICTURE'];
    }

    if ($srcPic) {
        $resPic = CFile::ResizeImageGet($srcPic, array(
            'height' => 150,
            'width' => 250
        ), BX_RESIZE_IMAGE_PROPORTIONAL, true);

        if ($resPic) {
            $arSection['PICTURE']['SRC'] = $resPic['src'];
            $arSection['PICTURE']['WIDTH'] = $resPic['width'];
            $arSection['PICTURE']['HEIGHT'] = $resPic['height'];
        }
    }
}
?>