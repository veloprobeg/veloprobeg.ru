<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

if (!empty($arResult['SECTIONS'])) {
    $strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
    $strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
    $arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

    ?>
    <div class="cat-slider slider slider--navs">
        <div class="cat-slider__text">
            <div class="cat-slider__name"><?= Loc::getMessage("CT_BCSL_CATALOG"); ?></div>
            <a href="/catalog/" class="cat-slider__link"><?= Loc::getMessage("CT_BCSL_VIEW_ALL"); ?></a>
        </div>
        <div class="cat-slider__slider">
            <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                <?
                $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                ?>
		<div id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"  class="cat-slider__item">
	                <div class="cat-slider__item-name"><?= $arSection['NAME'] ?></div>
	                <div class="cat-slider__item-image"><img src="<?= $arSection['PICTURE']['SRC'] ?>" alt=""></div>
	                <a class='item-link' href="<?= $arSection['SECTION_PAGE_URL'] ?>"></a>
		</div>
            <? endforeach ?>
        </div>
    </div>
<? } ?>
