<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

foreach ($arResult['SECTIONS'] as &$arSection) {
  $srcPic = false;

  if (is_array($arSection['PICTURE'])) {
    $srcPic = $arSection['PICTURE'];
  }

  if ($arSection['UF_MEM_IMAGE'] > 0) {
    $srcPic = $arSection['UF_MEM_IMAGE'];
  }

  if ($srcPic) {
    $resPic = CFile::ResizeImageGet($srcPic, array(
      'width' => 340,
      'height' => 340
    ), BX_RESIZE_IMAGE_EXACT, true);

    if ($resPic) {
      $arSection['PICTURE']['SRC'] = $resPic['src'];
      $arSection['PICTURE']['WIDTH'] = $resPic['width'];
      $arSection['PICTURE']['HEIGHT'] = $resPic['height'];
    }
  }
}
?>