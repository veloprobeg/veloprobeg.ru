<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

if (!empty($arResult['SECTIONS'])) {
  $strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
  $strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
  $arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

  ?>
  <div class="not-found">
    <div class="not-found__backface"><?= Loc::getMessage("CT_BCSL_404"); ?></div>
    <div class="not-found__head"><?= Loc::getMessage("CT_BCSL_NOT_FOUND_INFO"); ?></div>
    <div class="not-found__content">
      <div class="not-found__main"><?= Loc::getMessage("CT_BCSL_GO_TO_MAIN", array(
          "#LINK#" => '/'
        )); ?></div>
    </div>
    <div class="not-found__slider slider slider--navs">
      <? foreach ($arResult['SECTIONS'] as $arSection): ?>
        <?
        $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
        $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
        ?>
        <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="not-found__slide"
           id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
          <div style="background-image: url(<?= $arSection['PICTURE']['SRC'] ?>)"
               class="not-found__slide-image"></div>
          <div class="not-found__slide-text"><?= $arSection['NAME'] ?></div>
        </a>
      <? endforeach ?>
    </div>
    <div class="not-found__mems"><?= Loc::getMessage("CT_BCSL_NOT_FOUND_NOTE"); ?></div>
  </div>
<? } ?>