<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

//$component = $this->__component;
//$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

$obCache = new CPHPCache();
$cacheLifetime = 86400;

$cacheID = 'profileProps_' . $groupId;
$cachePath = '/profile/'.$cacheID;
$arProps = Array();
$addrProps = Array(
    "ZIP",
    "LOCATION",
    "STREET",
    "HOUSE",
    "HOUSING",
    "AP_NUMBER",
	
	"ZIP_UR",
    "LOCATION_UR",
    "STREET_UR",
    "HOUSE_UR",
    "HOUSING_UR",
    "AP_NUMBER_UR",
	
);
if( $obCache->InitCache( $cacheLifetime, $cacheID, $cachePath ) ) {
    $arProps = $obCache->GetVars();
}
elseif( $obCache->StartDataCache() ) {


	
		$dbProps = CSaleOrderProps::GetList(
			array("SORT" => "ASC"),
			array(
				"!CODE" => Array(
					"CITY",
				)
			),
			false,
			false,
			array(
				"ID",
				"NAME",
				"CODE",
				"PROPS_GROUP_ID",
				"PERSON_TYPE_ID",
				"REQUIRED"
			)
		);

		while ( $prop = $dbProps->GetNext() ) {
		
			$prop['ADDR_PROP'] = in_array($prop['CODE'], $addrProps) ? true : false;
			$arProps[$prop['ID']] = $prop;
		}

	
    $obCache->EndDataCache($arProps);
}

$arResult['PROPS'] = $arProps;




// Get user fields
global $USER;
if ($USER->IsAuthorized()) {
    $arResult['USER_INFO'] = $USER->GetList($by = '', $sort = '', array(
        'ID' => $USER->GetID()
    ), array(
        'SELECT' => array('UF_*'),
        'FIELDS' => array(
            'ID',
            'NAME',
            'LAST_NAME',
        )
    ))->Fetch();
}

// Set init values for delivery time
$startTime = DEFAULT_DELIVERY_TIME_FROM;
$endTime = DEFAULT_DELIVERY_TIME_TO;
if (!empty($arResult['USER_INFO']['UF_DELIV_TIME_FINISH'])) {
    $endTime = $arResult['USER_INFO']['UF_DELIV_TIME_FINISH'];
}

if (!empty($arResult['USER_INFO']['UF_DELIV_TIME_START'])) {
    $startTime = $arResult['USER_INFO']['UF_DELIV_TIME_START'];
}

$arResult['USER_INFO']['START_DELIVERY_TIME_JS'] = getTimeSliderJS($startTime);
$arResult['USER_INFO']['END_DELIVERY_TIME_JS'] = getTimeSliderJS($endTime);

foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $orderProp) {
    $arResult['ORDER_PROP_VALUES'][$orderProp['CODE']] = $orderProp['VALUE'][0];
}

//$arResult['VIEW_DATA']['PERSONAL_DATA_FIRST_ROW'] = $arResult['VIEW_DATA']['PERSONAL_DATA_SECOND_ROW'] = $arResult['VIEW_DATA']['DELIVERY_DATA_FIRST_ROW'] = $arResult['VIEW_DATA']['DELIVERY_DATA_SECOND_ROW'] = array();

$user = CUser::GetByID($USER->GetID())->Fetch();




	foreach ($arResult['PROPS']  as $prop) {
		if ($prop['ADDR_PROP']) {
			$arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['DELIVERY_DATA_FIRST_ROW'][$prop['ID']] = $prop;
		}
	}





//foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $prop) {

foreach ($arResult['PROPS'] as $prop) {
    // first row personal data
    if (in_array($prop['CODE'], array(
        'LAST_NAME',
        'NAME',
        'BONUS_CARD',
		'NAME_UR',
		'PHONE_UR',
		'EMAIL_UR'
    ))) {
        switch ($prop['CODE']) {
            case ('NAME'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $USER->GetFirstName();
                }
                break;
			case ('EMAIL_UR'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $USER->GetEmail();
                }
                break;	
				
			case ('PHONE_UR'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $user['PERSONAL_PHONE'];
                }
                break;	
				
            case ('LAST_NAME'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $USER->GetLastName();
                }
                break;

            default:
                break;
        }
        $arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['PERSONAL_DATA_FIRST_ROW'][] = $prop;
    }

    // second row personal data
    if (in_array($prop['CODE'], array(
        'EMAIL',
        'PHONE',
		'COMPANY_NAME',
		'BANK_RS', 
		'INN',
		'KPP',
		'BANK_COR',
		'BIK',
		'ADRESS_UR'
    ))) {
        switch ($prop['CODE']) {
            case ('EMAIL'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $USER->GetEmail();
                }
                break;
            case ('PHONE'):
                if (empty($prop['VALUE'][0])) {
                    $prop['VALUE'][0] = $user['PERSONAL_PHONE'];
                }
                break;

            default:
                break;
        }

        $arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['PERSONAL_DATA_SECOND_ROW'][] = $prop;
    }

    // first row delivery data
    if (in_array($prop['CODE'], array(
        'LOCATION',
        'STREET',
        'HOUSE',
        'HOUSING',
        'ZIP',
        'AP_NUMBER'
    ))) {
        $arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['DELIVERY_DATA_FIRST_ROW'][$prop['ID']] = $prop;
    }

    // second row delivery data
    // delivery time start
    if ($prop['CODE'] == 'DELIVERY_TIME_FROM' || $prop['CODE'] == 'DELIVERY_TIME_FROM_UR') {
        if (!empty($prop['VALUE'][0])) {
            $prop['JS_VALUE'] = getTimeSliderJS($prop['VALUE'][0]);
        } else {
            $prop['JS_VALUE'] = $arResult['USER_INFO']['START_DELIVERY_TIME_JS'];
            $prop['VALUE'][0] = $startTime;
        }

        $arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['DELIVERY_TIME_START'] = $prop;
    }

    // delivery time end
    if ($prop['CODE'] == 'DELIVERY_TIME_TO' || $prop['CODE'] == 'DELIVERY_TIME_TO_UR' ) {
        if (!empty($prop['VALUE'][0])) {
            $prop['JS_VALUE'] = getTimeSliderJS($prop['VALUE'][0]);
        } else {
            $prop['JS_VALUE'] = $arResult['USER_INFO']['END_DELIVERY_TIME_JS'];
            $prop['VALUE'][0] = $endTime;
        }

        $arResult['VIEW_DATA'][$prop['PERSON_TYPE_ID']]['DELIVERY_TIME_END'] = $prop;
    }

    if ($prop['REQUIRED'] == 'Y') {
        $arResult['REQUIRED_FIELDS'][] = $prop;
    }
}




// Rules and messages for validate fields
$arResult['RULES'] = $arResult['RULES_MESSAGES'] = array();

foreach ($arResult['REQUIRED_FIELDS'] as $required) {
    $arResult['RULES']['ORDER_PROP_' . $required['ID']] = array(
        'required' => true
    );

    $arResult['RULES_MESSAGES']['ORDER_PROP_' . $required['ID']] = '';
}

// Get user profile values for view name
$res = \CSaleOrderUserPropsValue::GetList(array(), array(
    'USER_PROPS_ID' => array_keys($arResult['ORDER_PROP']['USER_PROFILES'])
));

// TODO refactor and cache
while ($userPropValue = $res->Fetch()) {
    if ($userPropValue['PROP_IS_LOCATION'] == 'Y') {
        // Get city name
        $city = \Bitrix\Sale\Location\LocationTable::getList(array(
            'select' => array(
                'ID' => 'ID',
                'CODE' => 'CODE',
                'CITY_NAME' => 'NAME.NAME',
                'CITY_SHORT_NAME' => 'NAME.SHORT_NAME'
            ),
            'filter' => array(
                'CITY_ID' => $userPropValue['VALUE'],
            )
        ))->fetch();

        $userPropValue['CITY'] = $city;
    }

    $arResult['ORDER_PROP']['USER_PROFILES'][$userPropValue['USER_PROPS_ID']]['VALUES'][$userPropValue['PROP_CODE']] = $userPropValue;
}

foreach ($arResult['ORDER_PROP']['USER_PROFILES'] as &$userProfile) {
    $newName = '';

    if (!empty($userProfile['VALUES'])) {
        if (isset($userProfile['VALUES']['LOCATION']) && !empty($userProfile['VALUES']['LOCATION']['CITY']['CITY_NAME'])) {
            $newName .= $userProfile['VALUES']['LOCATION']['CITY']['CITY_NAME'];
        }

        if (isset($userProfile['VALUES']['STREET']) && !empty($userProfile['VALUES']['STREET']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', ') . $userProfile['VALUES']['STREET']['VALUE'];
        }

        if (isset($userProfile['VALUES']['HOUSE']) && !empty($userProfile['VALUES']['HOUSE']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', кв ') . $userProfile['VALUES']['HOUSE']['VALUE'];
        }

        if (isset($userProfile['VALUES']['HOUSING']) && !empty($userProfile['VALUES']['HOUSING']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', к ') . $userProfile['VALUES']['HOUSING']['VALUE'];
        }

        if (isset($userProfile['VALUES']['DELIVERY_TIME_FROM']) && !empty($userProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE'])) {
            $newName .= (empty($newName) ? '' : ', ') . $userProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE'];

            if (isset($userProfile['VALUES']['DELIVERY_TIME_TO']) && !empty($userProfile['VALUES']['DELIVERY_TIME_TO']['VALUE'])) {
                $newName .= (empty($newName) ? '' : ' - ') . $userProfile['VALUES']['DELIVERY_TIME_TO']['VALUE'];
            }
        }
    }

    if (!empty($newName)) {
        $userProfile['NAME'] = $newName;
    }
}

// Check date
$incPath = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/classes/BankDay.php';

// TODO use standard lang or move to .template.php lang file
$months = array(
    1 => 'Января',
    2 => 'Февраля',
    3 => 'Марта',
    4 => 'Апреля',
    5 => 'Мая',
    6 => 'Июня',
    7 => 'Июля',
    8 => 'Августа',
    9 => 'Сентября',
    10 => 'Октября',
    11 => 'Ноября',
    12 => 'Декабря',
);

if (file_exists($incPath)) {
    include_once($incPath);

    $arResult['IS_WORK_DAY'] = BankDay::isWorkDay(date('Y-m-d'));
    $date = new \DateTime();
    $tomorrow = $date->add(new DateInterval('P1D'));
    $arResult['IS_TOMORROW_WORK_DAY'] = BankDay::isWorkDay($tomorrow->format('Y-m-d'));

    $nextWorkDay = $tomorrow;
    if (!$arResult['IS_TOMORROW_WORK_DAY']) {
        while (!$isWork) {
            $nextWorkDay = $tomorrow->add(new DateInterval('P1D'));
            $isWork = BankDay::isWorkDay($nextWorkDay->format('Y-m-d'));
        }

        $month = $months[$nextWorkDay->format('n')];
        $arResult['NEXT_WORK_DAY'] = $nextWorkDay->format('d ' . $month . ' Y');
    } else {
        $month = $months[$tomorrow->format('n')];
        $arResult['NEXT_WORK_DAY'] = $tomorrow->format('d ' . $month . ' Y');
    }
}

if (is_array($arResult['DELIVERY'])) {
    $first = true;
    foreach ($arResult['DELIVERY'] as $key => $delivery) {
        if ($first) {
            $arResult['DELIVERY'][$key]['CHECKED'] = 'Y';
            $first = false;
        }
        else {
            $arResult['DELIVERY'][$key]['CHECKED'] = '';
        }
    }
}

if (!empty($arResult['ORDER_PROP']['USER_PROFILES'])) {
    $curProfile = current($arResult['ORDER_PROP']['USER_PROFILES']);
    if (!empty($curProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE'])) {
        $arResult['VIEW_DATA'][1]['DELIVERY_TIME_START']['JS_VALUE'] = getTimeSliderJS($curProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE']);
		$arResult['VIEW_DATA'][3]['DELIVERY_TIME_START']['JS_VALUE'] = getTimeSliderJS($curProfile['VALUES']['DELIVERY_TIME_FROM']['VALUE']);
    
	}
    if (!empty($curProfile['VALUES']['DELIVERY_TIME_TO']['VALUE'])) {
        $arResult['VIEW_DATA'][1]['DELIVERY_TIME_END']['JS_VALUE'] = getTimeSliderJS($curProfile['VALUES']['DELIVERY_TIME_TO']['VALUE']);
		$arResult['VIEW_DATA'][3]['DELIVERY_TIME_END']['JS_VALUE'] = getTimeSliderJS($curProfile['VALUES']['DELIVERY_TIME_TO']['VALUE']);
   
   }

    foreach ($arResult['VIEW_DATA']['DELIVERY_DATA_FIRST_ROW'] as $key => $prop) {
        if (!empty($curProfile['VALUES'][$prop['CODE']])) {
            if ($prop['CODE'] == 'LOCATION') {
                $arResult['VIEW_DATA']['DELIVERY_DATA_FIRST_ROW'][$key]['VALUE'][0] = $curProfile['VALUES'][$prop['CODE']]['CITY']['CODE'];
            }
            else {
                $arResult['VIEW_DATA']['DELIVERY_DATA_FIRST_ROW'][$key]['VALUE'][0] = $curProfile['VALUES'][$prop['CODE']]['VALUE'];
            }
        }
    }
}