(function (window) {
    if (!!window.CJSOrderAjax) {
        return;
    }

    window.CJSOrderAjax = function (params) {
        this.obForm = null;
        this.obButton = null;
        this.ajaxUrl = params.AJAX_URL || null;
        this.obOrderPrice = null;
        this.obDelivery = null;
        this.obDeliveryPrice = null;
        this.obProfileChange = null;
        this.obProfileWrapper = null;
        this.obPaySystemWrapper = null;
        this.obDeliveryWrapper = null;
        this.mainErrorsNode = null;
        this.hasErrorSection = null;
        this.publicOffer = null;

        this.defaultDeliveryTimeFrom = params.DEFAULT_DELIVERY_TIME_FROM || '';
        this.defaultDeliveryTimeTo = params.DEFAULT_DELIVERY_TIME_TO || '';

        this.signedParamsString = params.signedParamsString || '';

        this.profileName = params.PROFILE_NAME || null;
        this.citySelectorId = params.JS_CONTROL_GLOBAL_ID || null;

        this.userProfiles = params.USER_PROFILES || [];
        this.refreshUserProfileFields = params.REFRESH_USER_PROFILE_FIELDS || [];

        this.rules = {};
        this.rulesMessages = {};
        this.result = {};

        this.isWorkDay = false;

        if (!!params.FORM_ID) {
            this.obForm = BX(params.FORM_ID);
        }

        if (!!params.BUTTON_ID) {
            this.obButton = BX(params.BUTTON_ID);
        }

        if (!!params.ORDER_PRICE_ID) {
            this.obOrderPrice = BX(params.ORDER_PRICE_ID);
        }

        if (!!params.DELIVERY_ID) {
            this.obDelivery = BX(params.DELIVERY_ID);
        }
	
	if (!!params.PERSON_TYPE) {
            this.obPersonType = BX(params.PERSON_TYPE);
        }
	

        if (!!params.DELIVERY_PRICE_ID) {
            this.obDeliveryPrice = BX(params.DELIVERY_PRICE_ID);
        }

        if (!!params.PROFILE_CHANGE_ID) {
            this.obProfileChange = BX(params.PROFILE_CHANGE_ID);
        }

        if (!!params.PROFILE_WRAPPER_ID) {
            this.obProfileWrapper = BX(params.PROFILE_WRAPPER_ID);
        }

        if (!!params.DELIVERY_COURIER_ID) {
            this.deliveryCourierId = params.DELIVERY_COURIER_ID;
        }

        if (!!params.DELIVERY_COURIER_WRAPPER_ID) {
            this.obDeliveryCourierWrapper = BX(params.DELIVERY_COURIER_WRAPPER_ID);
        }

        if (!!params.IS_WORK_DAY) {
            this.isWorkDay = params.IS_WORK_DAY;
        }

        if (!!params.RULES) {
            this.rules = params.RULES;
        }

        if (!!params.RULES_MESSAGES) {
            this.rulesMessages = params.RULES_MESSAGES;
        }

        if (!!params.RESULT) {
            this.result = params.RESULT;
        }

        if (!!params.PAY_SYSTEM_WRAPPER_ID) {
            this.obPaySystemWrapper = BX(params.PAY_SYSTEM_WRAPPER_ID);
        }

        if (!!params.DELiVERY_WRAPPER_ID) {
            this.obDeliveryWrapper = BX(params.DELiVERY_WRAPPER_ID);
        }
	
	if (!!params.PERSON_TYPE_WRAPPER_ID) {
            this.obPersonTypeWrapper = BX(params.PERSON_TYPE_WRAPPER_ID);
        }

        if (!!params.MAIN_ERRORS_BLOCK) {
            this.mainErrorsNode = BX(params.MAIN_ERRORS_BLOCK);
        }

        if (!!params.POPUP_ERRORS_BLOCK) {
            this.popupErrorsNode = BX(params.POPUP_ERRORS_BLOCK);
        }

        if (!!params.PUBLIC_OFFER) {
            this.publicOffer = BX(params.PUBLIC_OFFER);
        }

        if (!!params.PROPS) {
            this.props = params.PROPS;
        }

        if (!!params.DELIVERY_INFO) {
            this.deliveryInfoBlock = params.DELIVERY_INFO;
        }

        this.params = params;

        this.init();
    };

    window.CJSOrderAjax.prototype = {
        init: function () {
            if (!!this.obButton) {
                BX.bind(this.obButton, 'click', BX.proxy(function (e) {
                    this.sendRequest('saveOrderAjax');
                    e.preventDefault();
                }, this));
            }
	    
	    
            if (!!this.obPersonType) {
                $(this.obPersonType).on('select2:select', BX.proxy(this.changePersonType, this));
            }
	    
	    
            if (!!this.obDelivery) {
                $(this.obDelivery).on('select2:select', BX.proxy(this.changeDelivery, this));
            }
	    
	    
	    
	    this.obLocation =  BX('ORDER_PROP_6');
	    if ( this.obLocation ) {
		$(this.obLocation).on('change', BX.proxy(this.changeLocation, this));
	    }

	    

            for (var i in this.result.DELIVERY) {
                if (this.result.DELIVERY[i].CHECKED=='Y') {
                    this.deliveryTimer(this.result.DELIVERY[i].ID);
                    break;
                }
            }

            var profiles = BX.findChildren(this.obForm, {
                attr: {
                    name: this.profileName,
                    type: 'radio'
                }
            }, true);

            if (!!profiles) {
                for (var i in profiles) {
                    if (profiles.hasOwnProperty(i)) {
                        if (profiles[i].CODE != 'LOCATION') {
                            BX.bind(profiles[i], 'change', BX.proxy(this.changeProfile, this));
                        }
                    }
                }
            }

            this.sendRequest();
        },

        send: function () {
            BX.ajax.submit(this.obForm, BX.proxy(this.sendHandler, this));
        },

        sendHandler: function (response) {
            var data = BX.parseJSON(response);

            if (data.hasOwnProperty('ERROR')) {
                // TODO view error
                return;
            }

            if (data.hasOwnProperty('order') && data.order.hasOwnProperty('REDIRECT_URL')) {
                window.location.href = data.order.REDIRECT_URL;
            }
        },

        saveOrder: function (result) {
            var res = BX.parseJSON(result), redirected = false;

            if (res && res.order) {
                result = res.order;

                if (result.REDIRECT_URL && result.REDIRECT_URL.length) {
                    redirected = true;
                    document.location.href = result.REDIRECT_URL;
                }

                this.showErrors(result.ERROR, true);
            }

            if (!redirected) {
                this.endLoader();
            }
        },

        getFirstProperty: function () {
            for (var i in this) {
                return this[i];
                break;
            }
        },

        showError: function (node, msg, border) {
            if (BX.type.isArray(msg)) {
                msg = msg.join('<br />');
            }

            var errorContainer = node.querySelector('.alert.alert-danger'), animate;
            if (errorContainer && msg.length) {
                BX.cleanNode(errorContainer);
                errorContainer.appendChild(BX.create('DIV', {html: msg}));

                errorContainer.style.opacity = 0;
                errorContainer.style.display = '';
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
                    step: function (state) {
                        errorContainer.style.opacity = state.opacity / 100;
                    },
                    complete: function () {
                        errorContainer.removeAttribute('style');
                    }
                }).animate();

                if (!!border) {
                    BX.addClass(node, 'bx-step-error');
                }

                BX.addClass(this.popupErrorsNode, 'is-visible');
            }
        },

        showErrors: function (errors, scroll) {
            var isValid = $(this.obForm).valid();
            var errorNodes = this.obForm.querySelectorAll('div.alert.alert-danger'),
                section, k, blockErrors;

            for (k = 0; k < errorNodes.length; k++) {
                section = BX.findParent(errorNodes[k], {className: 'bx-soa-section'});
                BX.removeClass(section, 'bx-step-error');
                errorNodes[k].style.display = 'none';
                BX.cleanNode(errorNodes[k]);
            }

            if (!errors || BX.util.object_keys(errors).length < 1) {
                return;
            }

            for (k in errors) {
                if (!errors.hasOwnProperty(k)) {
                    continue;
                }

                blockErrors = errors[k];

                switch (k.toUpperCase()) {
                    case 'MAIN':
                        this.showError(this.mainErrorsNode, blockErrors);
                        //this.animateScrollTo(this.mainErrorsNode, 800, 20);
                        scroll = false;
                        break;
                    case 'AUTH':
                        //if (this.authBlockNode.style.display == 'none') {
                            this.showError(this.mainErrorsNode, blockErrors);
                            //this.showError(this.mainErrorsNode, blockErrors, true);
                        /*    scroll = false;
                        } else {
                            this.showError(this.mainErrorsNode, blockErrors);
                            //this.showError(this.authBlockNode, blockErrors, true);
                        }*/
                        break;
                    case 'REGION':
                        this.showError(this.mainErrorsNode, blockErrors);
                        //this.showError(this.regionBlockNode, blockErrors, true);
                        //this.showError(this.regionHiddenBlockNode, blockErrors);
                        break;
                    case 'DELIVERY':
                        this.showError(this.mainErrorsNode, blockErrors);
                        //this.showError(this.deliveryBlockNode, blockErrors, true);
                        //this.showError(this.deliveryHiddenBlockNode, blockErrors);
                        break;
                    case 'PAY_SYSTEM':
                        this.showError(this.mainErrorsNode, blockErrors);
                        //this.showError(this.paySystemBlockNode, blockErrors, true);
                        //this.showError(this.paySystemHiddenBlockNode, blockErrors);
                        break;
                    case 'PROPERTY':
                        this.showError(this.mainErrorsNode, blockErrors, true);
                        //this.showError(this.propsBlockNode, blockErrors, true);
                        //this.showError(this.propsHiddenBlockNode, blockErrors);
                        break;
                }
            }

            //!!scroll && this.scrollToError();
        },

        /**
         * Showing loader image with overlay.
         */
        startLoader: function () {
            if (this.BXFormPosting === true) {
                return false;
            }

            this.BXFormPosting = true;

            if (!this.loadingScreen) {
                this.loadingScreen = new BX.PopupWindow("loading_screen", null, {
                    overlay: {
                        backgroundColor: 'white',
                        opacity: '80'
                    },
                    events: {
                        onAfterPopupShow: BX.proxy(function () {
                            BX.cleanNode(this.loadingScreen.popupContainer);
                            BX.removeClass(this.loadingScreen.popupContainer, 'popup-window');
                            this.loadingScreen.popupContainer.appendChild(
                                BX.create('IMG', {props: {src: this.templateFolder + "/images/loader.gif"}})
                            );
                            this.loadingScreen.popupContainer.removeAttribute('style');
                            this.loadingScreen.popupContainer.style.display = 'block';
                        }, this)
                    }
                });
                BX.addClass(this.loadingScreen.popupContainer, 'bx-step-opacity');
            }

            return setTimeout(BX.proxy(function () {
                this.loadingScreen.show()
            }, this), 100);
        },

        /**
         * Hiding loader image with overlay.
         */
        endLoader: function (loaderTimer) {
            this.BXFormPosting = false;
            if (this.loadingScreen && this.loadingScreen.isShown()) {
                this.loadingScreen.close();
            }

            clearTimeout(loaderTimer);
        },

        changeProfile: function (e) {
            if (!!this.obProfileChange) {
                this.obProfileChange.value = 'Y';
            }

            var i = e.target.value,
                deliveryTimeFrom = this.defaultDeliveryTimeFrom,
                deliveryTimeTo = this.defaultDeliveryTimeTo,
                userProfile = false,
                userProfileValues = {},
                location = '';

            if (this.userProfiles.hasOwnProperty(i)) {
                userProfile = this.userProfiles[i];
            }

            if (userProfile && userProfile.hasOwnProperty('VALUES')) {
                userProfileValues = userProfile['VALUES'];
            }

            for (var j in this.refreshUserProfileFields) {
                if (this.refreshUserProfileFields.hasOwnProperty(j)) {
                    var field,
                        code = this.refreshUserProfileFields[j],
                        newValue = '';

                    switch (code) {
                        case 'DELIVERY_TIME_FROM':
                            field = BX('startTime');
                            break;
                        case 'DELIVERY_TIME_TO':
                            field = BX('endTime');
                            break;
                        default:
                            field = BX(code);
                    }

                    if (code == 'LOCATION') {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (userProfileValues[code].hasOwnProperty('CITY') && userProfileValues[code].CITY.hasOwnProperty('CODE')) {
                                location = userProfileValues[code].CITY.CODE;
                            }

                            if (userProfileValues[code].hasOwnProperty('PROP_ID')) {
                                this.locationId = userProfileValues[code].PROP_ID;
                            }
                        }
                    }

                    if (!!field) {
                        if (userProfileValues.hasOwnProperty(code)) {
                            if (code == 'DELIVERY_TIME_FROM') {
                                deliveryTimeFrom = userProfileValues[code].VALUE;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                deliveryTimeTo = userProfileValues[code].VALUE;
                            }

                            newValue = userProfileValues[code].VALUE;
                        } else {
                            if (code == 'DELIVERY_TIME_FROM') {
                                newValue = this.defaultDeliveryTimeFrom;
                            }

                            if (code == 'DELIVERY_TIME_TO') {
                                newValue = this.defaultDeliveryTimeTo;
                            }
                        }

                        field.value = newValue;
                    }
                }
            }

            window.BX.locationSelectors[this.citySelectorId].setValue(location);

            this.refreshDeliveryTimer(this.getDeliveryTimeForSlider(deliveryTimeFrom), this.getDeliveryTimeForSlider(deliveryTimeTo));

            this.sendRequest();
        },

        getDeliveryTimeForSlider: function (time) {
            if (isNaN(Number(time))) {
                var timeSourceJS = time.toString().split(':'),
                    addTimeJS = 0;

                if (timeSourceJS[1] == 30) {
                    addTimeJS++;
                }

                return timeSourceJS[0] * 2 + addTimeJS;
            } else {
                return time;
            }
        },

        refreshDeliveryTimer: function (startTime, endTime) {
            $('#reg__timer').slider("values", [
                startTime,
                endTime
            ]);
			
        }, 

        showDeliveryInfo: function (e) {
            //BX.removeClass(this.deliveryInfoBlock, 'hidden');
        },

        hideDeliveryInfo: function (e) {
            //BX.addClass(this.deliveryInfoBlock, 'hidden');
        },

        deliveryTimer: function (deliveryID) {
            if (deliveryID == 2) {
                this.showDeliveryInfo();
            }
            else {
                this.hideDeliveryInfo();
            }
        },
	
	changePersonType: function (e) {
		this.sendRequest();
        },
	
	changeLocation: function (e) {
		this.sendRequest();
        },

        changeDelivery: function (e) {
            this.deliveryTimer(e.target.value);
            if (!!this.obDeliveryCourierWrapper) {
                if (e.target.value == this.deliveryCourierId && this.isWorkDay) {
                    BX.show(this.obDeliveryCourierWrapper);
                } else {
                    BX.hide(this.obDeliveryCourierWrapper);
                }
            }

            this.sendRequest();
        },

        /**
         * Send ajax request with order data and executes callback by action
         */
        sendRequest: function (action, actionData) {
            // Validate fields
            $(this.obForm).validate({
                rules: this.rules,
                messages: this.rulesMessages
            });

            var isValid = $(this.obForm).valid();

            var loaderTimer, form;
            if (!(loaderTimer = this.startLoader())) {
                return;
            }

            this.firstLoad = false;
            action = BX.type.isString(action) ? action : 'refreshOrderAjax';

            if (action == 'saveOrderAjax') {
                if (!this.publicOffer.checked) {
                    this.showError(this.mainErrorsNode, BX.message('PUBLIC_OFFER_REQUIERED'));
                    this.endLoader(loaderTimer);

                    return;
                }
                else if (!isValid) {
                    this.showError(this.mainErrorsNode, BX.message('REQ_FIELDS'));
                    this.endLoader(loaderTimer);

                    return;
                }

                form = BX('bx-soa-order-form');
                if (form) {
                    form.querySelector('input[type=hidden][name=sessid]').value = BX.bitrix_sessid();
                }

                BX.ajax.submit(BX('bx-soa-order-form'), BX.proxy(this.saveOrder, this));
            } else {
                BX.ajax({
                    timeout: 60,
                    method: 'POST',
                    dataType: 'json',
                    url: this.ajaxUrl,
                    data: this.getData(action, actionData),
                    onsuccess: BX.proxy(function (result) {
                        if (result.redirect && result.redirect.length) {
                            document.location.href = result.redirect;
                        }

                        switch (action) {
                            case 'refreshOrderAjax':
                                this.refreshOrder(result);
                                break;
                        }

                        this.endLoader(loaderTimer);
                    }, this),
                    onfailure: BX.proxy(function () {
                        this.endLoader(loaderTimer);
                    }, this)
                });
            }
        },

        refreshOrder: function (result) {
            if (result.error) {
                this.showError(this.mainErrorsNode, result.error);
            } else if (result.order.SHOW_AUTH) {
                // TODO Show auth form
            } else {
                this.result = result.order;

                // refresh order price
                if (!!this.obOrderPrice) {
                    BX.adjust(this.obOrderPrice, {
                        html: BX.util.number_format(result.order.TOTAL.ORDER_TOTAL_PRICE, 0, '.', ' ') + ' <span class="icon-rouble">i</span>'
                        //html: BX.Currency.currencyFormat(result.order.TOTAL.ORDER_TOTAL_PRICE, result.order.CURRENCY, true)
                    });
                }
                // refresh delivery price
                var price = BX.message('SALE_FREE_DELIVERY');

                if (result.order.TOTAL.DELIVERY_PRICE > 0) {
                    price = BX.util.number_format(result.order.TOTAL.DELIVERY_PRICE, 0, '.', ' ') + ' <span class="icon-rouble">i</span>';
                    //price = BX.Currency.currencyFormat(result.order.TOTAL.DELIVERY_PRICE, result.order.CURRENCY, true);
                }

                var paysystems = this.result.PAY_SYSTEM;
		

                if (!!paysystems) {
                    for (var i in paysystems) {
                        if (paysystems[i].CHECKED === 'Y') {
                            this.changeOrderBtnName(paysystems[i].ID);
                        }
                    }
                }
				
				var persons = this.result.PERSON_TYPE;
		
                if (!!persons) {
                    for (var i in persons) {
                        if (persons[i].CHECKED === 'Y') {
                            this.changeOrderProps(persons[i].ID);
                        }
                    }
                }
				

		
		
		

		if ( result.order.TOTAL.DELIVERY_ID == '2' || result.order.TOTAL.DELIVERY_ID == '20' || result.order.TOTAL.DELIVERY_ID == '23'){
			$('#block__timer').show();
		}
		else {
			$('#block__timer').hide();
		}
		
		if ( result.order.TOTAL.DELIVERY_ID == '3' ){
			$('#reg__address').hide();
		}
		else {
			$('#reg__address').show();
		}
		
		

                if (!!this.obDeliveryPrice) {
                    BX.adjust(this.obDeliveryPrice, {
                        html: price
                    });
                }

		/*
                // Update need props
                if (result.order.hasOwnProperty('ORDER_PROP') && result.order.ORDER_PROP.hasOwnProperty('properties')) {
                    for (var i in this.props) {
                        BX.addClass(BX('order__block-' + this.props[i].ID), 'hidden');
                    }
                    for (var i in result.order.ORDER_PROP.properties) {
                        if (this.props[result.order.ORDER_PROP.properties[i].ID]) {
                            if (this.props[result.order.ORDER_PROP.properties[i].ID].ADDR_PROP) {
                                BX.removeClass(BX('order__block-' + this.props[result.order.ORDER_PROP.properties[i].ID].ID), 'hidden');
                            }
                            else {
                                BX.addClass(BX('order__block-' + this.props[result.order.ORDER_PROP.properties[i].ID].ID), 'hidden');
                            }
                        }

                        if (result.order.ORDER_PROP.properties.hasOwnProperty(i)) {
                            var prop = result.order.ORDER_PROP.properties[i];
                            if (BX.util.in_array(prop.CODE, this.refreshUserProfileFields)) {
                                if (prop.hasOwnProperty('VALUE')) {
                                    var field = BX(prop.CODE);
                                    if (!!field) {
                                        field.value = prop.VALUE[0];
                                    }
                                }
                            }
                        }
                    }
                }
		*/
		

                for (var i in result.order.DELIVERY) {
                    if (result.order.DELIVERY[i].CHECKED == 'Y') {
                        if (result.order.DELIVERY[i].ID == 2) {
                            this.showDeliveryInfo();
                        }
                        else {
                            this.hideDeliveryInfo();
                        }
                    }
                }

                // Set profile change N
                if (!!this.obProfileChange) {
                    this.obProfileChange.value = 'N';
                }

		
		// update person type
                this.renderPersonType();
		
                // update pay systems
                this.renderPaySystems();

                // update delivery
                this.renderDelivery();

                // update profiles
                this.renderProfiles();
            }
        },

        getData: function (action, actionData) {
            var data = {
                order: this.getAllFormData(),
                sessid: BX.bitrix_sessid(),
                via_ajax: 'Y',
                SITE_ID: this.siteId,
                action: action,
                signedParamsString: this.signedParamsString
            };

            if (action === 'enterCoupon' || action === 'removeCoupon') {
                data.coupon = actionData;
            }

            return data;
        },

        getAllFormData: function () {
            var prepared = BX.ajax.prepareForm(this.obForm),
                i;

            for (i in prepared.data) {
                if (prepared.data.hasOwnProperty(i) && i == '') {
                    delete prepared.data[i];
                }
            }

            return !!prepared && prepared.data ? prepared.data : {};
        },

        renderPaySystems: function () {
            BX.cleanNode(this.obPaySystemWrapper);

            for (var i in this.result.PAY_SYSTEM) {
                if (this.result.PAY_SYSTEM.hasOwnProperty(i)) {
                    var paySystem = this.result.PAY_SYSTEM[i];
                    this.obPaySystemWrapper.appendChild(BX.create('DIV', {
                        attrs: {
                            className: 'order__radio'
                        },
                        children: [
                            BX.create('INPUT', {
                                attrs: {
                                    type: 'radio',
                                    name: 'PAY_SYSTEM_ID',
                                    value: paySystem['ID'],
                                    id: 'PAY_SYSTEM_' + paySystem['ID'],
                                    checked: paySystem["CHECKED"] == 'Y',
                                    className: 'radio'
                                },
                                events: {
                                    change: BX.proxy(this.sendRequest, this)
                                }
                            }),
                            BX.create('LABEL', {
                                attrs: {
                                    for: 'PAY_SYSTEM_' + paySystem['ID']
                                },
                                text: paySystem['NAME']
                            })
                        ]
                    }));
                }
            }
        },

        renderProfiles: function () {
            //console.info('renderProfiles');

        },

        renderDelivery: function () {
            BX.cleanNode(this.obDeliveryWrapper);

            var options = [];

            for (var i in this.result.DELIVERY) {
                if (this.result.DELIVERY.hasOwnProperty(i)) {
                    var delivery = this.result.DELIVERY[i];

                    options.push(BX.create('OPTION', {
                        text: delivery['NAME'],
                        attrs: {
                            value: delivery['ID'],
                            selected: delivery['CHECKED'] == 'Y'
                        }
                    }));
                }
            }

            this.obDelivery = BX.create('SELECT', {
                attrs: {
                    className: 'select',
                    id: 'DELIVERY_ID',
                    name: "DELIVERY_ID"
                },
                children: options,
                events: {
                    change: BX.proxy(this.changeDelivery, this)
                }
            });
	    
	     
	    

            this.obDeliveryWrapper.appendChild(this.obDelivery);

            $(this.obDelivery).select2();
            $(this.obDelivery).on('select2:select', BX.proxy(this.changeDelivery, this));
	    
	    
	    
        },
	
	renderPersonType: function () {
            BX.cleanNode(this.obPersonTypeWrapper);

            var options = [];

            for (var i in this.result.PERSON_TYPE) {
                if (this.result.PERSON_TYPE.hasOwnProperty(i)) {
                    var PERSON_TYPE = this.result.PERSON_TYPE[i];


                    options.push(BX.create('OPTION', {
                        text: PERSON_TYPE['NAME'],
                        attrs: {
                            value: PERSON_TYPE['ID'],
                            selected: PERSON_TYPE['CHECKED'] == 'Y'
                        }
                    }));
                }
            }

            this.obPersonType = BX.create('SELECT', {
                attrs: {
                    className: 'select',
                    id: 'PERSON_TYPE',
                    name: "PERSON_TYPE"
                },
                children: options,
                events: {
                    change: BX.proxy(this.changePersonType, this)
                }
            });
	    
	     
	    

            this.obPersonTypeWrapper.appendChild(this.obPersonType);

            $(this.obPersonType).select2();
            $(this.obPersonType).on('select2:select', BX.proxy(this.changePersonType, this));
	    
	    
	    
        },
	


        changeOrderProps: function (personTypeID) {
            
			$('[person-data]').hide();
			$('[person-data=' + personTypeID + ']').show();
			
        },
		
		changeOrderBtnName: function (paysystemID) {
            var text = '';
            if (paysystemID == 1) {
                text = BX.message('ORDER_CASH_PAYMENT');
            }
            else {
                text = BX.message('ORDER_ONLINE_PAYMENT');
            }
            BX.adjust(this.obButton, {
                html: text
            });
        }
		
		
    }
})(window);