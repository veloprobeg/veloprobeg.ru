<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @var $component SaleOrderAjax
 */
?>
    <NOSCRIPT>
        <div style="color:red"><?= Loc::getMessage("SOA_NO_JS") ?></div>
    </NOSCRIPT>
<?
$context = Bitrix\Main\Application::getInstance()->getContext();
if (strlen($context->getRequest()->get('ORDER_ID')) > 0):
    include($context->getServer()->getDocumentRoot() . $templateFolder . "/confirm.php");
elseif ($arParams["DISABLE_BASKET_REDIRECT"] == 'Y' && $arResult["SHOW_EMPTY_BASKET"]):
    include($context->getServer()->getDocumentRoot() . $templateFolder . "/empty.php");
else:
    $messages = Loc::loadLanguageFile(__FILE__);
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');

    ?>
    

    

    <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="bx-soa-order-form"
          enctype="multipart/form-data" class="order">
        <?= bitrix_sessid_post() ?>
        <?
        if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0) {
            echo $arResult["PREPAY_ADIT_FIELDS"];
        }
        ?>
        <input type="hidden" name="action" value="saveOrderAjax">
        <input type="hidden" name="location_type" value="code">
        <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>">
        <input type="hidden" name="profile_change" value="N" id="profile_change">
        <div class="order__header">
            <div class="order__comp">
                <div class="order__header-name"><?= Loc::getMessage("SALE_THE_ORDER"); ?></div>
                <? foreach ($arResult['BASKET_ITEMS'] as $arBasketItem): ?>
                    <div class="order__comp-item">
                        <div class="order__comp-name"><?= $arBasketItem['NAME'] ?><span
                                    class="order__comp-count"><?= Loc::getMessage("SALE_QUANTITY", array(
                                    '#QUANTITY#' => $arBasketItem['QUANTITY'],
                                    '#MEASURE#' => $arBasketItem['MEASURE_TEXT']
                                )); ?></span></div>
                    </div>
                <? endforeach ?>
            </div>
            <div class="order__price">
                <div class="order__header-name"><?= Loc::getMessage("SALE_SUM_TO_PAY"); ?></div>
                <div class="order__header-price" id="order-price"><?= $arResult['ORDER_TOTAL_PRICE_FORMATED']; ?></div>
            </div>
            <? //-------LOGICTIM BONUS FEILD-------//
            if(isset($arResult["PAY_BONUS"]) && $arResult["PAY_BONUS"] >= 0) {
            ?>
           <!-- <div class="order__price">
                <div class="order__header-name">Оплата баллами</div>
                <div class="order__header-price" id="order-price-bonus"><?/*= $arResult['PAY_BONUS']; */?> <span class="icon-rouble">i</span></div>
            </div>-->

                        <!--<input type="hidden" maxlength="250" size="0" value="<?/*=$arResult["PAY_BONUS"]*/?>" name="ORDER_PROP_<?/*=$arResult["ORDER_PROP_PAYMENT_BONUS_ID"]*/?>" id="LOGICTIM_PAYMENT_BONUS">-->

            <? }
            //-------LOGICTIM BONUS FEILD-------//?>
        </div>
	
	
	
        <div class="order__content">
	
		<div class="order__section">
			<div class="order__head"><?= Loc::getMessage("SALE_PERSONAL_TYPE"); ?></div>
			<div class="order__row">
				<div class="order__block">
					<div class="order__select" id="person-wrapper">
						<select class="select" id="PERSON_TYPE" name="PERSON_TYPE">
						    <? foreach ($arResult['PERSON_TYPE'] as $person): ?>
							<?
							if ($person['CHECKED']) {
							    $selectedPerson = $person;
							}
							?>
							<option
								value="<?= $person['ID'] ?>"<?= $person['CHECKED'] ? ' checked' : '' ?>><?= $person['NAME'] ?></option>
						    <? endforeach ?>
						</select>
					</div>
				</div>	
			</div>
		</div>
	
		<div class="order__head" person-data="1"><?= Loc::getMessage("SALE_PERSONAL_DATA"); ?></div>
	    <div class="order__head" person-data="2"><?= Loc::getMessage("SALE_COMPANY_DATA"); ?></div>
	
	
		<?foreach ( $arResult['VIEW_DATA'] as $personType => $arData):?>
            
			<div class="order__section" person-data="<?=$personType?>">
	    
    
                
                <div class="order__row">
                    <? foreach ($arData['PERSONAL_DATA_FIRST_ROW'] as $prop): ?>
                        <div class="order__block" id="order__block-<?= $prop['ID'];?>">
                            <div class="order__name"><?= $prop['NAME'] ?></div>
                            <input type="text" name="ORDER_PROP_<?= $prop['ID'] ?>"
                                   class="default-input default-input--normal"
                                   id="<?= $prop['CODE'] ?>"
                                   value="<?= $prop['VALUE'][0] ?>">
                        </div>
                    <? endforeach ?>
                </div>
                <div class="order__row">
					<?$count = 0;?>
                    <? foreach ($arData['PERSONAL_DATA_SECOND_ROW'] as $prop): ?>
						<?$count++;?>
						<?if ( $count > 3 ):?>
							</div>
							<div class="order__row">
							<?$count = 1;?>
						<?endif;?>
                        <div class="order__block">
                            <div class="order__name"><?= $prop['NAME'] ?></div>
                            <input type="text" name="ORDER_PROP_<?= $prop['ID'] ?>" id="<?= $prop['CODE'] ?>"
                                   class="default-input default-input--normal<?= $prop['CODE'] == 'PHONE' ? ' mask-phone' : '' ?>"
                                   value="<?= $prop['VALUE'][0] ?>">
                        </div>
                    <? endforeach ?>
                </div>
            </div>
			
		<?endforeach;?>	
			
            <div class="order__section">
                <div class="order__head"><?= Loc::getMessage("DELIVERY_BLOCK_NAME_DEFAULT"); ?></div>
                <div class="order__row order__row--center order__row--mb">
                    <? $selectedDelivery = false ?>
                    <div class="order__select" id="delivery-wrapper">
                        <select class="select" id="DELIVERY_ID" name="DELIVERY_ID">
                            <? foreach ($arResult['DELIVERY'] as $delivery): ?>
                                <?
                                if ($delivery['CHECKED']) {
                                    $selectedDelivery = $delivery;
                                }
                                ?>
                                <option
                                        value="<?= $delivery['ID'] ?>"<?= $delivery['CHECKED'] ? ' checked' : '' ?>><?= $delivery['NAME'] ?></option>
                            <? endforeach ?>
                        </select>
                    </div>
                    <div class="order__free"
                         id="delivery-price"><?= $selectedDelivery['PRICE'] <= 0 ? Loc::getMessage("SALE_FREE_DELIVERY") : $selectedDelivery['PRICE_FORMATED']; ?></div>
                    <a href="/payment-and-shipping/"
                       class="order__about"><?= Loc::getMessage("SALE_MORE_ABOUT_DELIVERY"); ?></a>
                </div>
                <div class="order__row order__row--mb<?= $selectedDelivery['ID'] != DELIVERY_COURIER_ID ? ' hidden' : '' ?>"
                     id="delivery-courier-wrapper">
                    <div class="order__waited"><?= Loc::getMessage("SALE_EXPECTED_TIME_DELIVERY", array(
                            '#DAY#' => $arResult['IS_TOMORROW_WORK_DAY'] ? Loc::getMessage("SALE_EXPECTED_TIME_DELIVERY_TOMORROW") : '',
                            '#DATE#' => $arResult['NEXT_WORK_DAY']
                        )); ?></div>
                </div>
                <div id="reg__address">
                    <div class="order__section order__section--address" id="profile-wrapper">
                        <? foreach ($arResult['ORDER_PROP']['USER_PROFILES'] as $profile): ?>
                            <div class="order__radio order__radio--old">
                                <input type="radio" name="PROFILE_ID" value="<?= $profile['ID'] ?>"
                                       id="address_<?= $profile['ID'] ?>"
                                       class="radio radio-js"<?= $profile['CHECKED'] == 'Y' ? ' checked' : '' ?>>
                                <label for="address_<?= $profile['ID'] ?>"><?= str_replace('кв', 'дом', $profile['NAME']) ?></label>
				<a href="javascript:void(0)" data-id="<?= $profile['ID'] ?>" class="icon__closer del_profile"></a>
                            </div>
                        <? endforeach ?>
                        <div class="order__radio order__radio--new">
                            <input type="radio" name="PROFILE_ID" value="0" id="address_new"
                                   class="radio radio-js"<?= empty($arResult['ORDER_PROP']['USER_PROFILES']) ? ' checked' : '' ?>>
                            <label for="address_new"><?= Loc::getMessage("SALE_NEW_ADDRESS"); ?></label>
                        </div>
                    </div>
                    
					
							<?foreach ( $arResult['VIEW_DATA'] as $personType => $arData):?>
            
								<div class="order__row" person-data="<?=$personType?>">
					
									<? foreach ($arResult['VIEW_DATA'][$personType]['DELIVERY_DATA_FIRST_ROW'] as $prop): ?>
										<? $isBig = in_array($prop['CODE'], array(
											'LOCATION',
											'STREET',
											'LOCATION_UR',
											'STREET_UR'
										)); ?>
										<div class="order__block<?= $isBig ? '' : ' order__block--smallest' ?>" id="order__block-<?= $prop['ID'];?>">
											<div class="order__name"><?= $prop['NAME'] ?></div>
											<?
											if ($prop['CODE'] == 'LOCATION' || $prop['CODE'] == 'LOCATION_UR') {
												$arResult['LOCATION_BLOCK'] = 'order__block-' . $prop['ID'];
												//$currentCityCookie = empty($arResult['ORDER_PROP']['USER_PROFILES']) ? $APPLICATION->get_cookie('CURRENT_CITY') : $prop['VALUE'][0];
											global $CITY_CODE;
											$currentCityCookie = $CITY_CODE;
								
										$APPLICATION->IncludeComponent(
													"bitrix:sale.location.selector.search",
													"choice_location",
													array(
														"COMPONENT_TEMPLATE" => "choice_location",
														"ID" => "",
														"CODE" => $currentCityCookie,
														"INPUT_NAME" => 'ORDER_PROP_' . $prop['ID'],
														"PROVIDE_LINK_BY" => "code",
														"FILTER_BY_SITE" => "N",
														"SHOW_DEFAULT_LOCATIONS" => "Y",
														"CACHE_TYPE" => "A",
														"CACHE_TIME" => "36000000",
														"JS_CALLBACK" => "",
														"SUPPRESS_ERRORS" => "N",
														"INITIALIZE_BY_GLOBAL_EVENT" => "",
														"FILTER_SITE_ID" => "current",
														'JS_CONTROL_GLOBAL_ID' => 'ORDER_CITY_SEARCH'
													),
													false
												);
											} else {
												?>
												<input type="text" name="ORDER_PROP_<?= $prop['ID'] ?>"
													   placeholder="<?= $prop['DEFAULT_VALUE'] ?>"
													   id="<?= $prop['CODE'] ?>"
													   class="<?= $isBig ? 'default-input default-input--normal' : 'default-input default-input--smallest' ?>"
													   value="<?= $prop['VALUE'][0] ?>">
											<? } ?>
										</div>
									<? endforeach ?>
									
									
								</div>
							<? endforeach ?>
							
					<div class="order__row" id="block__timer" person-data="1">		
					
							<div class="order__block order__block--timer">
								<div class="reg__timer">
									<div class="reg__time"><span
												class="reg__desc"><?= Loc::getMessage("SALE_DELIVERY_TIME_FROM"); ?></span>
										<input id="startTime" class="default-input default-input--small"
											   name="ORDER_PROP_<?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_START']['ID'] ?>"
											   value="<?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_START']['VALUE'][0] ?>"><span
												class="reg__desc"><?= Loc::getMessage("SALE_DELIVERY_TIME_TO"); ?></span>
										<input id="endTime" class="default-input default-input--small"
											   name="ORDER_PROP_<?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_END']['ID'] ?>"
											   value="<?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_END']['VALUE'][0] ?>">
									</div>
									<div id="reg__timer"></div>
								</div>
							</div>

					</div>
					
                </div>
            </div>
            <div class="order__comment">
                <div class="order__name"><?= Loc::getMessage("ORDER_DESC_DEFAULT"); ?></div>
                <textarea name="ORDER_DESCRIPTION" class="add-comment__text-area"
                          id="orderDescription"><?= $arResult['USER_VALS']['ORDER_DESCRIPTION'] ?></textarea>
            </div>
            <div class="order__section">
                <div class="order__head"><?= Loc::getMessage("PAYMENT_BLOCK_NAME_DEFAULT"); ?></div>
                <div id="pay-system-wrapper">
                    <? foreach ($arResult['PAY_SYSTEM'] as $paySystem): ?>
                        <div class="order__radio">
                            <input type="radio" name="PAY_SYSTEM_ID" value="<?= $paySystem['ID'] ?>"
                                   id="PAY_SYSTEM_<?= $paySystem['ID'] ?>" <?= $paySystem['CHECKED'] == 'Y' ? 'checked' : '' ?>
                                   class="radio">
                            <label for="PAY_SYSTEM_<?= $paySystem['ID'] ?>"><?= $paySystem['NAME'] ?></label>
                        </div>
                    <? endforeach ?>
                </div>
            </div>
            <div id="popup-errors" class="popup">
                <div class="popup__inner">
                    <a href="javascript:void(0)" class="popup__closer"></a>
                    <div class="popup__content-wrapper">
                        <div id="main-errors">
                            <div class="alert alert-danger">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?if($arResult["PAY_BONUS"]>0){?>
            <div class="basket__doit">
                <div class="basket__doit-input">
                    <div class="basket__end-gray">вы можете списать до <?=$arResult["PAY_BONUS"]?> бонусов:</div>
                    <input type="number" max="<?=$arResult["PAY_BONUS"]?>" value="<?=$arResult["PAY_BONUS"]?>" class="default-input default-input--full" name="ORDER_PROP_<?=$arResult["ORDER_PROP_PAYMENT_BONUS_ID"]?>" id="LOGICTIM_PAYMENT_BONUS">
                </div><!--<a href="#" class="button button--yel-transp button--hover">Списать</a>-->
            </div>
            <?}?>
            <input type="checkbox" class="checkbox" name="public_offer" id="public_offer" requiered>
            <label for="public_offer">Ознакомлен и согласен с <a href="/public_offer/" class="link">публичной офертой</a></label>
            <div class="order__submit">
                <button type="submit" class="button button--orange"
                        id="submit-form"><?= $arResult['PAY_SYSTEM'][0]['ID'] == 1 ? Loc::getMessage("ORDER_CASH_PAYMENT") : Loc::getMessage("ORDER_ONLINE_PAYMENT"); ?></button>
            </div>
        </div>
    </form>
    <?
    $jsParams = array(
        'FORM_ID' => 'bx-soa-order-form',
        'BUTTON_ID' => 'submit-form',
        'AJAX_URL' => $APPLICATION->GetCurPage(),
        'ORDER_PRICE_ID' => 'order-price',
        'DELIVERY_ID' => 'DELIVERY_ID',
	'PERSON_TYPE' => 'PERSON_TYPE',
        'DELIVERY_PRICE_ID' => 'delivery-price',
        'USER_PROFILES' => $arResult['ORDER_PROP']['USER_PROFILES'],
        'REFRESH_USER_PROFILE_FIELDS' => array(
            'LOCATION',
            'STREET',
            'HOUSE',
            'HOUSING',
            'AP_NUMBER',
            'ZIP',
            'DELIVERY_TIME_FROM',
            'DELIVERY_TIME_TO'
        ),
        'NAME_PROFILE' => 'radio-js',
        'DEFAULT_DELIVERY_TIME_FROM' => DEFAULT_DELIVERY_TIME_FROM,
        'DEFAULT_DELIVERY_TIME_TO' => DEFAULT_DELIVERY_TIME_TO,
        'PROFILE_CHANGE_ID' => 'profile_change',
        'JS_CONTROL_GLOBAL_ID' => 'ORDER_CITY_SEARCH',
        'PROFILE_WRAPPER_ID' => 'profile-wrapper',
        'DELIVERY_COURIER_ID' => DELIVERY_COURIER_ID,
        'DELIVERY_COURIER_WRAPPER_ID' => 'delivery-courier-wrapper',
        'IS_WORK_DAY' => $arResult['IS_WORK_DAY'] === true,
        'RULES' => $arResult['RULES'],
        'RULES_MESSAGES' => $arResult['RULES_MESSAGES'],
        'RESULT' => $arResult,
        "PROPS" => $arResult['PROPS'],
        'PAY_SYSTEM_WRAPPER_ID' => 'pay-system-wrapper',
        'DELiVERY_WRAPPER_ID' => 'delivery-wrapper',
	'PERSON_TYPE_WRAPPER_ID' => 'person-wrapper',
        'signedParamsString' => CUtil::JSEscape($signedParams),
        'PROFILE_NAME' => 'PROFILE_ID',
        'MAIN_ERRORS_BLOCK' => 'main-errors',
        'POPUP_ERRORS_BLOCK' => 'popup-errors',
        "PUBLIC_OFFER" => 'public_offer',
        "DELIVERY_INFO" => 'reg__address',
    );
    ?>
	
    
    <script>
        var orderAjax;
        // Пробрасываем языковые фразы в JS
        BX.message(<?= CUtil::PhpToJsObject($messages) ?>);

        BX.ready(function () {
            var delivery = <?= json_encode($arResult['DELIVERY']) ?>;
            orderAjax = new CJSOrderAjax(<?= CUtil::PhpToJSObject($jsParams) ?>);

            // Init delivery time slider
			regTimer(<?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_START']['JS_VALUE'] ?>, <?= $arResult['VIEW_DATA'][1]['DELIVERY_TIME_END']['JS_VALUE'] ?>);
	  });
    </script>
<? endif ?>
<? \Debug::dtc($arResult, 'result'); ?>