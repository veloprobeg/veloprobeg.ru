<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);
if(CSite::InDir(SITE_DIR.'catalog/')){$isCatalog = true;}
if(CSite::InDir(SITE_DIR.'promotion/')){$isCatalog = true;}
if(CSite::InDir(SITE_DIR.'news/')){$isCatalog = true;}
if(CSite::InDir(SITE_DIR.'contacts/')){$isCatalog = true;}
if(CSite::InDir(SITE_DIR.'about/jobs/')){$isCatalog = true;}
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <? $APPLICATION->ShowHead() ?>
  <title><? $APPLICATION->ShowTitle() ?></title>
  <?
  CJSCore::Init(array('popup'));

  // Basket
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/select.min.css");

  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/owl.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/scroll.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/main.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/media.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/ui.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/jquery.bxslider.css");
  Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=PT+Sans:400,700");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery-3.1.1.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/select2.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery.mCustomScrollbar.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery.validate.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/owl.carousel.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery-ui.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery.bxslider.min.js");

  // Contacts map
  Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=AIzaSyBS68AJSWAKGU0NXLFdfmtGKIbeExpUTUI");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/infobox.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/map.js");

  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/main.js");

  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/css/lib/dropzone.css");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/dropzone.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery.maskedinput.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/js/lib/jquery.elevateZoom-3.0.8.min.js");
 ?>
</head>
<body>
<?
$curDir = $APPLICATION->GetCurDir();
?>
<div id="panel"><? $APPLICATION->ShowPanel() ?></div>
<div class="page <?= $_SESSION['SESS_INCLUDE_AREAS'] == 1 ? 'edit_mode' : '' ?>">
  <div class="header<?= $USER->IsAuthorized() ? ' header--logged' : '' ?>">
    <div class="header__top">
      <div class="wrapper wrapper--header wrapper--end">
        <? if (PAGE_TYPE != 'main'): ?><a href="/"<? else: ?><span<? endif ?> class="header__logo"><? $APPLICATION->IncludeComponent("bitrix:main.include", "logo_top", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/logo_top.php",	// Путь к файлу области
	),
	false
); ?><? if (PAGE_TYPE != 'main'): ?></a>
        <? else: ?></span><? endif ?>
        <div class="header__nav<?= $USER->IsAuthorized() ? ' header__nav--logged' : '' ?>" id="header__nav">
			<?$APPLICATION->IncludeComponent(
                "cakelabs:location.selector.city", 
                ".default", 
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000"
                ),
                false
);?>
          <div class="header__logo-mobile"></div>
          <? $APPLICATION->IncludeComponent(
                "bitrix:menu", 
                "top", 
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "3",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "N",
                    "COMPONENT_TEMPLATE" => "top"
                ),
                false
            ); ?>
          <div class="header__service">
            <a href="javascript:void(0)" class="header__service-link">Сервис - центр</a>
          </div>
          <div class="header__phones<?= $USER->IsAuthorized() ? ' header__phones--logged' : '' ?>">
           
			<?global $REGION_ID;?>
			<?if ( $REGION_ID == 3 ):?>
			   <? $APPLICATION->IncludeComponent(
					"bitrix:main.include", 
					"main_phone", 
					array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_TEMPLATE_PATH."/include_areas/phone.php",
						"COMPONENT_TEMPLATE" => "main_phone"
					),
					false
				); ?>
				<span class="header__phone-city">
					<?= Loc::getMessage("PHONE_HEADER", array(
						'#IN_CITY#' => 'в Москве'
					));?>
				</span>
			<?else:?>
				 <? $APPLICATION->IncludeComponent(
					"bitrix:main.include", 
					"main_phone", 
					array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => SITE_TEMPLATE_PATH."/include_areas/phone_rus.php",
						"COMPONENT_TEMPLATE" => "main_phone"
					),
					false
				); ?>
				<span class="header__phone-city">
					<?= Loc::getMessage("PHONE_HEADER", array(
						'#IN_CITY#' => 'по России'
					));?>
				</span>
			<?endif;?>
			
          </div>
          <div class="header__socio">
            <a href="javascript:void(0)" class="header__socio-link header__socio-link--vk"></a>
            <a href="javascript:void(0)" class="header__socio-link header__socio-link--fb"></a>
            <a href="javascript:void(0)" class="header__socio-link header__socio-link--yt"></a>
          </div>
          <? $APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"main_auth", 
	array(
		"COMPONENT_TEMPLATE" => "main_auth",
		"REGISTER_URL" => "/register/",
		"FORGOT_PASSWORD_URL" => "/forgot-password/",
		"PROFILE_URL" => "/personal/profile/",
		"SHOW_ERRORS" => "Y"
	),
	false
);?><? $APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => IBLOCK_CATALOG_ID,
		"POSITION_FIXED" => "N",
		"POSITION" => "top left",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
		"COMPARE_URL" => "/catalog/compare/",
		"NAME" => "CATALOG_COMPARE_LIST",
		"ACTION_VARIABLE" => "action_ccl",
		"PRODUCT_ID_VARIABLE" => "id"
	),
	false
); ?>
</div>
        <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"small_basket", 
	array(
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "N",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"PATH_TO_REGISTER" => SITE_DIR."register/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"COMPONENT_TEMPLATE" => "small_basket",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"SHOW_EMPTY_VALUES" => "Y",
		"HIDE_ON_BASKET_PAGES" => "Y",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N",
		"SHOW_IMAGE" => "Y",
		"SHOW_PRICE" => "Y",
		"SHOW_SUMMARY" => "Y",
		"POSITION_HORIZONTAL" => "right",
		"POSITION_VERTICAL" => "top"
	),
	false
);?>
        <a href="javascript:void(0)" class="header__menu-opener">
          <div class="header__menu-opener-inner">
            <span class="header__menu-opener-pin"></span>
            <span class="header__menu-opener-pin"></span>
            <span class="header__menu-opener-pin"></span>
          </div>
        </a>
      </div>
    </div>
    <div class="header__bottom">
      <div class="wrapper wrapper--header wrapper--start">
        <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "main",
		"DELAY" => "N",
		"MAX_LEVEL" => "4",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "main",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "main",
		"MENU_THEME" => "site"
	),
	false
); ?>
        <?$APPLICATION->IncludeComponent(
          "bitrix:search.form",
          "search",
          Array(
            "PAGE" => "#SITE_DIR#search/index.php",
            "USE_SUGGEST" => "Y"
          )
        );?>
      </div>
    </div>
  </div>
  <div class="content">
    <? if (PAGE_TYPE == 'main'): ?>
    <!-- SLIDER -->
      <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_slider", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "main_slider"
	),
	false
);?>
<div class="wrapper wrapper--flex wrapper--between wrapper--flex-wrap">
<?

$GLOBALS['arMainChoiceWeekFilter'] = IBlockFilters::getMainFilterChooseWeek();
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"choice_week", 
	array(
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => "79",
		"BASKET_URL" => "/personal/cart/",
		"COMPONENT_TEMPLATE" => "choice_week",
		"IBLOCK_TYPE" => "catalog_view",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arMainChoiceWeekFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "3",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "COLOR_REF",
			2 => "SIZES_SHOES",
			3 => "SIZES_CLOTHES",
			4 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "50",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"OFFERS_CART_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"BACKGROUND_IMAGE" => "-",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"IMAGE_WIDTH" => "609",
		"IMAGE_HEIGHT" => "409",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"DISPLAY_COMPARE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CUSTOM_FILTER" => ""
	),
	false
);?>
          <!--700 × 550-->
    <?$APPLICATION->IncludeComponent(
        "mwi:bike.choose",
        "",
        Array(
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "IBLOCK_ID" => "86",
            "IBLOCK_TYPE" => "bike_choose"
        )
    );?>

        </div>
    <? endif ?>
    <div class="wrapper">
      <? if (PAGE_TYPE != 'main'): ?>
      <div class="bread">
        <div class="bread__start">
          <? if (!empty($_GET['back_url'])): ?>
            <a href="<?= $_GET['back_url'] ?>" class="bread__back"><?= Loc::getMessage("MAIN_BACK"); ?></a>
          <? endif ?>
          <h1 class="bread__head"><? $APPLICATION->ShowTitle(false) ?></h1>
        </div>
      <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumbs",
            array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
                "COMPONENT_TEMPLATE" => "breadcrumbs"
            ),
            false
        );?>
      </div>
	  
	  	<?if ( $isCatalog ):?>	
			<div id="comp_velo_catalog">

			<?
			$request = \Bitrix\Main\Context::getCurrent()->getRequest();
			if ($request->isAjaxRequest())
				$APPLICATION->RestartBuffer();
			?>
		<?else:?>
			<div class="contacts">
		<?endif;?>
	  
	  
    <? endif ?>