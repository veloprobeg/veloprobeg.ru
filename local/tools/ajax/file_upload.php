<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$result = array(
    'MESSAGE' => '',
    'STATUS' => 'OK'
);

try {
    $arFile = array_pop($_FILES);
    $fileId = \CFile::SaveFile($arFile);

    if ($fileId <= 0) {
        throw new \Exception('Не удалось сохранить файл');
    }

    $result['FILE_ID'] = $fileId;
} catch (\Exception $ex) {
    $result['STATUS'] = 'ERROR';
    $result['MESSAGE'] = $ex->getMessage();
}

echo json_encode($result);