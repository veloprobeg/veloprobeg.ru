<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$elementId = isset($_REQUEST['ELEMENT_ID']) ? intval($_REQUEST['ELEMENT_ID']) : 0;
$offerId = isset($_REQUEST['OFFER_ID']) ? intval($_REQUEST['OFFER_ID']) : 0;
$sectionId = isset($_REQUEST['SECTION_ID']) ? intval($_REQUEST['SECTION_ID']) : 0;
$isAjax = isset($_REQUEST['AJAX']) ? ($_REQUEST['AJAX'] == 'Y') : false;


//$file = $_SERVER["DOCUMENT_ROOT"].'/click.txt';
//$string = "<?php\n return ".var_export($_REQUEST, true).';';
//file_put_contents($file, $string);

?>


<? if (intval($_REQUEST['ELEMENT_ID']) > 0 && !$isAjax) { ?>
  <? $APPLICATION->IncludeComponent(
    "bitrix:catalog.element",
    "one_click",
    array(
      "IBLOCK_TYPE" => "catalog",
      "IBLOCK_ID" => IBLOCK_CATALOG_ID,
      "COMPONENT_TEMPLATE" => "one_click",
      "ELEMENT_ID" => $elementId,
      "ELEMENT_CODE" => "",
      "SECTION_ID" => $sectionId,
      "SECTION_CODE" => "",
      "HIDE_NOT_AVAILABLE" => "N",
      "PROPERTY_CODE" => array(
        0 => "",
        1 => "",
      ),
      "OFFERS_FIELD_CODE" => array(
        0 => "",
        1 => "",
      ),
      "OFFERS_PROPERTY_CODE" => array(
        0 => "ARTNUMBER",
        1 => "COLOR_REF",
        2 => "SIZES_SHOES",
        3 => "SIZES_CLOTHES",
        4 => "MORE_PHOTO",
        5 => "",
      ),
      "OFFERS_SORT_FIELD" => "sort",
      "OFFERS_SORT_ORDER" => "asc",
      "OFFERS_SORT_FIELD2" => "id",
      "OFFERS_SORT_ORDER2" => "desc",
      "OFFERS_LIMIT" => "0",
      "BACKGROUND_IMAGE" => "-",
      "TEMPLATE_THEME" => "blue",
      "ADD_PICT_PROP" => "-",
      "LABEL_PROP" => "NEWPRODUCT",
      "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
      "OFFER_TREE_PROPS" => array(
        0 => "COLOR_REF",
        1 => "SIZES_SHOES",
        2 => "SIZES_CLOTHES",
      ),
      "DISPLAY_NAME" => "Y",
      "DETAIL_PICTURE_MODE" => "IMG",
      "ADD_DETAIL_TO_SLIDER" => "N",
      "DISPLAY_PREVIEW_TEXT_MODE" => "E",
      "PRODUCT_SUBSCRIPTION" => "N",
      "SHOW_DISCOUNT_PERCENT" => "Y",
      "SHOW_OLD_PRICE" => "Y",
      "SHOW_MAX_QUANTITY" => "N",
      "SHOW_CLOSE_POPUP" => "N",
      "MESS_BTN_BUY" => "Купить",
      "MESS_BTN_ADD_TO_BASKET" => "В корзину",
      "MESS_BTN_SUBSCRIBE" => "Подписаться",
      "MESS_BTN_COMPARE" => "Сравнить",
      "MESS_NOT_AVAILABLE" => "Нет в наличии",
      "USE_VOTE_RATING" => "N",
      "USE_COMMENTS" => "N",
      "BRAND_USE" => "N",
      "SECTION_URL" => "",
      "DETAIL_URL" => "",
      "SECTION_ID_VARIABLE" => "SECTION_ID",
      "CHECK_SECTION_ID_VARIABLE" => "N",
      "SEF_MODE" => "N",
      "CACHE_TYPE" => "N",
      "CACHE_TIME" => "36000000",
      "CACHE_GROUPS" => "Y",
      "SET_TITLE" => "Y",
      "SET_CANONICAL_URL" => "N",
      "SET_BROWSER_TITLE" => "Y",
      "BROWSER_TITLE" => "-",
      "SET_META_KEYWORDS" => "Y",
      "META_KEYWORDS" => "-",
      "SET_META_DESCRIPTION" => "Y",
      "META_DESCRIPTION" => "-",
      "SET_LAST_MODIFIED" => "N",
      "USE_MAIN_ELEMENT_SECTION" => "N",
      "ADD_SECTIONS_CHAIN" => "Y",
      "ADD_ELEMENT_CHAIN" => "N",
      "ACTION_VARIABLE" => "action",
      "PRODUCT_ID_VARIABLE" => "id",
      "DISPLAY_COMPARE" => "N",
      "PRICE_CODE" => array(
        0 => "BASE",
      ),
      "USE_PRICE_COUNT" => "N",
      "SHOW_PRICE_COUNT" => "1",
      "PRICE_VAT_INCLUDE" => "Y",
      "PRICE_VAT_SHOW_VALUE" => "N",
      "CONVERT_CURRENCY" => "N",
      "BASKET_URL" => "/personal/basket.php",
      "USE_PRODUCT_QUANTITY" => "N",
      "PRODUCT_QUANTITY_VARIABLE" => "",
      "ADD_PROPERTIES_TO_BASKET" => "Y",
      "PRODUCT_PROPS_VARIABLE" => "prop",
      "PARTIAL_PRODUCT_PROPERTIES" => "N",
      "PRODUCT_PROPERTIES" => array(),
      "OFFERS_CART_PROPERTIES" => array(
        0 => "ARTNUMBER",
        1 => "COLOR_REF",
        2 => "SIZES_SHOES",
        3 => "SIZES_CLOTHES",
      ),
      "ADD_TO_BASKET_ACTION" => array(
        0 => "BUY",
      ),
      "LINK_IBLOCK_TYPE" => "",
      "LINK_IBLOCK_ID" => "",
      "LINK_PROPERTY_SID" => "",
      "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
      "USE_GIFTS_DETAIL" => "N",
      "USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
      "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
      "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
      "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
      "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
      "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
      "GIFTS_SHOW_OLD_PRICE" => "Y",
      "GIFTS_SHOW_NAME" => "Y",
      "GIFTS_SHOW_IMAGE" => "Y",
      "GIFTS_MESS_BTN_BUY" => "Выбрать",
      "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
      "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
      "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
      "SET_STATUS_404" => "N",
      "SHOW_404" => "N",
      "MESSAGE_404" => "",
      "USE_ELEMENT_COUNTER" => "Y",
      "SHOW_DEACTIVATED" => "N",
      "DISABLE_INIT_JS_IN_COMPONENT" => "N",
      "SET_VIEWED_IN_COMPONENT" => "N",
      "BONUS_RATIO" => BONUS_RATIO,
      'OFFER_ID' => $offerId
    ),
    false
  ); ?>
<? } elseif (intval($_REQUEST['ELEMENT_ID']) > 0 && $isAjax) { ?>
  <?
  $name = isset($_REQUEST['NAME']) ? $_REQUEST['NAME'] : '';
  $text = isset($_REQUEST['text']) ? $_REQUEST['text'] : '';
  $phone = isset($_REQUEST['PHONE']) ? $_REQUEST['PHONE'] : '';

  $result = array(
    'STATUS' => 'OK'
  );

  if(!\Bitrix\Main\Loader::includeModule('iblock')) {
    $errors[] = 'Не подключен модуль ИБ';
  }

  if (empty($name)) {
    $errors[] = 'Не заполнено Имя!';
  }

  if (empty($phone)) {
    $errors[] = 'Не заполнено Телефон!';
  }

  if (intval($elementId) <= 9) {
    $errors[] = 'Не передан идентификатор товара';
  }

  if (!check_bitrix_sessid()) {
    $errors[] = 'Ваша сессия устарела';
  }

  if (empty($errors)) {
    $product = \CIBlockElement::GetList(array(), array(
      'ID' => $elementId
    ), false, false, array(
      'ID',
      'IBLOCK_ID',
      'IBLOCK_TYPE_ID',
      'NAME'
    ))->Fetch();

    $fields = array(
      'ID' => $product['ID'],
      'IBLOCK_ID' => $product['IBLOCK_ID'],
      'IBLOCK_TYPE' => $product['IBLOCK_TYPE_ID'],
      'PRODUCT_NAME' => $product['NAME'],
      'NAME' => $name,
      'TEXT' => $text,
      'PHONE' => $phone,
    );

    CEvent::Send('ONE_CLICK_SALE', array('s1'), $fields);
	$result['STATUS'] = 'SEND';
	
  } else {
    $result['STATUS'] = 'ERROR';
    $result['ERRORS'] = $errors;
	$result['EMEIL_VALIDATE'] = $email_validate;
  }

  $APPLICATION->RestartBuffer();
  echo \CUtil::PhpToJSObject($result);
  die();
  ?>
<? } ?>
