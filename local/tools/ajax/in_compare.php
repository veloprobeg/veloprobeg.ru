<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$productId = isset($_REQUEST['productId']) ? intval($_REQUEST['productId']) : 0;
$iBlockId = isset($_REQUEST['iBlockId']) ? intval($_REQUEST['iBlockId']) : 0;
$compareName = isset($_REQUEST['compareName']) ? $_REQUEST['compareName'] : '';

$compareList = $_SESSION[$compareName][$iBlockId]["ITEMS"];

if (!is_array($compareList)) {
  $compareList = array();
}

$result = array(
  'IN_COMPARE' => 'N'
);

foreach ($compareList as $productInCompare) {
  if (isset($_SESSION[$compareName][$iBlockId]["ITEMS"][$productId])) {
    $result['IN_COMPARE'] = 'Y';
  }
}

echo json_encode($result);