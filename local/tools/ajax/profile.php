<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define("NO_AGENT_STATISTIC", true);

use Bitrix\Main\Loader;

if (isset($_REQUEST['site_id']) && is_string($_REQUEST['site_id'])) {
    $siteID = trim($_REQUEST['site_id']);
    if ($siteID !== '' && preg_match('/^[a-z0-9_]{2}$/i', $siteID) === 1) {
        define('SITE_ID', $siteID);
    }
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!check_bitrix_sessid() || $_SERVER["REQUEST_METHOD"] != "POST") {
    $arResult['failure'] = 'Ваша сессия истекла.';
}

if (!Loader::includeModule('sale') || !Loader::includeModule('catalog')) {
    return;
}
?>

<?
if (empty($arResult['failure'])) {
    global $USER;

    $arProps = Array();
    $arResult = Array();

    $dbProps = CSaleOrderProps::GetList(
        array("SORT" => "ASC"),
        array(
            "PROPS_GROUP_ID" => 2,
            "USER_PROPS" => "Y"
        ),
        false,
        false,
        array()
    );

    while ($prop = $dbProps->GetNext()) {
        if ($prop['CODE'] != 'CITY') {
            $prop['VALUE'] = '';
            $arProps[$prop['CODE']] = $prop;
        }
    }

    $profileID = (int)$_POST['profileID'];
    unset($_POST['profileID']);

    $dbUserProfiles = CSaleOrderUserProps::GetList(
        array("DATE_UPDATE" => "DESC"),
        array(
            "USER_ID" => $USER->GetID(),
            "ID" => $profileID
        )
    );

    if ($arUserProfiles = $dbUserProfiles->GetNext()) {
        foreach ($arProps as $prop) {
            $profileProp = Array(
                "USER_PROPS_ID" => $profileID,
                "ORDER_PROPS_ID" => $prop['ID'],
                "NAME" => $prop['NAME'],
                "VALUE" => htmlspecialchars($_POST[$prop['CODE']])
            );

            CSaleOrderUserPropsValue::Add($profileProp);
        }

        $arResult['success'] = true;
    }
    else {
        $arResult['failure'] = 'Профиль не найден';
    }
}

$APPLICATION->RestartBuffer();
header('Content-Type: application/json; charset=' . LANG_CHARSET);
echo CUtil::PhpToJSObject($arResult);
die();
?>
