<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");

global $USER;

use \Bitrix\Main\Localization\Loc;

?>
    <div class="data">
        <div class="data__nav"><a href="#"
                                  class="data__nav-button button button--yel-transp button--yel-thin is-active"><?= Loc::getMessage("PERSONAL_DATA"); ?></a><a
                    href="#"
                    class="data__nav-button button button--yel-transp button--yel-thin"><?= Loc::getMessage("ORDERS_HISTORY"); ?></a>
        </div>
        <div class="data__content">
            <div class="data__tab is-active">
                <div class="data__reg">
                    <div class="reg reg--data">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.profile",
                            "profile",
                            array(
                                "SET_TITLE" => "N",
                                "COMPONENT_TEMPLATE" => "profile",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "undefined",
                                "USER_PROPERTY" => array(
                                    0 => "UF_BONUS_CARD",
                                ),
                                "SEND_INFO" => "N",
                                "CHECK_RIGHTS" => "N",
                                "USER_PROPERTY_NAME" => ""
                            ),
                            false
                        ); ?>
                        <? if ($USER->IsAuthorized()): ?>

                            <?php
                            $APPLICATION->IncludeComponent(
                                "bitrix:sale.personal.profile.list",
                                "personal",
                                array(
                                    "PATH_TO_DETAIL" => $arResult['PATH_TO_PROFILE_DETAIL'],
                                    "PATH_TO_DELETE" => $arResult['PATH_TO_PROFILE_DELETE'],
                                    "PER_PAGE" => $arParams["PROFILES_PER_PAGE"],
                                    "SET_TITLE" => $arParams["SET_TITLE"],
                                ),
                                $component
                            );
                            ?>

                            <? /*$APPLICATION->IncludeComponent(
                                "bitrix:main.profile",
                                "profile_address",
                                array(
                                    "SET_TITLE" => "N",
                                    "COMPONENT_TEMPLATE" => "profile_address",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "undefined",
                                    "USER_PROPERTY" => array(
                                        0 => "UF_CITY",
                                        1 => "UF_STREET",
                                        2 => "UF_HOUSE",
                                        3 => "UF_HOUSING",
                                        4 => "UF_INDEX",
                                        5 => "UF_AP_NUMBER",
                                        6 => "UF_DELIV_TIME_START",
                                        7 => "UF_DELIV_TIME_FINISH",
                                    ),
                                    "SEND_INFO" => "N",
                                    "CHECK_RIGHTS" => "N",
                                    "USER_PROPERTY_NAME" => ""
                                ),
                                false
                            );*/ ?>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.profile",
                                "profile_password",
                                array(
                                    "SET_TITLE" => "N",
                                    "COMPONENT_TEMPLATE" => "profile_password",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "undefined",
                                    "USER_PROPERTY" => array(
                                        0 => "UF_PROC_PERS_DATA",
                                        1 => "UF_SUBSCRIBE",
                                    ),
                                    "SEND_INFO" => "N",
                                    "CHECK_RIGHTS" => "N",
                                    "USER_PROPERTY_NAME" => ""
                                ),
                                false
                            ); ?>
                        <? endif ?>
                    </div>
                    <? /*
          <div class="data__bonus">
            <div class="data__bonus-head">бонусный счет</div>
            <div class="data__bonus-content">
              <div class="data__bonus-count"><span>1 200</span> бонусов</div>
              <div class="data__bonus-text">1 бонус = 1 Р <br> 1 бонус начисляется за каждые <br> потраченные 30 Р</div>
            </div>
          </div>
			*/ ?>
                </div>
            </div>
            <div class="data__tab">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:sale.personal.order.list",
                    "orders",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "HISTORIC_STATUSES" => array(
                            0 => "F",
                        ),
                        "ID" => '',
                        "NAV_TEMPLATE" => "",
                        "ORDERS_PER_PAGE" => "20",
                        "PATH_TO_BASKET" => "/personal/cart/",
                        "PATH_TO_CANCEL" => "/personal/order/cancel/#ID#/",
                        "PATH_TO_COPY" => "/personal/order/?COPY_ORDER=Y&ID=#ID#",
                        "PATH_TO_DETAIL" => "/personal/order/detail/#ID#/",
                        "PATH_TO_PAYMENT" => "/personal/order/payment/",
                        "SAVE_IN_SESSION" => "Y",
                        "SET_TITLE" => "N",
                        "STATUS_COLOR_F" => "gray",
                        "STATUS_COLOR_N" => "green",
                        "STATUS_COLOR_P" => "yellow",
                        "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
                        "COMPONENT_TEMPLATE" => "orders",
                        "TRACK_NUMBER_URL" => "https://track24.ru/?code=#TRACK_NUMBER#"
                    ),
                    false
                ); ?>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>