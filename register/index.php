<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	"register",
	Array(
		"AUTH" => "Y",
		"REQUIRED_FIELDS" => array("EMAIL"),
		"SET_TITLE" => "Y",
		"SHOW_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_PHONE"),
		"SUCCESS_PAGE" => "/personal/profile/",
		"USER_PROPERTY" => array("UF_PROC_PERS_DATA","UF_SUBSCRIBE","UF_CITY","UF_STREET","UF_HOUSE","UF_HOUSING","UF_INDEX","UF_AP_NUMBER","UF_DELIV_TIME_START","UF_DELIV_TIME_FINISH"),
		"USER_PROPERTY_NAME" => "",
		"USE_BACKURL" => "Y"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>