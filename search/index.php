<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<?

$iBlockTypes = getStructureIBlocks();



$params = Array(
  "RESTART" => "N",
  "CHECK_DATES" => "N",
  "USE_TITLE_RANK" => "N",
  "DEFAULT_SORT" => "rank",
  "SHOW_WHERE" => "N",
  "SHOW_WHEN" => "N",
  "PAGE_RESULT_COUNT" => "25",
  "AJAX_MODE" => "N",
  "AJAX_OPTION_SHADOW" => "Y",
  "AJAX_OPTION_JUMP" => "N",
  "AJAX_OPTION_STYLE" => "Y",
  "AJAX_OPTION_HISTORY" => "N",
  "CACHE_TYPE" => "A",
  "CACHE_TIME" => "36000000",
  "DISPLAY_TOP_PAGER" => "N",
  "DISPLAY_BOTTOM_PAGER" => "Y",
  "PAGER_TITLE" => "Результаты поиска",
  "PAGER_SHOW_ALWAYS" => "N",
  "PAGER_TEMPLATE" => "arrows",
  "USE_SUGGEST" => "N",
  "SHOW_ITEM_TAGS" => "N",
  "SHOW_ITEM_DATE_CHANGE" => "N",
  "SHOW_ORDER_BY" => "N",
  "SHOW_TAGS_CLOUD" => "N",
  "AJAX_OPTION_ADDITIONAL" => "",
  "USE_LANGUAGE_GUESS" => 'N'
);

$filter = array();

$isSetGoods = false;
$isSetText = false;

if (isset($_REQUEST['show_goods']) && $_REQUEST['show_goods'] == 'on') {
  $filter = array(
    'iblock_catalog'
  );
  $params["arrFILTER_iblock_catalog"] = array(
    0 => 'all'
  );

  $isSetGoods = true;
}

if (isset($_REQUEST['show_except_goods']) && $_REQUEST['show_except_goods'] == 'on') {
  $filter[] = 'main';

  foreach ($iBlockTypes as $iBlockType => $iBlocks) {
    if ($iBlockType != 'catalog') {
      $filter[] = 'iblock_'.$iBlockType;
      $params["arrFILTER_iblock_".$iBlockType] = array(
        0 => 'all'
      );
    }
  }
}

if (empty($filter)) {
  $filter[] = 'no';
}

$params["arrFILTER"] = $filter;
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"search", 
	array(
		"COMPONENT_TEMPLATE" => "search",
		"RESTART" => "N",
		"NO_WORD_LOGIC" => "N",
		"CHECK_DATES" => "N",
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "rank",
		"FILTER_NAME" => "",
		"arrFILTER" => array(
		),
		"SHOW_WHERE" => "Y",
		"arrWHERE" => array(
		),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => "50",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_SUGGEST" => "N",
		"SHOW_RATING" => "",
		"RATING_TYPE" => "",
		"PATH_TO_USER_PROFILE" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ""
	),
	false
); ?>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>